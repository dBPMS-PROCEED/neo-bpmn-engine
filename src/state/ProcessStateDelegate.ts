import { Definitions, FlowNode } from 'bpmn-moddle';
import { applyMiddleware, createStore, DeepPartial, Store } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { Observable } from 'rxjs';
import { ProcessAction } from './actions';
import allBpmnEpics from './epics/allBpmnEpics';
import dispatcherEpic from './epics/dispatcher';
import sendMessageEpic from './epics/event/sendMessageEpic';
import sendSignalEpic from './epics/event/sendSignalEpic';
import rootEpic from './epics/rootEpic';
import { DEFAULT_PROCESS_STATE, ProcessState, rootReducer } from './reducers';
import LoggerFactory from '../LoggerFactory';

/**
 * Wrap the state handling logic and provide public methods for
 * action dispatch and getting the current state
 */
export class ProcessStateDelegate {
  private store: Store<ProcessState, ProcessAction>;
  private log = LoggerFactory.getLogger('state-logging');

  private constructor(
    initState: ProcessState = DEFAULT_PROCESS_STATE,
    moddleDefinitions: Definitions,
    shouldPassTokenHook: (from: string, to: string, tokenId: string) => Promise<boolean> = () => Promise.resolve(true),
    shouldActivateFlowNodeHook: (tokenId: string, flowNode: FlowNode) => Promise<boolean> = () => Promise.resolve(true),
    sendSignalHook: (signalId: string) => any = () => null,
    sendMessageHook: (messageId: string) => any = () => null
  ) {
    initState = { ...initState, moddleDefinitions };
    const epicMiddleware = createEpicMiddleware();
    this.store = createStore(rootReducer, initState as DeepPartial<ProcessState>, applyMiddleware(epicMiddleware));

    epicMiddleware.run(
      rootEpic(
        dispatcherEpic(this.log, shouldPassTokenHook, shouldActivateFlowNodeHook),
        allBpmnEpics(this.log),
        sendSignalEpic(sendSignalHook),
        sendMessageEpic(sendMessageHook)
      ) as any
    );
  }

  static fromDefaultState({
    moddleDefinitions,
    shouldPassTokenHook,
    shouldActivateFlowNodeHook
  }: {
    moddleDefinitions: Definitions;
    shouldPassTokenHook?: (from: string, to: string, tokenId: string) => Promise<boolean>;
    shouldActivateFlowNodeHook?: (tokenId: string, flowNode: FlowNode) => Promise<boolean>;
  }) {
    return ProcessStateDelegate.fromState({
      state: DEFAULT_PROCESS_STATE,
      moddleDefinitions,
      shouldPassTokenHook,
      shouldActivateFlowNodeHook
    });
  }

  static fromState({
    state,
    moddleDefinitions,
    shouldPassTokenHook,
    shouldActivateFlowNodeHook,
    sendSignalHook,
    sendMessageHook
  }: {
    state: ProcessState;
    moddleDefinitions: Definitions;
    shouldPassTokenHook?: (from: string, to: string, tokenId: string) => Promise<boolean>;
    shouldActivateFlowNodeHook?: (tokenId: string, flowNode: FlowNode) => Promise<boolean>;
    sendSignalHook?: (signalId: string) => any;
    sendMessageHook?: (messageId: string) => any;
  }) {
    return new ProcessStateDelegate(
      state,
      moddleDefinitions,
      shouldPassTokenHook,
      shouldActivateFlowNodeHook,
      sendSignalHook,
      sendMessageHook
    );
  }

  /**
   * Get the current state object
   * @returns object
   */
  getState() {
    return this.store.getState();
  }

  /**
   * Get state stream
   * @returns stream of state objects
   */
  getState$() {
    return new Observable<ProcessState>(observer => {
      return this.store.subscribe(() => observer.next(this.store.getState()));
    });
  }

  /**
   * Dispatch an action
   * @param {ProcessAction} action - any of the possible actions in the system
   */
  dispatch(action: ProcessAction) {
    this.store.dispatch(action);
  }
}
