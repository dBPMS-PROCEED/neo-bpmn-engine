import { ProcessAction, ProcessActionType, UpdateAdaptationLogAction, LogAdaptationAction } from '../actions';

export const enum AdaptationType {
  MIGRATION = 'MIGRATION',
  TOKEN_ADD = 'TOKEN-ADD',
  TOKEN_REMOVE = 'TOKEN-REMOVE',
  TOKEN_MOVE = 'TOKEN-MOVE',
  VARIABLE_ADAPTATION = 'VARIABLE-ADAPTATION'
}

export interface AdaptationEntry {
  type: AdaptationType | string;
  time: number;
  [key: string]: any;
}

export interface MigrationEntry extends AdaptationEntry {
  type: AdaptationType.MIGRATION;
  from: string;
  to: string;
}

export interface TokenAdaptationEntry extends AdaptationEntry {
  type: AdaptationType.TOKEN_ADD | AdaptationType.TOKEN_MOVE | AdaptationType.TOKEN_REMOVE;
  tokenId: string;
  currentFlowElementId?: string;
  targetFlowElementId?: string;
}

export interface VariableAdaptationEntry extends AdaptationEntry {
  type: AdaptationType.VARIABLE_ADAPTATION;
  changes: {
    variableName: string;
    oldValue: any;
    newValue: any;
  }[];
}

export type AdaptationLogState = AdaptationEntry[];

export const DEFAULT_ADAPTATION_LOG_STATE: AdaptationLogState = [];

export function adaptationLogReducer(state = DEFAULT_ADAPTATION_LOG_STATE, action: ProcessAction) {
  if (action.type === ProcessActionType.LogAdaptation) {
    const a = action as LogAdaptationAction;

    // create new log entry
    return [
      ...state,
      {
        ...a.payload
      }
    ] as AdaptationLogState;
  }

  if (action.type === ProcessActionType.UpdateAdaptationLog) {
    const a = action as UpdateAdaptationLogAction;

    const idx = state.findIndex(adaptation => adaptation.time === a.payload.time && adaptation.type === a.payload.type);

    // log entry exists -> override
    if (idx >= 0) {
      const newState = [...state];
      newState.splice(idx, 1, {
        ...state[idx],
        ...a.payload
      });
      return newState;
    }
  }

  return state;
}
