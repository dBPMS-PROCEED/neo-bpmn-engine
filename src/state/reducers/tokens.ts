import {
  ProcessAction,
  ProcessActionType,
  WillPassTokenAction,
  RemoveTokenAction,
  InitFlowNodeAction,
  UpdateTokenAction,
  CreateTokenAction,
  InitBoundaryEventAction,
  EndFlowNodeAction,
  ActivateFlowNodeAction,
  SetFlowNodeStateAction,
  SetIntermediateVariableValueAction
} from '../actions';
import { ExecutionState } from './log';

import { getCustomTokenEndStates } from '../../Configuration';

export const enum TokenState {
  RUNNING = 'RUNNING',
  READY = 'READY',
  ERROR_SEMANTIC = 'ERROR-SEMANTIC',
  ERROR_TECHNICAL = 'ERROR-TECHNICAL',
  ERROR_UNKNOWN = 'ERROR-UNKNOWN ',
  ENDED = 'ENDED',
  ABORTED = 'ABORTED',
  FAILED = 'FAILED',
  TERMINATED = 'TERMINATED',
  SKIPPED = 'SKIPPED'
}

export const enum FlowNodeState {
  READY = 'READY',
  ACTIVE = 'ACTIVE',
  COMPLETED = 'COMPLETED',
  FAILED = 'FAILED',
  TERMINATED = 'TERMINATED'
}

export type Token = {
  tokenId: string;
  state: TokenState | string;
  localStartTime: number;
  localExecutionTime: number;
  currentFlowElement: {
    id: string;
    startTime?: number;
    state?: FlowNodeState | string;
  };
  previousFlowElementId?: string;
  intermediateVariablesState: null | {
    [key: string]: any;
  };
  [key: string]: any;
};

export type ExportedToken = {
  tokenId: string;
  state: TokenState | string;
  localStartTime: number;
  localExecutionTime: number;
  currentFlowElementId: string;
  currentFlowNodeState?: FlowNodeState | string;
  currentFlowElementStartTime?: number;
  previousFlowElementId?: string;
  intermediateVariablesState: {
    [key: string]: any;
  };
  [key: string]: any;
};

export function exportToken(token: Token): ExportedToken {
  const {
    tokenId,
    state,
    startTime,
    currentFlowElement,
    localExecutionTime,
    previousFlowElementId,
    intermediateVariablesState,
    ...optional
  } = token;
  const tokenInformation = {
    ...optional,
    tokenId,
    state,
    localStartTime: token.localStartTime,
    currentFlowElementId: token.currentFlowElement.id,
    currentFlowNodeState: token.currentFlowElement.state,
    currentFlowElementStartTime: token.currentFlowElement.startTime,
    previousFlowElementId,
    intermediateVariablesState: JSON.parse(JSON.stringify(intermediateVariablesState)),
    localExecutionTime: token.localExecutionTime
  };

  if (!currentFlowElement?.state) {
    delete tokenInformation.currentFlowNodeState;
  }

  return tokenInformation;
}

export type TokensState = Token[];

export const DEFAULT_TOKENS_STATE: TokensState = [];

export function tokenReducer(state = DEFAULT_TOKENS_STATE, action: ProcessAction) {
  if (action.type === ProcessActionType.TokenWillPass) {
    const a = action as WillPassTokenAction;
    const idx = state.findIndex(t => t.tokenId === a.payload.tokenId);

    if (idx >= 0) {
      // token already exists
      const newState = [...state];
      if (state[idx].state === TokenState.RUNNING || state[idx].state === TokenState.READY) {
        newState.splice(idx, 1, {
          ...state[idx],
          state: TokenState.READY,
          currentFlowElement: { id: a.payload.sequenceId, startTime: +new Date() },
          previousFlowElementId: state[idx].currentFlowElement.id,
          intermediateVariablesState: null
        });
      }
      return newState;
    }
  }

  if (action.type === ProcessActionType.BoundaryEventInit) {
    const a = action as InitBoundaryEventAction;
    const idx = state.findIndex(
      t => t.tokenId === a.payload.tokenId && t.currentFlowElement.id === a.payload.flowNodeId
    );
    const newState = [...state];
    if (idx >= 0) {
      newState.splice(idx, 1, {
        ...state[idx],
        currentFlowElement: {
          ...newState[idx].currentFlowElement,
          startTime: +new Date(),
          state: FlowNodeState.READY
        }
      });
    }

    return newState;
  }

  if (action.type === ProcessActionType.FlowNodeInit) {
    const a = action as InitFlowNodeAction;
    const idx = state.findIndex(
      t => t.tokenId === a.payload.tokenId && t.currentFlowElement.id === a.payload.flowNodeId
    );
    const newState = [...state];

    if (idx >= 0) {
      newState.splice(idx, 1, {
        ...state[idx],
        currentFlowElement: {
          ...newState[idx].currentFlowElement,
          startTime: newState[idx].currentFlowElement.startTime || +new Date(),
          state: FlowNodeState.READY
        },
        intermediateVariablesState: state[idx].intermediateVariablesState || {}
      });
    }

    return newState;
  }

  if (action.type === ProcessActionType.FlowNodeActivate) {
    const a = action as ActivateFlowNodeAction;
    const idx = state.findIndex(
      t => t.tokenId === a.payload.tokenId && t.currentFlowElement.id === a.payload.flowNodeId
    );
    const newState = [...state];
    if (idx >= 0) {
      newState.splice(idx, 1, {
        ...state[idx],
        currentFlowElement: {
          ...newState[idx].currentFlowElement,
          state: FlowNodeState.ACTIVE
        }
      });
    }

    return newState;
  }

  if (action.type === ProcessActionType.FlowNodeEnd) {
    const a = action as EndFlowNodeAction;
    const idx = state.findIndex(
      t => t.tokenId === a.payload.tokenId && t.currentFlowElement.id === a.payload.flowNodeId
    );
    const newState = [...state];
    if (idx >= 0) {
      const startTime = newState[idx].currentFlowElement.startTime;
      const currentExecutionTime = newState[idx].localExecutionTime;

      if (a.payload.state === ExecutionState.COMPLETED) {
        newState.splice(idx, 1, {
          ...state[idx],
          currentFlowElement: {
            ...newState[idx].currentFlowElement,
            state: FlowNodeState.COMPLETED
          },
          localExecutionTime:
            startTime && a.payload.endTime
              ? currentExecutionTime + (a.payload.endTime - startTime)
              : currentExecutionTime
        });
      } else {
        let flowNodeState;

        switch (a.payload.state) {
          case ExecutionState.ABORTED:
          case ExecutionState.SKIPPED:
          case ExecutionState.TERMINATED:
            flowNodeState = FlowNodeState.TERMINATED;
            break;
          case ExecutionState.FAILED:
          case ExecutionState.ERROR_SEMANTIC:
          case ExecutionState.ERROR_TECHNICAL:
          case ExecutionState.ERROR_UNKNOWN:
            flowNodeState = FlowNodeState.FAILED;
            break;
        }

        newState.splice(idx, 1, {
          ...state[idx],
          currentFlowElement: {
            ...newState[idx].currentFlowElement,
            state: flowNodeState
          }
        });
      }
    }

    return newState;
  }

  if (action.type === ProcessActionType.SetFlowNodeState) {
    const a = action as SetFlowNodeStateAction;
    const newState = [...state];
    const idx = newState.findIndex(t => t.tokenId === a.payload.tokenId && t.currentFlowElement);

    if (idx >= 0 && newState[idx].state === TokenState.RUNNING) {
      newState.splice(idx, 1, {
        ...newState[idx],
        currentFlowElement: { ...newState[idx].currentFlowElement, state: a.payload.state }
      });
    }
    return newState;
  }

  if (action.type === ProcessActionType.RemoveToken) {
    return state.filter(t => t.tokenId !== (action as RemoveTokenAction).payload.tokenId);
  }

  if (action.type === ProcessActionType.UpdateToken) {
    const a = action as UpdateTokenAction;
    const idx = state.findIndex(t => t.tokenId === a.payload.tokenId);

    if (idx >= 0) {
      const newState = [...state];
      newState.splice(idx, 1, { ...state[idx], ...a.payload, state: a.payload.state || state[idx].state });
      return newState;
    }
  }

  if (action.type === ProcessActionType.CreateToken) {
    const a = action as CreateTokenAction;
    const idx = state.findIndex(t => t.tokenId === a.payload.tokenId);

    if (idx < 0) {
      return [
        ...state,
        {
          ...a.payload,
          intermediateVariablesState: {}
        }
      ];
    }
  }

  if (action.type === ProcessActionType.SetIntermediateVariableValue) {
    const a = action as SetIntermediateVariableValueAction;
    const { tokenId, key, value } = a.payload;
    const idx = state.findIndex(t => t.tokenId === tokenId);

    if (idx >= 0) {
      const newState = [...state];
      newState.splice(idx, 1, {
        ...state[idx],
        intermediateVariablesState: { ...state[idx].intermediateVariablesState, [key]: value }
      });
      return newState;
    }
  }

  return state;
}

export function isTokenEndedState(state: TokenState | string): boolean {
  return (
    state === TokenState.ENDED ||
    state === TokenState.ERROR_SEMANTIC ||
    state === TokenState.ERROR_TECHNICAL ||
    state === TokenState.ERROR_UNKNOWN ||
    state === TokenState.ABORTED ||
    state === TokenState.TERMINATED ||
    state === TokenState.FAILED ||
    state === TokenState.SKIPPED ||
    // allow states defined by a program using this library to be recognized as end states
    getCustomTokenEndStates().includes(state)
  );
}
