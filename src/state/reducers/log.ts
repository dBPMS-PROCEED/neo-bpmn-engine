import { ProcessAction, ProcessActionType, LogExecutionAction, UpdateLogAction } from '../actions';

export const enum ExecutionState {
  COMPLETED = 'COMPLETED',
  ERROR_SEMANTIC = 'ERROR-SEMANTIC',
  ERROR_TECHNICAL = 'ERROR-TECHNICAL',
  ERROR_UNKNOWN = 'ERROR-UNKNOWN',
  ABORTED = 'ABORTED',
  TERMINATED = 'TERMINATED',
  FAILED = 'FAILED',
  SKIPPED = 'SKIPPED'
}

export type LogState = {
  flowElementId: string;
  tokenId: string;
  executionState: ExecutionState | string;
  startTime?: number;
  endTime?: number;
  errorMessage?: string;
  [key: string]: any;
}[];

export const DEFAULT_LOG_STATE: LogState = [];

export function logReducer(state = DEFAULT_LOG_STATE, action: ProcessAction) {
  if (action.type === ProcessActionType.LogExecution) {
    const a = action as LogExecutionAction;

    // create new log entry
    return [
      ...state,
      {
        ...a.payload
      }
    ] as LogState;
  }

  if (action.type === ProcessActionType.UpdateLog) {
    const a = action as UpdateLogAction;

    const idx = state.findIndex(
      execution => execution.flowElementId === a.payload.flowElementId && execution.tokenId === a.payload.tokenId
    );

    // log entry exists -> override
    if (idx >= 0) {
      const newState = [...state];
      newState.splice(idx, 1, {
        ...state[idx],
        ...a.payload
      });
      return newState;
    }
  }

  return state;
}

/**
 * Will use the given log to decide if a specific execution of a flowElement has already ended
 *
 * @param tokenId the token that triggered the execution
 * @param flowElementId the element that was executed
 * @param startTime the time at which this specific execution started
 * @param log the log state to evaluate
 * @returns information if the specific execution has already ended
 */
export function isExecutionEnded(tokenId: string, flowElementId: string, startTime: Number | undefined, log: LogState) {
  if (!startTime) {
    return false;
  }

  return log.some(
    entry =>
      entry.tokenId === tokenId &&
      entry.flowElementId === flowElementId &&
      entry.startTime === startTime &&
      entry.endTime
  );
}
