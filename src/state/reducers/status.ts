import { UpdateProcessStatusAction, ProcessAction, ProcessActionType } from '../actions';

export const enum ProcessStatus {
  RUNNING = 'RUNNING',
  STOPPED = 'STOPPED',
  PAUSED = 'PAUSED',
  ENDED = 'ENDED',
  REMOVED = 'REMOVED'
}

export type StatusState = ProcessStatus | string;

export const DEFAULT_STATUS_STATE: StatusState = ProcessStatus.RUNNING;

export function statusReducer(state = DEFAULT_STATUS_STATE, action: ProcessAction): StatusState {
  if (action.type === ProcessActionType.EndProcess) {
    return ProcessStatus.ENDED;
  }

  if (action.type === ProcessActionType.StopProcess && state !== ProcessStatus.ENDED) {
    return ProcessStatus.STOPPED;
  }

  if (action.type === ProcessActionType.ReactivateProcess && isInstanceEndedStatus(state)) {
    return ProcessStatus.RUNNING;
  }

  if (action.type === ProcessActionType.PauseProcess && state !== ProcessStatus.ENDED) {
    return ProcessStatus.PAUSED;
  }

  if (action.type === ProcessActionType.ResumeProcess && state !== ProcessStatus.ENDED) {
    return ProcessStatus.RUNNING;
  }

  if (action.type === ProcessActionType.TearDownProcess) {
    return ProcessStatus.REMOVED;
  }

  if (action.type === ProcessActionType.UpdateProcessStatus) {
    const a = action as UpdateProcessStatusAction;
    return a.payload.status;
  }

  return state;
}

export function isInstanceEndedStatus(status: ProcessStatus | string) {
  return status === ProcessStatus.ENDED || status === ProcessStatus.STOPPED;
}
