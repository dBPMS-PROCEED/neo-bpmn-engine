import { ProcessAction, ProcessActionType, SetVariablesAction, UpdateVariablesAction } from '../actions';

export type VariablesState = {
  [key: string]: {
    value: any;
    log: { changedTime: number; changedBy?: string; oldValue?: any }[];
  };
};

export const DEFAULT_VARIABLES_STATE: VariablesState = {};

export function variablesReducer(state = DEFAULT_VARIABLES_STATE, action: ProcessAction): VariablesState {
  if (action.type === ProcessActionType.SetVariables) {
    const newVariables = { ...state };
    const a = action as SetVariablesAction;
    for (const key in a.payload.variables as object) {
      if (!(key in newVariables)) {
        newVariables[key] = { value: undefined, log: [] };
        newVariables[key].log.push({ changedTime: +new Date(), changedBy: a.payload.changedBy });
      } else {
        newVariables[key].log.push({
          changedTime: +new Date(),
          changedBy: a.payload.changedBy,
          oldValue: newVariables[key].value
        });
      }
      newVariables[key].value = a.payload.variables[key];
    }
    return newVariables;
  }

  if (action.type === ProcessActionType.UpdateVariables) {
    const newVariables = { ...state };
    const a = action as UpdateVariablesAction;
    for (const key in a.payload.variables as object) {
      const updateVariable = a.payload.variables[key];
      if (!(key in newVariables)) {
        newVariables[key] = { value: updateVariable.value, log: updateVariable.log };
      } else {
        const newLogEntries = updateVariable.log.reduce((acc, entry) => {
          const logEntryExists = !!newVariables[key].log.find(e => e.changedTime === entry.changedTime);

          if (!logEntryExists) {
            acc.push(entry);
          }

          return acc;
        }, [] as { changedTime: number; changedBy?: string; oldValue?: any }[]);

        const newestLogEntryUpdatedVariable = Math.max(...updateVariable.log.map(e => e.changedTime), 0);
        const newestLogEntryExistingVariable = Math.max(...newVariables[key].log.map(e => e.changedTime), 0);

        newVariables[key].value =
          newestLogEntryUpdatedVariable >= newestLogEntryExistingVariable
            ? updateVariable.value
            : newVariables[key].value;

        newVariables[key].log = [...newVariables[key].log, ...newLogEntries];
      }
    }
    return newVariables;
  }

  return state;
}
