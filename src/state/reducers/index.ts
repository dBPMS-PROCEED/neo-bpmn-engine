import { combineReducers, Reducer } from 'redux';
import { ProcessAction, ProcessActionType, UpdateProcessIdAction } from '../actions';
import { DEFAULT_TOKENS_STATE, tokenReducer, TokensState, ExportedToken, exportToken } from './tokens';
import { DEFAULT_STATUS_STATE, statusReducer, StatusState, ProcessStatus } from './status';
import { DEFAULT_VARIABLES_STATE, variablesReducer, VariablesState } from './variables';
import { DEFAULT_LOG_STATE, LogState, logReducer } from './log';
import { Definitions } from 'bpmn-moddle';
import { DEFAULT_MODDLE_DEFINITIONS_STATE, moddleDefinitionsReducer } from './moddleDefinitions';
import { AdaptationLogState, DEFAULT_ADAPTATION_LOG_STATE, adaptationLogReducer } from './adaptationLog';

export interface ProcessState {
  processId: string;
  processInstanceId: string;
  creationTime: number;
  status: StatusState;
  tokens: TokensState;
  log: LogState;
  adaptationLog: AdaptationLogState;
  variables: VariablesState;
  moddleDefinitions: Definitions;
}

export const DEFAULT_PROCESS_STATE: ProcessState = {
  processId: '',
  processInstanceId: '',
  creationTime: 0,
  status: DEFAULT_STATUS_STATE,
  tokens: DEFAULT_TOKENS_STATE,
  log: DEFAULT_LOG_STATE,
  adaptationLog: DEFAULT_ADAPTATION_LOG_STATE,
  variables: DEFAULT_VARIABLES_STATE,
  moddleDefinitions: DEFAULT_MODDLE_DEFINITIONS_STATE
};

export interface ExportedProcessState {
  processId: string;
  processInstanceId: string;
  globalStartTime: number;
  instanceState: string[];
  tokens: ExportedToken[];
  variables: VariablesState;
  log: LogState;
  adaptationLog: AdaptationLogState;
}

export function exportProcessState(state: ProcessState): ExportedProcessState {
  const tokens = state.tokens.map(token => exportToken(token));

  const instanceState =
    state.status === ProcessStatus.RUNNING || state.status === ProcessStatus.ENDED
      ? Array.from(new Set(tokens.map(token => token.state)))
      : [state.status];

  return {
    processId: state.processId,
    processInstanceId: state.processInstanceId,
    globalStartTime: state.creationTime,
    instanceState,
    tokens,
    variables: { ...state.variables },
    log: [...state.log],
    adaptationLog: [...state.adaptationLog]
  };
}

function processIdReducer(state = DEFAULT_PROCESS_STATE.processId, action: ProcessAction) {
  if (action.type === ProcessActionType.UpdateProcessId) {
    const a = action as UpdateProcessIdAction;
    return a.payload.processId;
  }

  return state;
}

export const rootReducer: Reducer<ProcessState, ProcessAction> = combineReducers({
  processId: processIdReducer,
  processInstanceId: (processInstanceId: string = DEFAULT_PROCESS_STATE.processInstanceId) => processInstanceId,
  creationTime: (creationTime: number = DEFAULT_PROCESS_STATE.creationTime) => creationTime,
  status: statusReducer,
  tokens: tokenReducer,
  log: logReducer,
  adaptationLog: adaptationLogReducer,
  variables: variablesReducer,
  moddleDefinitions: moddleDefinitionsReducer
});
