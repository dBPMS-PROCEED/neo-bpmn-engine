import { ProcessAction, ProcessActionType, UpdateModdleDefinitionsAction } from '../actions';
import BpmnModdle from 'bpmn-moddle';

export const DEFAULT_MODDLE_DEFINITIONS_STATE = new BpmnModdle().create('bpmn:Definitions');

export function moddleDefinitionsReducer(state = DEFAULT_MODDLE_DEFINITIONS_STATE, action: ProcessAction) {
  if (action.type === ProcessActionType.UpdateModdleDefinitions) {
    const a = action as UpdateModdleDefinitionsAction;
    return a.payload.moddleDefinitions;
  }

  return state;
}
