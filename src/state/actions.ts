import { AnyAction } from 'redux';
import { TokenState } from './reducers/tokens';
import { ExecutionState } from './reducers/log';
import { VariablesState } from './reducers/variables';
import { Definitions } from 'bpmn-moddle';
import { AdaptationEntry, AdaptationType } from './reducers/adaptationLog';

export enum ProcessActionType {
  PauseProcess = 'PauseProcess',
  ResumeProcess = 'ResumeProcess',
  EndProcess = 'EndProcess',
  StopProcess = 'StopProcess',
  ReactivateProcess = 'ReactivateProcess',
  TearDownProcess = 'TearDownProcess',
  UpdateProcessStatus = 'UpdateProcessStatus',

  FlowNodeInit = 'FlowNodeInit',
  FlowNodeWillActivate = 'FlowNodeWillActivate',
  FlowNodeActivate = 'FlowNodeActivate',
  FlowNodeEnd = 'FlowNodeEnd',
  FlowNodeInterrupt = 'FlowNodeInterrupt',
  TokenWillPass = 'TokenWillPass',
  TokenDidPass = 'TokenDidPass',
  UpdateToken = 'UpdateToken',
  SetIntermediateVariableValue = 'SetIntermediateVariableValue',
  PauseToken = 'PauseToken',
  EndToken = 'EndToken',
  CreateToken = 'CreateToken',
  ContinueToken = 'ContinueToken',
  MoveToken = 'MoveToken',
  RemoveToken = 'RemoveToken',
  SetFlowNodeProgress = 'SetFlowNodeProgress',
  SetFlowNodeState = 'SetFlowNodeState',

  EventReceiveMessage = 'EventReceiveMessage',
  EventSendMessage = 'EventSendMessage',
  EventReceiveSignal = 'EventReceiveSignal',
  EventSendSignal = 'EventSendSignal',
  PropagateEscalation = 'PropagateEscalation',
  PropagateError = 'PropagateError',
  TaskThrowError = 'TaskThrowError',
  TaskThrowEscalation = 'TaskThrowEscalation',
  ActivityEnd = 'ActivityEnd',
  RespondThrowingEvent = 'RespondThrowingEvent',
  BoundaryEventListen = 'BoundaryEventListen',
  BoundaryEventInit = 'BoundaryEventInit',

  UpdateProcessId = 'UpdateProcessId',
  UpdateModdleDefinitions = 'UpdateModdleDefinitions',
  SetVariables = 'SetVariables',
  UpdateVariables = 'UpdateVariables',

  LogExecution = 'LogExecution',
  UpdateLog = 'UpdateLog',

  LogAdaptation = 'LogAdaptation',
  UpdateAdaptationLog = 'UpdateAdaptationLog',

  StartMigration = 'StartMigration',
  EndMigration = 'EndMigration'
}

type EndTokenPayload = { endToken: TokenState } | { removeToken: boolean };
type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never };
type XOR<T, U> = T | U extends object ? (Without<T, U> & U) | (Without<U, T> & T) : T | U;

export interface ProcessAction extends AnyAction {
  type: ProcessActionType;
  payload: object;
}

export interface PauseProcessAction extends ProcessAction {
  type: ProcessActionType.PauseProcess;
  payload: {};
}

export function pauseProcessAction(): PauseProcessAction {
  return { type: ProcessActionType.PauseProcess, payload: {} };
}

export interface ResumeProcessAction extends ProcessAction {
  type: ProcessActionType.ResumeProcess;
  payload: {};
}

export function resumeProcessAction(): ResumeProcessAction {
  return { type: ProcessActionType.ResumeProcess, payload: {} };
}

export interface EndProcessAction extends ProcessAction {
  type: ProcessActionType.EndProcess;
  payload: {
    tokenId: string;
    messageRef?: string;
    signalRef?: string;
    aborted: boolean;
  };
}

export function endProcessAction({
  tokenId,
  messageRef,
  signalRef,
  aborted = false
}: {
  tokenId: string;
  messageRef?: string;
  signalRef?: string;
  errorRef?: string;
  aborted?: boolean;
}): EndProcessAction {
  return { type: ProcessActionType.EndProcess, payload: { tokenId, messageRef, signalRef, aborted } };
}

export interface StopProcessAction extends ProcessAction {
  type: ProcessActionType.StopProcess;
  payload: {};
}

export function stopProcessAction(): StopProcessAction {
  return { type: ProcessActionType.StopProcess, payload: {} };
}

export interface ReactivateProcessAction extends ProcessAction {
  type: ProcessActionType.ReactivateProcess;
  payload: {};
}

export function reactivateProcessAction(): ReactivateProcessAction {
  return { type: ProcessActionType.ReactivateProcess, payload: {} };
}

export interface TearDownProcessAction extends ProcessAction {
  type: ProcessActionType.TearDownProcess;
  payload: {};
}

export function tearDownProcessAction(): TearDownProcessAction {
  return { type: ProcessActionType.TearDownProcess, payload: {} };
}

export interface UpdateProcessStatusAction extends ProcessAction {
  type: ProcessActionType.UpdateProcessStatus;
  payload: {
    status: string;
  };
}

export function updateProcessStatusAction({ status }: { status: string }): UpdateProcessStatusAction {
  return { type: ProcessActionType.UpdateProcessStatus, payload: { status } };
}

export interface TokenAction extends ProcessAction {
  payload: {
    tokenId: string;
    [key: string]: any;
  };
}

export interface PauseTokenAction extends TokenAction {
  type: ProcessActionType.PauseToken;
  payload: {
    tokenId: string;
  };
}

export function pauseTokenAction({ tokenId }: { tokenId: string }): PauseTokenAction {
  return {
    type: ProcessActionType.PauseToken,
    payload: {
      tokenId
    }
  };
}

export interface WillPassTokenAction extends TokenAction {
  type: ProcessActionType.TokenWillPass;
  payload: {
    sequenceId: string;
    tokenId: string;
  };
}

export function willPassTokenAction(payload: { sequenceId: string; tokenId: string }): WillPassTokenAction {
  return { payload, type: ProcessActionType.TokenWillPass };
}

export interface ContinueTokenAction extends TokenAction {
  type: ProcessActionType.ContinueToken;
  payload: {
    tokenId: string;
  };
}

export function continueTokenAction(payload: { tokenId: string }): ContinueTokenAction {
  return { payload, type: ProcessActionType.ContinueToken };
}

export interface CreateTokenAction extends TokenAction {
  type: ProcessActionType.CreateToken;
  payload: {
    tokenId: string;
    state: TokenState | string;
    localStartTime: number;
    currentFlowElement: {
      id: string;
    };
    localExecutionTime: number;
    [key: string]: any;
  };
}

export function createTokenAction(payload: {
  tokenId: string;
  state: TokenState | string;
  localStartTime: number;
  currentFlowElement: {
    id: string;
  };
  localExecutionTime: number;
  [key: string]: any;
}): CreateTokenAction {
  return { payload, type: ProcessActionType.CreateToken };
}

export interface DidPassTokenAction extends TokenAction {
  type: ProcessActionType.TokenDidPass;
  payload: {
    tokenId: string;
    to: string;
  };
}

export function didPassTokenAction(payload: { tokenId: string; to: string }): DidPassTokenAction {
  return { payload, type: ProcessActionType.TokenDidPass };
}

export interface UpdateTokenAction extends TokenAction {
  type: ProcessActionType.UpdateToken;
  payload: {
    tokenId: string;
    state?: TokenState | string;
    [key: string]: any;
  };
}

export function updateTokenAction(payload: {
  tokenId: string;
  state?: TokenState | string;
  [key: string]: any;
}): UpdateTokenAction {
  return { payload, type: ProcessActionType.UpdateToken };
}

export interface SetIntermediateVariableValueAction extends TokenAction {
  type: ProcessActionType.SetIntermediateVariableValue;
  payload: {
    tokenId: string;
    key: string;
    value: any;
  };
}

export function setIntermediateVariableValueAction(payload: {
  tokenId: string;
  key: string;
  value: any;
}): SetIntermediateVariableValueAction {
  return {
    type: ProcessActionType.SetIntermediateVariableValue,
    payload
  };
}

export interface EndTokenAction extends TokenAction {
  type: ProcessActionType.EndToken;
  payload: {
    tokenId: string;
    state: TokenState | string;
    endTime?: number;
    errorMessage?: string;
    removeAfterEnding?: boolean;
    [key: string]: any;
  };
}

export function endTokenAction(payload: {
  tokenId: string;
  state: TokenState | string;
  endTime?: number;
  errorMessage?: string;
  removeAfterEnding?: boolean;
  [key: string]: any;
}): EndTokenAction {
  return { payload, type: ProcessActionType.EndToken };
}

export interface MoveTokenAction extends AnyAction {
  type: ProcessActionType.MoveToken;
  payload: {
    token: {
      tokenId: string;
      [key: string]: any;
    };
    elementId: string;
    targetDefinitions?: Definitions;
  };
}

export function moveTokenAction(payload: {
  token: { tokenId: string; [key: string]: any };
  elementId: string;
  targetDefinitions?: Definitions;
}): MoveTokenAction {
  return { payload, type: ProcessActionType.MoveToken };
}

export interface RemoveTokenAction extends TokenAction {
  type: ProcessActionType.RemoveToken;
  payload: {
    tokenId: string;
  };
}

export function removeTokenAction(payload: { tokenId: string }): RemoveTokenAction {
  return { payload, type: ProcessActionType.RemoveToken };
}

export interface FlowNodeAction extends TokenAction {
  payload: {
    flowNodeId: string;
    tokenId: string;
    [key: string]: any;
  };
}

export interface InitFlowNodeAction extends FlowNodeAction {
  type: ProcessActionType.FlowNodeInit;
  payload: {
    flowNodeId: string;
    tokenId: string;
  };
}

export function initFlowNodeAction({
  flowNodeId,
  tokenId
}: {
  flowNodeId: string;
  tokenId: string;
}): InitFlowNodeAction {
  return {
    type: ProcessActionType.FlowNodeInit,
    payload: {
      flowNodeId,
      tokenId
    }
  };
}

export interface WillActivateFlowNodeAction extends FlowNodeAction {
  type: ProcessActionType.FlowNodeWillActivate;
  payload: {
    flowNodeId: string;
    tokenId: string;
  };
}

export function willActivateFlowNodeAction(payload: {
  tokenId: string;
  flowNodeId: string;
}): WillActivateFlowNodeAction {
  return {
    type: ProcessActionType.FlowNodeWillActivate,
    payload
  };
}

export interface ActivateFlowNodeAction extends FlowNodeAction {
  type: ProcessActionType.FlowNodeActivate;
  payload: {
    flowNodeId: string;
    tokenId: string;
  };
}

export function activateFlowNodeAction(payload: { tokenId: string; flowNodeId: string }): ActivateFlowNodeAction {
  return {
    type: ProcessActionType.FlowNodeActivate,
    payload
  };
}

export interface EndFlowNodeAction extends FlowNodeAction {
  type: ProcessActionType.FlowNodeEnd;
  payload: {
    flowNodeId: string;
    tokenId: string;
    state: ExecutionState | string;
    endTime?: number;
    tokenHandling?: XOR<{ passTokenTo: string }, { endToken?: TokenState; removeToken?: boolean }>;
    errorMessage?: string;
    [key: string]: any;
  };
}

export function endFlowNodeAction(payload: {
  flowNodeId: string;
  tokenId: string;
  state: ExecutionState | string;
  endTime?: number;
  tokenHandling?: XOR<{ passTokenTo: string }, { endToken?: TokenState; removeToken?: boolean }>;
  errorMessage?: string;
  [key: string]: any;
}): EndFlowNodeAction {
  return {
    type: ProcessActionType.FlowNodeEnd,
    payload
  };
}

export interface EndActivityAction extends FlowNodeAction {
  type: ProcessActionType.ActivityEnd;
  payload: {
    flowNodeId: string;
    tokenId: string;
    state: ExecutionState;
  };
}

export function endActivityAction(payload: {
  flowNodeId: string;
  tokenId: string;
  state: ExecutionState;
}): EndActivityAction {
  return {
    type: ProcessActionType.ActivityEnd,
    payload
  };
}

export interface ReceiveMessageAction extends ProcessAction {
  type: ProcessActionType.EventReceiveMessage;
  payload: {
    id: string;
  };
}

export function receiveMessageAction(payload: { id: string }): ReceiveMessageAction {
  return { payload, type: ProcessActionType.EventReceiveMessage };
}

export interface SendMessageAction extends ProcessAction {
  type: ProcessActionType.EventSendMessage;
  payload: {
    id: string;
  };
}

export function sendMessageAction(payload: { id: string }): SendMessageAction {
  return { payload, type: ProcessActionType.EventSendMessage };
}

export interface SendSignalAction extends ProcessAction {
  type: ProcessActionType.EventSendSignal;
  payload: {
    id: string;
  };
}

export function sendSignalAction(payload: { id: string }): SendSignalAction {
  return { payload, type: ProcessActionType.EventSendSignal };
}

export interface ReceiveSignalAction extends ProcessAction {
  type: ProcessActionType.EventReceiveSignal;
  payload: {
    id: string;
  };
}

export function receiveSignalAction(payload: { id: string }): ReceiveSignalAction {
  return { payload, type: ProcessActionType.EventReceiveSignal };
}

export interface PropagateEscalationAction extends FlowNodeAction {
  type: ProcessActionType.PropagateEscalation;
  payload: {
    flowNodeId: string;
    tokenId: string;
    refId?: string;
  };
}

export function propagateEscalationAction(payload: {
  flowNodeId: string;
  tokenId: string;
  refId?: string;
}): PropagateEscalationAction {
  return { payload, type: ProcessActionType.PropagateEscalation };
}

export interface PropagateErrorAction extends FlowNodeAction {
  type: ProcessActionType.PropagateError;
  payload: {
    flowNodeId: string;
    tokenId: string;
    refId?: string;
  };
}

export function propagateErrorAction(payload: {
  flowNodeId: string;
  tokenId: string;
  refId?: string;
}): PropagateErrorAction {
  return { payload, type: ProcessActionType.PropagateError };
}

export interface TaskThrowEscalationAction extends FlowNodeAction {
  type: ProcessActionType.TaskThrowEscalation;
  payload: {
    flowNodeId: string;
    tokenId: string;
    refId?: string;
  };
}

export function taskThrowEscalationAction(payload: {
  flowNodeId: string;
  tokenId: string;
  refId?: string;
}): TaskThrowEscalationAction {
  return { payload, type: ProcessActionType.TaskThrowEscalation };
}

export interface TaskThrowErrorAction extends FlowNodeAction {
  type: ProcessActionType.TaskThrowError;
  payload: {
    flowNodeId: string;
    tokenId: string;
    refId?: string;
  };
}

export function taskThrowErrorAction(payload: {
  flowNodeId: string;
  tokenId: string;
  refId?: string;
}): TaskThrowErrorAction {
  return { payload, type: ProcessActionType.TaskThrowError };
}

export interface RespondThrowingEventAction extends FlowNodeAction {
  type: ProcessActionType.RespondThrowingEvent;
  payload: {
    flowNodeId: string;
    tokenId: string;
    caught: boolean;
    continueExecution: boolean;
  };
}

export function respondThrowingEventAction(payload: {
  flowNodeId: string;
  tokenId: string;
  caught: boolean;
  continueExecution: boolean;
}): RespondThrowingEventAction {
  return { payload, type: ProcessActionType.RespondThrowingEvent };
}

export interface InterruptAction extends FlowNodeAction {
  type: ProcessActionType.FlowNodeInterrupt;
  payload: {
    flowNodeId: string;
    tokenId: string;
  };
}

export function interruptAction(payload: { flowNodeId: string; tokenId: string }): InterruptAction {
  return { payload, type: ProcessActionType.FlowNodeInterrupt };
}

export interface ListenBoundaryEventAction extends FlowNodeAction {
  type: ProcessActionType.BoundaryEventListen;
  payload: {
    flowNodeId: string;
    tokenId: string;
  };
}

export function listenBoundaryEventAction({
  flowNodeId,
  tokenId
}: {
  flowNodeId: string;
  tokenId: string;
}): ListenBoundaryEventAction {
  return {
    type: ProcessActionType.BoundaryEventListen,
    payload: {
      flowNodeId,
      tokenId
    }
  };
}

export interface InitBoundaryEventAction extends FlowNodeAction {
  type: ProcessActionType.BoundaryEventInit;
  payload: {
    flowNodeId: string;
    tokenId: string;
    eventOrigin?: {
      flowNodeId: string;
      tokenId: string;
    };
  };
}

export function initBoundaryEventAction({
  flowNodeId,
  tokenId,
  eventOrigin
}: {
  flowNodeId: string;
  tokenId: string;
  eventOrigin?: { flowNodeId: string; tokenId: string };
}): InitBoundaryEventAction {
  return {
    type: ProcessActionType.BoundaryEventInit,
    payload: {
      flowNodeId,
      tokenId,
      eventOrigin
    }
  };
}

export interface UpdateVariablesAction extends ProcessAction {
  type: ProcessActionType.UpdateVariables;
  payload: {
    variables: VariablesState;
  };
}

export function updateVariablesAction(payload: { variables: VariablesState }): UpdateVariablesAction {
  return { payload, type: ProcessActionType.UpdateVariables };
}

export interface SetVariablesAction extends ProcessAction {
  type: ProcessActionType.SetVariables;
  payload: {
    changedBy: string;
    variables: {
      [key: string]: any;
    };
  };
}

export function setVariablesAction(payload: {
  changedBy: string;
  variables: { [key: string]: any };
}): SetVariablesAction {
  return { payload, type: ProcessActionType.SetVariables };
}

export interface LogExecutionAction extends ProcessAction {
  type: ProcessActionType.LogExecution;
  payload: {
    flowElementId: string;
    tokenId: string;
    executionState?: ExecutionState | string;
    startTime?: number;
    endTime?: number;
    errorMessage?: string;
    [key: string]: any;
  };
}

export function logExecutionAction(payload: {
  flowElementId: string;
  tokenId: string;
  executionState?: ExecutionState | string;
  startTime?: number;
  endTime?: number;
  errorMessage?: string;
  [key: string]: any;
}): LogExecutionAction {
  return { payload, type: ProcessActionType.LogExecution };
}

export interface UpdateLogAction extends ProcessAction {
  type: ProcessActionType.UpdateLog;
  payload: {
    flowElementId: string;
    tokenId: string;
    [key: string]: any;
  };
}

export function updatelogAction(payload: {
  flowElementId: string;
  tokenId: string;
  [key: string]: any;
}): UpdateLogAction {
  return { payload, type: ProcessActionType.UpdateLog };
}

export interface LogAdaptationAction extends ProcessAction {
  type: ProcessActionType.LogAdaptation;
  payload: AdaptationEntry;
}

export function logAdaptationAction(payload: AdaptationEntry): LogAdaptationAction {
  return { payload, type: ProcessActionType.LogAdaptation };
}

export interface UpdateAdaptationLogAction extends ProcessAction {
  type: ProcessActionType.UpdateAdaptationLog;
  payload: {
    type: AdaptationType | string;
    time: number;
    [key: string]: any;
  };
}

export function updateAdaptationLogAction(payload: {
  type: AdaptationType | string;
  time: number;
  [key: string]: any;
}): UpdateAdaptationLogAction {
  return { payload, type: ProcessActionType.UpdateAdaptationLog };
}

export interface SetFlowNodeStateAction extends TokenAction {
  type: ProcessActionType.SetFlowNodeState;
  payload: {
    tokenId: string;
    state: string;
  };
}

export function setFlowNodeStateAction(payload: { tokenId: string; state: string }): SetFlowNodeStateAction {
  return {
    type: ProcessActionType.SetFlowNodeState,
    payload
  };
}

export interface UpdateProcessIdAction extends ProcessAction {
  type: ProcessActionType.UpdateProcessId;
  payload: {
    processId: string;
  };
}

export function updateProcessIdAction(processId: string): UpdateProcessIdAction {
  return {
    type: ProcessActionType.UpdateProcessId,
    payload: {
      processId
    }
  };
}

export interface UpdateModdleDefinitionsAction extends ProcessAction {
  type: ProcessActionType.UpdateModdleDefinitions;
  payload: {
    moddleDefinitions: Definitions;
  };
}

export function updateModdleDefinitionsAction(moddleDefinitions: Definitions): UpdateModdleDefinitionsAction {
  return {
    type: ProcessActionType.UpdateModdleDefinitions,
    payload: {
      moddleDefinitions
    }
  };
}

export interface StartMigrationAction extends ProcessAction {
  type: ProcessActionType.StartMigration;
  payload: {};
}

export function startMigrationAction(): StartMigrationAction {
  return {
    type: ProcessActionType.StartMigration,
    payload: {}
  };
}

export interface EndMigrationAction extends ProcessAction {
  type: ProcessActionType.EndMigration;
  payload: {};
}

export function endMigrationAction(): EndMigrationAction {
  return {
    type: ProcessActionType.EndMigration,
    payload: {}
  };
}
