import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from } from 'rxjs';
import { switchMap, filter } from 'rxjs/operators';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../../util/streamOperators/extendActions';
import { ProcessAction, ProcessActionType, endTokenAction, EndFlowNodeAction } from '../../../actions';
import { ProcessState } from '../../../reducers';
import { Logger } from '../../../../LoggerFactory';
import { isTokenEndedState } from '../../../reducers/tokens';
import { ExecutionState } from '../../../reducers/log';

/**
 * bpmn:Subprocess epic to handle tokens contained inside the subprocess when the subprocess itself is finished
 *
 * @returns {Epic} epic handles end actions for subprocesses by ending all tokens inside of them that are still active
 * */
export default function embeddedSubprocessEndFlowNodeEpic(
  log: Logger
): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType<EndFlowNodeAction>(ProcessActionType.FlowNodeEnd).pipe(
      extendFlowNodeAction(_state$),
      filterFlowNodeTypeMismatch('bpmn:SubProcess'),
      filterTokenFlowNodeMismatch(),
      filter(
        ({ payload }) =>
          payload.state === ExecutionState.TERMINATED ||
          payload.state === ExecutionState.FAILED ||
          payload.state === ExecutionState.ABORTED ||
          payload.state === ExecutionState.SKIPPED
      ),
      switchMap(({ flowNode, payload }) => {
        // get tokens inside the subprocess (naming scheme: [suprocess token id]#[specific token id inside subprocess])
        const allSubprocessTokens = _state$.value.tokens.filter(
          t => t.tokenId !== payload.tokenId && t.tokenId.match(new RegExp(`^${payload.tokenId}\\#.+`))
        );

        // only finish tokens that are still running
        const activeSubprocessTokens = allSubprocessTokens.filter(t => !isTokenEndedState(t.state));

        const endTokenActions = activeSubprocessTokens.map(t =>
          endTokenAction({ tokenId: t.tokenId, state: payload.state, endTime: +new Date() })
        );

        if (payload.state !== ExecutionState.ABORTED) {
          log.debug({
            message: `Terminating SubProcess ${flowNode!.id} with all containing elements`,
            params: { processInstanceId: _state$.value.processInstanceId }
          });
        }

        return from(endTokenActions);
      })
    );
  };
}
