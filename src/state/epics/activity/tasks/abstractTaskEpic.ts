import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../../util/streamOperators/extendActions';
import { ProcessAction, ProcessActionType, ActivateFlowNodeAction, endActivityAction } from '../../../actions';
import { ProcessState } from '../../../reducers';
import { ExecutionState } from '../../../reducers/log';

/**
 * bpmn:Task epic to handle Activate action
 *
 * @returns {Epic} epic handles Activate actions and completes activity
 * */
export default function abstractTaskActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(_state$),
      filterFlowNodeTypeMismatch('bpmn:Task'),
      filterTokenFlowNodeMismatch(),
      switchMap(({ payload }) => {
        return from([
          // No execution logic for abstract tasks => complete on activation
          endActivityAction({
            flowNodeId: payload.flowNodeId,
            tokenId: payload.tokenId,
            state: ExecutionState.COMPLETED
          })
        ]);
      })
    );
  };
}
