import { ScriptTask } from 'bpmn-moddle';
import { ActionsObservable, combineEpics, Epic, ofType, StateObservable } from 'redux-observable';
import { from, merge, of } from 'rxjs';
import { catchError, filter, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { getService } from '../../../../CustomService';
import { Logger } from '../../../../LoggerFactory';
import { getCurrentScriptExecutor } from '../../../../ScriptExecutionEnvironment';
import { filterByBpmnType } from '../../../../util/streamOperators/filterByBpmnType';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../../util/streamOperators/extendActions';
import {
  ProcessAction,
  ProcessActionType,
  setVariablesAction,
  propagateEscalationAction,
  propagateErrorAction,
  RespondThrowingEventAction,
  endTokenAction,
  TaskThrowErrorAction,
  TaskThrowEscalationAction,
  taskThrowErrorAction,
  taskThrowEscalationAction,
  InterruptAction,
  EndTokenAction,
  endActivityAction,
  ActivateFlowNodeAction
} from '../../../actions';
import { ProcessState } from '../../../reducers';
import { BpmnEscalation, BpmnError } from '../../../../errors';
import { TokenState } from '../../../reducers/tokens';
import { ExecutionState } from '../../../reducers/log';

/**
 * bpmn:ScriptTask epic to handle Activate action
 *
 * @returns {Epic} epic handles Activate actions and executes script
 * */
export function scriptTaskActivateEpic(logger: Logger): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(state$),
      filterFlowNodeTypeMismatch('bpmn:ScriptTask'),
      filterTokenFlowNodeMismatch(),
      filter(({ flowNode }) => {
        const element = flowNode as ScriptTask;
        return !!element.script;
      }),
      mergeMap(({ payload, flowNode }) => {
        const element = flowNode as ScriptTask;

        const scriptString = (element.script || '').trim();
        const { processId, processInstanceId } = state$.value;

        const log = new Proxy(
          {},
          {
            get: function(target: any, name: string) {
              if (name in logger && typeof (logger as any)[name] === 'function') {
                return (message: any) =>
                  (logger as any)[name]({ params: { processId, processInstanceId }, message: message.toString() });
              }

              return target[name];
            }
          }
        );

        console = <any>{
          trace: log.trace,
          debug: log.debug,
          info: log.info,
          warn: log.warn,
          error: log.error,
          log: console.log,
          time: console.time,
          timeEnd: console.timeEnd
        };

        const variable = new Proxy(
          {},
          {
            get: function(target: any, name: string) {
              const variableService = getService('variable') as any;
              if (name in variableService && typeof variableService[name] === 'function') {
                if (name === 'getGlobal' || name === 'getAllGlobal' || name === 'getWithLogsGlobal')
                  return (...args: any[]) => variableService[name](processId, processInstanceId, ...args);
                return (...args: any[]) =>
                  variableService[name](processId, processInstanceId, payload.tokenId, ...args);
              }

              return target[name];
            }
          }
        );

        return from(
          getCurrentScriptExecutor().execute(processId, processInstanceId, payload.tokenId, scriptString, {
            log,
            console,
            variable,
            getService,
            BpmnEscalation,
            BpmnError
          })
        ).pipe(
          mergeMap((execResult: unknown) => {
            const actions = [
              endActivityAction({
                flowNodeId: payload.flowNodeId,
                tokenId: payload.tokenId,
                state: ExecutionState.COMPLETED
              })
            ];
            return Object.keys((execResult || {}) as object).length === 0
              ? actions
              : [setVariablesAction({ changedBy: payload.flowNodeId, variables: execResult as object }), ...actions];
          }),
          catchError((err: Error) => {
            if (err instanceof BpmnEscalation) {
              logger.debug({ message: err.explanation, params: { processInstanceId } });
              return from([
                taskThrowEscalationAction({
                  flowNodeId: payload.flowNodeId,
                  tokenId: payload.tokenId,
                  refId: err.name
                })
              ]);
            }
            if (err instanceof BpmnError) {
              logger.debug({ message: err.explanation, params: { processInstanceId } });
              return from([
                taskThrowErrorAction({
                  flowNodeId: payload.flowNodeId,
                  tokenId: payload.tokenId,
                  refId: err.name
                })
              ]);
            }

            return from([
              endTokenAction({
                tokenId: payload.tokenId,
                state: TokenState.ERROR_TECHNICAL,
                errorMessage: `Technical Error occured: ${err.name} | ${err.message}`,
                endTime: +new Date()
              })
            ]);
          }),
          takeUntil(
            merge(
              action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                filter(iax => {
                  const ia = iax as InterruptAction;
                  return ia.payload.flowNodeId === payload.flowNodeId && ia.payload.tokenId === payload.tokenId;
                })
              ),
              action$
                .ofType(ProcessActionType.EndToken)
                .pipe(filter(as => (as as EndTokenAction).payload.tokenId === payload.tokenId))
            )
          )
        );
      })
    );
  };
}

/**
 * bpmn:ScriptTask epic to throw escalation
 *
 * @returns {Epic} epic throws escalation and ends token
 * */
export function scriptTaskThrowEscalationEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType(ProcessActionType.TaskThrowEscalation).pipe(
      filterByBpmnType('bpmn:ScriptTask', _state$),
      switchMap(ax => {
        const { payload } = ax as TaskThrowEscalationAction;

        return merge(
          action$.pipe(
            ofType(ProcessActionType.RespondThrowingEvent),
            filter(
              re =>
                (re as RespondThrowingEventAction).payload.flowNodeId === payload.flowNodeId &&
                (re as RespondThrowingEventAction).payload.tokenId === payload.tokenId
            ),
            mergeMap(re => {
              const response = re as RespondThrowingEventAction;
              if (response.payload.caught) {
                if (response.payload.continueExecution) {
                  return from([
                    endActivityAction({
                      flowNodeId: payload.flowNodeId,
                      tokenId: payload.tokenId,
                      state: ExecutionState.COMPLETED
                    })
                  ]);
                } else {
                  return from([]);
                }
              } else {
                return from([
                  endTokenAction({
                    tokenId: payload.tokenId,
                    state: TokenState.ERROR_SEMANTIC,
                    errorMessage: `Thrown Escalation ${payload.refId} was not caught. Execution of Token is ended.`,
                    endTime: +new Date()
                  })
                ]);
              }
            })
          ),
          of(
            propagateEscalationAction({
              flowNodeId: payload.flowNodeId,
              tokenId: payload.tokenId,
              refId: payload.refId
            })
          )
        );
      })
    );
  };
}

/**
 * bpmn:ScriptTask epic to throw error
 *
 * @returns {Epic} epic throws error and ends token
 * */
export function scriptTaskThrowErrorEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType(ProcessActionType.TaskThrowError).pipe(
      filterByBpmnType('bpmn:ScriptTask', _state$),
      switchMap(ax => {
        const { payload } = ax as TaskThrowErrorAction;

        return merge(
          action$.pipe(
            ofType(ProcessActionType.RespondThrowingEvent),
            filter(
              re =>
                (re as RespondThrowingEventAction).payload.flowNodeId === payload.flowNodeId &&
                (re as RespondThrowingEventAction).payload.tokenId === payload.tokenId
            ),
            mergeMap(re => {
              const response = re as RespondThrowingEventAction;
              if (response.payload.caught) {
                return from([]);
              } else {
                return from([
                  endTokenAction({
                    tokenId: payload.tokenId,
                    state: TokenState.ERROR_SEMANTIC,
                    errorMessage: `Thrown Error ${payload.refId} was not caught. Execution of Token is ended.`,
                    endTime: +new Date()
                  })
                ]);
              }
            })
          ),
          of(
            propagateErrorAction({
              flowNodeId: payload.flowNodeId,
              tokenId: payload.tokenId,
              refId: payload.refId
            })
          )
        );
      })
    );
  };
}

/**
 * Combine all epics for implementing bpmn:ScriptTask type element
 */
export default (logger: Logger) =>
  combineEpics(scriptTaskActivateEpic(logger), scriptTaskThrowErrorEpic(), scriptTaskThrowEscalationEpic());
