import { ActionsObservable, combineEpics, Epic, StateObservable } from 'redux-observable';
import { from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import {
  extendFlowNodeAction,
  filterTokenFlowNodeMismatch,
  filterNonActivityType
} from '../../../util/streamOperators/extendActions';
import { ProcessAction, ProcessActionType, endFlowNodeAction, EndActivityAction } from '../../actions';
import { ProcessState } from '../../reducers';
import { TokenState } from '../../reducers/tokens';
import { ExecutionState } from '../../reducers/log';
import { Logger } from '../../../LoggerFactory';

import abstractTaskEpic from './tasks/abstractTaskEpic';
import scriptTaskEpic from './tasks/scriptTaskEpic';
import embeddedSubprocessEpic from './subprocess/embeddedSubprocessEpic';

/**
 * Will handle incoming EndActivityActions by triggering the correct EndFlowNodeAction and by forwarding the token to outgoing sequence flows
 *
 * @returns {Epic} handles activit ending actions and forwards the token to the outgoing sequence flows
 */
export function endActivityEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType<EndActivityAction>(ProcessActionType.ActivityEnd).pipe(
      extendFlowNodeAction(_state$),
      filterTokenFlowNodeMismatch(),
      filterNonActivityType(), // Needed? The event is already flowNode specific
      mergeMap(({ payload, flowNode }) => {
        let actions: ProcessAction[] = [];

        // check if the token should be ended based on how the flowNode ended
        let endToken;
        switch (payload.state) {
          case ExecutionState.TERMINATED:
            endToken = TokenState.TERMINATED;
            break;
          case ExecutionState.FAILED:
            endToken = TokenState.FAILED;
            break;
        }

        if (endToken) {
          // create action to end the flowNode
          actions.push(
            endFlowNodeAction({
              flowNodeId: payload.flowNodeId,
              tokenId: payload.tokenId,
              state: payload.state,
              endTime: +new Date(),
              tokenHandling: { endToken }
            })
          );
        } else {
          const [outgoingSequenceFlow] = flowNode.outgoing;
          // start the following element if the token should not be ended
          actions.push(
            endFlowNodeAction({
              flowNodeId: payload.flowNodeId,
              tokenId: payload.tokenId,
              state: payload.state,
              endTime: +new Date(),
              tokenHandling: { passTokenTo: outgoingSequenceFlow.id }
            })
          );
        }

        return from(actions);
      })
    );
  };
}

export default (log: Logger) =>
  combineEpics(abstractTaskEpic(), scriptTaskEpic(log), embeddedSubprocessEpic(log), endActivityEpic());
