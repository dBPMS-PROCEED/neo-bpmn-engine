import { SubProcess, StartEvent, FlowNode, SequenceFlow, BoundaryEvent, Process, FlowElement } from 'bpmn-moddle';
import { ActionsObservable, Epic, StateObservable, combineEpics, ofType } from 'redux-observable';
import { from, merge, iif, of } from 'rxjs';
import { filter, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { findElementById, getFlowElements } from '../../util/getFlowElements';
import { bufferDuringMigration } from '../../util/streamOperators/migrationBuffering';
import {
  extendFlowNodeAction,
  extendTokenAction,
  filterNonExistantToken,
  filterTokenFlowNodeMismatch
} from '../../util/streamOperators/extendActions';
import { Logger } from '../../LoggerFactory';
import {
  initFlowNodeAction,
  ProcessAction,
  ProcessActionType,
  WillPassTokenAction,
  didPassTokenAction,
  DidPassTokenAction,
  endProcessAction,
  updateTokenAction,
  createTokenAction,
  EndTokenAction,
  logExecutionAction,
  EndFlowNodeAction,
  endFlowNodeAction,
  InterruptAction,
  listenBoundaryEventAction,
  PauseTokenAction,
  UpdateTokenAction,
  endActivityAction,
  WillActivateFlowNodeAction,
  activateFlowNodeAction,
  InitFlowNodeAction,
  willActivateFlowNodeAction,
  endTokenAction,
  RemoveTokenAction,
  stopProcessAction,
  CreateTokenAction,
  reactivateProcessAction,
  setVariablesAction,
  removeTokenAction,
  ContinueTokenAction,
  LogExecutionAction,
  MoveTokenAction,
  willPassTokenAction
} from '../actions';
import { ProcessState } from '../reducers';
import uuid from 'uuid';
import { TokenState, isTokenEndedState } from '../reducers/tokens';
import { ExecutionState, isExecutionEnded } from '../reducers/log';
import { isInstanceEndedStatus } from '../reducers/status';

/**
 * willPassTokenEpic is responsible for delegating to bpmn activity type epics based on
 * the current state and element type. It also handles the async token transition
 * between one activity to the other by invoking appropriate hooks and routing tokens
 * accordingly
 *
 * @returns {Epic} epic handles actions and emits new actions asynchronously
 * */
export function willPassTokenEpic(
  shouldPassTokenHook?: (from: string, to: string, tokenId: string) => Promise<boolean>
): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<WillPassTokenAction>(ProcessActionType.TokenWillPass).pipe(
      bufferDuringMigration(action$), // hold off on passing tokens until after a migration has finished (the target element might not yet exist in the internal definitions)
      extendTokenAction(state$),
      filterNonExistantToken(),
      filter(({ payload, token }) => {
        const { moddleDefinitions } = state$.value;
        const sequenceFlow = findElementById(moddleDefinitions, payload.sequenceId) as SequenceFlow;

        const tokenReady = token.state === TokenState.READY;

        return tokenReady && !!sequenceFlow;
      }),
      mergeMap(({ payload, token }) => {
        const { moddleDefinitions } = state$.value;
        const sequenceFlow = findElementById(moddleDefinitions, payload.sequenceId) as SequenceFlow;

        const targetElementId = sequenceFlow.targetRef.id;

        const sourceElementId = sequenceFlow.sourceRef.id;

        if (shouldPassTokenHook) {
          return from(shouldPassTokenHook(sourceElementId, targetElementId, payload.tokenId)).pipe(
            switchMap((result: boolean) => {
              if (result) {
                return from([didPassTokenAction({ tokenId: payload.tokenId, to: targetElementId })]);
              }
              return [];
            }),
            takeUntil(
              merge(
                action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                  filter(iax => {
                    const ia = iax as InterruptAction;
                    return ia.payload.tokenId === payload.tokenId;
                  })
                ),
                action$.ofType(ProcessActionType.PauseToken).pipe(
                  filter(iax => {
                    const ia = iax as PauseTokenAction;
                    return ia.payload.tokenId === payload.tokenId;
                  })
                ),
                action$
                  .ofType(ProcessActionType.EndToken)
                  .pipe(filter(as => (as as EndTokenAction).payload.tokenId === payload.tokenId))
              )
            )
          );
        }
        return from([didPassTokenAction({ tokenId: payload.tokenId, to: targetElementId })]);
      })
    );
  };
}

/**
 * Will handle flow nodes that have been reached by a new token
 */
export function flowNodeInitEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (actions$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return actions$.ofType<InitFlowNodeAction>(ProcessActionType.FlowNodeInit).pipe(
      extendFlowNodeAction(state$),
      filterTokenFlowNodeMismatch(),
      mergeMap(({ payload }) => {
        return from([willActivateFlowNodeAction(payload)]);
      })
    );
  };
}

/**
 * Will call a provided hook to decide if the given flow node can be started
 *
 * Will default to true if no hook is provided (exceptions are flowNodes that can't be handled by the engine e.g. UserTasks and callActivities)
 *
 * @param shouldActivateFlowNode decides if the flowNode should be put in an active state
 */
export function willActivateFlowNodeEpic(
  shouldActivateFlowNodeHook?: (tokenId: string, flowNode: FlowNode) => Promise<boolean>
): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<WillActivateFlowNodeAction>(ProcessActionType.FlowNodeWillActivate).pipe(
      extendFlowNodeAction(state$),
      filterTokenFlowNodeMismatch(),
      mergeMap(({ payload, flowNode }) => {
        if (shouldActivateFlowNodeHook) {
          return from(shouldActivateFlowNodeHook(payload.tokenId, flowNode)).pipe(
            switchMap((result: boolean) => iif(() => result, from([activateFlowNodeAction(payload)]), from([]))),
            takeUntil(
              merge(
                action$.ofType(ProcessActionType.PauseToken).pipe(
                  filter(iax => {
                    const ia = iax as PauseTokenAction;
                    return ia.payload.tokenId === payload.tokenId;
                  })
                ),
                action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                  filter(iax => {
                    const ia = iax as InterruptAction;
                    return ia.payload.tokenId === payload.tokenId;
                  })
                ),
                action$.ofType(ProcessActionType.FlowNodeEnd).pipe(
                  filter(eax => {
                    const ea = eax as EndFlowNodeAction;
                    return ea.payload.tokenId === payload.tokenId;
                  })
                ),
                action$.ofType(ProcessActionType.EndToken).pipe(
                  filter(eax => {
                    const ea = eax as EndTokenAction;
                    return ea.payload.tokenId === payload.tokenId;
                  })
                )
              )
            )
          );
        } else {
          // prevent activation of elements that can't be handled inside the neo-engine (user tasks, call activies)
          const externalTasks = ['bpmn:UserTask', 'bpmn:CallActivity'];

          if (externalTasks.includes(flowNode.$type)) {
            return from([]);
          }
        }

        return from([activateFlowNodeAction(payload)]);
      })
    );
  };
}

/**
 * didPassTokenEpic is responsible for initialising the flowNode element the token lies on
 *
 * @returns {Epic} epic handles actions and emits new actions asynchronously
 * */
export function didPassTokenEpic(log: Logger): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<DidPassTokenAction>(ProcessActionType.TokenDidPass).pipe(
      bufferDuringMigration(action$), // hold off on passing tokens until after a migration has finished (the target element might not yet exist in the internal definitions)
      extendTokenAction(state$),
      filterNonExistantToken(),
      switchMap(({ payload, token }) => {
        const { tokenId, to } = payload;
        const targetElementId = payload.to;
        const { moddleDefinitions } = state$.value;
        const targetElement = findElementById(moddleDefinitions, targetElementId) as FlowNode;

        let parentElement = targetElement.$parent as Process | SubProcess;
        const flowElements = getFlowElements(parentElement);

        const boundaryEvents = flowElements.filter(e => {
          if (e.$type === 'bpmn:BoundaryEvent') {
            const boundaryEvent = e as BoundaryEvent;
            return boundaryEvent.attachedToRef.id === targetElement.id;
          }
        });

        const actions: ProcessAction[] = boundaryEvents.map(b =>
          listenBoundaryEventAction({ flowNodeId: b.id, tokenId: payload.tokenId })
        );

        log.debug({
          message: `Token ${payload.tokenId} gets passed to element ${payload.to} on this machine`,
          params: { processInstanceId: state$.value.processInstanceId }
        });

        if (targetElement.$type === 'bpmn:SubProcess') {
          // check if token in subprocess already running
          const tokenInSubprocess = state$.value.tokens.find(token =>
            token.tokenId.match(new RegExp(`^${payload.tokenId}\\#.+`))
          );

          if (!tokenInSubprocess) {
            // create token at startevent of subprocess if no token is already running in subprocess
            const subProcessStartEvent = (targetElement as SubProcess).flowElements.find(
              f => f.$type === 'bpmn:StartEvent'
            ) as StartEvent;

            const tokenId = `${payload.tokenId}#${uuid.v4()}`;

            actions.push(
              createTokenAction({
                ...token,
                tokenId: tokenId,
                state: TokenState.READY,
                startTime: token.startTime,
                currentFlowElement: { id: subProcessStartEvent.id },
                localExecutionTime: token.localExecutionTime
              })
            );
            actions.push(
              initFlowNodeAction({
                flowNodeId: subProcessStartEvent.id,
                tokenId
              })
            );
          }
        }

        if (targetElement.$parent.$type === 'bpmn:SubProcess') {
          const parentSubProcessId = (targetElement.$parent as SubProcess).id;

          const parentTokenId = (() => {
            const tokenParts = payload.tokenId.split('#');
            return tokenParts.length > 1 ? tokenParts.slice(0, -1).join('#') : tokenParts[0];
          })();

          const tokenAtSubprocess = state$.value.tokens.find(
            token => token.tokenId !== payload.tokenId && token.tokenId === parentTokenId
          );

          // init subprocess of element inside
          if (!tokenAtSubprocess) {
            actions.push(
              createTokenAction({
                ...token,
                tokenId: parentTokenId,
                state: TokenState.READY,
                currentFlowElement: { id: parentSubProcessId },
                startTime: token.startTime,
                localExecutionTime: token.localExecutionTime
              })
            );
            actions.push(
              didPassTokenAction({
                to: parentSubProcessId,
                tokenId: parentTokenId
              })
            );
          }
        }

        return from([
          updateTokenAction({
            tokenId: payload.tokenId,
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: payload.to,
              // for cases where the didPassToken Action is used to retrigger an element avoid to overwrite the initial start time of the element
              startTime: payload.to === token.currentFlowElement.id ? token.currentFlowElement.startTime : undefined
            },
            previousFlowElementId:
              token.currentFlowElement.id !== payload.to ? token.currentFlowElement.id : token.previousFlowElementId
          }),
          initFlowNodeAction({
            flowNodeId: to,
            tokenId
          }),
          ...actions
        ]);
      })
    );
  };
}

/**
 * continueTokenEpic is responsible for handling tokens that should continue execution after an instance import
 *
 * it is used to trigger logic that would have been started at the activation of an element but wasn't since the element was imported in an active state
 * */
export function continueTokenEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<ContinueTokenAction>(ProcessActionType.ContinueToken).pipe(
      extendTokenAction(state$),
      filterNonExistantToken(),
      switchMap(({ payload, token }) => {
        const { moddleDefinitions } = state$.value;
        const currentElement = findElementById(moddleDefinitions, token.currentFlowElement.id) as FlowElement;

        let parentElement = currentElement.$parent as Process | SubProcess;
        const flowElements = getFlowElements(parentElement);

        // make sure that boundary events are set back to listening for events
        const boundaryEvents = flowElements.filter(e => {
          if (e.$type === 'bpmn:BoundaryEvent') {
            const boundaryEvent = e as BoundaryEvent;
            return boundaryEvent.attachedToRef.id === currentElement.id;
          }
        });

        const actions: ProcessAction[] = boundaryEvents.map(b =>
          listenBoundaryEventAction({ flowNodeId: b.id, tokenId: payload.tokenId })
        );

        return actions;
      })
    );
  };
}

/**
 * endFlowNodeEpic is responsible for logging the succesfully ended flowNode execution and for writing variables that were changed during the execution of an activity
 *
 * @returns {Epic} epic handles actions and emits new actions asynchronously
 * */
export function endFlowNodeEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<EndFlowNodeAction>(ProcessActionType.FlowNodeEnd).pipe(
      extendTokenAction(state$),
      filterNonExistantToken(),
      switchMap(({ payload, token }) => {
        const { flowNodeId, tokenId, state, endTime, errorMessage, tokenHandling, ...optional } = payload;

        const actions = [];

        // if the flow node is succesfully completed write any intermediate changes to variables into the permanent instance state
        if (
          state === ExecutionState.COMPLETED &&
          token.intermediateVariablesState &&
          Object.keys(token.intermediateVariablesState).length
        ) {
          actions.push(setVariablesAction({ changedBy: flowNodeId, variables: token.intermediateVariablesState }));
        }

        if (errorMessage) {
          actions.push(
            logExecutionAction({
              ...optional,
              flowElementId: flowNodeId,
              tokenId,
              executionState: state,
              startTime: token.currentFlowElement.startTime,
              endTime,
              errorMessage
            })
          );
        } else {
          actions.push(
            logExecutionAction({
              ...optional,
              flowElementId: flowNodeId,
              tokenId,
              executionState: state,
              startTime: token.currentFlowElement.startTime,
              endTime
            })
          );
        }

        if (tokenHandling && tokenHandling.endToken && !isTokenEndedState(token.state)) {
          actions.push(
            endTokenAction({
              tokenId,
              state: tokenHandling.endToken,
              removeAfterEnding: tokenHandling.removeToken,
              endTime
            })
          );
        } else if (tokenHandling && tokenHandling.removeToken) {
          actions.push(removeTokenAction({ tokenId }));
        } else if (tokenHandling && tokenHandling.passTokenTo) {
          actions.push(willPassTokenAction({ tokenId, sequenceId: tokenHandling.passTokenTo }));
        }

        return from(actions);
      })
    );
  };
}

/**
 * endTokenEpic is responsible for managing ended tokens.
 * When a token is ended, also the flowNode the token lies on has to be ended.
 * If every token is in an end state the whole process/subprocess will be ended
 *
 * @returns {Epic} epic handles actions and emits new actions asynchronously
 * */
export function endTokenEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<EndTokenAction>(ProcessActionType.EndToken).pipe(
      extendTokenAction(state$),
      filterNonExistantToken(),
      switchMap(({ payload, token }) => {
        const flowNode = token.currentFlowElement;

        if (!flowNode) {
          return from([]);
        }

        const tokenAlreadyEnded = isTokenEndedState(token.state);

        if (tokenAlreadyEnded) {
          return from([]);
        }

        const { state, tokenId, errorMessage, endTime, removeAfterEnding, ...optional } = payload;

        const actions = [];

        if (!isExecutionEnded(token.tokenId, flowNode.id, token.currentFlowElement?.startTime, state$.value.log)) {
          actions.push(
            endFlowNodeAction({
              ...optional,
              errorMessage,
              endTime,
              flowNodeId: flowNode.id,
              tokenId,
              state: state === TokenState.ENDED ? ExecutionState.COMPLETED : state
            })
          );
        }

        actions.push(
          updateTokenAction({
            ...optional,
            tokenId,
            state,
            intermediateVariablesState: null
          })
        );

        if (removeAfterEnding) {
          // remove token after last execution of flowNode was logged
          return merge(
            from(actions),
            action$.pipe(
              ofType(ProcessActionType.LogExecution),
              filter(
                re =>
                  (re as LogExecutionAction).payload.flowElementId === flowNode.id &&
                  (re as LogExecutionAction).payload.tokenId === tokenId &&
                  (re as LogExecutionAction).payload.endTime === endTime
              ),
              mergeMap(() => {
                return from([removeTokenAction({ tokenId })]);
              })
            )
          );
        }

        return actions;
      })
    );
  };
}

export function createTokenEpic(log: Logger): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<CreateTokenAction>(ProcessActionType.CreateToken).pipe(
      extendTokenAction(state$),
      filterNonExistantToken(),
      switchMap(({ token }) => {
        // reactivate an ended instance if the new token is in a running state
        if (isInstanceEndedStatus(state$.value.status) && !isTokenEndedState(token.state)) {
          log.info({
            message: `Reactivating the instance due to a running token being added.`,
            params: { processInstanceId: state$.value.processInstanceId }
          });
          return from([reactivateProcessAction()]);
        }

        return from([]);
      })
    );
  };
}

export function moveTokenEpic(log: Logger): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<MoveTokenAction>(ProcessActionType.MoveToken).pipe(
      switchMap(action => {
        const actions: ProcessAction[] = [];

        const { token, elementId, targetDefinitions } = action.payload;
        const { moddleDefinitions } = state$.value;

        const existingToken = state$.value.tokens.find(t => t.tokenId === token.tokenId);

        if (!existingToken) {
          return from([]);
        }

        const currentFlowElement = findElementById(moddleDefinitions, existingToken.currentFlowElement.id);
        const newFlowElement = findElementById(targetDefinitions || moddleDefinitions, elementId);

        if (!currentFlowElement || !newFlowElement) {
          return from([]);
        }

        actions.push(
          updateTokenAction({
            ...token,
            tokenId: token.tokenId,
            state: TokenState.READY,
            currentFlowElement: { id: elementId },
            previousFlowElementId: existingToken?.currentFlowElement.id
          })
        );

        // if a sequence flow is targeted make sure that shouldPassTokenHook is called after moving
        if (newFlowElement.$type === 'bpmn:SequenceFlow') {
          actions.push(
            willPassTokenAction({
              sequenceId: elementId,
              tokenId: token.tokenId
            })
          );
        } else {
          // start execution of the element
          actions.push(
            didPassTokenAction({
              to: elementId,
              tokenId: token.tokenId
            })
          );
        }

        if (
          currentFlowElement.$type !== 'bpmn:SequenceFlow' &&
          !isExecutionEnded(
            token!.tokenId,
            currentFlowElement.id,
            existingToken!.currentFlowElement.startTime,
            state$.value.log
          )
        ) {
          // abort the current flowNode if it is being executed

          log.info({
            message: `Ending execution of flowNode ${currentFlowElement.name || currentFlowElement.id} to move token ${
              token.tokenId
            } to flow element ${currentFlowElement.name || currentFlowElement.id}`,
            params: { processInstanceId: state$.value.processInstanceId, tokenId: token.tokenId }
          });

          const endTime = +new Date();

          return merge(
            action$.pipe(
              ofType(ProcessActionType.LogExecution),
              filter(
                re =>
                  (re as LogExecutionAction).payload.flowElementId === currentFlowElement.id &&
                  (re as LogExecutionAction).payload.tokenId === token.tokenId &&
                  (re as LogExecutionAction).payload.endTime === endTime
              ),
              mergeMap(() => {
                return from(actions);
              })
            ),
            of(
              endFlowNodeAction({
                tokenId: token.tokenId,
                flowNodeId: currentFlowElement.id,
                state: ExecutionState.SKIPPED,
                endTime
              })
            )
          );
        }

        return from(actions);
      })
    );
  };
}

export function removeTokenEpic(log: Logger): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<RemoveTokenAction>(ProcessActionType.RemoveToken).pipe(
      // when a migration is running token removals should be buffered until the migration is finished
      // otherwise it might happen that the instance will go into a stopped state during the migration
      // this can happen when some tokens are removed as result of the migration and only afterwards new tokens are added
      bufferDuringMigration(action$),
      switchMap(action => {
        if (isInstanceEndedStatus(state$.value.status)) {
          return from([]);
        }

        // if the instance is not ended yet and there are no running tokens left => stop the instance
        const stopProcess = state$.value.tokens.every(token => isTokenEndedState(token.state));

        if (stopProcess) {
          log.info({
            message: `There is no running token left after removal of token ${action.payload.tokenId}. The instance will be stopped!`,
            params: { processInstanceId: state$.value.processInstanceId }
          });

          return from([stopProcessAction()]);
        }

        return from([]);
      })
    );
  };
}

/**
 * updateToken is responsible for managing updated tokens.
 * If every token is in an end state the whole process/subprocess will be ended
 *
 * @returns {Epic} epic handles actions and emits new actions asynchronously
 * */
export function updateTokenEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<UpdateTokenAction>(ProcessActionType.UpdateToken).pipe(
      bufferDuringMigration(action$), // hold off on updating tokens until after a migration has finished (the target element might not yet exist in the internal definitions)
      extendTokenAction(state$),
      filterNonExistantToken(),
      switchMap(({ payload, token }) => {
        const flowNode = token.currentFlowElement;

        if (!flowNode) {
          return from([]);
        }

        const { moddleDefinitions } = state$.value;
        const element = findElementById(moddleDefinitions, flowNode.id) as FlowNode;

        if (element.$parent.$type === 'bpmn:SubProcess') {
          const subprocess = element.$parent as SubProcess;

          const subprocessToken = state$.value.tokens.find(
            t => t.currentFlowElement.id === subprocess.id && t.state === TokenState.RUNNING
          );

          if (subprocessToken) {
            const tokensInSubprocess = state$.value.tokens.filter(
              t =>
                t.tokenId !== subprocessToken.tokenId && t.tokenId.match(new RegExp(`^${subprocessToken.tokenId}\\#.+`))
            );

            const { log } = state$.value;
            const endSubprocess =
              tokensInSubprocess.every(token => isTokenEndedState(token.state)) &&
              !isExecutionEnded(
                subprocessToken.tokenId,
                subprocess.id,
                subprocessToken.currentFlowElement.startTime,
                log
              );

            if (endSubprocess) {
              return from([
                endActivityAction({
                  flowNodeId: subprocessToken.currentFlowElement.id,
                  tokenId: subprocessToken.tokenId,
                  state: ExecutionState.COMPLETED
                })
              ]);
            }
          }
        }

        // end the instance if every token is in an ended state
        const endProcess =
          !isInstanceEndedStatus(state$.value.status) &&
          state$.value.tokens.every(token => isTokenEndedState(token.state));
        if (endProcess) {
          return from([endProcessAction({ tokenId: payload.tokenId })]);
        }

        // reactivate the previously ended instance if a token is changed to a non ended state
        const reactivateProcess =
          isInstanceEndedStatus(state$.value.status) &&
          !state$.value.tokens.every(token => isTokenEndedState(token.state));
        if (reactivateProcess) {
          return from([reactivateProcessAction()]);
        }

        return from([]);
      })
    );
  };
}

/**
 * Combine all epics for implementing token state management
 */
export default (
  log: Logger,
  shouldPassTokenHook?: (from: string, to: string, tokenId: string) => Promise<boolean>,
  shouldActivateFlowNodeHook?: (tokenId: string, flowNode: FlowNode) => Promise<boolean>
) =>
  combineEpics(
    willPassTokenEpic(shouldPassTokenHook),
    didPassTokenEpic(log),
    continueTokenEpic(),
    flowNodeInitEpic(),
    willActivateFlowNodeEpic(shouldActivateFlowNodeHook),
    endFlowNodeEpic(),
    endTokenEpic(),
    createTokenEpic(log),
    moveTokenEpic(log),
    removeTokenEpic(log),
    updateTokenEpic()
  );
