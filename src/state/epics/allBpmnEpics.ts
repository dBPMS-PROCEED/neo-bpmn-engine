import { ActionsObservable, combineEpics } from 'redux-observable';
import { from } from 'rxjs';
import LoggerFactory, { Logger } from '../../LoggerFactory';
import endEventEpic from './event/endEventEpic';
import intermediateCatchEpic from './event/intermediateCatchEpic';
import intermediateThrowEpic from './event/intermediateThrowEpic';
import startEventEpic from './event/startEventEpic';
import eventBasedGatewayEpic from './gateway/eventBasedGatewayEpic';
import exclusiveGatewayEpic from './gateway/exclusiveGatewayEpic';
import inclusiveGatewayEpic from './gateway/inclusiveGatewayEpic';
import parallelGatewayEpic from './gateway/parallelGatewayEpic';
import activityEpic from './activity/activityEpic';
import boundaryEventEpic from './event/boundaryEventEpic';
import { errorEventEpic } from './event/errorEventEpic';
import { escalationEventEpic } from './event/escalationEventEpic';

const debugLoggerEpic = (action$: ActionsObservable<any>) => {
  action$.subscribe(a => {
    console.log('--> Log: ', a);
  });

  return from([]);
};

const tempLogger = LoggerFactory.getLogger('script-task-logger');
// LoggerFactory.getLog$()
//   .pipe(filter(logItem => logItem.name === 'script-task-logger'))
//   .subscribe(logItem => console.log(logItem));

/**
 * Combine all task epics and export
 */
export default (log: Logger) =>
  combineEpics(
    // debugLoggerEpic,
    activityEpic(log),
    exclusiveGatewayEpic(),
    inclusiveGatewayEpic(),
    parallelGatewayEpic(),
    eventBasedGatewayEpic(),
    startEventEpic(),
    endEventEpic(),
    intermediateCatchEpic(),
    intermediateThrowEpic(),
    boundaryEventEpic(log),
    errorEventEpic(),
    escalationEventEpic()
  );
