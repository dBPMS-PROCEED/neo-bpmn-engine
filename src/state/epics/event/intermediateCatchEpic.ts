import { CatchEvent, FlowNode, MessageEventDefinition, SignalEventDefinition, TimerEventDefinition } from 'bpmn-moddle';
import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from, merge, of, timer } from 'rxjs';
import { filter, takeUntil, mergeMap, take, switchMap } from 'rxjs/operators';
import { parseIsoDuration } from '../../../util/durationParser';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import {
  InterruptAction,
  ProcessAction,
  ProcessActionType,
  ReceiveMessageAction,
  ReceiveSignalAction,
  updateTokenAction,
  endFlowNodeAction,
  EndTokenAction,
  PauseTokenAction,
  ActivateFlowNodeAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import { TokenState } from '../../reducers/tokens';
import { ExecutionState } from '../../reducers/log';

/**
 * bpmn:IntermediateCatchEvent epic to handle Activate action, which will create a new instance
 * and wait for the event, message, signal or a timer
 *
 * @returns {Epic} epic handles Activate actions, waits for event and emit log action and pass token
 * */
export default function intermediateCatchEventActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(state$),
      filterFlowNodeTypeMismatch('bpmn:IntermediateCatchEvent'),
      filterTokenFlowNodeMismatch(),
      filter(({ flowNode }) => {
        const element = flowNode as FlowNode & CatchEvent;

        return !!element.outgoing && !!element.eventDefinitions && element.eventDefinitions.length > 0;
      }),
      mergeMap(({ payload, flowNode, token }) => {
        const thisElement = flowNode as FlowNode & CatchEvent;
        // TODO: Refactor this to extract signal and message common code
        if (thisElement.eventDefinitions[0].$type === 'bpmn:MessageEventDefinition') {
          return merge(
            action$.ofType(ProcessActionType.EventReceiveMessage).pipe(
              filter(
                ma =>
                  (ma as ReceiveMessageAction).payload.id ===
                  (thisElement.eventDefinitions[0] as MessageEventDefinition).messageRef.id
              ),
              switchMap(() => {
                const [outgoingSequenceFlow] = thisElement.outgoing;
                return from([
                  endFlowNodeAction({
                    flowNodeId: payload.flowNodeId,
                    tokenId: payload.tokenId,
                    state: ExecutionState.COMPLETED,
                    endTime: +new Date(),
                    tokenHandling: { passTokenTo: outgoingSequenceFlow.id }
                  })
                ]);
              }),
              takeUntil(
                merge(
                  action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                    filter(iax => {
                      const ia = iax as InterruptAction;
                      return ia.payload.flowNodeId === thisElement.id && ia.payload.tokenId === payload.tokenId;
                    })
                  ),
                  action$.ofType(ProcessActionType.PauseToken).pipe(
                    filter(iax => {
                      const ia = iax as PauseTokenAction;
                      return ia.payload.tokenId === payload.tokenId;
                    })
                  ),
                  action$
                    .ofType(ProcessActionType.EndToken)
                    .pipe(filter(as => (as as EndTokenAction).payload.tokenId === payload.tokenId))
                )
              )
            ),
            of(updateTokenAction({ tokenId: payload.tokenId, state: TokenState.READY }))
          );
        }

        if (thisElement.eventDefinitions[0].$type === 'bpmn:SignalEventDefinition') {
          return merge(
            action$.ofType(ProcessActionType.EventReceiveSignal).pipe(
              filter(
                sa =>
                  (sa as ReceiveSignalAction).payload.id ===
                  (thisElement.eventDefinitions[0] as SignalEventDefinition).signalRef.id
              ),
              switchMap(() => {
                const [outgoingSequenceFlow] = thisElement.outgoing;
                return from([
                  endFlowNodeAction({
                    flowNodeId: payload.flowNodeId,
                    tokenId: payload.tokenId,
                    state: ExecutionState.COMPLETED,
                    endTime: +new Date(),
                    tokenHandling: { passTokenTo: outgoingSequenceFlow.id }
                  })
                ]);
              }),
              takeUntil(
                merge(
                  action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                    filter(iax => {
                      const ia = iax as InterruptAction;
                      return ia.payload.flowNodeId === thisElement.id && ia.payload.tokenId === payload.tokenId;
                    })
                  ),
                  action$.ofType(ProcessActionType.PauseToken).pipe(
                    filter(iax => {
                      const ia = iax as PauseTokenAction;
                      return ia.payload.tokenId === payload.tokenId;
                    })
                  ),
                  action$
                    .ofType(ProcessActionType.EndToken)
                    .pipe(filter(as => (as as EndTokenAction).payload.tokenId === payload.tokenId))
                )
              )
            ),
            of(updateTokenAction({ tokenId: payload.tokenId, state: TokenState.READY }))
          );
        }

        if (thisElement.eventDefinitions[0].$type === 'bpmn:TimerEventDefinition') {
          const timerEventDefinition = thisElement.eventDefinitions[0] as TimerEventDefinition;
          let duration = 0;

          if (timerEventDefinition.timeDuration) {
            [, duration] = parseIsoDuration(timerEventDefinition.timeDuration.body);

            // we can't just do interval(milliseconds) since we also want to handle cases where the instance might be restarted after the event had already started
            // in that case we consider the time between the event start and the current date as being elapsed execution time including the time in which the instance was not running
            const eventStartTime = token.currentFlowElement.startTime!;
            const currentTime = +new Date();
            duration = eventStartTime + duration - currentTime;
          } else if (timerEventDefinition.timeDate) {
            const timeDifference = Date.parse(timerEventDefinition.timeDate.body) - Date.now();
            duration = timeDifference > 0 ? timeDifference : 0;
          }

          return merge(
            timer(duration).pipe(
              take(1),
              switchMap(() => {
                const [outgoingSequenceFlow] = thisElement.outgoing;
                return from([
                  endFlowNodeAction({
                    flowNodeId: payload.flowNodeId,
                    tokenId: payload.tokenId,
                    state: ExecutionState.COMPLETED,
                    endTime: +new Date(),
                    tokenHandling: { passTokenTo: outgoingSequenceFlow.id }
                  })
                ]);
              }),
              takeUntil(
                merge(
                  action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                    filter(iax => {
                      const ia = iax as InterruptAction;
                      return ia.payload.flowNodeId === thisElement.id && ia.payload.tokenId === payload.tokenId;
                    })
                  ),
                  action$.ofType(ProcessActionType.PauseToken).pipe(
                    filter(iax => {
                      const ia = iax as PauseTokenAction;
                      return ia.payload.tokenId === payload.tokenId;
                    })
                  ),
                  action$
                    .ofType(ProcessActionType.EndToken)
                    .pipe(filter(as => (as as EndTokenAction).payload.tokenId === payload.tokenId))
                )
              )
            ),
            of(updateTokenAction({ tokenId: payload.tokenId, state: TokenState.READY }))
          );
        }
        return from([]);
      })
    );
  };
}
