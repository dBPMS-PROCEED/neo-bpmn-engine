import {
  FlowNode,
  MessageEventDefinition,
  SignalEventDefinition,
  ThrowEvent,
  EscalationEventDefinition
} from 'bpmn-moddle';
import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from, merge, of } from 'rxjs';
import { filter, mergeMap, switchMap } from 'rxjs/operators';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import {
  ProcessAction,
  ProcessActionType,
  sendMessageAction,
  sendSignalAction,
  propagateEscalationAction,
  RespondThrowingEventAction,
  endTokenAction,
  endFlowNodeAction,
  ActivateFlowNodeAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import { TokenState } from '../../reducers/tokens';
import { ExecutionState } from '../../reducers/log';

/**
 * bpmn:IntermediateThrowEvent epic to handle Activate action, which will create a new instance
 * and throws the event message, signal or a timer.
 *
 * @returns {Epic} epic handles Activate actions, throws event and emit log action and pass token
 * */
export default function intermediateThrowEventActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(state$),
      filterFlowNodeTypeMismatch('bpmn:IntermediateThrowEvent'),
      filterTokenFlowNodeMismatch(),
      switchMap(({ payload, flowNode }) => {
        const thisElement = flowNode as FlowNode & ThrowEvent;

        const [sequenceFlow] = thisElement.outgoing;
        const actions: ProcessAction[] = [
          endFlowNodeAction({
            flowNodeId: payload.flowNodeId,
            tokenId: payload.tokenId,
            state: ExecutionState.COMPLETED,
            endTime: +new Date(),
            tokenHandling: { passTokenTo: sequenceFlow.id }
          })
        ];
        if (thisElement.eventDefinitions && thisElement.eventDefinitions.length > 0) {
          if (thisElement.eventDefinitions[0].$type === 'bpmn:MessageEventDefinition') {
            return from([
              sendMessageAction({
                id: (thisElement.eventDefinitions[0] as MessageEventDefinition).messageRef.id
              }),
              ...actions
            ]);
          }

          if (thisElement.eventDefinitions[0].$type === 'bpmn:EscalationEventDefinition') {
            const escalationRefId = (
              (thisElement.eventDefinitions[0] as EscalationEventDefinition).escalationRef || { escalationRef: '' }
            ).id;
            return merge(
              action$.ofType(ProcessActionType.RespondThrowingEvent).pipe(
                filter(
                  re =>
                    (re as RespondThrowingEventAction).payload.flowNodeId === payload.flowNodeId &&
                    (re as RespondThrowingEventAction).payload.tokenId === payload.tokenId
                ),
                mergeMap(re => {
                  const response = re as RespondThrowingEventAction;
                  if (response.payload.caught) {
                    if (response.payload.continueExecution) {
                      return from(actions);
                    } else {
                      return from([]);
                    }
                  } else {
                    return from([
                      endTokenAction({
                        tokenId: payload.tokenId,
                        state: TokenState.ERROR_SEMANTIC,
                        errorMessage: `Thrown Escalation ${escalationRefId} was not caught. Execution of Token is ended.`,
                        endTime: +new Date()
                      })
                    ]);
                  }
                })
              ),
              of(
                propagateEscalationAction({
                  flowNodeId: payload.flowNodeId,
                  tokenId: payload.tokenId,
                  refId: escalationRefId
                })
              )
            );
          }

          if (thisElement.eventDefinitions[0].$type === 'bpmn:SignalEventDefinition') {
            return from([
              sendSignalAction({
                id: (thisElement.eventDefinitions[0] as SignalEventDefinition).signalRef.id
              }),
              ...actions
            ]);
          }
        } else {
          // empty throw event
          return from(actions);
        }
        return from([]);
      })
    );
  };
}
