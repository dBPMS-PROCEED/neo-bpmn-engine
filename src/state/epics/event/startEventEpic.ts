import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import { ActivateFlowNodeAction, ProcessAction, ProcessActionType, endFlowNodeAction } from '../../actions';
import { ProcessState } from '../../reducers';
import { ExecutionState } from '../../reducers/log';

/**
 * bpmn:StartEvent epic to handle Activate action, which will create a new instance
 * and pass token to next flow node
 *
 * @returns {Epic} epic handles Activate actions and passes token to next flow node
 * */
export default function startEventActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(_state$),
      filterFlowNodeTypeMismatch('bpmn:StartEvent'),
      filterTokenFlowNodeMismatch(),
      switchMap(({ payload, flowNode }) => {
        const [sequenceFlow] = flowNode!.outgoing;
        return from([
          endFlowNodeAction({
            flowNodeId: payload.flowNodeId,
            tokenId: payload.tokenId,
            state: ExecutionState.COMPLETED,
            endTime: +new Date(),
            tokenHandling: { passTokenTo: sequenceFlow.id }
          })
        ]);
      })
    );
  };
}
