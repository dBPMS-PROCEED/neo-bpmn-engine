import { EndEvent, SubProcess } from 'bpmn-moddle';
import { ActionsObservable, combineEpics, Epic, StateObservable } from 'redux-observable';
import { from, of } from 'rxjs';
import { mergeMap, switchMap } from 'rxjs/operators';
import { filterByBpmnType } from '../../../util/streamOperators/filterByBpmnType';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import {
  ActivateFlowNodeAction,
  ProcessAction,
  ProcessActionType,
  sendMessageAction,
  sendSignalAction,
  propagateErrorAction,
  propagateEscalationAction,
  RespondThrowingEventAction,
  endTokenAction,
  endActivityAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import { TokenState, isTokenEndedState } from '../../reducers/tokens';
import { ExecutionState } from '../../reducers/log';

/**
 * bpmn:endEvent epic to handle Activate action, which will create a new instance
 * and perform event
 *
 * @returns {Epic} epic handles Activate actions and performs event
 * */
export function endEventActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(_state$),
      filterFlowNodeTypeMismatch('bpmn:EndEvent'),
      filterTokenFlowNodeMismatch(),
      switchMap(({ payload, flowNode }) => {
        const element = flowNode as EndEvent;

        if (element.eventDefinitions) {
          const terminateEventDefinition = (element.eventDefinitions as any[]).find(
            e => e.$type === 'bpmn:TerminateEventDefinition'
          );

          if (terminateEventDefinition) {
            if (element.$parent.$type === 'bpmn:SubProcess') {
              const subprocessToken = _state$.value.tokens.find(
                t => t.currentFlowElement.id === (element.$parent as SubProcess).id && t.state === TokenState.RUNNING
              );

              if (subprocessToken) {
                return from([
                  endTokenAction({
                    tokenId: payload.tokenId,
                    state: TokenState.ENDED,
                    endTime: +new Date()
                  }),
                  endActivityAction({
                    flowNodeId: subprocessToken.currentFlowElement.id,
                    tokenId: subprocessToken.tokenId,
                    state: ExecutionState.ABORTED
                  })
                ]);
              }
            } else {
              const abortTokenActions = _state$.value.tokens
                .filter(t => t.tokenId !== payload.tokenId && !isTokenEndedState(t.state))
                .map(t => endTokenAction({ tokenId: t.tokenId, state: TokenState.ABORTED, endTime: +new Date() }));

              return from([
                endTokenAction({
                  tokenId: payload.tokenId,
                  state: TokenState.ENDED,
                  endTime: +new Date()
                }),
                ...abortTokenActions
              ]);
            }
          }

          const messageEventDefinition = (element.eventDefinitions as any[]).find(
            e => e.$type === 'bpmn:MessageEventDefinition'
          );

          if (messageEventDefinition) {
            return from([
              endTokenAction({ tokenId: payload.tokenId, state: TokenState.ENDED, endTime: +new Date() }),
              sendMessageAction({ id: messageEventDefinition.messageRef.id })
            ]);
          }

          const signalEventDefinition = (element.eventDefinitions as any[]).find(
            e => e.$type === 'bpmn:SignalEventDefinition'
          );

          if (signalEventDefinition) {
            return from([
              endTokenAction({ tokenId: payload.tokenId, state: TokenState.ENDED, endTime: +new Date() }),
              sendSignalAction({ id: signalEventDefinition.signalRef.id })
            ]);
          }

          const escalationEventDefinition = (element.eventDefinitions as any[]).find(
            e => e.$type === 'bpmn:EscalationEventDefinition'
          );

          if (escalationEventDefinition) {
            const escalationRefId = (escalationEventDefinition.escalationRef || { escalationRef: '' }).id;
            return of(
              propagateEscalationAction({
                flowNodeId: payload.flowNodeId,
                tokenId: payload.tokenId,
                refId: escalationRefId
              })
            );
          }

          const errorEventDefinition = (element.eventDefinitions as any[]).find(
            e => e.$type === 'bpmn:ErrorEventDefinition'
          );

          if (errorEventDefinition) {
            const errorRefId = (errorEventDefinition.errorRef || { errorRef: '' }).id;
            return of(
              propagateErrorAction({ flowNodeId: payload.flowNodeId, tokenId: payload.tokenId, refId: errorRefId })
            );
          }
        }

        return from([endTokenAction({ tokenId: payload.tokenId, state: TokenState.ENDED, endTime: +new Date() })]);
      })
    );
  };
}

/**
 * bpmn:endEvent epic to handle response from thrown event in end event
 * and end token depending on response
 *
 * @returns {Epic} epic handles response from thrown event and ends token
 * */
export function endEventResponseEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType(ProcessActionType.RespondThrowingEvent).pipe(
      filterByBpmnType('bpmn:EndEvent', state$),
      mergeMap(a => {
        const ax = a as RespondThrowingEventAction;
        if (ax.payload.caught) {
          if (ax.payload.continueExecution) {
            return from([
              endTokenAction({ tokenId: ax.payload.tokenId, state: TokenState.ENDED, endTime: +new Date() })
            ]);
          } else {
            return from([]);
          }
        } else {
          return from([
            endTokenAction({
              tokenId: ax.payload.tokenId,
              state: TokenState.ERROR_SEMANTIC,
              errorMessage: `Thrown Event was not caught. Execution of Token is ended.`,
              endTime: +new Date()
            })
          ]);
        }
      })
    );
  };
}

/**
 * Combine all epics for implementing bpmn:endEvent epic
 */
export default () => combineEpics(endEventActivateEpic(), endEventResponseEpic());
