import { BoundaryEvent, FlowNode, TimerEventDefinition } from 'bpmn-moddle';
import { ActionsObservable, combineEpics, Epic, StateObservable } from 'redux-observable';
import { from, merge, timer } from 'rxjs';
import { switchMap, filter, takeUntil, mergeMap } from 'rxjs/operators';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterNonExistantTokenOrFlowNode,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import {
  ProcessAction,
  ProcessActionType,
  ListenBoundaryEventAction,
  createTokenAction,
  InitBoundaryEventAction,
  respondThrowingEventAction,
  initBoundaryEventAction,
  endFlowNodeAction,
  EndFlowNodeAction,
  InterruptAction,
  PauseTokenAction,
  endTokenAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import { TokenState } from '../../reducers/tokens';
import { Logger } from '../../../LoggerFactory';
import { ExecutionState } from '../../reducers/log';
import { parseIsoDuration } from '../../../util/durationParser';
import uuid from 'uuid';

/**
 * bpmn:BoundaryEvent epic to listen for event at boundary of activity
 *
 * @returns {Epic} epic listens for event and emits INIT action if event caught
 * */
export function boundaryEventListenEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<ListenBoundaryEventAction>(ProcessActionType.BoundaryEventListen).pipe(
      extendFlowNodeAction(state$),
      filterFlowNodeTypeMismatch('bpmn:BoundaryEvent'),
      filterNonExistantTokenOrFlowNode(),
      mergeMap(({ payload, flowNode, token: attachedActivityToken }) => {
        const thisElement = flowNode as FlowNode & BoundaryEvent;
        const attachedActivityId = thisElement.attachedToRef.id;

        if (thisElement.eventDefinitions && thisElement.eventDefinitions.length > 0) {
          if (
            thisElement.eventDefinitions[0].$type === 'bpmn:TimerEventDefinition' &&
            (thisElement.eventDefinitions[0] as TimerEventDefinition).timeDuration
          ) {
            const [, milliseconds] = parseIsoDuration(
              (thisElement.eventDefinitions[0] as TimerEventDefinition).timeDuration.body
            );

            // calculate the time that the boundary event should wait

            // we can't just do timer(milliseconds) since we also want to handle cases where the instance might be restarted after the host task has already started
            // in that case we consider the time between the activity start and the current date as being elapsed execution time including the time in which the instance was not running
            const flowNodeStartTime = attachedActivityToken.currentFlowElement.startTime!;
            const currentTime = +new Date();
            const remainingTime = flowNodeStartTime + milliseconds - currentTime;

            return timer(remainingTime).pipe(
              mergeMap(() => {
                const boundaryTokens = state$.value.tokens.filter(token =>
                  token.tokenId.match(new RegExp(`^${payload.tokenId}\\|.+-x-.+`))
                );

                const boundaryTokenId = `${payload.tokenId}|${boundaryTokens.length + 1}-x-${uuid.v4()}`;
                return [
                  createTokenAction({
                    ...attachedActivityToken,
                    tokenId: boundaryTokenId,
                    currentFlowElement: {
                      id: payload.flowNodeId
                    },
                    state: TokenState.READY
                  }),
                  initBoundaryEventAction({
                    flowNodeId: thisElement.id,
                    tokenId: boundaryTokenId
                  })
                ];
              }),
              takeUntil(
                merge(
                  action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                    filter(iax => {
                      const ia = iax as InterruptAction;
                      return ia.payload.flowNodeId === attachedActivityId && ia.payload.tokenId === payload.tokenId;
                    })
                  ),
                  action$.ofType(ProcessActionType.PauseToken).pipe(
                    filter(iax => {
                      const ia = iax as PauseTokenAction;
                      return ia.payload.tokenId === payload.tokenId;
                    })
                  ),
                  action$
                    .ofType(ProcessActionType.FlowNodeEnd)
                    .pipe(
                      filter(
                        as =>
                          (as as EndFlowNodeAction).payload.flowNodeId === attachedActivityId &&
                          (as as EndFlowNodeAction).payload.tokenId === payload.tokenId
                      )
                    )
                )
              )
            );
          }
        }
        return from([]);
      })
    );
  };
}

/**
 * bpmn:BoundaryEvent epic to handle INIT action
 *
 * @returns {Epic} epic handles INIT actions and emits corresponding action for attached activity
 * */
export function boundaryEventInitEpic(log: Logger): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<InitBoundaryEventAction>(ProcessActionType.BoundaryEventInit).pipe(
      extendFlowNodeAction(state$),
      filterFlowNodeTypeMismatch('bpmn:BoundaryEvent'),
      filterTokenFlowNodeMismatch(),
      switchMap(({ payload, flowNode }) => {
        const thisElement = flowNode as BoundaryEvent;
        const [outgoingSequenceFlow] = thisElement.outgoing;
        const actions: ProcessAction[] = [
          endFlowNodeAction({
            flowNodeId: payload.flowNodeId,
            tokenId: payload.tokenId,
            state: ExecutionState.COMPLETED,
            endTime: +new Date(),
            tokenHandling: { passTokenTo: outgoingSequenceFlow.id }
          })
        ];

        const attachedActivityTokenId = (() => {
          const tokenParts = payload.tokenId.split('|');
          return tokenParts.length > 1 ? tokenParts.slice(0, -1).join('|') : tokenParts[0];
        })();

        if (thisElement.cancelActivity) {
          log.debug({
            message: `Interrupting Boundary Event at ${thisElement.attachedToRef.id} got triggered, main execution flow is interrupted and alternative execution flow is running now`,
            params: { processInstanceId: state$.value.processInstanceId }
          });

          actions.push(
            endTokenAction({
              tokenId: attachedActivityTokenId,
              endTime: +new Date(),
              state:
                thisElement.eventDefinitions[0].$type === 'bpmn:ErrorEventDefinition'
                  ? ExecutionState.FAILED
                  : ExecutionState.TERMINATED,
              removeAfterEnding: true
            })
          );

          if (payload.eventOrigin) {
            // stop execution at location of thrown event
            actions.push(
              respondThrowingEventAction({
                flowNodeId: payload.eventOrigin.flowNodeId,
                tokenId: payload.eventOrigin.tokenId,
                caught: true,
                continueExecution: false
              })
            );
          }
        } else {
          // continue execution at location of thrown event
          if (payload.eventOrigin) {
            actions.push(
              respondThrowingEventAction({
                flowNodeId: payload.eventOrigin.flowNodeId,
                tokenId: payload.eventOrigin.tokenId,
                caught: true,
                continueExecution: true
              })
            );
          }

          log.debug({
            message: `Non-interrupting Boundary Event at ${thisElement.attachedToRef.id} got triggered, main execution flow and alternative execution flow are running now`,
            params: { processInstanceId: state$.value.processInstanceId }
          });
        }

        return actions;
      })
    );
  };
}

/**
 * Combine all epics for boundaryEvents
 */
export default (log: Logger) => combineEpics(boundaryEventListenEpic(), boundaryEventInitEpic(log));
