import { ActionsObservable, Epic } from 'redux-observable';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ProcessAction, ProcessActionType, SendSignalAction } from '../../actions';
import { ProcessState } from '../../reducers';

export default (sendSignalHook: (signalId: string) => any): Epic<ProcessAction, ProcessAction, ProcessState> => {
  return (action$: ActionsObservable<ProcessAction>, _) => {
    return action$.ofType(ProcessActionType.EventSendSignal).pipe(
      switchMap(action => {
        sendSignalHook((action as SendSignalAction).payload.id);
        return from([]);
      })
    );
  };
};
