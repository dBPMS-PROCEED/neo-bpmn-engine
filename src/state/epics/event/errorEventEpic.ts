import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
  ProcessAction,
  ProcessActionType,
  PropagateErrorAction,
  createTokenAction,
  respondThrowingEventAction,
  initBoundaryEventAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import { BoundaryEvent, SubProcess, Process, ErrorEventDefinition, FlowElement } from 'bpmn-moddle';
import uuid from 'uuid';
import { findElementById, getFlowElements } from '../../../util/getFlowElements';
import { TokenState } from '../../reducers/tokens';

/**
 * errorEventEpic is responsible for assigning a thrown error event to fitting boundary event
 * or end running token if no boundary event found
 *
 * @returns {Epic} epic handles error and propagates until handled
 * */
export function errorEventEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType(ProcessActionType.PropagateError).pipe(
      switchMap(a => {
        const ax = a as PropagateErrorAction;

        const errorOriginToken = state$.value.tokens.find(
          token => token.currentFlowElement.id === ax.payload.flowNodeId && token.tokenId === ax.payload.tokenId
        );

        const { moddleDefinitions } = state$.value;
        const errorOriginElement = findElementById(moddleDefinitions, ax.payload.flowNodeId);

        if (errorOriginToken && errorOriginElement) {
          let currentTokenId = ax.payload.tokenId;
          let currentElement = errorOriginElement;

          while (currentElement.$type !== 'bpmn:Definitions') {
            let parentElement = currentElement.$parent as Process | SubProcess;
            const flowElements = getFlowElements(parentElement);

            const attachedBoundaryEvents = flowElements.filter(e => {
              if (e.$type !== 'bpmn:BoundaryEvent') {
                return false;
              }

              const boundaryEvent = e as BoundaryEvent;

              return (
                boundaryEvent.attachedToRef.id !== currentElement.id ||
                boundaryEvent.eventDefinitions[0].$type !== 'bpmn:ErrorEventDefinition'
              );
            });

            const matchingBoundaryEvent = attachedBoundaryEvents.find(b => {
              const boundaryEvent = b as BoundaryEvent;
              const errorEventDefinition = boundaryEvent.eventDefinitions[0] as ErrorEventDefinition;

              if (errorEventDefinition.errorRef) {
                return (
                  errorEventDefinition.errorRef.id === ax.payload.refId ||
                  errorEventDefinition.errorRef.errorCode === ax.payload.refId
                );
              } else if (boundaryEvent.name) {
                return boundaryEvent.name === ax.payload.refId;
              }
              return false;
            });

            const catchingBoundaryEvent = matchingBoundaryEvent
              ? matchingBoundaryEvent
              : attachedBoundaryEvents.find(b => {
                  const boundaryEvent = b as BoundaryEvent;
                  const errorEventDefinition = boundaryEvent.eventDefinitions[0] as ErrorEventDefinition;

                  return !errorEventDefinition.errorRef && !boundaryEvent.name;
                });

            if (catchingBoundaryEvent) {
              const currentToken = state$.value.tokens.find(t => t.tokenId === currentTokenId);

              if (currentToken) {
                const boundaryTokenId = `${currentTokenId}|${uuid.v4()}`;
                return from([
                  createTokenAction({
                    ...currentToken,
                    tokenId: boundaryTokenId,
                    state: TokenState.READY,
                    startTime: currentToken.startTime,
                    currentFlowElement: { id: catchingBoundaryEvent.id },
                    localExecutionTime: currentToken.localExecutionTime
                  }),
                  initBoundaryEventAction({
                    flowNodeId: catchingBoundaryEvent.id,
                    tokenId: boundaryTokenId,
                    eventOrigin: {
                      tokenId: ax.payload.tokenId,
                      flowNodeId: ax.payload.flowNodeId
                    }
                  })
                ]);
              }
            }

            currentElement = currentElement.$parent as FlowElement;
            currentTokenId = (() => {
              const tokenParts = currentTokenId.split('#');
              return tokenParts.length > 1 ? tokenParts.slice(0, -1).join('|') : tokenParts[0];
            })();
          }

          return from([
            respondThrowingEventAction({
              flowNodeId: ax.payload.flowNodeId,
              tokenId: ax.payload.tokenId,
              caught: false,
              continueExecution: false
            })
          ]);
        }

        return from([]);
      })
    );
  };
}
