import { ActionsObservable, Epic } from 'redux-observable';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ProcessAction, ProcessActionType, SendMessageAction } from '../../actions';
import { ProcessState } from '../../reducers';

export default (sendMessageHook: (messageId: string) => any): Epic<ProcessAction, ProcessAction, ProcessState> => {
  return (action$: ActionsObservable<ProcessAction>, _) => {
    return action$.ofType(ProcessActionType.EventSendMessage).pipe(
      switchMap(action => {
        sendMessageHook((action as SendMessageAction).payload.id);
        return from([]);
      })
    );
  };
};
