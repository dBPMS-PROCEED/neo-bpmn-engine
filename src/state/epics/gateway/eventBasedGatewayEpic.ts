import {
  EventBasedGateway,
  IntermediateCatchEvent,
  MessageEventDefinition,
  SignalEventDefinition,
  TimerEventDefinition
} from 'bpmn-moddle';
import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from, merge, of, timer } from 'rxjs';
import { filter, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { parseIsoDuration } from '../../../util/durationParser';
import { findElementById } from '../../../util/getFlowElements';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import {
  ActivateFlowNodeAction,
  ProcessAction,
  ProcessActionType,
  ReceiveMessageAction,
  ReceiveSignalAction,
  endFlowNodeAction,
  InterruptAction,
  updateTokenAction,
  EndTokenAction,
  PauseTokenAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import { TokenState } from '../../reducers/tokens';
import { ExecutionState } from '../../reducers/log';

/**
 * Returns the number of milliseconds that are defined as a timer events duration
 *
 * @param event the event for which we try to get the duration
 * @returns the number of milliseconds that are defined in a TimerEvents TimerDefinition
 */
function getTimerEventTime(event: IntermediateCatchEvent) {
  return parseIsoDuration((event.eventDefinitions[0] as TimerEventDefinition).timeDuration.body)[1];
}

/**
 * bpmn:EventBasedGateway epic to wait for event and pass token to suitable sequence flow
 *
 * @returns {Epic} epic handles Activate actions and waits for event and passes token to suitable sequence flow
 * */
export function eventBasedGatewayActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(_state$),
      filterFlowNodeTypeMismatch('bpmn:EventBasedGateway'),
      filterTokenFlowNodeMismatch(),
      mergeMap(({ payload, flowNode, token }) => {
        const gateway = flowNode as EventBasedGateway;

        // get all (catch-)events that are following the gateway
        let events = gateway.outgoing
          .map(o => {
            const { moddleDefinitions } = _state$.value;
            return findElementById(moddleDefinitions, o.targetRef.id) as IntermediateCatchEvent;
          })
          .filter(flowNode => flowNode && flowNode.$type === 'bpmn:IntermediateCatchEvent');

        // if there is more than one timer event we only need to consider the one with the shortest duration(/closest timeout) since that should be the one that always triggers first
        // especially important in cases where an instance is restarted after more than one duration has ran out already
        // => we need to make sure that only the one with the shortest timeout is trigerred as it would have been if the engine had run normally
        const timerEvents = events.filter(event => event.eventDefinitions[0].$type === 'bpmn:TimerEventDefinition');

        if (timerEvents.length > 1) {
          timerEvents.sort((a, b) => {
            const aDuration = getTimerEventTime(a);
            const bDuration = getTimerEventTime(b);
            return aDuration - bDuration;
          });

          events = events.filter(
            event => event.eventDefinitions[0].$type !== 'bpmn:TimerEventDefinition' || event === timerEvents[0]
          );
        }

        const merged$ = merge(
          ...events.map(event => {
            if (event.eventDefinitions[0].$type === 'bpmn:MessageEventDefinition') {
              return action$.ofType(ProcessActionType.EventReceiveMessage).pipe(
                filter(
                  ma =>
                    (ma as ReceiveMessageAction).payload.id ===
                    (event.eventDefinitions[0] as MessageEventDefinition).messageRef.id
                ),
                switchMap(() => {
                  const [outgoingSequenceFlow] = event.outgoing;
                  return from([
                    endFlowNodeAction({
                      flowNodeId: payload.flowNodeId,
                      tokenId: payload.tokenId,
                      state: ExecutionState.COMPLETED,
                      endTime: +new Date(),
                      tokenHandling: { passTokenTo: outgoingSequenceFlow.id }
                    })
                  ]);
                })
              );
            }

            if (event.eventDefinitions[0].$type === 'bpmn:SignalEventDefinition') {
              return action$.ofType(ProcessActionType.EventReceiveSignal).pipe(
                filter(
                  sa =>
                    (sa as ReceiveSignalAction).payload.id ===
                    (event.eventDefinitions[0] as SignalEventDefinition).signalRef.id
                ),
                switchMap(() => {
                  const [outgoingSequenceFlow] = event.outgoing;
                  return from([
                    endFlowNodeAction({
                      flowNodeId: payload.flowNodeId,
                      tokenId: payload.tokenId,
                      state: ExecutionState.COMPLETED,
                      endTime: +new Date(),
                      tokenHandling: { passTokenTo: outgoingSequenceFlow.id }
                    })
                  ]);
                })
              );
            }

            if (event.eventDefinitions[0].$type === 'bpmn:TimerEventDefinition') {
              const durationMillis = getTimerEventTime(event);

              // we can't just do interval(durationMillis) since we also want to handle cases where the instance might be restarted after the gateway has already started
              // in that case we consider the time between the gateway start and the current date as being elapsed execution time including the time in which the instance was not running
              const flowNodeStartTime = token.currentFlowElement.startTime!;
              const currentTime = Date.now();
              const remainingTime = flowNodeStartTime + durationMillis - currentTime;

              return timer(remainingTime).pipe(
                take(1),
                switchMap(() => {
                  const [outgoingSequenceFlow] = event.outgoing;
                  return from([
                    endFlowNodeAction({
                      flowNodeId: payload.flowNodeId,
                      tokenId: payload.tokenId,
                      state: ExecutionState.COMPLETED,
                      endTime: +new Date(),
                      tokenHandling: { passTokenTo: outgoingSequenceFlow.id }
                    })
                  ]);
                })
              );
            }

            return from([]);
          })
        );

        // to secure the 2 actions (updateTokenAction, endExecution) get emitted only once
        // (only one event is allowed to be triggered -> after that: cancel observable)
        return merge(merged$, of(updateTokenAction({ tokenId: payload.tokenId, state: TokenState.READY }))).pipe(
          take(2),
          takeUntil(
            merge(
              action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                filter(iax => {
                  const ia = iax as InterruptAction;
                  return ia.payload.flowNodeId === payload.flowNodeId && ia.payload.tokenId === payload.tokenId;
                })
              ),
              action$.ofType(ProcessActionType.PauseToken).pipe(
                filter(iax => {
                  const ia = iax as PauseTokenAction;
                  return ia.payload.tokenId === payload.tokenId;
                })
              ),
              action$
                .ofType(ProcessActionType.EndToken)
                .pipe(filter(as => (as as EndTokenAction).payload.tokenId === payload.tokenId))
            )
          )
        );
      })
    );
  };
}

export default () => eventBasedGatewayActivateEpic();
