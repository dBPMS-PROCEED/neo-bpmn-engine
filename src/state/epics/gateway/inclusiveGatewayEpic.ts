import { FlowNode, InclusiveGateway } from 'bpmn-moddle';
import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from, forkJoin, merge } from 'rxjs';
import { filter, switchMap, catchError, takeUntil } from 'rxjs/operators';
import Queue from '../../../util/queue';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import {
  ActivateFlowNodeAction,
  ProcessAction,
  ProcessActionType,
  willPassTokenAction,
  removeTokenAction,
  updateTokenAction,
  createTokenAction,
  endTokenAction,
  endFlowNodeAction,
  InterruptAction,
  EndTokenAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import { TokenState } from '../../reducers/tokens';
import uuid = require('uuid');
import { ExecutionState } from '../../reducers/log';
import { getCurrentScriptExecutor } from '../../../ScriptExecutionEnvironment';

/**
 * bpmn:InclusiveGateway epic to handle Activate action, which will create a new instance
 * and pass token to suitable sequence flow(s) or join tokens
 *
 * @returns {Epic} epic handles Activate actions and passes token to suitable sequence flow(s) or join tokens
 * */
export function inclusiveGatewayActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(state$),
      filterFlowNodeTypeMismatch('bpmn:InclusiveGateway'),
      filterTokenFlowNodeMismatch(),
      switchMap(({ payload, token: thisToken, flowNode: thisElement }) => {
        const { processId, processInstanceId, variables } = state$.value;
        let mappedVariables = Object.assign({}, ...Object.keys(variables).map(k => ({ [k]: variables[k].value })));

        const outgoingFlows = (thisElement as FlowNode).outgoing;
        const conditionExpressions = outgoingFlows.filter(o => o.conditionExpression !== undefined);

        const parentTokenId = (() => {
          const tokenParts = payload.tokenId.split('|');
          return tokenParts.length > 1 ? tokenParts.slice(0, -1).join('|') : tokenParts[0];
        })();

        const tokenExistsInPath = (() => {
          // TODO: Handle loops in the path
          const q = Queue.make<{ element: FlowNode; pathBeginAt: string }>(
            ...(thisElement.incoming || []).map(sf => ({
              element: sf.sourceRef,
              pathBeginAt: sf.sourceRef.id
            }))
          );
          const visited: { [k: string]: any } = {};
          const childTokens = state$.value.tokens.filter(
            token => token.tokenId !== parentTokenId && token.tokenId.match(new RegExp(`^${parentTokenId}\\|.+`))
          );

          while (!q.isEmpty()) {
            const nextElement = q.dequeue();

            if (nextElement && nextElement.element) {
              if (visited[nextElement.element.id]) {
                continue;
              }
            } else {
              continue;
            }
            // check if token is at element or at one of the outgoing sequenceFlows
            if (
              childTokens.find(
                t =>
                  t.currentFlowElement.id === nextElement.element.id ||
                  (nextElement.element.outgoing || []).find(flow => flow.id === t.currentFlowElement.id)
              )
            ) {
              return true;
            } else {
              q.enqueue(
                ...(nextElement.element.incoming || []).map(sf => ({
                  element: sf.sourceRef,
                  pathBeginAt: nextElement.pathBeginAt
                }))
              );
            }
          }

          return false;
        })();

        if (tokenExistsInPath) {
          // set token state to READY because not all required tokens are present
          return from([
            updateTokenAction({
              tokenId: payload.tokenId,
              state: TokenState.READY
            })
          ]);
        }

        const tokensAtGateway = state$.value.tokens.filter(
          token => token.tokenId.startsWith(parentTokenId) && token.currentFlowElement.id === payload.flowNodeId
        );

        // merge
        if (outgoingFlows.length === 1) {
          if (thisElement.incoming && thisElement.incoming.length > 1) {
            return from([
              endFlowNodeAction({
                flowNodeId: payload.flowNodeId,
                tokenId: payload.tokenId,
                state: ExecutionState.COMPLETED,
                tokenHandling: { removeToken: true },
                endTime: +new Date()
              }),
              createTokenAction({
                ...thisToken,
                tokenId: parentTokenId,
                state: TokenState.READY,
                currentFlowElement: { id: thisToken.currentFlowElement.id },
                localStartTime: Math.min(...tokensAtGateway.map(t => t.localStartTime)),
                localExecutionTime: Math.max(...tokensAtGateway.map(t => t.localExecutionTime))
              }),
              ...tokensAtGateway
                .filter(t => t.tokenId !== payload.tokenId)
                .map(t => removeTokenAction({ tokenId: t.tokenId })),
              willPassTokenAction({
                tokenId: parentTokenId,
                sequenceId: outgoingFlows[0].id
              })
            ]);
          }
          // else: single incoming + single outgoing flow -> neither merge nor split of token
        }

        const evaluations: Promise<[boolean, string]>[] = conditionExpressions.map(o => {
          return new Promise(resolve => {
            let result: [boolean, string];
            getCurrentScriptExecutor()
              .execute(
                processId,
                processInstanceId,
                payload.tokenId,
                `return ${o.conditionExpression.body}`,
                mappedVariables
              )
              .then(value => {
                result = [value as boolean, o.id];
                resolve(result);
              });
          });
        });

        return forkJoin(evaluations).pipe(
          switchMap(results => {
            const truePaths = results.filter((o: [boolean, string]) => o[0]).map((o: [boolean, string]) => o[1]);

            if (truePaths.length === 0) {
              const defaultFlow = (thisElement as InclusiveGateway).default;

              const tokenId =
                thisElement.incoming.length > 1
                  ? `${parentTokenId}|1-1-${uuid.v4()}` // split AND merge
                  : `${payload.tokenId}|1-1-${uuid.v4()}`; // split

              if (defaultFlow) {
                // outgoing: 1
                return from([
                  endFlowNodeAction({
                    flowNodeId: payload.flowNodeId,
                    tokenId: payload.tokenId,
                    state: ExecutionState.COMPLETED,
                    tokenHandling: { removeToken: true },
                    endTime: +new Date()
                  }),
                  createTokenAction({
                    ...thisToken,
                    tokenId: tokenId,
                    state: TokenState.READY,
                    localStartTime: Math.min(...tokensAtGateway.map(t => t.localStartTime)),
                    localExecutionTime: Math.max(...tokensAtGateway.map(t => t.localExecutionTime)),
                    currentFlowElement: { id: thisToken.currentFlowElement.id }
                  }),
                  ...tokensAtGateway
                    .filter(t => t.tokenId !== payload.tokenId)
                    .map(t => removeTokenAction({ tokenId: t.tokenId })),
                  willPassTokenAction({
                    tokenId,
                    sequenceId: defaultFlow.id
                  })
                ]);
              }
              // else: multiple outgoing flows but no true evaluation and no default flow -> token can not pass
            }

            // generate tokens for all valid outgoing paths
            const truePathTokenInfo = truePaths.map((truePath, index) => {
              const tokenId =
                thisElement.incoming.length > 1
                  ? `${parentTokenId}|${index + 1}-${truePaths.length}-${uuid.v4()}` // split AND merge
                  : `${payload.tokenId}|${index + 1}-${truePaths.length}-${uuid.v4()}`; // split
              return { tokenId, truePath };
            });

            // outgoing: 1 or multiple
            return from([
              endFlowNodeAction({
                flowNodeId: payload.flowNodeId,
                tokenId: payload.tokenId,
                state: ExecutionState.COMPLETED,
                tokenHandling: { removeToken: true },
                endTime: +new Date()
              }),
              ...truePathTokenInfo.map(({ tokenId }) =>
                createTokenAction({
                  ...thisToken,
                  tokenId,
                  state: TokenState.READY,
                  currentFlowElement: { id: thisToken.currentFlowElement.id },
                  localStartTime: Math.min(...tokensAtGateway.map(t => t.localStartTime)),
                  localExecutionTime: Math.max(...tokensAtGateway.map(t => t.localExecutionTime))
                })
              ),
              ...tokensAtGateway
                .filter(t => t.tokenId !== payload.tokenId)
                .map(t => removeTokenAction({ tokenId: t.tokenId })),
              ...truePathTokenInfo.map(({ tokenId, truePath }) =>
                willPassTokenAction({
                  tokenId,
                  sequenceId: truePath
                })
              )
            ]);
          }),
          catchError((err: Error) => {
            return from([
              endTokenAction({
                tokenId: payload.tokenId,
                state: TokenState.ERROR_TECHNICAL,
                errorMessage: `Technical Error occured: ${err.name} | ${err.message}`,
                endTime: +new Date()
              })
            ]);
          }),
          takeUntil(
            merge(
              action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                filter(iax => {
                  const ia = iax as InterruptAction;
                  return ia.payload.flowNodeId === payload.flowNodeId && ia.payload.tokenId === payload.tokenId;
                })
              ),
              action$
                .ofType(ProcessActionType.EndToken)
                .pipe(filter(as => (as as EndTokenAction).payload.tokenId === payload.tokenId))
            )
          )
        );
      })
    );
  };
}

export default () => inclusiveGatewayActivateEpic();
