import { ParallelGateway } from 'bpmn-moddle';
import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import {
  ActivateFlowNodeAction,
  ProcessAction,
  ProcessActionType,
  willPassTokenAction,
  removeTokenAction,
  updateTokenAction,
  createTokenAction,
  endFlowNodeAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import uuid = require('uuid');
import { TokenState, Token } from '../../reducers/tokens';
import { ExecutionState } from '../../reducers/log';

/**
 * bpmn:ParallelGateway epic to handle Activate action and take care of splitting/joining token
 *
 * @returns {Epic} epic handles Activate actions and takes care of splitting/joining token
 * */
export function parallelGatewayActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, _state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(_state$),
      filterFlowNodeTypeMismatch('bpmn:ParallelGateway'),
      filterTokenFlowNodeMismatch(),
      switchMap(({ payload, token: thisToken, flowNode }) => {
        const thisElement = flowNode as ParallelGateway;

        const parentTokenId = (() => {
          const tokenParts = payload.tokenId.split('|');
          return tokenParts.length > 1 ? tokenParts.slice(0, -1).join('|') : tokenParts[0];
        })();

        const tokensAtGateway = _state$.value.tokens.filter(
          token => token.currentFlowElement.id === payload.flowNodeId
        );

        // check if one incoming sequence flow is missing a token
        if (
          !thisElement.incoming.every(flow =>
            tokensAtGateway.some(gatewayToken => gatewayToken.previousFlowElementId === flow.id)
          )
        ) {
          // set token state to READY because not all required tokens are present
          return from([
            updateTokenAction({
              tokenId: payload.tokenId,
              state: TokenState.READY
            })
          ]);
        }

        // get one token from each incoming flow
        const flowToTokenMapping = thisElement.incoming
          .map(flow => flow.id)
          .reduce((obj: { [flowId: string]: Token | null }, flowId) => {
            obj[flowId] = null;
            return obj;
          }, {});

        tokensAtGateway.forEach((token: Token) => {
          if (token.tokenId === thisToken.tokenId) {
            // always use the current token
            flowToTokenMapping[token.previousFlowElementId!] = token;
          } else if (token.tokenId.includes(parentTokenId)) {
            // always prefer merging with tokens that were split from the same parent token
            flowToTokenMapping[token.previousFlowElementId!] = token;
          } else if (!flowToTokenMapping[token.previousFlowElementId!]) {
            // always take token if no other token was chosen for the sequence flow
            flowToTokenMapping[token.previousFlowElementId!] = token;
          }
        });

        const tokensToMerge: Token[] = Object.values(flowToTokenMapping as { [flowId: string]: Token });

        // merge
        if (thisElement.outgoing.length === 1) {
          const tokensToMergeHaveSameOrigin = tokensToMerge.every(
            token => token.tokenId !== parentTokenId && token.tokenId.match(new RegExp(`^${parentTokenId}\\|.+`))
          );

          const tokenId = tokensToMergeHaveSameOrigin ? parentTokenId : tokensToMerge.map(t => t.tokenId).join('_');
          if (thisElement.incoming.length > 1) {
            return from([
              endFlowNodeAction({
                flowNodeId: payload.flowNodeId,
                tokenId: payload.tokenId,
                state: ExecutionState.COMPLETED,
                tokenHandling: { removeToken: true },
                endTime: +new Date()
              }),
              createTokenAction({
                ...thisToken,
                tokenId: tokenId,
                state: TokenState.READY,
                currentFlowElement: { id: thisToken.currentFlowElement.id },
                previousFlowElementId: undefined,
                localStartTime: Math.min(...tokensToMerge.map(t => t.localStartTime)),
                localExecutionTime: Math.max(...tokensToMerge.map(t => t.localExecutionTime))
              }),
              ...tokensToMerge
                .filter(t => t.tokenId !== payload.tokenId)
                .map(t => removeTokenAction({ tokenId: t.tokenId })),
              willPassTokenAction({
                sequenceId: thisElement.outgoing[0].id,
                tokenId: tokenId
              })
            ]);
          }
          // else: single incoming + single outgoing flow -> neither merge nor split of token
        }

        const outgoingTokenInfo = thisElement.outgoing.map((o, index) => {
          const tokenId =
            thisElement.incoming.length > 1
              ? `${parentTokenId}|${index + 1}-${thisElement.outgoing.length}-${uuid.v4()}` // split AND merge
              : `${payload.tokenId}|${index + 1}-${thisElement.outgoing.length}-${uuid.v4()}`; // split
          return { tokenId, sequenceId: o.id };
        });

        return from([
          endFlowNodeAction({
            flowNodeId: payload.flowNodeId,
            tokenId: payload.tokenId,
            state: ExecutionState.COMPLETED,
            tokenHandling: { removeToken: true },
            endTime: +new Date()
          }),
          ...outgoingTokenInfo.map(({ tokenId }) =>
            createTokenAction({
              ...thisToken,
              tokenId,
              state: TokenState.READY,
              currentFlowElement: { id: thisToken.currentFlowElement.id },
              previousFlowElementId: undefined,
              localStartTime: Math.min(...tokensToMerge.map(t => t.localStartTime)),
              localExecutionTime: Math.max(...tokensToMerge.map(t => t.localExecutionTime))
            })
          ),
          ...tokensToMerge
            .filter(t => t.tokenId !== payload.tokenId)
            .map(t => removeTokenAction({ tokenId: t.tokenId })),
          ...outgoingTokenInfo.map(({ tokenId, sequenceId }) =>
            willPassTokenAction({
              tokenId,
              sequenceId
            })
          )
        ]);
      })
    );
  };
}

export default () => parallelGatewayActivateEpic();
