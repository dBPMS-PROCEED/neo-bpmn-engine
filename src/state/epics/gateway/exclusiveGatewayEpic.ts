import { ExclusiveGateway } from 'bpmn-moddle';
import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from, forkJoin, merge } from 'rxjs';
import { switchMap, catchError, filter, takeUntil } from 'rxjs/operators';
import {
  extendFlowNodeAction,
  filterFlowNodeTypeMismatch,
  filterTokenFlowNodeMismatch
} from '../../../util/streamOperators/extendActions';
import {
  ProcessAction,
  ProcessActionType,
  willPassTokenAction,
  createTokenAction,
  endTokenAction,
  ActivateFlowNodeAction,
  endFlowNodeAction,
  InterruptAction,
  EndTokenAction
} from '../../actions';
import { ProcessState } from '../../reducers';
import { TokenState } from '../../reducers/tokens';
import { ExecutionState } from '../../reducers/log';
import { getCurrentScriptExecutor } from '../../../ScriptExecutionEnvironment';
import uuid from 'uuid';

/**
 * bpmn:ExclusiveGateway epic to handle Activate action, which will create a new instance
 * and pass token to suitable sequence flow
 *
 * @returns {Epic} epic handles Activate actions and passes token to suitable sequence flow
 * */
export function exclusiveGatewayActivateEpic(): Epic<ProcessAction, ProcessAction, ProcessState> {
  return (action$: ActionsObservable<ProcessAction>, state$: StateObservable<ProcessState>) => {
    return action$.ofType<ActivateFlowNodeAction>(ProcessActionType.FlowNodeActivate).pipe(
      extendFlowNodeAction(state$),
      filterFlowNodeTypeMismatch('bpmn:ExclusiveGateway'),
      filterTokenFlowNodeMismatch(),
      switchMap(({ payload, flowNode: thisElement, token: thisToken }) => {
        const { processId, processInstanceId, variables } = state$.value;
        let mappedVariables = Object.assign({}, ...Object.keys(variables).map(k => ({ [k]: variables[k].value })));

        const outgoingFlows = thisElement.outgoing;
        const conditionExpressions = outgoingFlows.filter(o => o.conditionExpression !== undefined);

        const parentTokenId = (() => {
          const tokenParts = payload.tokenId.split('|');
          return tokenParts.length > 1 ? tokenParts.slice(0, -1).join('|') : tokenParts[0];
        })();

        // merge
        if (outgoingFlows.length === 1) {
          if (thisElement.incoming && thisElement.incoming.length > 1) {
            return from([
              // the following prevents a rootToken from being completely removed from the process when encountering the gateway (e.g. when entering a loop)
              ...(parentTokenId !== payload.tokenId
                ? [
                    endFlowNodeAction({
                      flowNodeId: payload.flowNodeId,
                      tokenId: payload.tokenId,
                      state: ExecutionState.COMPLETED,
                      tokenHandling: { removeToken: true },
                      endTime: +new Date()
                    }),
                    createTokenAction({
                      ...thisToken,
                      tokenId: parentTokenId,
                      state: TokenState.READY,
                      currentFlowElement: { id: thisToken.currentFlowElement.id },
                      startTime: thisToken.startTime
                    })
                  ]
                : [
                    endFlowNodeAction({
                      flowNodeId: payload.flowNodeId,
                      tokenId: payload.tokenId,
                      state: ExecutionState.COMPLETED,
                      endTime: +new Date()
                    })
                  ]),
              willPassTokenAction({
                tokenId: parentTokenId,
                sequenceId: outgoingFlows[0].id
              })
            ]);
          }
          // else: single incoming + single outgoing flow -> neither merge nor split of token
        }

        const evaluations: Promise<[boolean, string]>[] = conditionExpressions.map(o => {
          return new Promise(resolve => {
            let result: [boolean, string];

            getCurrentScriptExecutor()
              .execute(
                processId,
                processInstanceId,
                payload.tokenId,
                `return ${o.conditionExpression.body}`,
                mappedVariables
              )
              .then(value => {
                result = [value as boolean, o.id];
                resolve(result);
              });
          });
        });

        return forkJoin(evaluations).pipe(
          catchError((err: Error) => {
            return from([
              endTokenAction({
                tokenId: payload.tokenId,
                state: TokenState.ERROR_TECHNICAL,
                errorMessage: `Technical Error occured: ${err.name} | ${err.message}`,
                endTime: +new Date()
              })
            ]);
          }),
          switchMap(results => {
            const trueEvaluations = results.filter((o: [boolean, string]) => o[0]);

            const tokenId =
              thisElement.incoming && thisElement.incoming.length > 1
                ? `${parentTokenId}|1-1-${uuid.v4()}` // split AND merge
                : `${payload.tokenId}|1-1-${uuid.v4()}`; // split

            if (trueEvaluations.length === 0) {
              const defaultFlow = (thisElement as ExclusiveGateway).default;

              if (defaultFlow) {
                return from([
                  endFlowNodeAction({
                    flowNodeId: payload.flowNodeId,
                    tokenId: payload.tokenId,
                    state: ExecutionState.COMPLETED,
                    tokenHandling: { removeToken: true },
                    endTime: +new Date()
                  }),
                  createTokenAction({
                    ...thisToken,
                    tokenId: tokenId,
                    state: TokenState.READY,
                    currentFlowElement: { id: thisToken.currentFlowElement.id },
                    startTime: thisToken.startTime
                  }),
                  willPassTokenAction({
                    tokenId: tokenId,
                    sequenceId: defaultFlow.id
                  })
                ]);
              }
              // else: multiple outgoing flows but no true evaluation and no default flow -> token can not pass
            }

            return from([
              endFlowNodeAction({
                flowNodeId: payload.flowNodeId,
                tokenId: payload.tokenId,
                state: ExecutionState.COMPLETED,
                tokenHandling: { removeToken: true },
                endTime: +new Date()
              }),
              createTokenAction({
                ...thisToken,
                tokenId: tokenId,
                state: TokenState.READY,
                currentFlowElement: { id: thisToken.currentFlowElement.id },
                startTime: thisToken.startTime
              }),
              willPassTokenAction({
                tokenId: tokenId,
                sequenceId: trueEvaluations[0][1]
              })
            ]);
          }),
          takeUntil(
            merge(
              action$.ofType(ProcessActionType.FlowNodeInterrupt).pipe(
                filter(iax => {
                  const ia = iax as InterruptAction;
                  return ia.payload.flowNodeId === payload.flowNodeId && ia.payload.tokenId === payload.tokenId;
                })
              ),
              action$
                .ofType(ProcessActionType.EndToken)
                .pipe(filter(as => (as as EndTokenAction).payload.tokenId === payload.tokenId))
            )
          )
        );
      })
    );
  };
}

export default () => exclusiveGatewayActivateEpic();
