import { ActionsObservable, combineEpics, Epic, StateObservable } from 'redux-observable';
import { from, of } from 'rxjs';
import { bufferToggle, catchError, filter, merge, switchMap, takeUntil } from 'rxjs/operators';
import { endProcessAction, ProcessAction, ProcessActionType, updateTokenAction, endFlowNodeAction } from '../actions';
import { ProcessState } from '../reducers';
import { ProcessStatus } from '../reducers/status';
import { isTokenEndedState, TokenState } from '../reducers/tokens';
import { isExecutionEnded, ExecutionState } from '../reducers/log';

const rootEpic = (...epics: Epic<ProcessAction, ProcessAction, ProcessState, any>[]) => (
  action$: ActionsObservable<ProcessAction>,
  state$: StateObservable<ProcessState>,
  dependencies?: unknown
) => {
  const epic = combineEpics(...epics);

  const pause$ = action$.ofType(ProcessActionType.PauseProcess);
  const resume$ = action$.ofType(ProcessActionType.ResumeProcess);
  const teardown$ = action$.ofType(ProcessActionType.TearDownProcess);
  const filteredAction$ = action$.pipe(
    filter(a => a.type !== ProcessActionType.PauseProcess && a.type !== ProcessActionType.ResumeProcess),
    filter(() => state$.value.status !== ProcessStatus.PAUSED)
  );

  const pausableAction$ = action$.pipe(
    bufferToggle(pause$, () => resume$),
    merge(filteredAction$),
    switchMap(val => (Array.isArray(val) ? of(...val) : of(val))),
    takeUntil(teardown$)
  );

  const output = epic(pausableAction$ as ActionsObservable<ProcessAction>, state$, dependencies).pipe(
    catchError(err => {
      const tokens = state$.value.tokens;
      const actions = [];
      for (const { currentFlowElement, tokenId, state } of tokens) {
        if (isTokenEndedState(state)) continue;

        if (!isExecutionEnded(tokenId, currentFlowElement.id, currentFlowElement?.startTime, state$.value.log)) {
          actions.push(
            endFlowNodeAction({
              errorMessage: 'Aborted due to runtime error',
              endTime: Date.now(),
              flowNodeId: currentFlowElement.id,
              tokenId: tokenId,
              state: ExecutionState.ERROR_UNKNOWN
            })
          );
        }

        actions.push(updateTokenAction({ tokenId: tokenId, state: TokenState.ERROR_UNKNOWN }));
      }

      return from([
        ...actions,
        endProcessAction({
          tokenId: tokens[tokens.length - 1].tokenId,
          messageRef: `Aborted process due to runtime error: ${err.message}`,
          aborted: true
        })
      ]);
    })
  );

  return from(output);
};

export default rootEpic;
