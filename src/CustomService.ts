const services: { [name: string]: object } = {};

/**
 * Provide a custom service object for service tasks and script tasks.
 * @param name the name of the service (it is recommended to give a fully qualified reverse DNS name here).
 * @param handlers the individual handlers for the service, calls are synchronous
 */
export function provideService(name: string, handlers: object) {
  services[name] = handlers;
}

/**
 * Get an existing service by name
 * @param name the service name
 * @returns the service if exists else undefined
 */
export function getService(name: string): object | undefined {
  return services[name];
}
