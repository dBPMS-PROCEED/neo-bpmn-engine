import { ScriptExecutionError, BpmnEscalation, BpmnError } from './errors';

export interface ScriptExecutor {
  /**
   * Method for executing the given script string.
   * It received processId, processInstanceId and tokenId as
   * the first two params along with the script string and any dependencies.
   */
  execute: (
    processId: string,
    processInstanceId: string,
    tokenId: string,
    scriptString: string,
    dependencies: { [key: string]: unknown }
  ) => Promise<unknown>;
  /**
   * Stop the given processId and processInstanceId.
   * The executor should consider passing  the value, '*' as all.
   */
  stop: (processId: string, processInstanceId: string) => any;
}

/**
 * The default script executor using the new Function() syntax.
 */
export const DEFAULT_SCRIPT_EXECUTOR: ScriptExecutor = {
  execute: async (
    processId: string,
    processInstanceId: string,
    tokenId: string,
    scriptString: string,
    dependencies: { [key: string]: unknown } = {}
  ) => {
    return new Promise((resolve, reject) => {
      const getService =
        dependencies['getService'] &&
        ((serviceName: string) => {
          const service = (dependencies as any).getService(serviceName);
          if (service) {
            return new Proxy(service, {
              get: function(target: any, name: string) {
                if (name in target && typeof target[name] === 'function') {
                  return (...args: any[]) => target[name](processId, processInstanceId, tokenId, ...args);
                }

                return target[name];
              }
            });
          }

          return service;
        });

      const deps = { ...dependencies, getService, processId, processInstanceId, tokenId };

      const asyncFn = new Function(...Object.keys(deps), `return async function() { ${scriptString} };`);

      asyncFn(...Object.values(deps))()
        .then((value: any) => {
          resolve(value);
        })
        .catch((err: Error) => {
          if (err instanceof BpmnEscalation || err instanceof BpmnError) reject(err);
          else reject(new ScriptExecutionError(JSON.stringify(err)));
        });
    });
  },
  stop: () => null
};

let currentExecutor: ScriptExecutor = DEFAULT_SCRIPT_EXECUTOR;

/**
 * Returns the current script executor.
 */
export function getCurrentScriptExecutor(): ScriptExecutor {
  return currentExecutor;
}

/**
 * The script executor to be used from this point. Note that, it calls stop() on
 * the existing script executor.
 * @param scriptExecutor - The executor object with execute and stop methods
 */
export function provideScriptExecutor(scriptExecutor: ScriptExecutor) {
  currentExecutor.stop('*', '*');
  currentExecutor = scriptExecutor;
}
