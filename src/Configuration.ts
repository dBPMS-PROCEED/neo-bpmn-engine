let customTokenEndStates: string[] = [];

/**
 * Allows registration of custom end states for tokens.
 *
 * These states are then considered when the internal logic decides if an instance has ended based on all tokens having ended
 *
 * @param endStates The token states that the program using this library considers as being end states
 */
export function registerCustomTokenEndStates(endStates: string | string[]) {
  endStates = typeof endStates === 'string' ? [endStates] : endStates;
  customTokenEndStates = customTokenEndStates.concat(endStates);
}

/**
 * Allows token states being unregistered from being considered end states so they can be used without an instance ending when there are no other "in progress" states left
 *
 * @param endStates The token states that the program using this library does not consider being end states anymore
 */
export function unregisterCustomTokenEndStates(endStates: string | string[]) {
  endStates = typeof endStates === 'string' ? [endStates] : endStates;
  customTokenEndStates = customTokenEndStates.filter(state => !endStates.includes(state));
}

/**
 * Returns all token states that are considered being end states
 *
 * @returns The token states considered as end states by the program using this library
 */
export function getCustomTokenEndStates() {
  return customTokenEndStates;
}
