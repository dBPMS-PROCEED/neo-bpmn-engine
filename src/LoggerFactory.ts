import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

export enum LogLevel {
  TRACE,
  DEBUG,
  INFO,
  WARN,
  ERROR
}

export interface LogItem {
  timestamp: string; // ISO-8601
  level: LogLevel;
  name: string; // name of the logger
  message: string;
  params: {};
}

type LogArgs =
  | {
      message: string;
      params: { [key: string]: unknown };
    }
  | string;

export interface Logger {
  setLevel(level: LogLevel): void;
  trace(args: LogArgs): void;
  debug(args: LogArgs): void;
  info(args: LogArgs): void;
  warn(args: LogArgs): void;
  error(args: LogArgs): void;
}

class Log implements Logger {
  private localLevel = LogLevel.TRACE;

  constructor(private logger$: Subject<LogItem>, private name: string) {}

  private createLogger(level: LogLevel) {
    return (args: LogArgs) => {
      level >= this.localLevel &&
        this.logger$.next({
          level,
          message: typeof args === 'string' ? args : args.message,
          params: typeof args === 'string' ? {} : args.params,
          name: this.name,
          timestamp: new Date().toISOString()
        });
    };
  }

  /**
   * Set the minimum log level to accept
   * @param level log level to accept
   */
  setLevel(level: LogLevel) {
    this.localLevel = level;
  }

  /**
   * Log at trace level
   */
  trace = this.createLogger(LogLevel.TRACE);

  /**
   * Log at debug level
   */
  debug = this.createLogger(LogLevel.DEBUG);

  /**
   * Log at info level
   */
  info = this.createLogger(LogLevel.INFO);

  /**
   * Log at warn level
   */
  warn = this.createLogger(LogLevel.WARN);

  /**
   * Log at error level
   */
  error = this.createLogger(LogLevel.ERROR);
}

/**
 * @class Singleton factor for creating logger and provides
 *        a log stream which can be observed upon
 */
export default class LoggerFactory {
  private static logger$ = new Subject<LogItem>();
  private static level = LogLevel.TRACE;

  static getLogger(name: string) {
    return new Log(LoggerFactory.logger$, name);
  }

  static setLogLevel(level: LogLevel) {
    LoggerFactory.level = level;
  }

  static getLog$() {
    return LoggerFactory.logger$.pipe(filter(l => l.level >= LoggerFactory.level));
  }
}

/**
 * Set the level for all loggers.
 * @param level log level number trace(0) to error(5)
 */
export const setLogLevel = LoggerFactory.setLogLevel;

/**
 * Get the log stream, filters out logs based on global log level.
 * Emits logs of shape LogItem.
 */
export const getlog$ = LoggerFactory.getLog$;
