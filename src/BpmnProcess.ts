import {
  Definitions,
  FlowNode,
  MessageEventDefinition,
  Process,
  SignalEventDefinition,
  StartEvent,
  TimerEventDefinition
} from 'bpmn-moddle';
import { createStore, Reducer } from 'redux';
import { interval, SchedulerLike, Subject } from 'rxjs';
import { take, takeWhile } from 'rxjs/operators';
import BpmnProcessInstance from './BpmnProcessInstance';
import { provideService } from './CustomService';
import {
  AmbiguousStartEventError,
  BpmnElementNotFoundError,
  BpmnProcessNotDeployedError,
  InvalidBpmnXmlError,
  MessageRefNotFoundError,
  NoStartEventSpecifiedError,
  SignalRefNotFoundError
} from './errors';
import { ExportedProcessState, ProcessState } from './state/reducers';
import BpmnModdle from './util/BpmnModdle';
import { parseIsoDuration } from './util/durationParser';
import { getAllNestedFlowElements, getFlowElements } from './util/getFlowElements';
import uuid = require('uuid');
import { DEFAULT_LOG_STATE, LogState } from './state/reducers/log';
import { DEFAULT_VARIABLES_STATE, VariablesState } from './state/reducers/variables';
import { FlowNodeState } from './state/reducers/tokens';

// ========== Args interfaces ==========
interface StartArgs {
  messageRef?: string;
  signalRef?: string;
  variables?: {
    [key: string]: {
      value: unknown;
      log?: { changedTime: number; changedBy?: string; oldValue?: any }[];
    };
  };
  token?: {
    [key: string]: any;
  };
}

interface StartAtArgs {
  globalStartTime?: number;
  instanceId?: string;
  tokens: {
    tokenId?: string;
    currentFlowElementId: string;
    [key: string]: any;
  }[];
  variables?: {
    [key: string]: {
      value: unknown;
      log?: { changedTime: number; changedBy?: string; oldValue?: any }[];
    };
  };
  log?: LogState;
}

interface ProcessStoreState {
  instances: { [id: string]: BpmnProcessInstance };
}

interface ProcessHooks {
  shouldPassTokenHook?: (
    processId: string,
    processInstanceId: string,
    from: string,
    to: string,
    tokenId: string,
    state: ProcessState
  ) => Promise<boolean>;
  shouldActivateFlowNodeHook?: (
    processId: string,
    processInstanceId: string,
    tokenId: string,
    flowNode: FlowNode,
    state: ExportedProcessState
  ) => Promise<boolean>;
  sendSignalHook?: (processId: string, processInstanceId: string, signalId: string) => any;
  sendMessageHook?: (processId: string, processInstanceId: string, messageId: string) => any;
}

interface MigrationArgs {
  tokenMapping?: {
    add?: { targetFlowElementId: string; [key: string]: any }[];
    move?: { tokenId: string; targetFlowElementId: string; [key: string]: any }[];
    remove?: string[];
  };
  flowElementMapping?: {
    [sourceElementId: string]: string[];
  };
}

const INIT_PROCESS_STATE: ProcessStoreState = { instances: {} };

const STATIC_INITIALIZER = Symbol();

/**
 * @class BPMN process blueprint, which is responsible for instantiating
 *        new process instances
 */
export default class BpmnProcess {
  private static processes: { [id: string]: BpmnProcess } = {};

  private static variablesService = {
    get(processId: string, processInstanceId: string, tokenId: string, name: string): any {
      if (processId in BpmnProcess.processes) {
        const instance = BpmnProcess.processes[processId].getInstanceById(processInstanceId);
        if (instance) {
          return instance.getVariables(tokenId)[name];
        }
      }
    },

    getGlobal(processId: string, processInstanceId: string, name: string): any {
      if (processId in BpmnProcess.processes) {
        const instance = BpmnProcess.processes[processId].getInstanceById(processInstanceId);
        if (instance) {
          return instance.getVariables()[name];
        }
      }
    },

    getAll(processId: string, processInstanceId: string, tokenId: string): any {
      if (processId in BpmnProcess.processes) {
        const instance = BpmnProcess.processes[processId].getInstanceById(processInstanceId);
        if (instance) {
          return instance.getVariables(tokenId);
        }
      }
    },

    getAllGlobal(processId: string, processInstanceId: string): any {
      if (processId in BpmnProcess.processes) {
        const instance = BpmnProcess.processes[processId].getInstanceById(processInstanceId);
        if (instance) {
          return instance.getVariables();
        }
      }
    },

    getWithLogs(processId: string, processInstanceId: string, tokenId: string): any {
      if (processId in BpmnProcess.processes) {
        const instance = BpmnProcess.processes[processId].getInstanceById(processInstanceId);
        if (instance) {
          return instance.getVariablesWithLogs(tokenId);
        }
      }
    },

    getWithLogsGlobal(processId: string, processInstanceId: string): any {
      if (processId in BpmnProcess.processes) {
        const instance = BpmnProcess.processes[processId].getInstanceById(processInstanceId);
        if (instance) {
          return instance.getVariablesWithLogs();
        }
      }
    },

    set(processId: string, processInstanceId: string, tokenId: string, name: string, value: any) {
      if (processId in BpmnProcess.processes) {
        const instance = BpmnProcess.processes[processId].getInstanceById(processInstanceId);
        if (instance) {
          instance.setVariable(tokenId, name, value);
        }
      }
    },

    setGlobal(processId: string, processInstanceId: string, tokenId: string, name: string, value: any) {
      if (processId in BpmnProcess.processes) {
        const instance = BpmnProcess.processes[processId].getInstanceById(processInstanceId);
        if (instance) {
          instance.setVariable(tokenId, name, value, true);
        }
      }
    }
  };

  /**
   * Run code here for global static init
   */
  public static [STATIC_INITIALIZER] = () => {
    provideService('variable', BpmnProcess.variablesService);
  };

  private id: string;
  private moddleDefinitions: Definitions;
  private deployed = false;
  private hooks: ProcessHooks = {};

  // Observable for notifying creation of new instances
  private instance$ = new Subject<BpmnProcessInstance>();

  private constructor(id: string, definitions: Definitions, hooks: ProcessHooks) {
    this.id = id;
    this.moddleDefinitions = definitions;
    this.hooks = hooks;
  }

  /**
   * Factory method to construct a BPMN Process Object from a BPMN XML
   * @param {string} id identifier of the process, will be prefixed to process instance id
   * @param {string} bpmnXml BPMN XML representation
   * @returns {Promise<Process>} a promise which eventually gives the initialized object of BpmnProcess
   * @throws {InvalidBpmnXmlError} Given XML should be a valid process
   */
  static async fromXml(id: string, bpmnXml: string, hooks: ProcessHooks = {}) {
    return new Promise<BpmnProcess>((resolve, reject) => {
      const moddle = new BpmnModdle();
      moddle.fromXML(bpmnXml, (err, definitions) => {
        if (err) {
          reject(new InvalidBpmnXmlError(err.toString()));
        } else {
          BpmnProcess.processes[id] = new BpmnProcess(id, definitions, hooks);
          resolve(BpmnProcess.processes[id]);
        }
      });
    });
  }

  /**
   * Get the id of the process
   * @return {string} id
   */
  getId() {
    return this.id;
  }

  /**
   * Get the stream of instances to subscribe to
   */
  getInstance$() {
    return this.instance$.asObservable();
  }

  /**
   * Get all the instances created so far
   * @returns {BpmnProcessInstance[]} array of instances created so far
   */
  getAllInstances(): BpmnProcessInstance[] {
    return Object.values(this.store.getState().instances);
  }

  /**
   * Get an instance by its id
   * @param {string} id id of the required instance
   */
  getInstanceById(id: string) {
    return this.store.getState().instances[id];
  }

  /**
   * Delete an instance by its id
   * @param {string} id id of the instance to delete
   */
  deleteInstanceById(id: string) {
    const instance = this.getInstanceById(id);

    if (instance) {
      instance.tearDown();
      this.store.dispatch({ type: 'DELETE_INSTANCE', payload: { id } });
    }
  }

  /**
   * Marks the BPMN process ready to start instances. Auto-starts
   * the timer start event, listens for signals and messages in case
   * such start events are defined. If auto-starts are not defined,
   * a subsequent call to start() with optional message/signal is required
   * to create a new process instance.
   * TODO: Consider renaming this to start()
   */
  deploy() {
    this.deployed = true;

    // Schedule timers if there are timer start events
    const timerStartEvents = (this.getFlowElements().filter(s => s.$type === 'bpmn:StartEvent') as StartEvent[]).filter(
      s =>
        s.eventDefinitions &&
        s.eventDefinitions.length > 0 &&
        s.eventDefinitions[0].$type === 'bpmn:TimerEventDefinition'
    );

    if (timerStartEvents.length === 1) {
      if ((timerStartEvents[0].eventDefinitions[0] as TimerEventDefinition).timeDuration) {
        const [repeatTimes, intervalMillis] = parseIsoDuration(
          (timerStartEvents[0].eventDefinitions[0] as TimerEventDefinition).timeDuration.body
        );
        interval(intervalMillis, this.__deploy_scheduler__ || undefined)
          .pipe(
            take(repeatTimes),
            takeWhile(() => this.deployed)
          )
          .subscribe(() => {
            this.startAt({ tokens: [{ currentFlowElementId: timerStartEvents[0].id }] });
          });
      } else {
        // TODO: Handle non-duration timer event definitions
      }
    } else if (timerStartEvents.length > 1) {
      // TODO: Throw error on multiple timer start events
    }
  }

  /**
   * Stop upcoming scheduled deployments.
   */
  undeploy() {
    this.deployed = false;
  }

  /**
   * @returns {boolean} true if process is ready to start instances else false.
   */
  isDeployed() {
    return this.deployed;
  }

  /**
   * Start a new process instance with messageRef and initial variables.
   * @param {StartArgs} startArgs arguments for starting the process
   * @param {string} startArgs.messageRef optional string reference id of the message for the start event
   * @param {string} startArgs.signalRef optional string reference id of the signal for the start event
   * @param {object} startArgs.variables optional initial process variables
   * @param {object} startArgs.token optional initial attributes for token
   * @throws {BpmnProcessNotDeployedError} Instance can only be started when in deployed state
   * @throws {NoStartEventSpecifiedError} Start event should exist in bpmn
   * @throws {MessageRefNotFoundError} Given messageRef must exist in definition
   * @throws {SignalRefNotFoundError} Given signalRef must exist in definition
   */
  start(startArgs: StartArgs = {}) {
    const { messageRef = null, signalRef = null, variables = {}, token = {} } = startArgs;

    if (!this.deployed) {
      throw new BpmnProcessNotDeployedError();
    }

    const startEvents = this.getFlowElements().filter(
      flowElement => flowElement.$type === 'bpmn:StartEvent'
    ) as StartEvent[];

    const startActivities = this.getFlowElements().filter(flowElement => {
      if (
        flowElement.$type !== 'bpmn:Task' &&
        flowElement.$type !== 'bpmn:ScriptTask' &&
        flowElement.$type !== 'bpmn:UserTask' &&
        flowElement.$type !== 'bpmn:CallActivity' &&
        flowElement.$type !== 'bpmn:SubProcess'
      ) {
        return false;
      }

      const flowNode = flowElement as FlowNode;
      return !flowNode.incoming;
    }) as FlowNode[];

    let tokens = [];

    if (messageRef) {
      const messageStartEvents = startEvents.filter(
        s =>
          s.eventDefinitions &&
          s.eventDefinitions.length > 0 &&
          s.eventDefinitions[0].$type === 'bpmn:MessageEventDefinition' &&
          (s.eventDefinitions[0] as MessageEventDefinition).messageRef.id === messageRef
      );

      if (messageStartEvents.length !== 1) {
        throw new MessageRefNotFoundError(messageRef);
      }

      tokens.push({ ...token, currentFlowElementId: messageStartEvents[0].id });
    } else if (signalRef) {
      const signalStartEvents = startEvents.filter(
        s =>
          s.eventDefinitions &&
          s.eventDefinitions.length > 0 &&
          s.eventDefinitions[0].$type === 'bpmn:SignalEventDefinition' &&
          (s.eventDefinitions[0] as SignalEventDefinition).signalRef.id === signalRef
      );

      if (signalStartEvents.length !== 1) {
        throw new SignalRefNotFoundError(signalRef);
      }

      tokens.push({ ...token, currentFlowElementId: signalStartEvents[0].id });
    } else {
      // Look for none events
      const noneStartEvents = startEvents.filter(s => !s.eventDefinitions);
      const tokenStartNodes = [...noneStartEvents, ...startActivities];

      if (tokenStartNodes.length === 0) {
        throw new NoStartEventSpecifiedError();
      }

      tokens = tokenStartNodes.map(flowNode => ({ ...token, currentFlowElementId: flowNode.id }));
    }
    return this.startAt({ variables, tokens });
  }

  /**
   * Non-standard start at an arbitrary element with custom variables. Also use
   * the same method for multiple None start events.
   * @param {StartAtArgs} startAtArgs
   * @param {object} startAtArgs.tokens startElement and optional attributes of token
   * @param {object} startAtArgs.variables optional initial process variables
   * @param {string} startAtArgs.instanceId optional id of instance
   * @param {number} startAtArgs.globalStartTime optional global start time of instance
   * @param {object} startAtArgs.log optional log of already executed flownode elements of instance
   * @throws {BpmnElementNotFoundError} provided element id must exist
   */
  startAt(startAtArgs: StartAtArgs) {
    const { globalStartTime, instanceId, variables, tokens, log } = startAtArgs;

    tokens.forEach(token => {
      const element = getAllNestedFlowElements(this.moddleDefinitions).filter(s => s.id === token.currentFlowElementId);
      if (element.length === 0) {
        throw new BpmnElementNotFoundError(token.currentFlowElementId);
      }
    });

    const processInstance = BpmnProcessInstance.fromModdleDefinitions({
      processId: this.id,
      id: instanceId || `${this.id}-${uuid.v4()}`,
      globalStartTime: globalStartTime || +new Date(),
      definitions: this.moddleDefinitions,
      variables: variables || DEFAULT_VARIABLES_STATE,
      log: log || DEFAULT_LOG_STATE,
      tokens: tokens.map(token => {
        const { currentFlowElementId, currentFlowNodeState, currentFlowElementStartTime, tokenId, ...optional } = token;
        return {
          ...optional,
          currentFlowElement: {
            id: currentFlowElementId,
            state: currentFlowNodeState || FlowNodeState.READY,
            startTime: currentFlowElementStartTime || +new Date()
          },
          tokenId: token.tokenId || uuid.v4()
        };
      }),
      shouldPassTokenHook: async (processInstanceId, from, to, tokenId, state) =>
        this.hooks.shouldPassTokenHook
          ? await this.hooks.shouldPassTokenHook(this.id, processInstanceId, from, to, tokenId, state)
          : Promise.resolve(true),
      shouldActivateFlowNodeHook: async (processInsanceId, tokenId, flowNode, state) =>
        this.hooks.shouldActivateFlowNodeHook
          ? await this.hooks.shouldActivateFlowNodeHook(this.id, processInsanceId, tokenId, flowNode, state)
          : Promise.resolve(true),
      sendSignalHook: (processInstanceId, signalId) =>
        this.hooks.sendSignalHook ? this.hooks.sendSignalHook(this.id, processInstanceId, signalId) : null,
      sendMessageHook: (processInstanceId, messageId) =>
        this.hooks.sendMessageHook ? this.hooks.sendMessageHook(this.id, processInstanceId, messageId) : null
    });

    this.store.dispatch({ type: 'START_INSTANCE', payload: processInstance });
    this.instance$.next(processInstance);
    return processInstance;
  }

  /**
   * Convenience method for calling deploy and start together
   */
  deployAndStart(args?: StartArgs) {
    this.deploy();
    return this.start(args);
  }

  /**
   * Convenience method for calling deploy and startAt together
   */
  deployAndStartAt(args: StartAtArgs) {
    this.deploy();
    return this.startAt(args);
  }

  /**
   * Get flow elements from current moddle definition
   */
  private getFlowElements() {
    return getFlowElements(this.moddleDefinitions);
  }

  public static migrate(
    currentProcessId: string,
    targetProcessId: string,
    instanceIds: string[],
    { tokenMapping, flowElementMapping }: MigrationArgs
  ) {
    if (tokenMapping && flowElementMapping) {
      throw new Error('Use either a tokenMapping or flowElementMapping but not both at the same time');
    }

    const currentProcess = this.processes[currentProcessId];
    if (!currentProcess) {
      throw new Error(`Migration failed! Process with id ${currentProcessId} does not exist.`);
    }
    const targetProcess = this.processes[targetProcessId];
    if (!targetProcess) {
      throw new Error(`Migration failed! Process with id ${targetProcessId} does not exist.`);
    }

    if (!Array.isArray(instanceIds) || !instanceIds.length) {
      throw new Error(`Expected an array of instanceIds to migrate but got ${instanceIds}!`);
    }

    // make sure that every one of the instances exists
    for (const instanceId of instanceIds) {
      if (!currentProcess.getInstanceById(instanceId)) {
        throw new Error(
          `Migration failed! Instance with id ${instanceId} does not exist for process with id ${currentProcessId}.`
        );
      }
    }

    if (instanceIds.length > 1 && tokenMapping) {
      throw new Error('Migrating with a tokenMapping is only allowed when migrating a single instance!');
    }

    for (const instanceId of instanceIds) {
      const instance = currentProcess.getInstanceById(instanceId);

      let finalTokenMapping = tokenMapping || {};

      const targetDefinitions = targetProcess.moddleDefinitions;
      const targetFlowElements = getAllNestedFlowElements(targetDefinitions);

      if (flowElementMapping) {
        // create a tokenMapping from the flowElementMapping
        Object.entries(flowElementMapping).forEach(([flowelementId, targetElements]) => {
          if (targetElements.length < 1) return;

          // check if there is a token to map
          const token = instance.getState().tokens.find(t => t.currentFlowElementId === flowelementId);

          if (!token) return;

          targetElements.forEach((targetElementId, index) => {
            if (index === 0) {
              // TODO: map if the flow element has changed its type
              if (flowelementId === targetElementId) return;

              if (!finalTokenMapping.move) finalTokenMapping.move = [];

              // move the token to the first element in the array if it is not the original element
              finalTokenMapping.move!.push({ tokenId: token.tokenId, targetFlowElementId: targetElementId });
            } else {
              if (!finalTokenMapping.add) finalTokenMapping.add = [];

              // add token for all the following elements
              finalTokenMapping.add!.push({ targetFlowElementId: targetElementId });
            }
          });
        });
      }

      // check if there are tokens that would be in an inconsistent state in the new definitions (i.e. if their currentFlowElement doesn't exist anymore)
      instance.getState().tokens.forEach(token => {
        if (!targetFlowElements.some(flowElement => flowElement.id === token.currentFlowElementId)) {
          // if the token is handled by a move we don't need to remove it
          if (finalTokenMapping.move?.some(movedToken => token.tokenId === movedToken.tokenId)) return;
          // if the token is already being removed don't do it another time
          if (finalTokenMapping.remove?.includes(token.tokenId)) return;

          if (!finalTokenMapping.remove) finalTokenMapping.remove = [];

          finalTokenMapping.remove.push(token.tokenId);
        }
      });

      // check if there tokens on activities that have changed their type
      instance.getState().tokens.forEach(token => {
        // early exit if the token is being removed anyway
        if (finalTokenMapping.remove?.includes(token.tokenId)) return;

        // early exit if the token is being moved anyway
        if (finalTokenMapping.move?.some(tokenMove => tokenMove.tokenId === token.tokenId)) return;

        // check if the type of the activity the token resides on has changed and ensure that the element is restarted if it did
        const sourceFlowElement = instance.getFlowElement(token.currentFlowElementId);
        const targetFlowElement = targetFlowElements.find(element => element.id === token.currentFlowElementId);
        if (sourceFlowElement?.$type !== targetFlowElement?.$type) {
          // map the token to the same flow node to force a reactivation
          if (!finalTokenMapping.move) finalTokenMapping.move = [];

          finalTokenMapping.move.push({
            tokenId: token.tokenId,
            targetFlowElementId: targetFlowElement!.id
          });
        }
      });

      instance.migrate(targetProcessId, {
        moddleDefinitions: targetDefinitions,
        tokenMapping: finalTokenMapping
      });

      currentProcess.store.dispatch({ type: 'DELETE_INSTANCE', payload: { id: instance.getId() } });
      targetProcess.store.dispatch({ type: 'ADD_INSTANCE', payload: instance });
    }
  }

  // ======== Internal state store ========
  /**
   * Reducer for internal redux store
   */
  private storeReducer: Reducer<ProcessStoreState> = (state = INIT_PROCESS_STATE, action) => {
    if (action.type === 'START_INSTANCE' || action.type === 'ADD_INSTANCE') {
      if (action.payload instanceof BpmnProcessInstance) {
        const pi = action.payload as BpmnProcessInstance;
        return {
          ...state,
          instances: {
            ...state.instances,
            [pi.getId()]: action.payload
          }
        };
      }
    }

    if (action.type === 'DELETE_INSTANCE') {
      if (action.payload.id) {
        const { [action.payload.id]: deletedInstance, ...remainingInstances } = state.instances;

        return {
          ...state,
          instances: remainingInstances
        };
      }
    }

    return state;
  };

  /**
   * Keep track of instances
   */
  private store = createStore(this.storeReducer);

  /**
   * Internal test, used for testing with virtual scheduler
   */
  public __deploy_scheduler__: SchedulerLike | undefined;

  public __set_deploy_scheduler(scheduler: SchedulerLike) {
    this.__deploy_scheduler__ = scheduler;
  }

  public __restore_deploy_scheduler() {
    this.__deploy_scheduler__ = undefined;
  }
}

/**
 * GLOBAL SETUP
 */
BpmnProcess[STATIC_INITIALIZER]();
