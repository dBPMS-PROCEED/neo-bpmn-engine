import {
  Definitions,
  SequenceFlow,
  CallActivity,
  EndEvent,
  UserTask,
  FlowNode,
  FlowElement,
  Process
} from 'bpmn-moddle';
import { concatMap, filter, takeUntil } from 'rxjs/operators';
import { InvalidTokenPlacementError } from './errors';
import LoggerFactory from './LoggerFactory';
import {
  pauseProcessAction,
  receiveMessageAction,
  receiveSignalAction,
  resumeProcessAction,
  setVariablesAction,
  propagateErrorAction,
  updateTokenAction,
  stopProcessAction,
  logExecutionAction,
  createTokenAction,
  updateVariablesAction,
  updatelogAction,
  removeTokenAction,
  endTokenAction,
  pauseTokenAction,
  interruptAction,
  didPassTokenAction,
  willPassTokenAction,
  updateProcessStatusAction,
  setFlowNodeStateAction,
  endActivityAction,
  endFlowNodeAction,
  tearDownProcessAction,
  updateModdleDefinitionsAction,
  updateProcessIdAction,
  logAdaptationAction,
  updateAdaptationLogAction,
  setIntermediateVariableValueAction,
  startMigrationAction,
  endMigrationAction,
  continueTokenAction,
  moveTokenAction
} from './state/actions';
import { ProcessStateDelegate } from './state/ProcessStateDelegate';
import { DEFAULT_PROCESS_STATE, ProcessState, exportProcessState, ExportedProcessState } from './state/reducers';
import { findElementById, getFlowElements, activityTypes } from './util/getFlowElements';
import { isTokenEndedState, TokenState, Token } from './state/reducers/tokens';
import { isInstanceEndedStatus, ProcessStatus } from './state/reducers/status';
import { ExecutionState, LogState, isExecutionEnded } from './state/reducers/log';
import { VariablesState } from './state/reducers/variables';
import { from } from 'rxjs';
import uuid from 'uuid';
import { AdaptationLogState, AdaptationType } from './state/reducers/adaptationLog';

interface FromModdleDefinitionsArgs {
  processId: string;
  id: string;
  globalStartTime: number;
  definitions: Definitions;
  variables: {
    [key: string]: {
      value: unknown;
      log?: { changedTime: number; changedBy?: string; oldValue?: any }[];
    };
  };
  log: LogState;
  tokens: {
    tokenId: string;
    currentFlowElement: {
      id: string;
    };
    [key: string]: any;
  }[];
  shouldPassTokenHook?: (
    processInstanceId: string,
    from: string,
    to: string,
    tokenId: string,
    state: ProcessState
  ) => Promise<boolean>;
  shouldActivateFlowNodeHook?: (
    processInstanceId: string,
    tokenId: string,
    flowNode: FlowNode,
    state: ExportedProcessState
  ) => Promise<boolean>;
  sendSignalHook?: (processInstanceId: string, signalId: string) => any;
  sendMessageHook?: (processInstanceId: string, messageId: string) => any;
}

interface MigrationArgs {
  moddleDefinitions: Definitions;
  tokenMapping?: {
    add?: { targetFlowElementId: string; [key: string]: any }[];
    move?: { tokenId: string; targetFlowElementId: string; [key: string]: any }[];
    remove?: string[];
  };
}

/**
 * @class Holds the state for a BPMN process instance
 */
export default class BpmnProcessInstance {
  private id: string;
  private stateDelegate: ProcessStateDelegate;
  private log = LoggerFactory.getLogger('processInstance-logging');

  private constructor(
    moddleDefinitions: Definitions,
    globalStartTime: number,
    processId: string,
    id: string,
    variables: {
      [key: string]: {
        value: unknown;
        log?: { changedTime: number; changedBy?: string; oldValue?: any }[];
      };
    },
    log: LogState,
    tokens: {
      tokenId: string;
      currentFlowElement: {
        id: string;
      };
      [key: string]: any;
    }[],
    shouldPassTokenHook?: (
      processInstanceId: string,
      from: string,
      to: string,
      tokenId: string,
      state: ProcessState
    ) => Promise<boolean>,
    shouldActivateFlowNodeHook?: (
      processInstanceId: string,
      tokenId: string,
      flowNode: FlowNode,
      state: ExportedProcessState
    ) => Promise<boolean>,
    sendSignalHook?: (processInstanceId: string, signalId: string) => any,
    sendMessageHook?: (processInstanceId: string, messageId: string) => any
  ) {
    this.id = id;

    const initialTokens = tokens.map(token => ({
      ...token,
      tokenId: token.tokenId,
      state: token.state || TokenState.READY,
      currentFlowElement: token.currentFlowElement,
      localStartTime: token.localStartTime || +new Date(),
      localExecutionTime: token.localExecutionTime || 0,
      intermediateVariablesState: token.intermediateVariablesState || {}
    }));

    this.stateDelegate = ProcessStateDelegate.fromState({
      moddleDefinitions,
      state: {
        ...DEFAULT_PROCESS_STATE,
        processId,
        processInstanceId: this.id,
        creationTime: globalStartTime,
        status: DEFAULT_PROCESS_STATE.status,
        variables: Object.keys(variables).reduce((acc, key) => {
          return {
            ...acc,
            [key]: { value: variables[key].value, log: variables[key].log || [{ changedTime: +new Date() }] }
          };
        }, {}),
        log: log,
        tokens: initialTokens
      },
      shouldPassTokenHook: async (from: string, to: string, tokenId: string) =>
        shouldPassTokenHook
          ? await shouldPassTokenHook(this.id, from, to, tokenId, this.getInternalState())
          : Promise.resolve(true),
      shouldActivateFlowNodeHook: async (tokenId: string, flowNode: FlowNode) =>
        shouldActivateFlowNodeHook
          ? await shouldActivateFlowNodeHook(this.id, tokenId, flowNode, exportProcessState(this.getInternalState()))
          : Promise.resolve(true),
      sendSignalHook: (signalId: string) => (sendSignalHook ? sendSignalHook(this.id, signalId) : null),
      sendMessageHook: (messageId: string) => (sendMessageHook ? sendMessageHook(this.id, messageId) : null)
    });

    setTimeout(() => {
      this.log.debug({ message: `processInstance ${this.id} started.`, params: { processInstanceId: this.id } });

      initialTokens.forEach(token => {
        if (token.state === 'READY') {
          const startingElement = this.getFlowElement(token.currentFlowElement.id);
          if (startingElement && startingElement.$type === 'bpmn:SequenceFlow') {
            this.stateDelegate.dispatch(
              willPassTokenAction({ sequenceId: startingElement.id, tokenId: token.tokenId })
            );
          } else {
            this.stateDelegate.dispatch(
              didPassTokenAction({ to: token.currentFlowElement.id, tokenId: token.tokenId })
            );
          }
        } else if (!isTokenEndedState(token.state)) {
          // (re)trigger logic that should be running when the token is active
          this.stateDelegate.dispatch(continueTokenAction({ tokenId: token.tokenId }));
        }
      });
    });
  }

  /**
   * Create a process instance
   * @param {FromModdleDefinitionsArgs} args
   * @param {Definitions} args.definitions Definitions obtained out of Moddle BPMN XML parser
   * @param {string} args.globalStartTime global startTime of instance
   * @param {string} args.processId id of process for this instance
   * @param {string} args.id optional id for instance, defaults to auto-generated uuid.v4()
   * @param {object} args.variables optional initial variables for process instance
   * @param {LogState} args.log optional initial log for process instance
   * @param {Array} args.tokens initial tokens of the instance
   * @param {Function} args.shouldPassTokenHook hook to be called when token is passed to next flowNode
   * @param {Function} args.shouldActivateFlowNodeHook hook to be called when a new flowNode is activated
   * @param {Function} args.sendSignalHook hook to be called when signal is send
   * @param {Function} args.sendMessageHook hook to be called when message is send
   * @returns {BpmnProcessInstance} a new process instance created from the moddle definitions
   */
  static fromModdleDefinitions(args: FromModdleDefinitionsArgs) {
    return new BpmnProcessInstance(
      args.definitions,
      args.globalStartTime,
      args.processId,
      args.id,
      args.variables,
      args.log,
      args.tokens,
      args.shouldPassTokenHook,
      args.shouldActivateFlowNodeHook,
      args.sendSignalHook,
      args.sendMessageHook
    );
  }

  /****************************** Instance State ******************************/

  /**
   * Get the id of the process instance
   * @returns {string} id of the process instance
   */
  getId() {
    return this.id;
  }

  /**
   * Get the state stream
   */
  getState$() {
    return this.stateDelegate.getState$();
  }

  /**
   * Will return the current internal state of the instance
   */
  private getInternalState() {
    return this.stateDelegate.getState();
  }

  /**
   * Get a copy of the current state
   * @returns {BpmnProcessInstanceState} state snapshot of the current state
   */
  getState() {
    const state = this.getInternalState();
    return exportProcessState(state);
  }

  /**
   * Pause the process instance
   */
  pause() {
    if (!this.isPaused()) {
      this.log.debug({ message: `processInstance ${this.id} paused.`, params: { processInstanceId: this.id } });
      this.stateDelegate.dispatch(pauseProcessAction());
    }
  }

  /**
   * Resume the process instance
   */
  resume() {
    if (this.isPaused()) {
      this.log.debug({ message: `processInstance ${this.id} resumed.`, params: { processInstanceId: this.id } });
      this.stateDelegate.dispatch(resumeProcessAction());
    }
  }

  /**
   * Stop the process instance
   * @param reason log the reason for abrupt stop
   */
  stop(reason?: string) {
    if (!this.isEnded()) {
      this.log.debug({
        message: `processInstance ${this.id} stopped. ${reason}`,
        params: { processInstanceId: this.id }
      });

      this.stateDelegate.dispatch(stopProcessAction());
    }
  }

  /**
   * Clear up any data or functionality (like rxjs subscriptions)
   */
  tearDown() {
    this.stateDelegate.dispatch(tearDownProcessAction());
  }

  /**
   * update status of process
   * @param status - new status of process
   */
  updateProcessStatus(status: string) {
    this.stateDelegate.dispatch(
      updateProcessStatusAction({
        status
      })
    );
  }

  /**
   * Is the process currently paused?
   */
  isPaused() {
    return this.getInternalState().status === ProcessStatus.PAUSED;
  }

  /**
   * Has the process instance ended?
   */
  isEnded() {
    return isInstanceEndedStatus(this.getInternalState().status);
  }

  /**
   * Notify on process pause
   * @param callback
   */
  onPaused(callback: Function) {
    let alreadyPaused = false;
    this.getState$()
      .pipe(takeUntil(this.getState$().pipe(filter(state => state.status === ProcessStatus.REMOVED))))
      .subscribe(state => {
        if (state.status === ProcessStatus.PAUSED && !alreadyPaused) {
          callback();
        }
        alreadyPaused = state.status === ProcessStatus.PAUSED;
      });
  }

  /**
   * Notify on process end
   * @param callback
   */
  onEnded(callback: Function) {
    let oldStatus = this.getInternalState().status;
    this.getState$()
      .pipe(takeUntil(this.getState$().pipe(filter(state => state.status === ProcessStatus.REMOVED))))
      .subscribe(state => {
        if (oldStatus !== ProcessStatus.ENDED && state.status === ProcessStatus.ENDED) {
          callback();
        }
        oldStatus = state.status;
      });
  }

  /**
   * Notify on process abortion
   * @param callback
   */
  onAborted(callback: Function) {
    this.getState$()
      .pipe(takeUntil(this.getState$().pipe(filter(state => state.status === ProcessStatus.REMOVED))))
      .subscribe(state => {
        const aborted = state.tokens
          .filter(t => t.state === TokenState.ENDED)
          .some(t => {
            const element = this.getFlowElement(t.currentFlowElement.id) as EndEvent;
            if (element && element.eventDefinitions) {
              const terminateEventDefinition = element.eventDefinitions.find(
                e => e.$type === 'bpmn:TerminateEventDefinition'
              );
              return !!terminateEventDefinition;
            }
          });
        if (aborted && state.status === ProcessStatus.ENDED) {
          callback();
        }
      });
  }

  /**
   * given callback called with current instanceState when changed
   * @param callback
   */
  onInstanceStateChange(callback: Function) {
    let instanceState: string[] = this.getState().instanceState;
    this.getState$()
      .pipe(takeUntil(this.getState$().pipe(filter(state => state.status === ProcessStatus.REMOVED))))
      .subscribe(state => {
        const newInstanceState =
          state.status === ProcessStatus.PAUSED || state.status === ProcessStatus.STOPPED
            ? [state.status]
            : Array.from(new Set(state.tokens.map(token => token.state)));

        const stateUpdated =
          instanceState.length !== newInstanceState.length || !instanceState.every(s => newInstanceState.includes(s));

        if (stateUpdated) {
          instanceState = newInstanceState;
          callback(newInstanceState);
        }
      });
  }

  /**
   * Receive a message on behalf of the process. The send message is invoked via hooks
   * @param messageId - the id of the message
   */
  receiveMessage(messageId: string) {
    this.stateDelegate.dispatch(receiveMessageAction({ id: messageId }));
  }

  /**
   * Receive a signal on behalf of the process. The send signal is invoked via hooks
   * @param signalid - the id of the signal
   */
  receiveSignal(signalId: string) {
    this.stateDelegate.dispatch(receiveSignalAction({ id: signalId }));
  }

  migrate(newProcessId: string, { moddleDefinitions, tokenMapping }: MigrationArgs) {
    // this is to signal that some functionality that is needed during migration should be toggled on(see: state/epics/dispatcher.ts removeTokenEpic)
    this.stateDelegate.dispatch(startMigrationAction());

    // change the process referenced in the state
    const oldProcessId = this.getInternalState().processId;
    this.stateDelegate.dispatch(updateProcessIdAction(newProcessId));

    // add information about the migration the adaptation log
    this.stateDelegate.dispatch(
      logAdaptationAction({
        type: AdaptationType.MIGRATION,
        time: +new Date(),
        from: oldProcessId,
        to: newProcessId
      })
    );

    // remove/move tokens before the underlying process model is changed
    // this is needed to allow the internal logic to handle the removal of tokens from elements while the old process definitions are still present
    if (tokenMapping) {
      if (tokenMapping.remove) {
        tokenMapping.remove.forEach(tokenId => {
          this.internalRemoveToken(tokenId, false);
        });
      }

      if (tokenMapping.move) {
        tokenMapping.move.forEach((token: any) => {
          const target = token.targetFlowElementId;
          delete token.targetFlowElementId;
          // we pass the target definitions to allow the internal token move logic to move a token to an element that might not yet exist in the internally used definitions
          this.internalMoveToken(target, token, moddleDefinitions);
        });
      }
    }

    // change to the new moddleDefinitions
    this.stateDelegate.dispatch(updateModdleDefinitionsAction(moddleDefinitions));

    // add the user defined tokens
    // we do theis after changing the model since the targets might be new elements that don't exist in the old model
    if (tokenMapping?.add) {
      tokenMapping.add.forEach((token: any) => {
        const target = token.targetFlowElementId;
        delete token.targetFlowElementId;

        this.addToken(target, token);
      });
    }

    // signal the internal systems to toggle off the migration specific logic
    this.stateDelegate.dispatch(endMigrationAction());
  }

  /****************************** Token State ******************************/

  /**
   * update token with the given id
   * @param tokenId - the id of the token
   * @param {attributes} - attributes to be updated
   */
  updateToken(tokenId: string, attributes: { [key: string]: any }) {
    const token = this.getInternalState().tokens.find(token => token.tokenId === tokenId);
    if (token) {
      this.stateDelegate.dispatch(
        updateTokenAction({
          tokenId,
          ...attributes
        })
      );
    }
  }

  /**
   * Pause the execution of flowNode at given token
   * @param tokenId
   */
  pauseToken(tokenId: string) {
    const token = this.getInternalState().tokens.find(t => t.tokenId === tokenId);

    if (token) {
      this.stateDelegate.dispatch(pauseTokenAction({ tokenId: token.tokenId }));
    }
  }

  /**
   * end token with the given id:
   * current execution will be ended and logged with given state and token state will be set to given state
   * @param tokenId - the id of the token
   * @param {attributes} - attributes to be updated
   */
  endToken(tokenId: string, attributes: { state: string; [key: string]: any }) {
    const token = this.getInternalState().tokens.find(token => token.tokenId === tokenId);
    if (token) {
      this.stateDelegate.dispatch(
        endTokenAction({
          tokenId,
          ...attributes
        })
      );
    }
  }

  /**
   * Interrupt the execution at given token: state of token remains
   *
   * @param tokenId
   */
  interruptToken(tokenId: string) {
    const token = this.getInternalState().tokens.find(t => t.tokenId === tokenId);

    if (token) {
      this.stateDelegate.dispatch(interruptAction({ flowNodeId: token.currentFlowElement.id, tokenId: token.tokenId }));
    }
  }

  /**
   * A function that will ensure that a possibly executing flowNode is stopped when the activating token is changed (removed/moved somewhere else)
   *
   * @param token The token that will be changed
   * @param logMessage The message to print on flowNodeChange
   */
  private stopFlowNodeExecutionOnTokenChange(token: Token, logMessage: (flowElement: FlowElement) => string) {
    const flowElement = this.getFlowElement(token.currentFlowElement.id);
    const log = this.getExecutionLog();

    if (
      flowElement &&
      flowElement.$type !== 'bpmn:SequenceFlow' &&
      !isExecutionEnded(token.tokenId, flowElement.id, token.currentFlowElement.startTime, log)
    ) {
      // abort the current flowNode if it is being executed
      if (logMessage) {
        this.log.info({
          message: logMessage(flowElement),
          params: { processInstanceId: this.id, tokenId: token.tokenId }
        });
      }

      this.stateDelegate.dispatch(
        endFlowNodeAction({
          tokenId: token.tokenId,
          flowNodeId: flowElement.id,
          state: ExecutionState.SKIPPED,
          endTime: +new Date()
        })
      );
    }
  }

  /**
   * Remove the token with the given id from the instance
   *
   * @param tokenId - the id of the token
   * @param silent if true the removal will not be logged as an adaptation
   */
  removeToken(tokenId: string, silent?: boolean) {
    this.internalRemoveToken(tokenId, silent);
  }

  /**
   * Remove the token with the given id from the instance
   *
   * @param tokenId - the id of the token
   * @param silent if true the removal will not be logged as an adaptation
   */
  private internalRemoveToken(tokenId: string, silent?: boolean) {
    const token = this.getInternalState().tokens.find(token => token.tokenId === tokenId);
    if (token) {
      this.stopFlowNodeExecutionOnTokenChange(
        token,
        flowElement =>
          `Ending execution of flowNode ${flowElement.name || flowElement.id} since the activating token ${
            token.tokenId
          } is being removed`
      );

      if (!silent) {
        this.stateDelegate.dispatch(
          logAdaptationAction({
            type: AdaptationType.TOKEN_REMOVE,
            time: +new Date(),
            tokenId: token.tokenId,
            currentFlowElementId: token.currentFlowElement.id
          })
        );
      }

      this.stateDelegate.dispatch(
        removeTokenAction({
          tokenId: token.tokenId
        })
      );
    }
  }

  /**
   * Add a new token to the instance
   *
   * @param elementId the element the token should be placed at
   * @param token optional information about the token to add
   * @param silent if true the addition will not be logged as an adaptation
   * @returns the id of the added token
   */
  addToken(elementId: string, token: { [key: string]: any } = {}, silent?: boolean) {
    const flowElement = this.getFlowElement(elementId);

    if (!flowElement) {
      throw new InvalidTokenPlacementError(`Flow element ${elementId} not found`);
    } else if (flowElement.$type === 'bpmn:ParallelGateway') {
      throw new InvalidTokenPlacementError(
        `Tokens cannot be placed directly on parallel gateways. Merging requirement is a token arriving on every sequence flow.`
      );
    }

    if (!token.tokenId) {
      token.tokenId = uuid.v4();
    }

    this.stateDelegate.dispatch(
      createTokenAction({
        ...token,
        tokenId: token.tokenId,
        state: TokenState.READY,
        localStartTime: +new Date(),
        currentFlowElement: { id: elementId },
        localExecutionTime: 0
      })
    );

    if (!silent) {
      this.stateDelegate.dispatch(
        logAdaptationAction({
          type: AdaptationType.TOKEN_ADD,
          time: +new Date(),
          tokenId: token.tokenId,
          targetFlowElementId: elementId
        })
      );
    }

    // if a sequence flow is targeted make sure that shouldPassTokenHook is called after adding
    if (flowElement.$type === 'bpmn:SequenceFlow') {
      this.stateDelegate.dispatch(
        willPassTokenAction({
          sequenceId: elementId,
          tokenId: token.tokenId
        })
      );
    } else {
      // start execution of the element
      this.stateDelegate.dispatch(
        didPassTokenAction({
          to: elementId,
          tokenId: token.tokenId
        })
      );
    }

    return token.tokenId;
  }

  /**
   * Will move an existing token from one flow element to another
   *
   * @param elementId - the point of token placement
   * @param {token} - the id of the token and optional attributes
   * @param silent if true the move will not be logged as an adaptation
   */
  moveToken(elementId: string, token: { tokenId: string; [key: string]: any }, silent?: boolean) {
    return this.internalMoveToken(elementId, token, undefined, silent);
  }

  /**
   * Will move an existing token from one flow element to another
   *
   * @param elementId - the point of token placement
   * @param {token} - the id of the token and optional attributes
   * @param targetDefinitions used when the definitions will change after the token change to allow registering moves to elements in the new model
   * @param silent if true the move will not be logged as an adaptation
   */
  private internalMoveToken(
    elementId: string,
    token: { tokenId: string; [key: string]: any },
    targetDefinitions?: Definitions,
    silent?: boolean
  ) {
    const flowElement = this.getFlowElement(elementId, targetDefinitions);

    if (!flowElement) {
      throw new InvalidTokenPlacementError(`Flow element ${elementId} not found`);
    } else if (flowElement.$type === 'bpmn:ParallelGateway') {
      throw new InvalidTokenPlacementError(
        `Tokens cannot be placed directly on parallel gateways. Merging requirement is a token arriving on every sequence flow.`
      );
    }

    const existingToken = this.getInternalState().tokens.find(t => t.tokenId === token.tokenId);

    this.stateDelegate.dispatch(moveTokenAction({ token, elementId, targetDefinitions }));

    if (!silent) {
      this.stateDelegate.dispatch(
        logAdaptationAction({
          type: AdaptationType.TOKEN_MOVE,
          time: +new Date(),
          tokenId: token.tokenId,
          currentFlowElementId: existingToken?.currentFlowElement.id,
          targetFlowElementId: elementId
        })
      );
    }

    return token.tokenId;
  }

  /**
   * Place a token at an arbitrary sequence (wrapper function for addToken or moveToken)
   *
   * @param sequenceId - the point of token placement, must be a valid sequence
   * @param {token} - the id of the token and optional attributes
   * @param silent if true the change will not be logged as an adaptation
   */
  placeTokenAt(sequenceId: string, token: { [key: string]: any } = {}, silent?: boolean) {
    // check if we are placing a new token or moving an existing token
    const existingToken = this.getInternalState().tokens.find(t => t.tokenId === token.tokenId);
    if (!!existingToken) {
      return this.internalMoveToken(sequenceId, { ...token, tokenId: existingToken.tokenId }, undefined, silent);
    } else {
      return this.addToken(sequenceId, token, silent);
    }
  }

  /**
   * Notifiy on end of token
   * @param callback
   */
  onTokenEnded(callback: Function) {
    let alreadyEndedTokens: string[] = this.getInternalState()
      .tokens.filter(token => isTokenEndedState(token.state))
      .map(({ tokenId }) => tokenId);
    this.getState$()
      .pipe(takeUntil(this.getState$().pipe(filter(state => state.status === ProcessStatus.REMOVED))))
      .subscribe(state => {
        const endedTokens = this.getState()
          .tokens.filter(token => isTokenEndedState(token.state))
          .filter(token => !alreadyEndedTokens.includes(token.tokenId));

        endedTokens.forEach(token => {
          this.log.debug({
            message: `token ${token.tokenId} of processInstance ${this.id} ended `,
            params: { processInstanceId: this.id, tokenId: token.tokenId }
          });
          callback(token);
        });
        alreadyEndedTokens = alreadyEndedTokens.concat(endedTokens.map(t => t.tokenId));

        // remove tokens that were put back into a running state from alreadyEnded list
        alreadyEndedTokens = alreadyEndedTokens.filter(token => {
          const instanceToken = state.tokens.find(t => t.tokenId === token);

          if (instanceToken && !isTokenEndedState(instanceToken.state)) {
            return false;
          }
          return true;
        });
      });
  }

  /****************************** Flow Element State ******************************/

  /**
   * Get a list of all elements of a specific type that are currently open for work
   *
   * @param bpmnType the type of element to get a list of
   * @returns an array of all open elements of the given type alongside the id of the token that activated them for each one
   */
  getOpenElements(bpmnType: string) {
    return this.getInternalState()
      .tokens.filter(
        t =>
          t.state === TokenState.RUNNING &&
          (this.getFlowElement(t.currentFlowElement.id) || { $type: '' }).$type === bpmnType
      )
      .map(openToken => {
        const openElement = this.getFlowElement(openToken.currentFlowElement.id) as FlowElement;

        return {
          tokenId: openToken.tokenId,
          element: openElement
        };
      });
  }

  /**
   * Get a list of currently active user tasks
   */
  getUserTasks() {
    const userTask = this.getOpenElements('bpmn:UserTask') as { tokenId: string; element: UserTask }[];
    return userTask;
  }

  /**
   * Get a list of currently active call activities
   */
  getCallActivities() {
    const callActivity = this.getOpenElements('bpmn:CallActivity') as { tokenId: string; element: CallActivity }[];
    return callActivity;
  }

  /**
   * Get Id of sequenceFlow between 2 elements
   * @param from - the element id of sourceRef of searched sequenceFlow
   * @param to - the element id of targetRef of searched sequenceFlow
   */
  getSequenceFlowId(from: string, to: string) {
    const flowElements = this.getFlowElements();
    const searchedSequenceFlow = flowElements.filter(e => {
      if (e.$type !== 'bpmn:SequenceFlow') return false;
      const sequenceFlow = e as SequenceFlow;
      return sequenceFlow.sourceRef.id === from && sequenceFlow.targetRef.id === to;
    })[0];

    return searchedSequenceFlow.id;
  }

  /**
   * Helper function for getting all the flow elements inside the instance
   *
   * @param moddleDefinitions the definitions to get the flow elements from (defaults to the one of the instance)
   * @returns the flow elements inside the instance or the given model
   */
  getFlowElements(moddleDefinitions = this.getInternalState().moddleDefinitions) {
    return getFlowElements(moddleDefinitions);
  }

  /**
   * Helper function for getting a specific flow element inside the instance
   *
   * @param moddleDefinitions the definitions to get the flow element from (defaults to the one of the instance)
   * @returns the flow element inside the instance or the given model if it exists
   */
  getFlowElement(flowElementId: string, moddleDefinitions = this.getInternalState().moddleDefinitions) {
    return findElementById(moddleDefinitions, flowElementId);
  }

  /**
   * Function that allows to end an activity that is currently active
   *
   * Used for completeActivity, terminateActivity, failActivity functions
   *
   * @param activityId
   * @param tokenId
   * @param state the state to end the activity with
   * @returns if the activity was ended and additional actions can be run
   */
  private endActivity(activityId: string, tokenId: string, state: ExecutionState) {
    const matchingActivities = this.getInternalState()
      .tokens.filter(
        t => t.currentFlowElement.id === activityId && t.tokenId === tokenId && t.state === TokenState.RUNNING
      )
      .map(t => t.currentFlowElement);

    if (matchingActivities.length === 1) {
      const element = this.getFlowElement(activityId);

      if (element && activityTypes.includes(element.$type)) {
        this.stateDelegate.dispatch(
          endActivityAction({
            flowNodeId: activityId,
            tokenId,
            state
          })
        );

        return true;
      }
    }

    return false;
  }

  /**
   * Manually complete an activity with a tokenId
   * @param id the id of the activity
   * @param tokenId the token id of the activity when it was activated
   */
  completeActivity(activityId: string, tokenId: string, variables: { [key: string]: unknown } = {}) {
    if (this.endActivity(activityId, tokenId, ExecutionState.COMPLETED) && variables) {
      this.stateDelegate.dispatch(setVariablesAction({ changedBy: activityId, variables }));
    }
  }

  /**
   * Manually terminate an activity with a tokenId
   * @param id the id of the activity
   * @param tokenId the token id of the activity when it was activated
   */
  terminateActivity(activityId: string, tokenId: string) {
    this.endActivity(activityId, tokenId, ExecutionState.TERMINATED);
  }

  /**
   * Manually abort an activity with a tokenId
   * @param id the id of the activity
   * @param tokenId the token id of the activity when it was activated
   */
  failActivity(activityId: string, tokenId: string) {
    if (this.endActivity(activityId, tokenId, ExecutionState.FAILED)) {
      this.stateDelegate.dispatch(
        propagateErrorAction({
          flowNodeId: activityId,
          tokenId: tokenId
        })
      );
    }
  }

  setFlowNodeState(tokenId: string, state: string) {
    const matchingTokens = this.getInternalState().tokens.filter(
      t => t.tokenId === tokenId && t.state === TokenState.RUNNING
    );

    if (matchingTokens.length === 1) {
      this.stateDelegate.dispatch(setFlowNodeStateAction({ tokenId, state }));
    }
  }

  /**
   * Creates a new rxjs stream that emits when a flowNode is completed with one of the given states
   *
   * @param allowedEndstates the end states on which to emit
   *
   * @returns a rxjs stream that emits every time a flowNode is completed with one of the given states
   */
  private flowNodeExecution$(
    allowedEndstates = ['COMPLETED', 'ABORTED', 'SKIPPED', 'ERROR-SEMANTIC', 'ERROR-TECHNICAL', 'FAILED', 'TERMINATED']
  ) {
    return this.getExecutionLog$().pipe(filter(({ execution }) => allowedEndstates.includes(execution.executionState)));
  }

  /**
   * Notifiy on execution of flow node
   * @param callback
   */
  onFlowNodeExecuted(callback: Function) {
    this.flowNodeExecution$().subscribe(({ execution, token }) => {
      callback(execution, token);
    });
  }

  onActivityInterrupted(activityType: string, callback: Function) {
    if (!activityTypes.includes(activityType)) {
      throw new TypeError('Not a valid activity type');
    }

    this.flowNodeExecution$(['FAILED', 'TERMINATED', 'SKIPPED', 'ABORTED', 'ERROR-SEMANTIC', 'ERROR-TECHNICAL'])
      .pipe(
        filter(
          ({ execution }) => (this.getFlowElement(execution.flowElementId) || { $type: '' }).$type === activityType
        )
      )
      .subscribe(({ execution, token }) => callback(execution, token));
  }

  /**
   * given callback called with token when user task interrupted
   * @param callback
   */
  onUserTaskInterrupted(callback: Function) {
    this.onActivityInterrupted('bpmn:UserTask', callback);
  }

  /**
   * given callback called with token when call activity is interrupted
   * @param callback
   */
  onCallActivityInterrupted(callback: Function) {
    this.onActivityInterrupted('bpmn:CallActivity', callback);
  }

  /**
   * given callback called with token when error occured in script Task
   * @param callback
   */
  onScriptTaskError(callback: Function) {
    this.flowNodeExecution$(['ERROR-TECHNICAL'])
      .pipe(
        filter(
          ({ execution }) => (this.getFlowElement(execution.flowElementId) || { $type: '' }).$type === 'bpmn:ScriptTask'
        )
      )
      .subscribe(({ execution, token }) => callback(execution, token));
  }

  /****************************** Log State ******************************/

  /**
   * Get log stream of processinstance
   */
  getLog$() {
    return LoggerFactory.getLog$().pipe(
      filter(logItem => {
        return (logItem.params as any).processInstanceId === this.id;
      }),
      takeUntil(this.getState$().pipe(filter(state => state.status === ProcessStatus.REMOVED)))
    );
  }

  /****************************** Execution Log State ******************************/

  /**
   * Creates an execution log stream that notifies about (flowNode completion) events in the instance
   */
  getExecutionLog$() {
    // don't notify about executions that happened before the stream was subscribed to
    let alreadyNotifiedExecutions: { flowElementId: string; tokenId: string; startTime?: Number }[] = this.getState()
      .log;
    return this.stateDelegate.getState$().pipe(
      // handle every state change only after the previous one was handled
      concatMap(state => {
        const tokens = [...state.tokens];
        const newExecutions = state.log.filter(
          execution =>
            !alreadyNotifiedExecutions.some(
              e =>
                e.flowElementId === execution.flowElementId &&
                e.tokenId === execution.tokenId &&
                execution.startTime === e.startTime
            )
        );

        // remember new executions
        alreadyNotifiedExecutions = alreadyNotifiedExecutions.concat(newExecutions);

        const newExecutionsWithTokenInfo = newExecutions.map(newExecution => {
          const token = tokens.find(token => token.tokenId === newExecution.tokenId);
          return { execution: newExecution, token };
        });
        return from(newExecutionsWithTokenInfo);
      }),
      // stop the stream when the instance ends
      takeUntil(this.getState$().pipe(filter(state => state.status === ProcessStatus.REMOVED)))
    );
  }

  getExecutionLog() {
    return this.getInternalState().log;
  }

  /**
   * Add a flowNode execution in log
   * @param {string} flowElementId - Id of the element
   * @param {string} tokenId - id of the token
   * @param {} attributes - attributes to be inserted into the flowNode log
   */
  logExecution(flowElementId: string, tokenId: string, attributes: {}) {
    this.stateDelegate.dispatch(logExecutionAction({ flowElementId, tokenId, ...attributes }));
  }

  /**
   * Update a flowNode execution in log
   * @param {string} flowElementId - Id of the element
   * @param {string} tokenId - id of the token
   * @param {} attributes - attributes to be updated in the flowNode log
   */
  updateLog(flowElementId: string, tokenId: string, attributes: {}) {
    this.stateDelegate.dispatch(updatelogAction({ flowElementId, tokenId, ...attributes }));
  }

  /**
   * Merge given execution-log with existing log
   * @param [log] - log of flow node executions to be added/updated to existing log
   */
  mergeFlowNodeLog(log: LogState) {
    const existingExecutions = this.getExecutionLog();
    log.forEach(execution => {
      const executionExists = !!existingExecutions.find(e => JSON.stringify(e) === JSON.stringify(execution));

      if (!executionExists) {
        this.stateDelegate.dispatch(logExecutionAction({ ...execution }));
      }
    });
  }

  /****************************** Adaptation Log State ******************************/

  /**
   * Creates an adaptation log stream that notifies about (manual change) events in the instance
   */
  getAdaptationLog$() {
    // don't notify about executions that happened before the stream was subscribed to
    let alreadyNotifiedAdaptations: { type: AdaptationType | string; time: number }[] = this.getState().adaptationLog;
    return this.stateDelegate.getState$().pipe(
      // handle every state change only after the previous one was handled
      concatMap(state => {
        const newAdaptations = state.adaptationLog.filter(
          adaptation => !alreadyNotifiedAdaptations.some(a => a.type === adaptation.type && a.time === adaptation.time)
        );

        // remember new executions
        alreadyNotifiedAdaptations = alreadyNotifiedAdaptations.concat(newAdaptations);

        return from(newAdaptations);
      }),
      // stop the stream when the instance ends
      takeUntil(this.getState$().pipe(filter(state => state.status === ProcessStatus.REMOVED)))
    );
  }

  getAdaptationLog() {
    return this.getInternalState().adaptationLog;
  }

  /**
   * Add a adaptation in the adaptation log
   * @param type the type of the adaptation
   * @param time optional if a specific time should be used
   * @param attributes attributes to be inserted into the adaptation log entry
   */
  logAdaptation(type: string, time: number = +new Date(), attributes = {}) {
    this.stateDelegate.dispatch(logAdaptationAction({ type, time, ...attributes }));
  }

  /**
   * Update a adaptation entry in the log
   * @param flowElementId type of the adaptation to update
   * @param time time the adaptation was logged
   * @param attributes attributes to be updated in the found adaptation log entry
   */
  updateAdaptationLog(type: string, time: number, attributes: {}) {
    this.stateDelegate.dispatch(updateAdaptationLogAction({ type, time, ...attributes }));
  }

  /**
   * Merge given adaptation-log with existing log
   * @param [log] - log of flow adaptations to be added/updated to existing log
   */
  mergeAdaptationLog(log: AdaptationLogState) {
    const existingAdaptations = this.getAdaptationLog();
    log.forEach(adaptation => {
      const adaptationExists = !!existingAdaptations.find(a => JSON.stringify(a) === JSON.stringify(adaptation));

      if (!adaptationExists) {
        this.stateDelegate.dispatch(logAdaptationAction({ ...adaptation }));
      }
    });
  }

  /****************************** Variable State ******************************/

  /**
   * Get a copy of the current variables
   * @param {string} tokenId id of a token for which the current variable values (including intermediate values) are requested
   * @returns {object} variables
   */
  getVariables(tokenId?: string) {
    // get the variable values in the instances global variable state
    let variables: { [key: string]: any } = Object.entries(this.getState().variables).reduce(
      (acc, [key, info]) => ({ ...acc, [key]: info.value }),
      {}
    );

    if (tokenId) {
      const token = this.getInternalState().tokens.find(t => t.tokenId === tokenId);

      if (!token) {
        throw new Error(`Could not find token with given id (id: ${tokenId})`);
      }

      // get variable values that have intermediate changes in the execution of the flow node the token resides on
      if (token.intermediateVariablesState) {
        variables = { ...variables, ...token.intermediateVariablesState };
      }
    }

    return variables;
  }

  /**
   * Get a copy of the current variables along with log information
   * @param {string} tokenId id of a token for which the current variable informations (including intermediate values) are requested
   * @returns {object} variables
   */
  getVariablesWithLogs(tokenId?: string) {
    // get variable values and logs from the instances global variable state
    const variables = { ...this.getState().variables };

    if (tokenId) {
      const token = this.getInternalState().tokens.find(t => t.tokenId === tokenId);

      if (!token) {
        throw new Error(`Could not find token with given id (id: ${tokenId})`);
      }

      // get variable values that have intermediate changes in the execution of the flow node the token resides on
      if (token.intermediateVariablesState) {
        Object.entries(token.intermediateVariablesState).forEach(([key, value]) => {
          // create a log for intermediate changes
          const intermediateLogEntry = { changedTime: +new Date(), changedBy: token.currentFlowElement.id };

          if (!variables[key]) {
            variables[key] = { value, log: [intermediateLogEntry] };
          } else {
            // merge the intermediate log with already existing log entries and store the current value of the variable in the instance as the old value
            variables[key] = {
              value,
              log: [{ ...intermediateLogEntry, oldValue: variables[key].value }, ...variables[key].log]
            };
          }
        });
      }
    }

    return variables;
  }

  /**
   * Set the intermediate value of a variable, by dispatching event
   *
   * @param {string} tokenId - Id of the token the variable should be changed in
   * @param {string} name - name of the variable
   * @param {*} value - value of the variable
   * @param global - if the variable should be written directly to the instance state
   */
  setVariable(tokenId: string, name: string, value: any, global?: boolean) {
    if (JSON.stringify(value) === undefined) {
      this.log.error({
        message: 'Data type of variable must be valid in JSON',
        params: { processInstanceId: this.id }
      });
      throw new TypeError('Non-valid JSON Datatype');
    }

    const token = this.getInternalState().tokens.find(t => t.tokenId === tokenId);

    if (!token?.intermediateVariablesState) {
      throw new Error('Token does not accept variable changes');
    }

    if (token) {
      if (global) {
        this.stateDelegate.dispatch(
          setVariablesAction({ changedBy: token.currentFlowElement.id, variables: { [name]: value } })
        );
      } else {
        this.stateDelegate.dispatch(setIntermediateVariableValueAction({ tokenId, key: name, value }));
      }
    }
  }

  /**
   * Merge new entries into variables log
   * @param variables variables to be merged
   */
  mergeVariableChanges(variables: VariablesState) {
    this.stateDelegate.dispatch(updateVariablesAction({ variables }));
  }

  /**
   * Will change the variables in the instance, this change is considered a manual adaptation
   *
   * @param variables a list of variables with the new values
   * @param silent if set to true the change will not be logged as an adaptation
   */
  updateVariables(variables: { [key: string]: any }, silent?: boolean) {
    const currentVariables: { [key: string]: any } = this.getVariables();

    const changes: { variableName: string; oldValue: any; newValue: any }[] = [];

    Object.entries(variables).forEach(([key, newValue]) => {
      this.stateDelegate.dispatch(setVariablesAction({ changedBy: 'manual', variables: { [key]: newValue } }));

      changes.push({ variableName: key, newValue, oldValue: currentVariables[key] });
    });

    if (!silent) {
      this.stateDelegate.dispatch(
        logAdaptationAction({
          type: AdaptationType.VARIABLE_ADAPTATION,
          time: +new Date(),
          changes
        })
      );
    }
  }
}
