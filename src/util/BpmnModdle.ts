import { BPMNModdleConstructor } from 'bpmn-moddle';
let BpmnModdle: BPMNModdleConstructor = require('bpmn-moddle');

if (typeof BpmnModdle !== 'function') {
  BpmnModdle = (BpmnModdle as any).default as BPMNModdleConstructor;
}

export default BpmnModdle;
