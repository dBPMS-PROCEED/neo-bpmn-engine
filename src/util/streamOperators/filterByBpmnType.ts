import { Observable } from 'rxjs';
import { ProcessState } from '../../state/reducers';
import { StateObservable } from 'redux-observable';
import { filter } from 'rxjs/operators';
import { ProcessAction } from '../../state/actions';
import { findElementById } from '../getFlowElements';

/**
 * Utility stream operator to filter based on bpmn element type. The operator
 * expects the given process action to have 'id' property in its payload.
 *
 *  @param bpmnType the type to allow
 * @param state$ the state stream of an instance
 */
export function filterByBpmnType(bpmnType: string, state$: StateObservable<ProcessState>) {
  return (source: Observable<ProcessAction>) =>
    source.pipe(
      filter(a => {
        const elementId = (a.payload as any).flowNodeId;
        if (!elementId) return false;

        const element = findElementById(state$.value.moddleDefinitions, elementId);
        if (!element) return false;

        return element.$type === bpmnType;
      })
    );
}
