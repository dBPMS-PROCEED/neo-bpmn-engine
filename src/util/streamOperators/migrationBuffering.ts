import { ActionsObservable } from 'redux-observable';
import { merge, Observable, of } from 'rxjs';
import { bufferToggle, mergeAll, switchMap, windowToggle } from 'rxjs/operators';
import { ProcessAction, ProcessActionType } from '../../state/actions';

/**
 * Provides an operator that can be used in an rxjs pipe to hold off on handling some events until after a migration has completed
 *
 * @param action$ An action stream that provides the actions that occur during process execution
 */
export function bufferDuringMigration<Type extends ProcessAction>(action$: ActionsObservable<ProcessAction>) {
  const instanceStart$ = of(['InstanceStart']);
  const migrationStart$ = action$.ofType(ProcessActionType.StartMigration);
  const migrationEnd$ = action$.ofType(ProcessActionType.EndMigration);

  return function(source: Observable<Type>) {
    return merge(
      // let all incoming values pass through in normal operations and buffer during migration
      source.pipe(
        bufferToggle(migrationStart$, () => migrationEnd$), // buffer while a migration is running and release when the migration ends
        switchMap(val => (Array.isArray(val) ? of(...val) : of(val))) // convert buffer back into single values
      ),
      source.pipe(
        windowToggle(
          // let values pass in normal operations and block during migration
          merge(instanceStart$, migrationEnd$), // we want to start using the normal stream from the start of the instance and every time a migration has ended
          () => migrationStart$ // don't use the stream during a migration
        ),
        mergeAll()
      )
    );
  };
}
