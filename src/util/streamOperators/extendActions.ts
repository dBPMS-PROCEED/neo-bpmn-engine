import { FlowNode } from 'bpmn-moddle';
import { Observable } from 'rxjs';
import { withLatestFrom, map, filter } from 'rxjs/operators';
import { StateObservable } from 'redux-observable';
import { ProcessState } from '../../state/reducers';

import { TokenAction, FlowNodeAction } from '../../state/actions';
import { Token } from '../../state/reducers/tokens';
import { findElementById, activityTypes } from '../getFlowElements';

export function extendedTokenAction<Type extends TokenAction>(action: Type, token: Token | undefined) {
  return {
    ...action,
    token
  };
}

export function extendedAndValidatedTokenAction<Type extends TokenAction>(action: Type, token: Token) {
  return {
    ...action,
    token
  };
}

export function extendedFlowNodeAction<Type extends FlowNodeAction>(
  action: Type,
  token: Token | undefined,
  flowNode: FlowNode | undefined
) {
  return {
    ...action,
    token,
    flowNode
  };
}

export function extendedAndValidatedFlowNodeAction<Type extends FlowNodeAction>(
  action: Type,
  token: Token,
  flowNode: FlowNode
) {
  return {
    ...action,
    token,
    flowNode
  };
}

/**
 * Will extend the incoming TokenAction with the correct token
 *
 * @param state$ the state stream to get the current token state of the instance
 */
export function extendTokenAction<Type extends TokenAction>(state$: StateObservable<ProcessState>) {
  return (source: Observable<Type>) =>
    source.pipe(
      withLatestFrom(state$),
      map(([action, state]) => {
        const token = state.tokens.find(t => t.tokenId === action.payload.tokenId);

        return extendedTokenAction(action, token);
      })
    );
}

/**
 * Will only forward actions that have a valid token
 */
export function filterNonExistantToken<Type extends TokenAction>() {
  return (source: Observable<Type>) =>
    source.pipe(
      filter(({ token }) => !!token),
      map(extendedAction => extendedAndValidatedTokenAction(extendedAction, extendedAction.token!))
    );
}

/**
 * Will extend the incoming FlowNodeAction with the correct token and moddle element
 *
 * @param state$ the state stream to get the current token state of the instance
 */
export function extendFlowNodeAction<Type extends FlowNodeAction>(state$: StateObservable<ProcessState>) {
  return (source: Observable<Type>) =>
    source.pipe(
      extendTokenAction<Type>(state$),
      map(action => {
        const flowNode = findElementById(state$.value.moddleDefinitions, action.payload.flowNodeId) as FlowNode;

        return extendedFlowNodeAction(action, action.token, flowNode);
      })
    );
}

/**
 * Will only forward actions if they have a valid token and flowNode
 */
export function filterNonExistantTokenOrFlowNode<Type extends FlowNodeAction>() {
  return (source: Observable<Type>) =>
    source.pipe(
      filterNonExistantToken(),
      filter(({ flowNode }) => !!flowNode),
      map(action => extendedAndValidatedFlowNodeAction(action, action.token, action.flowNode))
    );
}

/**
 * Will only forward action if flowNode and token exists and the token is active on the flowNode
 */
export function filterTokenFlowNodeMismatch<Type extends FlowNodeAction>() {
  return (source: Observable<Type>) =>
    source.pipe(
      filterNonExistantTokenOrFlowNode(),
      filter(({ flowNode, token }) => token.currentFlowElement?.id === flowNode.id)
    );
}

/**
 * Will only forward action if the flowNode has the
 *
 * @param bpmnType The expected type of the flowNode element
 */
export function filterFlowNodeTypeMismatch<Type extends FlowNodeAction>(bpmnType: String) {
  return (source: Observable<Type>) => source.pipe(filter(({ flowNode }) => flowNode?.$type === bpmnType));
}

/**
 * Will only forward actions on activity elements
 */
export function filterNonActivityType<Type extends FlowNodeAction>() {
  return (source: Observable<Type>) => source.pipe(filter(({ flowNode }) => activityTypes.includes(flowNode?.$type)));
}
