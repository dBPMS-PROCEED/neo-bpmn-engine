import { Definitions, FlowElement, Process, SubProcess } from 'bpmn-moddle';

/**
 * Extract the flow elements from the given bpmn moddle definitions
 * @param moddleDefinitions
 * @returns array of flow elements
 */
export function getFlowElements(moddleDefinitions: Definitions | Process | SubProcess): FlowElement[] {
  let processDefinition: Process | SubProcess | null = null;
  if (moddleDefinitions.$type === 'bpmn:Definitions') {
    const processes = (moddleDefinitions as Definitions).rootElements.filter(e => e.$type === 'bpmn:Process');
    if (processes.length > 0) {
      processDefinition = processes[0] as Process;
    }
  }

  if (moddleDefinitions.$type === 'bpmn:Process') {
    processDefinition = moddleDefinitions as Process;
  }

  if (moddleDefinitions.$type === 'bpmn:SubProcess') {
    processDefinition = moddleDefinitions as SubProcess;
  }

  return processDefinition ? processDefinition.flowElements : [];
}

export function getAllNestedFlowElements(moddleDefinitions: Definitions | Process | SubProcess): FlowElement[] {
  let nestedFlowElements: FlowElement[] = [];

  if (moddleDefinitions.$type === 'bpmn:Definitions') {
    // add the flow elements inside each process element in the definitions
    (moddleDefinitions as Definitions).rootElements.forEach(rootElement => {
      if (rootElement.$type === 'bpmn:Process') {
        nestedFlowElements = nestedFlowElements.concat(getAllNestedFlowElements(rootElement as Process));
      }
    });
  } else if (moddleDefinitions.$type === 'bpmn:Process' || moddleDefinitions.$type === 'bpmn:SubProcess') {
    let childElements = (moddleDefinitions as Process | SubProcess).flowElements;
    // add the direct children of the process or subprocess element
    nestedFlowElements = nestedFlowElements.concat(childElements);

    // add all elements that might be nested inside subprocess elements
    childElements.forEach(child => {
      if (child.$type === 'bpmn:SubProcess') {
        nestedFlowElements = nestedFlowElements.concat(getAllNestedFlowElements(child as SubProcess));
      }
    });
  }

  return nestedFlowElements;
}

/**
 * Find a given element by id
 * @param moddleDefinitions | SubProcess
 * @param id
 * @returns the element, if found else null
 */
export function findElementById(moddleDefinitions: Definitions | SubProcess, id: string): FlowElement | undefined {
  const found = getFlowElements(moddleDefinitions).reduce((acc: FlowElement[], f: FlowElement) => {
    if (f.id === id) acc.push(f);

    if (f && f.$type === 'bpmn:SubProcess') {
      const subprocessFlowElements = findElementById(f as SubProcess, id);
      if (subprocessFlowElements) acc.push(subprocessFlowElements);
    }

    return acc;
  }, []);

  return found.length > 0 ? found[0] : undefined;
}

export const activityTypes = ['bpmn:Task', 'bpmn:ScriptTask', 'bpmn:UserTask', 'bpmn:CallActivity', 'bpmn:SubProcess'];
