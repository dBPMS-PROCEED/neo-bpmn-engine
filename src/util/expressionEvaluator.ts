import { ExpressionEvalError } from '../errors';

/**
 * Javascript expression evaluator, by wrapping expression in function(), with "variables"
 * object in scope. expression is expected to return boolean. Expressions are wrapped
 * inside Function() string for evaluation, with 'use strict'.
 * @param {string} expression javascript boolean expression. e.g. "a < b"
 * @param {object} variables the process variables snapshot at the point of evaluation
 * @returns {boolean} true if expression evaluates to true, else false
 * @throws {ExpressionEvalError} expression has to be of correct syntax and evaluate during rutime
 * @throws {InsecureExpressionError} express should only use variables as operands
 */
export function evaluateExpression(expression: string, variables: { [k: string]: unknown }): boolean {
  // TODO: Check for InsecureExpressionError
  try {
    const keys = Object.keys(variables);
    const fn = new Function(...keys, `return ${expression}`);
    return !!fn(...keys.map(k => variables[k]));
  } catch (e) {
    throw new ExpressionEvalError(e.toString());
  }
}
