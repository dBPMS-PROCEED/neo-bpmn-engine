/**
 * Queue adapter for native array
 */
export default class Queue<T> {
  private container: Array<T> = [];

  /**
   * Make a queue with initial elements (ordered as supplied)
   * @param elements initial elements
   * @returns A newly initialized queue
   */
  static make<T>(...elements: Array<T>) {
    const q = new Queue<T>();
    q.container = [...elements];
    return q;
  }

  /**
   * Enqueue one or more elements to the queue, processed left to right
   * @param elements the elements to enqueue in sequence
   */
  enqueue(...elements: Array<T>) {
    elements.forEach(e => this.container.unshift(e));
  }

  /**
   * Dequeue an element from the queue. Returns undefined if queue is empty
   * @returns the first deleted element or undefined
   */
  dequeue(): T | undefined {
    return this.container.pop();
  }

  /**
   * Returns the first element in queue without deleting, undefined if queue is empty
   * @returns the first element or undefined
   */
  first(): T | undefined {
    if (this.container.length > 0) {
      return this.container[this.container.length - 1];
    }

    return undefined;
  }

  /**
   * Returns the size of the queue.
   * @returns number
   */
  size() {
    return this.container.length;
  }

  /**
   * Check if the queue is first
   * @returns boolean true if the size is 0
   */
  isEmpty() {
    return this.size() === 0;
  }
}
