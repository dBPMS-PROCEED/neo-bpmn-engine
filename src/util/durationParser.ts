import { parse, toSeconds } from 'iso8601-duration';
import { MalformedDurationError } from '../errors';

/**
 * Utility method for parsing ISO duration with repeat count additions
 * TODO: Write tests
 * @see https://en.wikipedia.org/wiki/ISO_8601#Durations
 * @see https://docs.zeebe.io/bpmn-workflows/timer-events.html for additions
 * @param {string} duration duration in ISO8601 duration format
 * @throws {MalformedDurationError} Must be in correct format and non-zero
 * @return {[number, number]} tuple containing repeat count and duration in milliseconds respectively
 */
export function parseIsoDuration(duration: string) {
  const parts = duration.split('/');
  const [repeatPart, durationPart] = parts.length > 1 ? parts : ['', parts[0]];
  const repeatCount =
    repeatPart === 'R' || parts.length === 1 ? Number.POSITIVE_INFINITY : parseInt(repeatPart.replace(/^R/, ''), 10);

  if (Object.is(repeatCount, NaN)) {
    throw new MalformedDurationError('repeat count is NaN');
  }

  try {
    const milliseconds = toSeconds(parse(durationPart)) * 1000;

    if (milliseconds === 0) {
      throw new MalformedDurationError('milliseconds 0');
    }

    return [repeatCount, milliseconds];
  } catch {
    throw new MalformedDurationError('milliseconds 0');
  }
}
