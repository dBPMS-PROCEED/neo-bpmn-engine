export class InvalidBpmnXmlError extends Error {
  constructor(m: string) {
    super(`Invalid BPMN XML: ${m}`);
    Object.setPrototypeOf(this, InvalidBpmnXmlError.prototype);
  }
}

export class BpmnElementNotFoundError extends Error {
  constructor(id: string) {
    super(`Element not found in BPMN, id=${id}`);
    Object.setPrototypeOf(this, BpmnElementNotFoundError.prototype);
  }
}

export class BpmnProcessNotDeployedError extends Error {
  constructor() {
    super(`Cannot instantiate process when in undeployed state`);
    Object.setPrototypeOf(this, BpmnProcessNotDeployedError.prototype);
  }
}

export class NoStartEventSpecifiedError extends Error {
  constructor() {
    super(`Start event not specified in bpmn`);
    Object.setPrototypeOf(this, NoStartEventSpecifiedError.prototype);
  }
}

export class AmbiguousStartEventError extends Error {
  constructor() {
    super(`Multiple start events specified in bpmn`);
    Object.setPrototypeOf(this, AmbiguousStartEventError.prototype);
  }
}

export class MessageRefNotFoundError extends Error {
  constructor(messageRef: string) {
    super(`Message ref not found: ref=${messageRef}`);
    Object.setPrototypeOf(this, MessageRefNotFoundError.prototype);
  }
}

export class SignalRefNotFoundError extends Error {
  constructor(signalRef: string) {
    super(`Signal ref not found: ref=${signalRef}`);
    Object.setPrototypeOf(this, SignalRefNotFoundError.prototype);
  }
}

export class MalformedDurationError extends Error {
  constructor(m: string) {
    super(`Duration did not match ISO8601 format or is invalid: ${m}`);
    Object.setPrototypeOf(this, MalformedDurationError.prototype);
  }
}

export class ExpressionEvalError extends Error {
  constructor(m: string) {
    super(`Error evaluating expression: ${m}`);
    Object.setPrototypeOf(this, ExpressionEvalError.prototype);
  }
}

export class InsecureExpressionError extends Error {
  constructor(m: string) {
    super(`Error evaluating expression: ${m}`);
    Object.setPrototypeOf(this, InsecureExpressionError.prototype);
  }
}

export class InvalidTokenPlacementError extends Error {
  constructor(m: string) {
    super(`Cannot place token at the requested location: ${m}`);
    Object.setPrototypeOf(this, InvalidTokenPlacementError.prototype);
  }
}

export class ScriptExecutionError extends Error {
  constructor(m: string) {
    super(`Error executing script: ${m}`);
    Object.setPrototypeOf(this, ScriptExecutionError.prototype);
  }
}

export class BpmnEscalation extends Error {
  explanation: string;
  constructor(name: string, explanation: string) {
    super(`escalation thrown: ${name}`);
    Object.setPrototypeOf(this, BpmnEscalation.prototype);

    if (!explanation) {
      this.explanation = name;
    } else {
      this.name = name;
      this.explanation = explanation;
    }
  }
}

export class BpmnError extends Error {
  explanation: string;
  constructor(name: string, explanation: string) {
    super(`error thrown: ${name}`);
    Object.setPrototypeOf(this, BpmnError.prototype);

    if (!explanation) {
      this.explanation = name;
    } else {
      this.name = name;
      this.explanation = explanation;
    }
  }
}
