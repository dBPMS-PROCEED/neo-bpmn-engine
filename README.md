# Neo BPMN Engine in Javascript

Yet another Javascript-based BPMN engine, still under active development, any bug report,
issues or questions are welcome, please refer to the [Contribution Guidelines](#contribution-guidelines) section.

## Quick Start

`npm install neo-bpmn-engine`

The engine is designed to be embeddable in any application executing in a javascript environment, browser or NodeJS and provides the absolute minimum interface for execution of the given BPMN XML, starting/stopping process instances and monitoring the same. Additionally, the application could extend the engine for domain specific applications by implementing custom serviceTask, messages, signals and script tasks. A thin wrapper could also be written to expose the engine via HTTP or any other transport, in addition to implementation of common enterprise functions such as authorization.

The two core classes of the engine are `BpmnProcess`, which serves as a template for creating process instances and the process instance represented by the `BpmnProcessInstance` class. A BPMNProcess must be created from a valid, executable BPMN XML string, given below.

```js
import * as Engine from 'neo-bpmn-engine';

const myBpmnProcess = await Engine.BpmnProcess.fromXml('process-id', '<...bpmn-process-xml...>');
```

The above step creates a BPMN process, ready for deployment. Deployment is an essential step, which tells the `BpmnProcess` class that any auto-triggered start events such as timers may start new instances and message/start start events may wait for invocation. Before starting a process instance, it is essential that we register a callback that will be notified on each creation of a new process instance.

```js
myBpmnProcess.getInstance$().subscribe(newInstance => {
  console.log('--> A new instance was created', newInstance.getId());
});
```

The above block prepares a callback that will print the id of the newly created process instance. `getInstance$` returns a stream of instances, errors in creation of a new instance can also be handled in the same subscription as follows:

```js
myBpmnProcess.getInstance$().subscribe({
  next: instance => {
    console.log('--> A new instance was created', newInstance.getId());
  },
  error: error => {
    console.log('--> Error on instance creation', error);
  }
});
```

```js
myBpmnProcess.getInstance$().subscribe(newInstance => {
  newInstance.getLog$().subscribe((l) => console.log('%s %s %s - %s %o', l.timestamp, getLevelName(l.level), l.name, l.message, l.params);)
});
```

Every processinstance has an own logstream which can be subscribed to via the function `getLog$`

After setting up the subscription, its time to create instances:

```js
myBpmnProcess.deployAndStart();
```

The above code will deploy the xml and start an instance at a none start event, schedule any timer start events and also wait for message/signal start events. If starting at a specific element is desired,

```js
myBpmnProcess.deployAndStartAt({ id: 'Task_sdf82n' });
```

Optionally, the process can be started with an initial set of variables,

```js
myBpmnProcess.deployAndStart({ variables: { a: 10, b: 20 } });
```

Subscription to `getInstance$()` is not the only way to obtain references to the instances. You can get an array of all the instances via `getAllInstances()` and if the instance id is known already, you can use `getInstanceById(id)` on the objects of the `BpmnProcess` class.

## Documentation

- [Theory](docs/THEORY.md)
- [Tests](docs/Tests.md)
- [API](docs/API.md)
- BPMN Elements
  - [Event](docs/Event.md)
  - [Gateway](docs/Gateway.md)
  - [Task](docs/Task.md)
  - [Subprocess](docs/Subprocess.md)

## BPMN Coverage Status

The engine covers the following BPMN elements:

|                            Element |       Status       | Notes                                                                                 |                                         Tests                                          |
| ---------------------------------: | :----------------: | :------------------------------------------------------------------------------------ | :------------------------------------------------------------------------------------: |
|                      Sequence Flow | :white_check_mark: | Supports condition expression evaluation in JS                                        |                      [Test cases](__tests__/BpmnProcess.spec.ts)                       |
|                       Message Flow |                    |                                                                                       |
|                   Data Association |                    |                                                                                       |
|                  Exclusive Gateway | :white_check_mark: |                                                                                       |         [Test cases](__tests__/state/epics/gateways/exclusiveGateway.spec.ts)          |
|                   Parallel Gateway | :white_check_mark: |                                                                                       |          [Test cases](__tests__/state/epics/gateways/parallelGateway.spec.ts)          |
|                Event-Based Gateway | :white_check_mark: | with intermediate catch events from below                                             |         [Test cases](__tests__/state/epics/gateways/eventBasedGateway.spec.ts)         |
|                  Inclusive Gateway | :white_check_mark: | With limitations, refer [Known Issues and Limitations](#known-issues-and-limitations) |         [Test cases](__tests__/state/epics/gateways/inclusiveGateway.spec.ts)          |
|                          User Task | :white_check_mark: |                                                                                       |               [Test cases](__tests__/state/epics/tasks/userTask.spec.ts)               |
|                       Service Task |                    |                                                                                       |
|                        Script Task | :white_check_mark: |                                                                                       |              [Test cases](__tests__/state/epics/tasks/scriptTask.spec.ts)              |
|                       Receive Task |                    |                                                                                       |
|                        Sub Process | :white_check_mark: |                                                                                       |       [Test cases](__tests__/state/epics/subprocess/embeddedSubprocess.spec.ts)        |
|                      Call Activity | :white_check_mark: |                                                                                       |          [Test cases](__tests__/state/epics/subprocess/callActivity.spec.ts)           |
|                               Pool |                    |                                                                                       |
|                               Lane |                    |                                                                                       |
|                        Data Object |                    |                                                                                       |
|                         Data Store |                    |                                                                                       |
|                   None Start Event | :white_check_mark: |                                                                                       |                      [Test cases](__tests__/BpmnProcess.spec.ts)                       |
|                Message Start Event | :white_check_mark: |                                                                                       |                      [Test cases](__tests__/BpmnProcess.spec.ts)                       |
|                 Signal Start Event | :white_check_mark: |                                                                                       |                      [Test cases](__tests__/BpmnProcess.spec.ts)                       |
|                  Timer Start Event | :white_check_mark: | cycle and date support pending                                                        |                      [Test cases](__tests__/BpmnProcess.spec.ts)                       |
|                     None End Event | :white_check_mark: |                                                                                       |              [Test cases](__tests__/state/epics/events/endEvent.spec.ts)               |
|                  Message End Event | :white_check_mark: |                                                                                       |              [Test cases](__tests__/state/epics/events/endEvent.spec.ts)               |
|                   Signal End Event | :white_check_mark: |                                                                                       |              [Test cases](__tests__/state/epics/events/endEvent.spec.ts)               |
|                Terminate End Event | :white_check_mark: |                                                                                       |              [Test cases](__tests__/state/epics/events/endEvent.spec.ts)               |
| Intermediate Catch Event (Message) | :white_check_mark: | message vars pending                                                                  |    [Test cases](__tests__/state/epics/events/intermediateCatchEventMessage.spec.ts)    |
|  Intermediate Catch Event (Signal) | :white_check_mark: |                                                                                       |    [Test cases](__tests__/state/epics/events/intermediateCatchEventSignal.spec.ts)     |
|   Intermediate Catch Event (Timer) | :white_check_mark: | cycle and date support pending                                                        | [Test cases](__tests__/state/epics/events/intermediateCatchEventTimerDuration.spec.ts) |
| Intermediate Throw Event (Message) | :white_check_mark: |                                                                                       |    [Test cases](__tests__/state/epics/events/intermediateThrowEventMessage.spec.ts)    |
|  Intermediate Throw Event (Signal) | :white_check_mark: |                                                                                       |    [Test cases](__tests__/state/epics/events/intermediateThrowEventSignal.spec.ts)     |
|               Error Boundary Event | :white_check_mark: |                                                                                       |            [Test cases](__tests__/state/epics/events/boundaryEvent.spec.ts)            |
|          Escalation Boundary Event | :white_check_mark: |                                                                                       |            [Test cases](__tests__/state/epics/events/boundaryEvent.spec.ts)            |
|               Timer Boundary Event | :white_check_mark: |                                                                                       |            [Test cases](__tests__/state/epics/events/boundaryEvent.spec.ts)            |
|                               Loop |                    |                                                                                       |
|                     Multi Instance |                    |                                                                                       |

## Known Issues and Limitations

- Handling Gateways: several corner cases related to gateways, described in [Russell, 2006] require heavy processing and are not required to be implemented if the bpmn is structured (splits and merges are balanced). Therefore, at this moment we do not implement WCP-38.

- Ambiguity in instance start: the BPMN specification provides several ways of creating a new process instance via start events [OMG, 2014], however, the execution semantics specifies the usage of the same start events to reuse instances, we do not support this as of now.
