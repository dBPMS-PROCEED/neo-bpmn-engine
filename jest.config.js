module.exports = {
  testEnvironment: 'node',
  transform: {
    '^.+\\.(ts|ts)$': 'ts-jest'
  },
  testRegex: '(/__tests__/.*|(\\.|/))\\.spec\\.(ts|tsx)$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json']
  // collectCoverage: true,
  // collectCoverageFrom: ['./src/**/*.ts']
};
