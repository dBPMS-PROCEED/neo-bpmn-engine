// Simple BPMN with none start, none activity and none end
// StartEvent_1 -> Task_175vb4p -> EndEvent_1umdfcw
export const BPMN_XML_SINGLE_NONE_START_EVENT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:task id="Task_175vb4p" name="Activity 1">
      <bpmn:incoming>SequenceFlow_07so23a</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_12bv6av</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_12bv6av</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_07so23a" sourceRef="StartEvent_1" targetRef="Task_175vb4p" />
    <bpmn:sequenceFlow id="SequenceFlow_12bv6av" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_07so23a</bpmn:outgoing>
    </bpmn:startEvent>
  </bpmn:process>
</bpmn:definitions>`;

// Process uses start timer repeats 3 times with 2 seconds interval (R3/PT2S)
export const BPMN_XML_SINGLE_DURATION_TIMER_START_EVENT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:task id="Task_175vb4p" name="Activity 1">
      <bpmn:incoming>SequenceFlow_07so23a</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_12bv6av</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_12bv6av</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_07so23a" sourceRef="StartEvent_1" targetRef="Task_175vb4p" />
    <bpmn:sequenceFlow id="SequenceFlow_12bv6av" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_07so23a</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">R3/PT2S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:startEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_MULTIPLE_NONE_START_EVENT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:task id="Task_175vb4p" name="Activity 1">
      <bpmn:incoming>SequenceFlow_07so23a</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_12bv6av</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_12bv6av</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_07so23a" sourceRef="StartEvent_1" targetRef="Task_175vb4p" />
    <bpmn:sequenceFlow id="SequenceFlow_07so23b" sourceRef="StartEvent_2" targetRef="Task_175vb4p" />
    <bpmn:sequenceFlow id="SequenceFlow_12bv6av" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_07so23a</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:startEvent id="StartEvent_2">
      <bpmn:outgoing>SequenceFlow_07so23b</bpmn:outgoing>
    </bpmn:startEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_WITHOUT_START_EVENT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:task id="Task_175vb4p" name="Activity 1">
      <bpmn:outgoing>SequenceFlow_12bv6av</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_12bv6av</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_12bv6av" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SINGLE_MESSAGE_START_EVENT = ` 
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:message id="startMessage1" name="myCustomStartMessage" />
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:task id="Task_175vb4p" name="Activity 1">
      <bpmn:incoming>SequenceFlow_07so23a</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_12bv6av</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_12bv6av</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_07so23a" sourceRef="StartEvent_1" targetRef="Task_175vb4p" />
    <bpmn:sequenceFlow id="SequenceFlow_12bv6av" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_07so23a</bpmn:outgoing>
      <bpmn:messageEventDefinition messageRef="startMessage1" />
    </bpmn:startEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SINGLE_SINGLE_START_EVENT = ` 
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:signal id="startSignal1" name="myCustomStartSignal" />
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:task id="Task_175vb4p" name="Activity 1">
      <bpmn:incoming>SequenceFlow_07so23a</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_12bv6av</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_12bv6av</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_07so23a" sourceRef="StartEvent_1" targetRef="Task_175vb4p" />
    <bpmn:sequenceFlow id="SequenceFlow_12bv6av" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_07so23a</bpmn:outgoing>
      <bpmn:signalEventDefinition signalRef="startSignal1" />
    </bpmn:startEvent>
  </bpmn:process>
</bpmn:definitions>`;

// Sequence SequenceFlow_1jybbbu has no condition
// Sequence SequenceFlow_1guale0 has condition a + b > 10
export const BPMN_XML_SEQUENCE_FLOWS = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:message id="myMessage" name="my-custom-message" />
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:task id="Task_175vb4p" name="Activity 1">
      <bpmn:incoming>SequenceFlow_0ge3i6q</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1jybbbu</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_1n095p0</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1jybbbu</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:task id="Task_1f62s02" name="Activity 2">
      <bpmn:incoming>SequenceFlow_1guale0</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1n095p0</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1n095p0" sourceRef="Task_1f62s02" targetRef="EndEvent_1umdfcw" />
    <bpmn:startEvent id="StartEvent_0wrq1m1">
      <bpmn:outgoing>SequenceFlow_1fa7nex</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_1jybbbu" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
    <bpmn:sequenceFlow id="SequenceFlow_1guale0" sourceRef="ExclusiveGateway_0h63eyu" targetRef="Task_1f62s02">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">a + b &gt; 10</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0ge3i6q" sourceRef="ExclusiveGateway_0h63eyu" targetRef="Task_175vb4p">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">a + b &lt; 10</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1fa7nex" sourceRef="StartEvent_0wrq1m1" targetRef="ExclusiveGateway_0h63eyu" />
    <bpmn:exclusiveGateway id="ExclusiveGateway_0h63eyu">
      <bpmn:incoming>SequenceFlow_1fa7nex</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1guale0</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0ge3i6q</bpmn:outgoing>
    </bpmn:exclusiveGateway>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_PARALLEL_NONE_ACTIVITY_SPLIT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:message id="myMessage" name="my-custom-message" />
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:task id="Task_175vb4p" name="Activity 1b">
      <bpmn:incoming>SequenceFlow_1w2uyg2</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1jybbbu</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_1n095p0</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1jybbbu</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:task id="Task_1f62s02" name="Activity 1a">
      <bpmn:incoming>SequenceFlow_1rum3wk</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1n095p0</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1n095p0" sourceRef="Task_1f62s02" targetRef="EndEvent_1umdfcw" />
    <bpmn:startEvent id="StartEvent_0wrq1m1">
      <bpmn:outgoing>SequenceFlow_06udvbi</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_1jybbbu" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
    <bpmn:task id="Task_0qqnex5" name="Activity 1">
      <bpmn:incoming>SequenceFlow_06udvbi</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1rum3wk</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_1w2uyg2</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1rum3wk" sourceRef="Task_0qqnex5" targetRef="Task_1f62s02" />
    <bpmn:sequenceFlow id="SequenceFlow_1w2uyg2" sourceRef="Task_0qqnex5" targetRef="Task_175vb4p" />
    <bpmn:sequenceFlow id="SequenceFlow_06udvbi" sourceRef="StartEvent_0wrq1m1" targetRef="Task_0qqnex5" />
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Exclusive Gateway
 *
 *                                                                                                             -> (age < 18) -> Task_1uro9hz ->
 *                                                                                                             -> (age < 60) -> Task_0948gst -> ExclusiveGateway_0mtspc3 -> (default) -> EndEvent_142vabd
 *                                          -> (hasGermanPassport) -> Task_1mlhorc -> ExclusiveGateway_0103oxb ->  (default) -> Task_0do52ol ->                                                  |
 * StartEvent_1 -> ExclusiveGateway_1577cb8 ->      (default)      -> Task_03jcu1g --------------------------------------------------------------------------------------------------------------|
 */
export const BPMN_XML_EXCLUSIVE_XOR_GATEWAY = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="Definitions_1kqtxqz" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
  <bpmn:task id="Task_03jcu1g" name="Immigrant">
    <bpmn:incoming>SequenceFlow_0eebdoc</bpmn:incoming>
    <bpmn:outgoing>SequenceFlow_0o6z4u2</bpmn:outgoing>
  </bpmn:task>
  <bpmn:endEvent id="EndEvent_142vabd">
    <bpmn:incoming>SequenceFlow_0o6z4u2</bpmn:incoming>
    <bpmn:incoming>SequenceFlow_0hkmgev</bpmn:incoming>
  </bpmn:endEvent>
  <bpmn:task id="Task_1mlhorc" name="Citizen">
    <bpmn:incoming>SequenceFlow_0g5xej0</bpmn:incoming>
    <bpmn:outgoing>SequenceFlow_04ecujr</bpmn:outgoing>
  </bpmn:task>
  <bpmn:startEvent id="StartEvent_0tmdz2f">
    <bpmn:outgoing>SequenceFlow_1owm0el</bpmn:outgoing>
  </bpmn:startEvent>
  <bpmn:sequenceFlow id="SequenceFlow_0o6z4u2" sourceRef="Task_03jcu1g" targetRef="EndEvent_142vabd" />
  <bpmn:exclusiveGateway id="ExclusiveGateway_1577cb8" default="SequenceFlow_0eebdoc">
    <bpmn:incoming>SequenceFlow_1owm0el</bpmn:incoming>
    <bpmn:outgoing>SequenceFlow_0g5xej0</bpmn:outgoing>
    <bpmn:outgoing>SequenceFlow_0eebdoc</bpmn:outgoing>
  </bpmn:exclusiveGateway>
  <bpmn:sequenceFlow id="SequenceFlow_0g5xej0" sourceRef="ExclusiveGateway_1577cb8" targetRef="Task_1mlhorc">
    <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">hasGermanPassport</bpmn:conditionExpression>
  </bpmn:sequenceFlow>
  <bpmn:sequenceFlow id="SequenceFlow_0eebdoc" sourceRef="ExclusiveGateway_1577cb8" targetRef="Task_03jcu1g" />
  <bpmn:sequenceFlow id="SequenceFlow_1owm0el" sourceRef="StartEvent_0tmdz2f" targetRef="ExclusiveGateway_1577cb8" />
  <bpmn:exclusiveGateway id="ExclusiveGateway_0103oxb" default="SequenceFlow_07ohu1w">
    <bpmn:incoming>SequenceFlow_04ecujr</bpmn:incoming>
    <bpmn:outgoing>SequenceFlow_0yub6x7</bpmn:outgoing>
    <bpmn:outgoing>SequenceFlow_07fi7oq</bpmn:outgoing>
    <bpmn:outgoing>SequenceFlow_07ohu1w</bpmn:outgoing>
  </bpmn:exclusiveGateway>
  <bpmn:sequenceFlow id="SequenceFlow_04ecujr" sourceRef="Task_1mlhorc" targetRef="ExclusiveGateway_0103oxb" />
  <bpmn:task id="Task_1uro9hz" name="Youth">
    <bpmn:incoming>SequenceFlow_0yub6x7</bpmn:incoming>
    <bpmn:outgoing>SequenceFlow_0l82wjz</bpmn:outgoing>
  </bpmn:task>
  <bpmn:sequenceFlow id="SequenceFlow_0yub6x7" sourceRef="ExclusiveGateway_0103oxb" targetRef="Task_1uro9hz">
    <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">age &lt; 18</bpmn:conditionExpression>
  </bpmn:sequenceFlow>
  <bpmn:task id="Task_0948gst" name="Adult">
    <bpmn:incoming>SequenceFlow_07fi7oq</bpmn:incoming>
    <bpmn:outgoing>SequenceFlow_129vddo</bpmn:outgoing>
  </bpmn:task>
  <bpmn:sequenceFlow id="SequenceFlow_07fi7oq" sourceRef="ExclusiveGateway_0103oxb" targetRef="Task_0948gst">
    <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">age &lt; 60</bpmn:conditionExpression>
  </bpmn:sequenceFlow>
  <bpmn:task id="Task_0do52ol" name="Senior">
    <bpmn:incoming>SequenceFlow_07ohu1w</bpmn:incoming>
    <bpmn:outgoing>SequenceFlow_0z3wy3k</bpmn:outgoing>
  </bpmn:task>
  <bpmn:sequenceFlow id="SequenceFlow_07ohu1w" sourceRef="ExclusiveGateway_0103oxb" targetRef="Task_0do52ol" />
  <bpmn:exclusiveGateway id="ExclusiveGateway_0mtspc3">
    <bpmn:incoming>SequenceFlow_0l82wjz</bpmn:incoming>
    <bpmn:incoming>SequenceFlow_129vddo</bpmn:incoming>
    <bpmn:incoming>SequenceFlow_0z3wy3k</bpmn:incoming>
    <bpmn:outgoing>SequenceFlow_0hkmgev</bpmn:outgoing>
  </bpmn:exclusiveGateway>
  <bpmn:sequenceFlow id="SequenceFlow_0l82wjz" sourceRef="Task_1uro9hz" targetRef="ExclusiveGateway_0mtspc3" />
  <bpmn:sequenceFlow id="SequenceFlow_129vddo" sourceRef="Task_0948gst" targetRef="ExclusiveGateway_0mtspc3" />
  <bpmn:sequenceFlow id="SequenceFlow_0z3wy3k" sourceRef="Task_0do52ol" targetRef="ExclusiveGateway_0mtspc3" />
  <bpmn:sequenceFlow id="SequenceFlow_0hkmgev" sourceRef="ExclusiveGateway_0mtspc3" targetRef="EndEvent_142vabd" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_EXCLUSIVE_XOR_GATEWAY_MULTIPLE_INOUT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="Definitions_0kif84k" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_04fsjai" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0lj2ssi</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:exclusiveGateway id="ExclusiveGateway_1hdcxk3" default="Flow_03e2892">
      <bpmn:incoming>Flow_0lj2ssi</bpmn:incoming>
      <bpmn:outgoing>Flow_0lts0y9</bpmn:outgoing>
      <bpmn:outgoing>Flow_1ifpbjm</bpmn:outgoing>
      <bpmn:outgoing>Flow_03e2892</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:sequenceFlow id="Flow_0lj2ssi" sourceRef="StartEvent_1" targetRef="ExclusiveGateway_1hdcxk3" />
    <bpmn:task id="Task_1v4me5n" name="Task 1 A">
      <bpmn:incoming>Flow_0lts0y9</bpmn:incoming>
      <bpmn:outgoing>Flow_1g1w4b9</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0lts0y9" name="age &#60; 18" sourceRef="ExclusiveGateway_1hdcxk3" targetRef="Task_1v4me5n">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">age &lt; 18</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0tb6lfh" name="Task 1 B">
      <bpmn:incoming>Flow_1ifpbjm</bpmn:incoming>
      <bpmn:outgoing>Flow_03ydb6a</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1ifpbjm" name="18 &#60;= age &#60; 67" sourceRef="ExclusiveGateway_1hdcxk3" targetRef="Task_0tb6lfh">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">age &gt;= 18 &amp;&amp; age &lt; 67</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_1wnlw8p" name="Task 1 C">
      <bpmn:incoming>Flow_03e2892</bpmn:incoming>
      <bpmn:outgoing>Flow_12lpwu3</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_03e2892" sourceRef="ExclusiveGateway_1hdcxk3" targetRef="Task_1wnlw8p" />
    <bpmn:exclusiveGateway id="ExclusiveGateway_068uetn" default="Flow_07vj5q0">
      <bpmn:incoming>Flow_1g1w4b9</bpmn:incoming>
      <bpmn:incoming>Flow_03ydb6a</bpmn:incoming>
      <bpmn:incoming>Flow_12lpwu3</bpmn:incoming>
      <bpmn:outgoing>Flow_0kyonpu</bpmn:outgoing>
      <bpmn:outgoing>Flow_07vj5q0</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:sequenceFlow id="Flow_1g1w4b9" sourceRef="Task_1v4me5n" targetRef="ExclusiveGateway_068uetn" />
    <bpmn:sequenceFlow id="Flow_03ydb6a" sourceRef="Task_0tb6lfh" targetRef="ExclusiveGateway_068uetn" />
    <bpmn:sequenceFlow id="Flow_12lpwu3" sourceRef="Task_1wnlw8p" targetRef="ExclusiveGateway_068uetn" />
    <bpmn:task id="Task_1wr870c" name="Task 2 A">
      <bpmn:incoming>Flow_0kyonpu</bpmn:incoming>
      <bpmn:outgoing>Flow_08nke87</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0kyonpu" name="has GermanPassport" sourceRef="ExclusiveGateway_068uetn" targetRef="Task_1wr870c">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">hasGermanPassport</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0lkklp5" name="Task 2 B">
      <bpmn:incoming>Flow_07vj5q0</bpmn:incoming>
      <bpmn:outgoing>Flow_11ccna7</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_07vj5q0" sourceRef="ExclusiveGateway_068uetn" targetRef="Task_0lkklp5" />
    <bpmn:endEvent id="EndEvent_1ve3kxo">
      <bpmn:incoming>Flow_08nke87</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_08nke87" sourceRef="Task_1wr870c" targetRef="EndEvent_1ve3kxo" />
    <bpmn:endEvent id="EndEvent_1x7hk5z">
      <bpmn:incoming>Flow_11ccna7</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_11ccna7" sourceRef="Task_0lkklp5" targetRef="EndEvent_1x7hk5z" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_PARALLEL_GATEWAY_MULTIPLE_INOUT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0k5n5qo" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_0lekgmh" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1y4ehu1</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1y4ehu1" sourceRef="StartEvent_1" targetRef="ParallelGateway_171ay3q" />
    <bpmn:parallelGateway id="ParallelGateway_171ay3q">
      <bpmn:incoming>Flow_1y4ehu1</bpmn:incoming>
      <bpmn:outgoing>Flow_1o9nuuf</bpmn:outgoing>
      <bpmn:outgoing>Flow_056d0wv</bpmn:outgoing>
      <bpmn:outgoing>Flow_0zfvwb1</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:task id="Task_0wvzeht" name="Task 1.1">
      <bpmn:incoming>Flow_1o9nuuf</bpmn:incoming>
      <bpmn:outgoing>Flow_0dd6rd9</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1o9nuuf" sourceRef="ParallelGateway_171ay3q" targetRef="Task_0wvzeht" />
    <bpmn:task id="Task_0enpu7i" name="Task 1.2">
      <bpmn:incoming>Flow_056d0wv</bpmn:incoming>
      <bpmn:outgoing>Flow_04b27g5</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_056d0wv" sourceRef="ParallelGateway_171ay3q" targetRef="Task_0enpu7i" />
    <bpmn:task id="Task_05j6ajf" name="Task 1.3">
      <bpmn:incoming>Flow_0zfvwb1</bpmn:incoming>
      <bpmn:outgoing>Flow_0uzt6b9</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0zfvwb1" sourceRef="ParallelGateway_171ay3q" targetRef="Task_05j6ajf" />
    <bpmn:sequenceFlow id="Flow_0dd6rd9" sourceRef="Task_0wvzeht" targetRef="ParallelGateway_1fjs5fh" />
    <bpmn:sequenceFlow id="Flow_04b27g5" sourceRef="Task_0enpu7i" targetRef="ParallelGateway_1fjs5fh" />
    <bpmn:sequenceFlow id="Flow_0uzt6b9" sourceRef="Task_05j6ajf" targetRef="ParallelGateway_1fjs5fh" />
    <bpmn:parallelGateway id="ParallelGateway_1fjs5fh">
      <bpmn:incoming>Flow_0dd6rd9</bpmn:incoming>
      <bpmn:incoming>Flow_04b27g5</bpmn:incoming>
      <bpmn:incoming>Flow_0uzt6b9</bpmn:incoming>
      <bpmn:outgoing>Flow_05qgj8n</bpmn:outgoing>
      <bpmn:outgoing>Flow_1dnqw30</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:task id="Task_0co3kqh" name="Task 2.1">
      <bpmn:incoming>Flow_1dnqw30</bpmn:incoming>
      <bpmn:outgoing>Flow_0z0w917</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0f27ao8" name="Task 2.2">
      <bpmn:incoming>Flow_05qgj8n</bpmn:incoming>
      <bpmn:outgoing>Flow_1il6zgb</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_05qgj8n" sourceRef="ParallelGateway_1fjs5fh" targetRef="Task_0f27ao8" />
    <bpmn:sequenceFlow id="Flow_1dnqw30" sourceRef="ParallelGateway_1fjs5fh" targetRef="Task_0co3kqh" />
    <bpmn:endEvent id="EndEvent_0xxe63e">
      <bpmn:incoming>Flow_0z0w917</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0z0w917" sourceRef="Task_0co3kqh" targetRef="EndEvent_0xxe63e" />
    <bpmn:endEvent id="EndEvent_0epyvhv">
      <bpmn:incoming>Flow_1il6zgb</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1il6zgb" sourceRef="Task_0f27ao8" targetRef="EndEvent_0epyvhv" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_EXCLUSIVE_XOR_GATEWAY_SIMPLE_LOOP = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:proceed="https://docs.proceed-labs.org/BPMN" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_9431fb00-b1da-4af4-9d6a-8d954060abf5" name="Simple Loop" targetNamespace="https://docs.proceed-labs.org/_9431fb00-b1da-4af4-9d6a-8d954060abf5" expressionLanguage="https://ecma-international.org/ecma-262/8.0/" typeLanguage="https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" exporter="PROCEED Management System" exporterVersion="1.0.0" proceed:creatorEnvironmentId="" proceed:creatorEnvironmentName="" xsi:schemaLocation="https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd">
  <process id="Process_1dooi9e" name="PROCEED Main Process" processType="Private" isExecutable="true" proceed:deploymentMethod="dynamic">
    <startEvent id="StartEvent_0l3khfx">
      <outgoing>Flow_0vu8z3r</outgoing>
    </startEvent>
    <exclusiveGateway id="Gateway_1ydo9ch">
      <incoming>Flow_0vu8z3r</incoming>
      <incoming>Flow_1k7fu3p</incoming>
      <outgoing>Flow_0yvqccu</outgoing>
    </exclusiveGateway>
    <sequenceFlow id="Flow_0vu8z3r" sourceRef="StartEvent_0l3khfx" targetRef="Gateway_1ydo9ch" />
    <sequenceFlow id="Flow_0yvqccu" sourceRef="Gateway_1ydo9ch" targetRef="Activity_1l4zfsg" />
    <exclusiveGateway id="Gateway_04kczqf" default="Flow_1k7fu3p">
      <incoming>Flow_0y08irt</incoming>
      <outgoing>Flow_19qa7a8</outgoing>
      <outgoing>Flow_1k7fu3p</outgoing>
    </exclusiveGateway>
    <sequenceFlow id="Flow_0y08irt" sourceRef="Activity_1l4zfsg" targetRef="Gateway_04kczqf" />
    <endEvent id="Event_1nb6v76">
      <incoming>Flow_19qa7a8</incoming>
    </endEvent>
    <sequenceFlow id="Flow_19qa7a8" sourceRef="Gateway_04kczqf" targetRef="Event_1nb6v76">
      <conditionExpression xsi:type="tFormalExpression">invocations &gt; 1</conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="Flow_1k7fu3p" sourceRef="Gateway_04kczqf" targetRef="Gateway_1ydo9ch" />
    <scriptTask id="Activity_1l4zfsg" scriptFormat="application/javascript">
      <incoming>Flow_0yvqccu</incoming>
      <outgoing>Flow_0y08irt</outgoing>
      <script>variable.set('invocations', variable.get('invocations') + 1);</script>
    </scriptTask>
  </process>
</definitions>`;

/**
 * Parallel Gateway
 *
 *                                                         -> Task_1p4tmqj ->
 * StartEvent_1 -> Task_0rav7hn -> ParallelGateway_1gb46mc -> Task_0j6jido -> ParallelGateway_1u9g4km -> EndEvent_02qfml5
 *                                                         -> Task_0l2ayv1 ->
 */
export const BPMN_XML_PARALLEL_GATEWAY = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1b6y0fm" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_0yklkyy</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:endEvent id="EndEvent_02qfml5">
      <bpmn:incoming>SequenceFlow_0ssjqj4</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:task id="Task_0rav7hn" name="Pre Split">
      <bpmn:incoming>SequenceFlow_0yklkyy</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_01das5j</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0yklkyy" sourceRef="StartEvent_1" targetRef="Task_0rav7hn" />
    <bpmn:sequenceFlow id="SequenceFlow_01das5j" sourceRef="Task_0rav7hn" targetRef="ParallelGateway_1gb46mc" />
    <bpmn:parallelGateway id="ParallelGateway_1gb46mc">
      <bpmn:incoming>SequenceFlow_01das5j</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0zcellh</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0glegwn</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_07jqmiu</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:task id="Task_1p4tmqj" name="Activity 1">
      <bpmn:incoming>SequenceFlow_0zcellh</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0fsi00m</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0zcellh" sourceRef="ParallelGateway_1gb46mc" targetRef="Task_1p4tmqj" />
    <bpmn:sequenceFlow id="SequenceFlow_0fsi00m" sourceRef="Task_1p4tmqj" targetRef="ParallelGateway_1u9g4km" />
    <bpmn:parallelGateway id="ParallelGateway_1u9g4km">
      <bpmn:incoming>SequenceFlow_0fsi00m</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0u19mqy</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_10n4zv7</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0ssjqj4</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:task id="Task_0j6jido" name="Activity 2">
      <bpmn:incoming>SequenceFlow_0glegwn</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0u19mqy</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0glegwn" sourceRef="ParallelGateway_1gb46mc" targetRef="Task_0j6jido" />
    <bpmn:sequenceFlow id="SequenceFlow_0u19mqy" sourceRef="Task_0j6jido" targetRef="ParallelGateway_1u9g4km" />
    <bpmn:task id="Task_0l2ayv1" name="Activity 3">
      <bpmn:incoming>SequenceFlow_07jqmiu</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_10n4zv7</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_07jqmiu" sourceRef="ParallelGateway_1gb46mc" targetRef="Task_0l2ayv1" />
    <bpmn:sequenceFlow id="SequenceFlow_10n4zv7" sourceRef="Task_0l2ayv1" targetRef="ParallelGateway_1u9g4km" />
    <bpmn:sequenceFlow id="SequenceFlow_0ssjqj4" sourceRef="ParallelGateway_1u9g4km" targetRef="EndEvent_02qfml5" />
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Inclusive Gateway
 *                                          -> (var1) -> Task_1i7zw55 -> Task_0tzw41g ------------------->
 * StartEvent_1 -> InclusiveGateway_1s9snq4 -> (var2) -> Task_1u5n9uw -----------------------------------> InclusiveGateway_0qdieps -> EndEvent_07rysvv
 *                                          -> (var3) -> Task_0tndtci -> Task_0vldujn ->
 *                                          -> (var4) -> Task_0ko2baa -> EndEvent_13juctc
 */
export const BPMN_XML_INCLUSIVE_GATEWAY = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="Definitions_0or326w" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1" name="Start">
      <bpmn:outgoing>SequenceFlow_0wterhl</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1i7zw55" name="Task 1">
      <bpmn:incoming>SequenceFlow_0yv17gb</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0gelete</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0yv17gb" sourceRef="InclusiveGateway_1s9snq4" targetRef="Task_1i7zw55">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">var1</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:inclusiveGateway id="InclusiveGateway_1s9snq4" default="SequenceFlow_0oekgd5">
      <bpmn:incoming>SequenceFlow_0wterhl</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0yv17gb</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_1tnox2d</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_1mifor4</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0oekgd5</bpmn:outgoing>
    </bpmn:inclusiveGateway>
    <bpmn:task id="Task_1u5n9uw" name="Task 2">
      <bpmn:incoming>SequenceFlow_1tnox2d</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1t0ixak</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0tndtci" name="Task 3">
      <bpmn:incoming>SequenceFlow_1mifor4</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1ys0mxj</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1tnox2d" sourceRef="InclusiveGateway_1s9snq4" targetRef="Task_1u5n9uw">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">var2</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1mifor4" sourceRef="InclusiveGateway_1s9snq4" targetRef="Task_0tndtci">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">var3</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0ko2baa" name="Task 4">
      <bpmn:incoming>SequenceFlow_0oekgd5</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_16wjbxs</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0oekgd5" sourceRef="InclusiveGateway_1s9snq4" targetRef="Task_0ko2baa">
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0wterhl" sourceRef="StartEvent_1" targetRef="InclusiveGateway_1s9snq4" />
    <bpmn:inclusiveGateway id="InclusiveGateway_0qdieps">
      <bpmn:incoming>SequenceFlow_1t0ixak</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1dubzug</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_07i2lvv</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0swggtx</bpmn:outgoing>
    </bpmn:inclusiveGateway>
    <bpmn:endEvent id="EndEvent_07rysvv" name="End">
      <bpmn:incoming>SequenceFlow_0swggtx</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0swggtx" sourceRef="InclusiveGateway_0qdieps" targetRef="EndEvent_07rysvv" />
    <bpmn:task id="Task_0tzw41g" name="Task 1a">
      <bpmn:incoming>SequenceFlow_0gelete</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1dubzug</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0gelete" sourceRef="Task_1i7zw55" targetRef="Task_0tzw41g" />
    <bpmn:sequenceFlow id="SequenceFlow_1t0ixak" sourceRef="Task_1u5n9uw" targetRef="InclusiveGateway_0qdieps" />
    <bpmn:sequenceFlow id="SequenceFlow_1dubzug" sourceRef="Task_0tzw41g" targetRef="InclusiveGateway_0qdieps" />
    <bpmn:endEvent id="EndEvent_13juctc">
      <bpmn:incoming>SequenceFlow_16wjbxs</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_16wjbxs" sourceRef="Task_0ko2baa" targetRef="EndEvent_13juctc" />
    <bpmn:sequenceFlow id="SequenceFlow_1ys0mxj" sourceRef="Task_0tndtci" targetRef="Task_0vldujn" />
    <bpmn:sequenceFlow id="SequenceFlow_07i2lvv" sourceRef="Task_0vldujn" targetRef="InclusiveGateway_0qdieps" />
    <bpmn:task id="Task_0vldujn" name="Task 3a">
      <bpmn:incoming>SequenceFlow_1ys0mxj</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_07i2lvv</bpmn:outgoing>
    </bpmn:task>
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Message, Signal and Timer intermediate catch events
 *
 * StartEvent_1 -> IntermediateCatchEvent_0wpudxa (Message_1m7hrlz) -> Task_0twpunq -> IntermediateCatchEvent_0o02gmp (Signal_1gcpp7v) -> // next line
 * Task_017n45t -> IntermediateCatchEvent_09pxge5 (TimerDuration PT2S) -> Task_141i1em -> EndEvent_03gcun2
 */
export const BPMN_XML_INTERMEDIATE_CATCH = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="Definitions_0810ppt" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_06rz3px</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_06rz3px" sourceRef="StartEvent_1" targetRef="IntermediateCatchEvent_0wpudxa" />
    <bpmn:task id="Task_0twpunq" name="After Message">
      <bpmn:incoming>SequenceFlow_0aj6p9w</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_14aau7v</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0aj6p9w" sourceRef="IntermediateCatchEvent_0wpudxa" targetRef="Task_0twpunq" />
    <bpmn:sequenceFlow id="SequenceFlow_14aau7v" sourceRef="Task_0twpunq" targetRef="IntermediateCatchEvent_0o02gmp" />
    <bpmn:task id="Task_017n45t" name="After Signal">
      <bpmn:incoming>SequenceFlow_0te89vi</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0b8wb69</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0te89vi" sourceRef="IntermediateCatchEvent_0o02gmp" targetRef="Task_017n45t" />
    <bpmn:task id="Task_141i1em" name="After Timer">
      <bpmn:incoming>SequenceFlow_0berg2o</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_08u409x</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0b8wb69" sourceRef="Task_017n45t" targetRef="IntermediateCatchEvent_09pxge5" />
    <bpmn:sequenceFlow id="SequenceFlow_0berg2o" sourceRef="IntermediateCatchEvent_09pxge5" targetRef="Task_141i1em" />
    <bpmn:endEvent id="EndEvent_03gcun2">
      <bpmn:incoming>SequenceFlow_08u409x</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_08u409x" sourceRef="Task_141i1em" targetRef="EndEvent_03gcun2" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_0wpudxa">
      <bpmn:incoming>SequenceFlow_06rz3px</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0aj6p9w</bpmn:outgoing>
      <bpmn:messageEventDefinition messageRef="Message_1m7hrlz" />
    </bpmn:intermediateCatchEvent>
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_0o02gmp">
      <bpmn:incoming>SequenceFlow_14aau7v</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0te89vi</bpmn:outgoing>
      <bpmn:signalEventDefinition signalRef="Signal_1gcpp7v" />
    </bpmn:intermediateCatchEvent>
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_09pxge5">
      <bpmn:incoming>SequenceFlow_0b8wb69</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0berg2o</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT2S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:intermediateCatchEvent>
  </bpmn:process>
  <bpmn:signal id="Signal_1gcpp7v" name="signal1" />
  <bpmn:message id="Message_1m7hrlz" name="message1" />
</bpmn:definitions>`;

export const BPMN_XML_INTERMEDIATE_THROW_SIGNAL = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0g8yixj" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:signal id="Signal_1qpz0p4" name="Signal_0pqcu92" />
  <bpmn:process id="Process_0r4b3hz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_019sxaa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_12j1bnp">
      <bpmn:incoming>Flow_019sxaa</bpmn:incoming>
      <bpmn:outgoing>Flow_1wkavkt</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_019sxaa" sourceRef="StartEvent_1" targetRef="Task_12j1bnp" />
    <bpmn:sequenceFlow id="Flow_1wkavkt" sourceRef="Task_12j1bnp" targetRef="ThrowEvent_05wouxt" />
    <bpmn:task id="Task_0d6e1ff">
      <bpmn:incoming>Flow_14ql0x8</bpmn:incoming>
      <bpmn:outgoing>Flow_13ojb8k</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_14ql0x8" sourceRef="ThrowEvent_05wouxt" targetRef="Task_0d6e1ff" />
    <bpmn:endEvent id="EndEvent_19zpdym">
      <bpmn:incoming>Flow_13ojb8k</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_13ojb8k" sourceRef="Task_0d6e1ff" targetRef="EndEvent_19zpdym" />
    <bpmn:intermediateThrowEvent id="ThrowEvent_05wouxt">
      <bpmn:incoming>Flow_1wkavkt</bpmn:incoming>
      <bpmn:outgoing>Flow_14ql0x8</bpmn:outgoing>
      <bpmn:signalEventDefinition signalRef="Signal_1qpz0p4" />
    </bpmn:intermediateThrowEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_INTERMEDIATE_THROW_MESSAGE = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0g8yixj" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:message id="Message_09n54fw" name="Message_09qlqm2" />
  <bpmn:process id="Process_0r4b3hz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_019sxaa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_12j1bnp">
      <bpmn:incoming>Flow_019sxaa</bpmn:incoming>
      <bpmn:outgoing>Flow_1wkavkt</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_019sxaa" sourceRef="StartEvent_1" targetRef="Task_12j1bnp" />
    <bpmn:sequenceFlow id="Flow_1wkavkt" sourceRef="Task_12j1bnp" targetRef="ThrowEvent_05wouxt" />
    <bpmn:task id="Task_0d6e1ff">
      <bpmn:incoming>Flow_14ql0x8</bpmn:incoming>
      <bpmn:outgoing>Flow_13ojb8k</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_14ql0x8" sourceRef="ThrowEvent_05wouxt" targetRef="Task_0d6e1ff" />
    <bpmn:endEvent id="EndEvent_19zpdym">
      <bpmn:incoming>Flow_13ojb8k</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_13ojb8k" sourceRef="Task_0d6e1ff" targetRef="EndEvent_19zpdym" />
    <bpmn:intermediateThrowEvent id="ThrowEvent_05wouxt">
      <bpmn:incoming>Flow_1wkavkt</bpmn:incoming>
      <bpmn:outgoing>Flow_14ql0x8</bpmn:outgoing>
      <bpmn:messageEventDefinition messageRef="Message_09n54fw" />
    </bpmn:intermediateThrowEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_INTERMEDIATE_THROW_ESCALATION = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0g8yixj" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0r4b3hz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_019sxaa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_12j1bnp">
      <bpmn:incoming>Flow_019sxaa</bpmn:incoming>
      <bpmn:outgoing>Flow_1wkavkt</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_019sxaa" sourceRef="StartEvent_1" targetRef="Task_12j1bnp" />
    <bpmn:sequenceFlow id="Flow_1wkavkt" sourceRef="Task_12j1bnp" targetRef="ThrowEvent_05wouxt" />
    <bpmn:task id="Task_0d6e1ff">
      <bpmn:incoming>Flow_14ql0x8</bpmn:incoming>
      <bpmn:outgoing>Flow_13ojb8k</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_14ql0x8" sourceRef="ThrowEvent_05wouxt" targetRef="Task_0d6e1ff" />
    <bpmn:endEvent id="EndEvent_19zpdym">
      <bpmn:incoming>Flow_13ojb8k</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_13ojb8k" sourceRef="Task_0d6e1ff" targetRef="EndEvent_19zpdym" />
    <bpmn:intermediateThrowEvent id="ThrowEvent_05wouxt">
      <bpmn:incoming>Flow_1wkavkt</bpmn:incoming>
      <bpmn:outgoing>Flow_14ql0x8</bpmn:outgoing>
      <bpmn:escalationEventDefinition id="EscalationEventDefinition_15z8yko" />
    </bpmn:intermediateThrowEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_INTERMEDIATE_THROW_NONE = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0g8yixj" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0r4b3hz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_019sxaa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_12j1bnp">
      <bpmn:incoming>Flow_019sxaa</bpmn:incoming>
      <bpmn:outgoing>Flow_1wkavkt</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_019sxaa" sourceRef="StartEvent_1" targetRef="Task_12j1bnp" />
    <bpmn:sequenceFlow id="Flow_1wkavkt" sourceRef="Task_12j1bnp" targetRef="ThrowEvent_05wouxt" />
    <bpmn:task id="Task_0d6e1ff">
      <bpmn:incoming>Flow_14ql0x8</bpmn:incoming>
      <bpmn:outgoing>Flow_13ojb8k</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_14ql0x8" sourceRef="ThrowEvent_05wouxt" targetRef="Task_0d6e1ff" />
    <bpmn:endEvent id="EndEvent_19zpdym">
      <bpmn:incoming>Flow_13ojb8k</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_13ojb8k" sourceRef="Task_0d6e1ff" targetRef="EndEvent_19zpdym" />
    <bpmn:intermediateThrowEvent id="ThrowEvent_05wouxt">
      <bpmn:incoming>Flow_1wkavkt</bpmn:incoming>
      <bpmn:outgoing>Flow_14ql0x8</bpmn:outgoing>
    </bpmn:intermediateThrowEvent>
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Script Task
 *
 * StartEvent_1 -> Task_0rozrh3 -> Task_1u9njea -> EndEvent_0mpeg2m
 *
 * Task_0rozrh3 - returns undefined and logs variables.x, variables.y and a simple hello world log.info
 * Task_1u9njea - returns the value { a: variables.a + 10 } after 2.5 seconds
 */
export const BPMN_XML_SCRIPT_TASK = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1h5w845" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_17hik6f</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_17hik6f" sourceRef="StartEvent_1" targetRef="Task_0rozrh3" />
    <bpmn:scriptTask id="Task_0rozrh3" name="Normal Task">
      <bpmn:incoming>SequenceFlow_17hik6f</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0t9daoz</bpmn:outgoing>
      <bpmn:script>
log.info('Hello');
log.info(\`Variables: \${variable.get('x')} \${variable.get('y')}\`);
</bpmn:script>
    </bpmn:scriptTask>
    <bpmn:endEvent id="EndEvent_0mpeg2m">
      <bpmn:incoming>SequenceFlow_12c9c6t</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0t9daoz" sourceRef="Task_0rozrh3" targetRef="Task_1u9njea" />
    <bpmn:scriptTask id="Task_1u9njea" name="Promise Task">
      <bpmn:incoming>SequenceFlow_0t9daoz</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_12c9c6t</bpmn:outgoing>
      <bpmn:script>return new Promise((resolve) => {
          setTimeout(() => resolve({ a: variable.get('a') + 10 }), 2500);
      })</bpmn:script>
    </bpmn:scriptTask>
    <bpmn:sequenceFlow id="SequenceFlow_12c9c6t" sourceRef="Task_1u9njea" targetRef="EndEvent_0mpeg2m" />
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Script Task where script sets variable using setVar() call
 *
 * StartEvent_1 -> Task_0rozrh3 -> Task_1u9njea -> EndEvent_0mpeg2m
 *
 * Task_1u9njea - sets the variable { a: getVar('a') + 10 } after 2.5 seconds
 */
export const BPMN_XML_SCRIPT_TASK_SET_VAR = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1h5w845" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_17hik6f</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_17hik6f" sourceRef="StartEvent_1" targetRef="Task_0rozrh3" />
    <bpmn:scriptTask id="Task_0rozrh3" name="Normal Task">
      <bpmn:incoming>SequenceFlow_17hik6f</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0t9daoz</bpmn:outgoing>
      <bpmn:script>
log.info('Hello');
log.info(\`Variables: \${variable.get('x')} \${variable.get('y')}\`);</bpmn:script>
    </bpmn:scriptTask>
    <bpmn:endEvent id="EndEvent_0mpeg2m">
      <bpmn:incoming>SequenceFlow_12c9c6t</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0t9daoz" sourceRef="Task_0rozrh3" targetRef="Task_1u9njea" />
    <bpmn:scriptTask id="Task_1u9njea" name="Promise Task">
      <bpmn:incoming>SequenceFlow_0t9daoz</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_12c9c6t</bpmn:outgoing>
      <bpmn:script>return new Promise((resolve) =&gt; {
    variable.set('a', variable.get('a') + 10);
    setTimeout(() => resolve({}), 2500);
})</bpmn:script>
    </bpmn:scriptTask>
    <bpmn:sequenceFlow id="SequenceFlow_12c9c6t" sourceRef="Task_1u9njea" targetRef="EndEvent_0mpeg2m" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SCRIPT_TASK_MULTIPLE_VAR = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_3fc415fc-fa49-44f3-9f5b-f459cc55fd08" name="Test" targetNamespace="https://docs.proceed-labs.org/_3fc415fc-fa49-44f3-9f5b-f459cc55fd08" exporter="PROCEED Management System" exporterVersion="1.0.0">
  <process id="Process_1gcn08i" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <startEvent id="StartEvent_0knyffd">
      <outgoing>Flow_1kzpfjw</outgoing>
    </startEvent>
    <sequenceFlow id="Flow_1kzpfjw" sourceRef="StartEvent_0knyffd" targetRef="Activity_045n21g" />
    <sequenceFlow id="Flow_152imfp" sourceRef="Activity_045n21g" targetRef="Activity_0r4rf4b" />
    <endEvent id="Event_0xk1gqb">
      <incoming>Flow_0kp5lbh</incoming>
    </endEvent>
    <sequenceFlow id="Flow_0kp5lbh" sourceRef="Activity_0r4rf4b" targetRef="Event_0xk1gqb" />
    <scriptTask id="Activity_045n21g" scriptFormat="application/javascript">
      <incoming>Flow_1kzpfjw</incoming>
      <outgoing>Flow_152imfp</outgoing>
<script>
  variable.set('a', 10);
</script>
    </scriptTask>
    <scriptTask id="Activity_0r4rf4b" scriptFormat="application/javascript">
      <incoming>Flow_152imfp</incoming>
      <outgoing>Flow_0kp5lbh</outgoing>
<script>
  variable.set('a', variable.get('a') + 10);
  variable.set('b', 'Test');
</script>
    </scriptTask>
  </process>
</definitions>`;

/**
 * Script Task where script provocates a semantic Error
 */
export const BPMN_XML_SCRIPT_TASK_SEMANTIC_ERROR = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_13b0ewy" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_1qig2aj" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wj8jmp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1wj8jmp" sourceRef="StartEvent_1" targetRef="Task_1egm6i7" />
    <bpmn:endEvent id="EndEvent_17n90cf">
      <bpmn:incoming>Flow_0ih9ixy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0ih9ixy" sourceRef="Task_1egm6i7" targetRef="EndEvent_17n90cf" />
    <bpmn:scriptTask id="Task_1egm6i7">
      <bpmn:incoming>Flow_1wj8jmp</bpmn:incoming>
      <bpmn:outgoing>Flow_0ih9ixy</bpmn:outgoing>
      <bpmn:script>
      variable.set('a', 10);
      throw new BpmnError('error', 'test error');
      </bpmn:script>
    </bpmn:scriptTask>
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Script Task where script provocates a semantic Error after writing to the global state
 */
export const BPMN_XML_SCRIPT_TASK_SEMANTIC_ERROR_GLOBAL_VARIABLE = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_13b0ewy" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_1qig2aj" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wj8jmp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1wj8jmp" sourceRef="StartEvent_1" targetRef="Task_1egm6i7" />
    <bpmn:endEvent id="EndEvent_17n90cf">
      <bpmn:incoming>Flow_0ih9ixy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0ih9ixy" sourceRef="Task_1egm6i7" targetRef="EndEvent_17n90cf" />
    <bpmn:scriptTask id="Task_1egm6i7">
      <bpmn:incoming>Flow_1wj8jmp</bpmn:incoming>
      <bpmn:outgoing>Flow_0ih9ixy</bpmn:outgoing>
      <bpmn:script>
      variable.setGlobal('a', 10);
      throw new BpmnError('error', 'test error');
      </bpmn:script>
    </bpmn:scriptTask>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SCRIPT_TASK_ACCESS_GLOBAL_VARIABLE = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_13b0ewy" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_1qig2aj" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wj8jmp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1wj8jmp" sourceRef="StartEvent_1" targetRef="Task_1egm6i7" />
    <bpmn:endEvent id="EndEvent_17n90cf">
      <bpmn:incoming>Flow_0ih9ixy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0ih9ixy" sourceRef="Task_1egm6i7" targetRef="EndEvent_17n90cf" />
    <bpmn:scriptTask id="Task_1egm6i7">
      <bpmn:incoming>Flow_1wj8jmp</bpmn:incoming>
      <bpmn:outgoing>Flow_0ih9ixy</bpmn:outgoing>
      <bpmn:script>
      variable.set('a', 10);
      variable.set('b', variable.get('a') + variable.getGlobal('a'));
      </bpmn:script>
    </bpmn:scriptTask>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SCRIPT_TASK_ACCESS_ALL_GLOBAL_VARIABLES = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_13b0ewy" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_1qig2aj" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wj8jmp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1wj8jmp" sourceRef="StartEvent_1" targetRef="Task_1egm6i7" />
    <bpmn:endEvent id="EndEvent_17n90cf">
      <bpmn:incoming>Flow_0ih9ixy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0ih9ixy" sourceRef="Task_1egm6i7" targetRef="EndEvent_17n90cf" />
    <bpmn:scriptTask id="Task_1egm6i7">
      <bpmn:incoming>Flow_1wj8jmp</bpmn:incoming>
      <bpmn:outgoing>Flow_0ih9ixy</bpmn:outgoing>
      <bpmn:script>
        const variables = variable.getAll();
        Object.keys(variables).forEach(key => variable.set(key, 1));

        const locallyChangedVariables = variable.getAll();
        let sum = 0;
        Object.values(locallyChangedVariables).forEach(val => sum += val);
        variable.set('sum', sum);

        let globalVariables = variable.getAllGlobal();
        let globalSum = 0;
        Object.values(globalVariables).forEach(val => globalSum += val);
        variable.set('globalSum', globalSum);
      </bpmn:script>
    </bpmn:scriptTask>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SCRIPT_TASK_ERROR_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_114jsk5" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:error id="error" escalationCode="EXAMPLE-CODE" /> 
  <bpmn:process id="Process_00s66o7" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_08zjpoa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_08zjpoa" sourceRef="StartEvent_1" targetRef="Task_0ob7s6d" />
    <bpmn:endEvent id="EndEvent_0bmqmy5">
      <bpmn:incoming>Flow_1oy8rkm</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1oy8rkm" sourceRef="Task_0ob7s6d" targetRef="EndEvent_0bmqmy5" />
    <bpmn:boundaryEvent id="BoundaryEvent_11ew6jk" attachedToRef="Task_0ob7s6d">
      <bpmn:outgoing>Flow_0zqane2</bpmn:outgoing>
      <bpmn:errorEventDefinition errorRef="error" id="ErrorEventDefinition_1204wys" />
    </bpmn:boundaryEvent>
    <bpmn:task id="Task_0ve1o20">
      <bpmn:incoming>Flow_0zqane2</bpmn:incoming>
      <bpmn:outgoing>Flow_1j8azpy</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0zqane2" sourceRef="BoundaryEvent_11ew6jk" targetRef="Task_0ve1o20" />
    <bpmn:endEvent id="EndEvent_01w8uee">
      <bpmn:incoming>Flow_1j8azpy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1j8azpy" sourceRef="Task_0ve1o20" targetRef="EndEvent_01w8uee" />
    <bpmn:scriptTask id="Task_0ob7s6d">
      <bpmn:incoming>Flow_08zjpoa</bpmn:incoming>
      <bpmn:outgoing>Flow_1oy8rkm</bpmn:outgoing>
      <bpmn:script>
      variable.set('a', 10);
      throw new BpmnError('error', 'test error');
      </bpmn:script>
    </bpmn:scriptTask>
  </bpmn:process>
  </bpmn:definitions>`;

export const BPMN_XML_SCRIPT_TASK_INTERRUPTING_ESCALATION_BOUNDARY_EVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_114jsk5" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:escalation id="escalation" escalationCode="EXAMPLE-CODE" />
  <bpmn:process id="Process_00s66o7" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_08zjpoa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_08zjpoa" sourceRef="StartEvent_1" targetRef="Task_0ob7s6d" />
    <bpmn:endEvent id="EndEvent_0bmqmy5">
      <bpmn:incoming>Flow_1oy8rkm</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1oy8rkm" sourceRef="Task_0ob7s6d" targetRef="EndEvent_0bmqmy5" />
    <bpmn:task id="Task_0ve1o20">
      <bpmn:incoming>Flow_0zqane2</bpmn:incoming>
      <bpmn:outgoing>Flow_1j8azpy</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0zqane2" sourceRef="BoundaryEvent_11ew6jk" targetRef="Task_0ve1o20" />
    <bpmn:endEvent id="EndEvent_01w8uee">
      <bpmn:incoming>Flow_1j8azpy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1j8azpy" sourceRef="Task_0ve1o20" targetRef="EndEvent_01w8uee" />
    <bpmn:scriptTask id="Task_0ob7s6d">
      <bpmn:incoming>Flow_08zjpoa</bpmn:incoming>
      <bpmn:outgoing>Flow_1oy8rkm</bpmn:outgoing>
      <bpmn:script>
        variable.set('a', 10);
        throw new BpmnEscalation('EXAMPLE-CODE', 'test escalation');
      </bpmn:script>
    </bpmn:scriptTask>
    <bpmn:boundaryEvent id="BoundaryEvent_11ew6jk" attachedToRef="Task_0ob7s6d">
      <bpmn:outgoing>Flow_0zqane2</bpmn:outgoing>
      <bpmn:escalationEventDefinition escalationRef="escalation" id="EscalationEventDefinition_0v9v0tx" />
    </bpmn:boundaryEvent>
  </bpmn:process>
  </bpmn:definitions>`;

export const BPMN_XML_SCRIPT_TASK_NON_INTERRUPTING_ESCALATION_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_114jsk5" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_00s66o7" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_08zjpoa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_08zjpoa" sourceRef="StartEvent_1" targetRef="Task_0ob7s6d" />
    <bpmn:endEvent id="EndEvent_0bmqmy5">
      <bpmn:incoming>Flow_1oy8rkm</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1oy8rkm" sourceRef="Task_0ob7s6d" targetRef="EndEvent_0bmqmy5" />
    <bpmn:task id="Task_0ve1o20">
      <bpmn:incoming>Flow_0zqane2</bpmn:incoming>
      <bpmn:outgoing>Flow_1j8azpy</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0zqane2" sourceRef="BoundaryEvent_11ew6jk" targetRef="Task_0ve1o20" />
    <bpmn:endEvent id="EndEvent_01w8uee">
      <bpmn:incoming>Flow_1j8azpy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1j8azpy" sourceRef="Task_0ve1o20" targetRef="EndEvent_01w8uee" />
    <bpmn:scriptTask id="Task_0ob7s6d">
      <bpmn:incoming>Flow_08zjpoa</bpmn:incoming>
      <bpmn:outgoing>Flow_1oy8rkm</bpmn:outgoing>
      <bpmn:script>
        throw new BpmnEscalation('escalation', 'test escalation');
      </bpmn:script>
    </bpmn:scriptTask>
    <bpmn:boundaryEvent id="BoundaryEvent_11ew6jk" cancelActivity="false" attachedToRef="Task_0ob7s6d">
      <bpmn:outgoing>Flow_0zqane2</bpmn:outgoing>
      <bpmn:escalationEventDefinition id="EscalationEventDefinition_1l9zx8b" />
    </bpmn:boundaryEvent>
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Script Task where script provocates a technical Error
 *
 */
export const BPMN_XML_SCRIPT_TASK_TECHNICAL_ERROR = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_13b0ewy" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_1qig2aj" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wj8jmp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1wj8jmp" sourceRef="StartEvent_1" targetRef="Task_1egm6i7" />
    <bpmn:endEvent id="EndEvent_17n90cf">
      <bpmn:incoming>Flow_0ih9ixy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0ih9ixy" sourceRef="Task_1egm6i7" targetRef="EndEvent_17n90cf" />
    <bpmn:scriptTask id="Task_1egm6i7">
      <bpmn:incoming>Flow_1wj8jmp</bpmn:incoming>
      <bpmn:outgoing>Flow_0ih9ixy</bpmn:outgoing>
      <bpmn:script>
      variable.set('a', 10);
      provocating syntax error
      </bpmn:script>
    </bpmn:scriptTask>
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Script Task using service
 *
 * StartEvent_1 -> Task_0rozrh3 -> Task_1u9njea -> EndEvent_0mpeg2m
 *
 * Task_0rozrh3 - returns undefined and logs variables.x, variables.y and a simple hello world log.info
 * Task_1u9njea - returns the value { a: variables.a + 10 } after 2.5 seconds
 */
export const BPMN_XML_SCRIPT_TASK_USING_SERVICE = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1h5w845" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_17hik6f</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_17hik6f" sourceRef="StartEvent_1" targetRef="Task_0rozrh3" />
    <bpmn:scriptTask id="Task_0rozrh3" name="Normal Task">
      <bpmn:incoming>SequenceFlow_17hik6f</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0t9daoz</bpmn:outgoing>
      <bpmn:script>
      getService('com.proceed.email').send('hello', 'world');
      </bpmn:script>
    </bpmn:scriptTask>
    <bpmn:endEvent id="EndEvent_0mpeg2m">
      <bpmn:incoming>SequenceFlow_12c9c6t</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0t9daoz" sourceRef="Task_0rozrh3" targetRef="Task_1u9njea" />
    <bpmn:scriptTask id="Task_1u9njea" name="Promise Task">
      <bpmn:incoming>SequenceFlow_0t9daoz</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_12c9c6t</bpmn:outgoing>
      <bpmn:script>return new Promise((resolve) => {
    setTimeout(() => resolve({ a: variable.get('a') + 10 }), 2500);
})</bpmn:script>
    </bpmn:scriptTask>
    <bpmn:sequenceFlow id="SequenceFlow_12c9c6t" sourceRef="Task_1u9njea" targetRef="EndEvent_0mpeg2m" />
  </bpmn:process>
</bpmn:definitions>`;

/**
 * User Task
 *
 * StartEvent_1 -> UserTask_1lmr4l7 -> EndEvent_0bzp8ym
 */
export const BPMN_XML_USER_TASK = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" id="Definitions_0ctpy90" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_0gm2ucm</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:endEvent id="EndEvent_0bzp8ym">
      <bpmn:incoming>SequenceFlow_0vq5x2o</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:userTask id="UserTask_1lmr4l7" name="User Task 1" camunda:candidateUsers="test-user" camunda:candidateGroups="admin">
      <bpmn:incoming>SequenceFlow_0gm2ucm</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0vq5x2o</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:sequenceFlow id="SequenceFlow_0gm2ucm" sourceRef="StartEvent_1" targetRef="UserTask_1lmr4l7" />
    <bpmn:sequenceFlow id="SequenceFlow_0vq5x2o" sourceRef="UserTask_1lmr4l7" targetRef="EndEvent_0bzp8ym" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_PARALLEL_USER_TASKS = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0hpv1zo" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0pho5iv" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1w30vuw</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1w30vuw" sourceRef="StartEvent_1" targetRef="ParallelGateway_1jop76m" />
    <bpmn:parallelGateway id="ParallelGateway_1jop76m">
      <bpmn:incoming>Flow_1w30vuw</bpmn:incoming>
      <bpmn:outgoing>Flow_1cwddpk</bpmn:outgoing>
      <bpmn:outgoing>Flow_1cpl8b2</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:sequenceFlow id="Flow_1cwddpk" sourceRef="ParallelGateway_1jop76m" targetRef="Task_05qgw49" />
    <bpmn:sequenceFlow id="Flow_1cpl8b2" sourceRef="ParallelGateway_1jop76m" targetRef="Task_1qvidtq" />
    <bpmn:sequenceFlow id="Flow_1ia5gkb" sourceRef="Task_05qgw49" targetRef="Task_0gyr7j5" />
    <bpmn:sequenceFlow id="Flow_1yeu05h" sourceRef="Task_1qvidtq" targetRef="Task_1ur9af5" />
    <bpmn:endEvent id="EndEvent_18yp3ae">
      <bpmn:incoming>Flow_1kfyh4y</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1kfyh4y" sourceRef="Task_0gyr7j5" targetRef="EndEvent_18yp3ae" />
    <bpmn:endEvent id="EndEvent_1xh95hx">
      <bpmn:incoming>Flow_0h3puik</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0h3puik" sourceRef="Task_1ur9af5" targetRef="EndEvent_1xh95hx" />
    <bpmn:userTask id="Task_05qgw49" name="User Task 1.1">
      <bpmn:incoming>Flow_1cwddpk</bpmn:incoming>
      <bpmn:outgoing>Flow_1ia5gkb</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:userTask id="Task_1qvidtq" name="User Task 1.2">
      <bpmn:incoming>Flow_1cpl8b2</bpmn:incoming>
      <bpmn:outgoing>Flow_1yeu05h</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:userTask id="Task_0gyr7j5" name="User Task 2.1">
      <bpmn:incoming>Flow_1ia5gkb</bpmn:incoming>
      <bpmn:outgoing>Flow_1kfyh4y</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:userTask id="Task_1ur9af5" name="User Task 2.2">
      <bpmn:incoming>Flow_1yeu05h</bpmn:incoming>
      <bpmn:outgoing>Flow_0h3puik</bpmn:outgoing>
    </bpmn:userTask>
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Parallel gateway with message catch event on one branch
 *
 * StartEvent_1 -> ParallelGateway_1x7cdyj -> Task_1ifk6ou (Task A1) -> IntermediateCatchEvent_1ws4k2m (Message_19v7p84) -> Task_00y2su1 (Task A2) -> (SequenceFlow_1kj0u4l) -> -> ParallelGateway_1ax8dnq -> (SequenceFlow_1kj0u4l) -> EndEvent_0rqs75c
 *                                         -> Task_1huc8jb (Task B1) -----------------------------------------------------> Task_097exiz (Task B2) ->
 */
export const BPMN_XML_PARALLEL_GATEWAY_MESSAGE_CATCH = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_0ctpy90" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_0hezcp7</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0hezcp7" sourceRef="StartEvent_1" targetRef="ParallelGateway_1x7cdyj" />
    <bpmn:parallelGateway id="ParallelGateway_1x7cdyj">
      <bpmn:incoming>SequenceFlow_0hezcp7</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0f7zy3x</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_1aqiqqx</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:task id="Task_1ifk6ou" name="Task A1">
      <bpmn:incoming>SequenceFlow_0f7zy3x</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0tmbo60</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_0rqs75c">
      <bpmn:incoming>SequenceFlow_1eo4vny</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0f7zy3x" sourceRef="ParallelGateway_1x7cdyj" targetRef="Task_1ifk6ou" />
    <bpmn:task id="Task_1huc8jb" name="Task B1">
      <bpmn:incoming>SequenceFlow_1aqiqqx</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0gu3uzv</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1aqiqqx" sourceRef="ParallelGateway_1x7cdyj" targetRef="Task_1huc8jb" />
    <bpmn:task id="Task_00y2su1" name="Task A2">
      <bpmn:incoming>SequenceFlow_15d8b4e</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1kj0u4l</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_097exiz" name="Task B2">
      <bpmn:incoming>SequenceFlow_0gu3uzv</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0jhmowl</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0gu3uzv" sourceRef="Task_1huc8jb" targetRef="Task_097exiz" />
    <bpmn:parallelGateway id="ParallelGateway_1ax8dnq">
      <bpmn:incoming>SequenceFlow_1kj0u4l</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0jhmowl</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1eo4vny</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:sequenceFlow id="SequenceFlow_1kj0u4l" sourceRef="Task_00y2su1" targetRef="ParallelGateway_1ax8dnq" />
    <bpmn:sequenceFlow id="SequenceFlow_0jhmowl" sourceRef="Task_097exiz" targetRef="ParallelGateway_1ax8dnq" />
    <bpmn:sequenceFlow id="SequenceFlow_1eo4vny" sourceRef="ParallelGateway_1ax8dnq" targetRef="EndEvent_0rqs75c" />
    <bpmn:sequenceFlow id="SequenceFlow_0tmbo60" sourceRef="Task_1ifk6ou" targetRef="IntermediateCatchEvent_1ws4k2m" />
    <bpmn:sequenceFlow id="SequenceFlow_15d8b4e" sourceRef="IntermediateCatchEvent_1ws4k2m" targetRef="Task_00y2su1" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_1ws4k2m">
      <bpmn:incoming>SequenceFlow_0tmbo60</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_15d8b4e</bpmn:outgoing>
      <bpmn:messageEventDefinition messageRef="Message_1v97zi4" />
    </bpmn:intermediateCatchEvent>
  </bpmn:process>
  <bpmn:message id="Message_1v97zi4" name="Message_19v7p84" />
</bpmn:definitions>`;

/**
 * Terminate end event with token stopped at a gateway and waiting on a duration timer catch
 */

export const BPMN_XML_TERMINATE_END_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_19w9rwf" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0ylzer8" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1lgdoy9</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1lgdoy9" sourceRef="StartEvent_1" targetRef="ParallelGateway_198xm9v" />
    <bpmn:parallelGateway id="ParallelGateway_198xm9v">
      <bpmn:incoming>Flow_1lgdoy9</bpmn:incoming>
      <bpmn:outgoing>Flow_01yimxh</bpmn:outgoing>
      <bpmn:outgoing>Flow_0k1si90</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:task id="Task_0a5ear3">
      <bpmn:incoming>Flow_01yimxh</bpmn:incoming>
      <bpmn:outgoing>Flow_04yeb4k</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_01yimxh" sourceRef="ParallelGateway_198xm9v" targetRef="Task_0a5ear3" />
    <bpmn:sequenceFlow id="Flow_04yeb4k" sourceRef="Task_0a5ear3" targetRef="EndEvent_0x3jocc" />
    <bpmn:endEvent id="EndEvent_0x3jocc">
      <bpmn:incoming>Flow_04yeb4k</bpmn:incoming>
      <bpmn:terminateEventDefinition id="TerminateEventDefinition_0zfgdav" />
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0k1si90" sourceRef="ParallelGateway_198xm9v" targetRef="IntermediateCatchEvent_103sfzn" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_103sfzn">
      <bpmn:incoming>Flow_0k1si90</bpmn:incoming>
      <bpmn:outgoing>Flow_0lwndql</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration>PT5S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:intermediateCatchEvent>
    <bpmn:endEvent id="EndEvent_0engbx0">
      <bpmn:incoming>Flow_0lwndql</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0lwndql" sourceRef="IntermediateCatchEvent_103sfzn" targetRef="EndEvent_0engbx0" />
  </bpmn:process>
  </bpmn:definitions>`;

export const BPMN_XML_MESSAGE_END_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0hqs4n1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:message id="Message_0rktjyo" name="Message_1et2nam" />
  <bpmn:process id="Process_1qolj4j" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wsz4rp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1bdc7xj">
      <bpmn:incoming>Flow_1wsz4rp</bpmn:incoming>
      <bpmn:outgoing>Flow_0xr5lnl</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1wsz4rp" sourceRef="StartEvent_1" targetRef="Task_1bdc7xj" />
    <bpmn:sequenceFlow id="Flow_0xr5lnl" sourceRef="Task_1bdc7xj" targetRef="EndEvent_19dup8l" />
    <bpmn:endEvent id="EndEvent_19dup8l">
      <bpmn:incoming>Flow_0xr5lnl</bpmn:incoming>
      <bpmn:messageEventDefinition messageRef="Message_0rktjyo" />
    </bpmn:endEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SIGNAL_END_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0hqs4n1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:signal id="Signal_12cv5fk" name="Signal_2j8p7pq" />
  <bpmn:process id="Process_1qolj4j" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wsz4rp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1bdc7xj">
      <bpmn:incoming>Flow_1wsz4rp</bpmn:incoming>
      <bpmn:outgoing>Flow_0xr5lnl</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1wsz4rp" sourceRef="StartEvent_1" targetRef="Task_1bdc7xj" />
    <bpmn:sequenceFlow id="Flow_0xr5lnl" sourceRef="Task_1bdc7xj" targetRef="EndEvent_19dup8l" />
    <bpmn:endEvent id="EndEvent_19dup8l">
      <bpmn:incoming>Flow_0xr5lnl</bpmn:incoming>
      <bpmn:signalEventDefinition signalRef="Signal_12cv5fk" />
    </bpmn:endEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_ERROR_END_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0hqs4n1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:error id="Error_1mf2gpw" name="Error_0c9a62j" />
  <bpmn:process id="Process_1qolj4j" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wsz4rp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1bdc7xj">
      <bpmn:incoming>Flow_1wsz4rp</bpmn:incoming>
      <bpmn:outgoing>Flow_0xr5lnl</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1wsz4rp" sourceRef="StartEvent_1" targetRef="Task_1bdc7xj" />
    <bpmn:sequenceFlow id="Flow_0xr5lnl" sourceRef="Task_1bdc7xj" targetRef="EndEvent_19dup8l" />
    <bpmn:endEvent id="EndEvent_19dup8l">
      <bpmn:incoming>Flow_0xr5lnl</bpmn:incoming>
      <bpmn:errorEventDefinition errorRef="Error_1mf2gpw" />
    </bpmn:endEvent>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_ESCALATION_END_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0hqs4n1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_1qolj4j" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1wsz4rp</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1bdc7xj">
      <bpmn:incoming>Flow_1wsz4rp</bpmn:incoming>
      <bpmn:outgoing>Flow_0xr5lnl</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1wsz4rp" sourceRef="StartEvent_1" targetRef="Task_1bdc7xj" />
    <bpmn:sequenceFlow id="Flow_0xr5lnl" sourceRef="Task_1bdc7xj" targetRef="EndEvent_19dup8l" />
    <bpmn:endEvent id="EndEvent_19dup8l">
      <bpmn:incoming>Flow_0xr5lnl</bpmn:incoming>
      <bpmn:escalationEventDefinition escalationRef="Escalation_1cyxu5c" />
    </bpmn:endEvent>
  </bpmn:process>
</bpmn:definitions>`;

/**
 * Event Based Gateway:
 * StartEvent_1 -> Task_1dxej6p -> EventBasedGateway_1b2kp9o -> IntermediateCatchEvent_1wf39sg (Message_0io414n) -> Task_022jsr6 -> ExclusiveGateway_01v8v7y -> EndEvent_1ssivqu
 *                                                           -> IntermediateCatchEvent_0b55x93 (Message_0iau9ls) -> Task_0oxs9yd ->
 *                                                           -> IntermediateCatchEvent_121oaaf (Signal_0bz6prz) --> Task_0vat2w4 ->
 *                                                           -> IntermediateCatchEvent_0zx20dj (Signal_124dvwx) --> Task_0u26ddi ->
 *                                                           -> IntermediateCatchEvent_10sp5e5 (1s) --------------> Task_1on8mml ->
 *                                                           -> IntermediateCatchEvent_0aqhvgx (2s) --------------> Task_0vvlaz8 ->
 *
 */
export const BPMN_XML_EVENT_BASED_GATEWAY = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1xunvbj" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="3.4.1">
  <bpmn:process id="Process_11k0fz9" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_0t62lk4</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1dxej6p" name="Task 1">
      <bpmn:incoming>SequenceFlow_0t62lk4</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_02fwbhq</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0t62lk4" sourceRef="StartEvent_1" targetRef="Task_1dxej6p" />
    <bpmn:eventBasedGateway id="EventBasedGateway_1b2kp9o">
      <bpmn:incoming>SequenceFlow_02fwbhq</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1ctrjgx</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0o9ooxt</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_136pm2n</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_07s1jew</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0uvsl0l</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0qtl96c</bpmn:outgoing>
    </bpmn:eventBasedGateway>
    <bpmn:sequenceFlow id="SequenceFlow_02fwbhq" sourceRef="Task_1dxej6p" targetRef="EventBasedGateway_1b2kp9o" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_1wf39sg">
      <bpmn:incoming>SequenceFlow_0o9ooxt</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1kf6gfl</bpmn:outgoing>
      <bpmn:messageEventDefinition messageRef="Message_0io414n" />
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0o9ooxt" sourceRef="EventBasedGateway_1b2kp9o" targetRef="IntermediateCatchEvent_1wf39sg" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_0b55x93">
      <bpmn:incoming>SequenceFlow_136pm2n</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0ryor4p</bpmn:outgoing>
      <bpmn:messageEventDefinition messageRef="Message_0iau9ls" />
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="SequenceFlow_136pm2n" sourceRef="EventBasedGateway_1b2kp9o" targetRef="IntermediateCatchEvent_0b55x93" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_121oaaf">
      <bpmn:incoming>SequenceFlow_07s1jew</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1efbxzn</bpmn:outgoing>
      <bpmn:signalEventDefinition signalRef="Signal_0bz6prz" />
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="SequenceFlow_07s1jew" sourceRef="EventBasedGateway_1b2kp9o" targetRef="IntermediateCatchEvent_121oaaf" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_0zx20dj">
      <bpmn:incoming>SequenceFlow_0uvsl0l</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1tw35wr</bpmn:outgoing>
      <bpmn:signalEventDefinition signalRef="Signal_124dvwx" />
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0uvsl0l" sourceRef="EventBasedGateway_1b2kp9o" targetRef="IntermediateCatchEvent_0zx20dj" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_10sp5e5">
      <bpmn:incoming>SequenceFlow_0qtl96c</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1geno7f</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0qtl96c" sourceRef="EventBasedGateway_1b2kp9o" targetRef="IntermediateCatchEvent_10sp5e5" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_0aqhvgx">
      <bpmn:incoming>SequenceFlow_1ctrjgx</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1j5a0ih</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT2S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="SequenceFlow_1ctrjgx" sourceRef="EventBasedGateway_1b2kp9o" targetRef="IntermediateCatchEvent_0aqhvgx" />
    <bpmn:task id="Task_022jsr6" name="Message Task 1">
      <bpmn:incoming>SequenceFlow_1kf6gfl</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0er9iqk</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1kf6gfl" sourceRef="IntermediateCatchEvent_1wf39sg" targetRef="Task_022jsr6" />
    <bpmn:task id="Task_0oxs9yd" name="Message Task 2">
      <bpmn:incoming>SequenceFlow_0ryor4p</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_01s1jsy</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0ryor4p" sourceRef="IntermediateCatchEvent_0b55x93" targetRef="Task_0oxs9yd" />
    <bpmn:task id="Task_0vat2w4" name="Signal Task 1">
      <bpmn:incoming>SequenceFlow_1efbxzn</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1xxak8n</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1efbxzn" sourceRef="IntermediateCatchEvent_121oaaf" targetRef="Task_0vat2w4" />
    <bpmn:task id="Task_0u26ddi" name="Signal Task 2">
      <bpmn:incoming>SequenceFlow_1tw35wr</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_06ffkrd</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1tw35wr" sourceRef="IntermediateCatchEvent_0zx20dj" targetRef="Task_0u26ddi" />
    <bpmn:task id="Task_1on8mml" name="Timer Task 1">
      <bpmn:incoming>SequenceFlow_1geno7f</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1tv2sw6</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1geno7f" sourceRef="IntermediateCatchEvent_10sp5e5" targetRef="Task_1on8mml" />
    <bpmn:task id="Task_0vvlaz8" name="Timer Task 2">
      <bpmn:incoming>SequenceFlow_1j5a0ih</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0cib6nt</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_1j5a0ih" sourceRef="IntermediateCatchEvent_0aqhvgx" targetRef="Task_0vvlaz8" />
    <bpmn:exclusiveGateway id="ExclusiveGateway_01v8v7y">
      <bpmn:incoming>SequenceFlow_0er9iqk</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_01s1jsy</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1xxak8n</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_06ffkrd</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1tv2sw6</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0cib6nt</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1361hlc</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:sequenceFlow id="SequenceFlow_0er9iqk" sourceRef="Task_022jsr6" targetRef="ExclusiveGateway_01v8v7y" />
    <bpmn:sequenceFlow id="SequenceFlow_01s1jsy" sourceRef="Task_0oxs9yd" targetRef="ExclusiveGateway_01v8v7y" />
    <bpmn:sequenceFlow id="SequenceFlow_1xxak8n" sourceRef="Task_0vat2w4" targetRef="ExclusiveGateway_01v8v7y" />
    <bpmn:sequenceFlow id="SequenceFlow_06ffkrd" sourceRef="Task_0u26ddi" targetRef="ExclusiveGateway_01v8v7y" />
    <bpmn:sequenceFlow id="SequenceFlow_1tv2sw6" sourceRef="Task_1on8mml" targetRef="ExclusiveGateway_01v8v7y" />
    <bpmn:sequenceFlow id="SequenceFlow_0cib6nt" sourceRef="Task_0vvlaz8" targetRef="ExclusiveGateway_01v8v7y" />
    <bpmn:endEvent id="EndEvent_1ssivqu">
      <bpmn:incoming>SequenceFlow_1361hlc</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_1361hlc" sourceRef="ExclusiveGateway_01v8v7y" targetRef="EndEvent_1ssivqu" />
  </bpmn:process>
  <bpmn:message id="Message_0io414n" name="Message_24beroh" />
  <bpmn:message id="Message_0iau9ls" name="Message_2qvmgii" />
  <bpmn:signal id="Signal_0bz6prz" name="Signal_36r3o14" />
  <bpmn:signal id="Signal_124dvwx" name="Signal_1q9ugjt" />
</bpmn:definitions>`;

/**
 * Parallel Gateway Split and multiple EndEvents:
 * StartEvent_1 -> Task_1y4wd2q -> ParallelGateway_1jgsyiy -> Task_14qncax -> EndEvent_0a4ljay
 *                                                          -> Task_0stbs9z -> EndEvent_018jvql
 *
 */
export const BPMN_XML_MULTIPLE_END_EVENTS = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:proceed="http://docs.snet.tu-berlin.de/proceed/201902/" id="sample-diagram" name="gvc" targetNamespace="http://bpmn.io/schema/bpmn" expressionLanguage="javascript" typeLanguage="json" exporter="PROCEED Management System" exporterVersion="0.1.0" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd">
  <bpmn:process id="_969178a8-ff78-4d19-ae51-47d2c1f94b9a" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_14mwzvq</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="SequenceFlow_14mwzvq" sourceRef="StartEvent_1" targetRef="Task_1y4wd2q"/>
    <bpmn:task id="Task_1y4wd2q" scriptFormat="Javascript">
      <bpmn:incoming>SequenceFlow_14mwzvq</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0xbtkf6</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0xbtkf6" sourceRef="Task_1y4wd2q" targetRef="ParallelGateway_1jgsyiy"/>
    <bpmn:sequenceFlow id="SequenceFlow_130lep0" sourceRef="ParallelGateway_1jgsyiy" targetRef="Task_14qncax"/>
    <bpmn:sequenceFlow id="SequenceFlow_00k6tno" sourceRef="ParallelGateway_1jgsyiy" targetRef="Task_0stbs9z"/>
    <bpmn:endEvent id="EndEvent_018jvql">
      <bpmn:incoming>SequenceFlow_0p6dlnq</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0p6dlnq" sourceRef="Task_0stbs9z" targetRef="EndEvent_018jvql"/>
    <bpmn:endEvent id="EndEvent_0a4ljay">
      <bpmn:incoming>SequenceFlow_0elmfim</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0elmfim" sourceRef="Task_14qncax" targetRef="EndEvent_0a4ljay"/>
    <bpmn:task id="Task_14qncax" scriptFormat="Javascript">
      <bpmn:incoming>SequenceFlow_130lep0</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0elmfim</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0stbs9z" scriptFormat="Javascript">
      <bpmn:incoming>SequenceFlow_00k6tno</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0p6dlnq</bpmn:outgoing>
    </bpmn:task>
    <bpmn:parallelGateway id="ParallelGateway_1jgsyiy">
      <bpmn:incoming>SequenceFlow_0xbtkf6</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_130lep0</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_00k6tno</bpmn:outgoing>
    </bpmn:parallelGateway>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SCRIPT_TASK_INTERRUPTING_TIMER_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0d0fn1x" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_18r1iqk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1t6jmdb</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1t6jmdb" sourceRef="StartEvent_1" targetRef="Task_1romxxp" />
    <bpmn:sequenceFlow id="Flow_02ty015" sourceRef="Task_1romxxp" targetRef="Task_1wg4yqd" />
    <bpmn:task id="Task_03dall0" name="Task 3">
      <bpmn:incoming>Flow_0t9fowb</bpmn:incoming>
      <bpmn:outgoing>Flow_1kml794</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0t9fowb" sourceRef="Task_1wg4yqd" targetRef="Task_03dall0" />
    <bpmn:endEvent id="EndEvent_0e6skps">
      <bpmn:incoming>Flow_1kml794</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1kml794" sourceRef="Task_03dall0" targetRef="EndEvent_0e6skps" />
    <bpmn:scriptTask id="Task_1romxxp" name="Task 1">
      <bpmn:incoming>Flow_1t6jmdb</bpmn:incoming>
      <bpmn:outgoing>Flow_02ty015</bpmn:outgoing>
      <bpmn:script>
      variable.set('a', 10);
      return new Promise((resolve) => {
        setTimeout(() => resolve(), 500) 
      })
      </bpmn:script>
    </bpmn:scriptTask>
    <bpmn:scriptTask id="Task_1wg4yqd" name="Task 2">
      <bpmn:incoming>Flow_02ty015</bpmn:incoming>
      <bpmn:outgoing>Flow_0t9fowb</bpmn:outgoing>
      <bpmn:script>
      variable.set('b', 300);
      return new Promise((resolve) => {
        setTimeout(() => resolve(), 1500) 
      })
      </bpmn:script>
    </bpmn:scriptTask>
    <bpmn:boundaryEvent id="BoundaryEvent_13govhg" cancelActivity="true" attachedToRef="Task_1romxxp">
      <bpmn:outgoing>Flow_0k42am6</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:boundaryEvent>
    <bpmn:boundaryEvent id="BoundaryEvent_14r9ipi" cancelActivity="true" attachedToRef="Task_1wg4yqd">
      <bpmn:outgoing>Flow_0e43ua8</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:boundaryEvent>
    <bpmn:task id="Task_15hjzq6" name="Alternative Task 1">
      <bpmn:incoming>Flow_0k42am6</bpmn:incoming>
      <bpmn:outgoing>Flow_1gv3sup</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0k42am6" sourceRef="BoundaryEvent_13govhg" targetRef="Task_15hjzq6" />
    <bpmn:task id="Task_1fgmhtf" name="Alternative Task 2">
      <bpmn:incoming>Flow_0e43ua8</bpmn:incoming>
      <bpmn:outgoing>Flow_0t5xhnp</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0e43ua8" sourceRef="BoundaryEvent_14r9ipi" targetRef="Task_1fgmhtf" />
    <bpmn:endEvent id="EndEvent_0ygj830">
      <bpmn:incoming>Flow_1gv3sup</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1gv3sup" sourceRef="Task_15hjzq6" targetRef="EndEvent_0ygj830" />
    <bpmn:endEvent id="EndEvent_0umagrf">
      <bpmn:incoming>Flow_0t5xhnp</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0t5xhnp" sourceRef="Task_1fgmhtf" targetRef="EndEvent_0umagrf" />
  </bpmn:process>
</bpmn:definitions> `;

export const BPMN_XML_SCRIPT_TASK_NON_INTERRUPTING_TIMER_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0d0fn1x" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_18r1iqk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1t6jmdb</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1t6jmdb" sourceRef="StartEvent_1" targetRef="Task_1romxxp" />
    <bpmn:sequenceFlow id="Flow_02ty015" sourceRef="Task_1romxxp" targetRef="Task_1wg4yqd" />
    <bpmn:task id="Task_03dall0" name="Task 3">
      <bpmn:incoming>Flow_0t9fowb</bpmn:incoming>
      <bpmn:outgoing>Flow_1kml794</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0t9fowb" sourceRef="Task_1wg4yqd" targetRef="Task_03dall0" />
    <bpmn:endEvent id="EndEvent_0e6skps">
      <bpmn:incoming>Flow_1kml794</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1kml794" sourceRef="Task_03dall0" targetRef="EndEvent_0e6skps" />
    <bpmn:scriptTask id="Task_1romxxp" name="Task 1">
      <bpmn:incoming>Flow_1t6jmdb</bpmn:incoming>
      <bpmn:outgoing>Flow_02ty015</bpmn:outgoing>
      <bpmn:script>
      variable.set('a', 10);
      return new Promise((resolve) => {
        setTimeout(() => resolve(), 500) 
      })
      </bpmn:script>
    </bpmn:scriptTask>
    <bpmn:scriptTask id="Task_1wg4yqd" name="Task 2">
      <bpmn:incoming>Flow_02ty015</bpmn:incoming>
      <bpmn:outgoing>Flow_0t9fowb</bpmn:outgoing>
      <bpmn:script>
      variable.set('b', 300);
      return new Promise((resolve) => {
        setTimeout(() => resolve(), 1500) 
      })
      </bpmn:script>
    </bpmn:scriptTask>
    <bpmn:boundaryEvent id="BoundaryEvent_13govhg" cancelActivity="false" attachedToRef="Task_1romxxp">
      <bpmn:outgoing>Flow_0k42am6</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:boundaryEvent>
    <bpmn:boundaryEvent id="BoundaryEvent_14r9ipi" cancelActivity="false" attachedToRef="Task_1wg4yqd">
      <bpmn:outgoing>Flow_0e43ua8</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:boundaryEvent>
    <bpmn:task id="Task_15hjzq6" name="Alternative Task 1">
      <bpmn:incoming>Flow_0k42am6</bpmn:incoming>
      <bpmn:outgoing>Flow_1gv3sup</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0k42am6" sourceRef="BoundaryEvent_13govhg" targetRef="Task_15hjzq6" />
    <bpmn:task id="Task_1fgmhtf" name="Alternative Task 2">
      <bpmn:incoming>Flow_0e43ua8</bpmn:incoming>
      <bpmn:outgoing>Flow_0t5xhnp</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0e43ua8" sourceRef="BoundaryEvent_14r9ipi" targetRef="Task_1fgmhtf" />
    <bpmn:endEvent id="EndEvent_0ygj830">
      <bpmn:incoming>Flow_1gv3sup</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1gv3sup" sourceRef="Task_15hjzq6" targetRef="EndEvent_0ygj830" />
    <bpmn:endEvent id="EndEvent_0umagrf">
      <bpmn:incoming>Flow_0t5xhnp</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0t5xhnp" sourceRef="Task_1fgmhtf" targetRef="EndEvent_0umagrf" />
  </bpmn:process>
</bpmn:definitions> `;

export const BPMN_XML_USER_TASK_INTERRUPTING_TIMER_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1t7bd40" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0y0lxo3" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1ryjcpa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1ryjcpa" sourceRef="StartEvent_1" targetRef="Task_1wznt2y" />
    <bpmn:endEvent id="EndEvent_17uy0hh">
      <bpmn:incoming>Flow_04du2vp</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_04du2vp" sourceRef="Task_1wznt2y" targetRef="EndEvent_17uy0hh" />
    <bpmn:boundaryEvent id="BoundaryEvent_1o6no5w" attachedToRef="Task_1wznt2y">
      <bpmn:outgoing>Flow_164xwb4</bpmn:outgoing>
      <bpmn:timerEventDefinition id="TimerEventDefinition_15j7glh" />
    </bpmn:boundaryEvent>
    <bpmn:userTask id="Task_1wznt2y">
      <bpmn:incoming>Flow_1ryjcpa</bpmn:incoming>
      <bpmn:outgoing>Flow_04du2vp</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:endEvent id="EndEvent_1rif7zq">
      <bpmn:incoming>Flow_164xwb4</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_164xwb4" sourceRef="BoundaryEvent_1o6no5w" targetRef="EndEvent_1rif7zq" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_TASK_INTERRUPTING_TIMER_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1t7bd40" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0y0lxo3" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1ryjcpa</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1ryjcpa" sourceRef="StartEvent_1" targetRef="Task_1wznt2y" />
    <bpmn:endEvent id="EndEvent_17uy0hh">
      <bpmn:incoming>Flow_04du2vp</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_04du2vp" sourceRef="Task_1wznt2y" targetRef="EndEvent_17uy0hh" />
    <bpmn:boundaryEvent id="BoundaryEvent_1o6no5w" attachedToRef="Task_1wznt2y">
      <bpmn:outgoing>Flow_1mlfe8i</bpmn:outgoing>
      <bpmn:timerEventDefinition id="TimerEventDefinition_15j7glh" />
    </bpmn:boundaryEvent>
    <bpmn:task id="Task_1wznt2y">
      <bpmn:incoming>Flow_1ryjcpa</bpmn:incoming>
      <bpmn:outgoing>Flow_04du2vp</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_1rif7zq">
      <bpmn:incoming>Flow_1mlfe8i</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1mlfe8i" sourceRef="BoundaryEvent_1o6no5w" targetRef="EndEvent_1rif7zq" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_CALL_ACTIVITY = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:p33c24="http://docs.snet.tu-berlin.de/proceed/_17c6fed0-8a8c-4722-a62f-86ebf1324c33" id="_e292e6c4-4d7f-4aff-b91f-c102d5ea4ae8" name="Global Task Process" targetNamespace="http://docs.snet.tu-berlin.de/proceed/_e292e6c4-4d7f-4aff-b91f-c102d5ea4ae8" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <import namespace="http://docs.snet.tu-berlin.de/proceed/_17c6fed0-8a8c-4722-a62f-86ebf1324c33" location="Other-Process-17c6fed0" importType="http://www.omg.org/spec/BPMN/20100524/MODEL" />
  <bpmn:process id="Process_1au95he" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1t2jdbt</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1t2jdbt" sourceRef="StartEvent_1" targetRef="CallActivity_1mcfm7t" />
    <bpmn:endEvent id="EndEvent_1snnjzn">
      <bpmn:incoming>Flow_1ydrl4y</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1ydrl4y" sourceRef="CallActivity_1mcfm7t" targetRef="EndEvent_1snnjzn" />
    <bpmn:callActivity id="CallActivity_1mcfm7t" name="Call the global process" calledElement="p33c24:_e069937f-27b6-464b-b397-b88a2599f1b9">
      <bpmn:incoming>Flow_1t2jdbt</bpmn:incoming>
      <bpmn:outgoing>Flow_1ydrl4y</bpmn:outgoing>
    </bpmn:callActivity>
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_EMBEDDED_SUBPROCESS = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0dl8r20" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_1f4d5wj" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_13xi3sc</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_13xi3sc" sourceRef="StartEvent_1" targetRef="Subprocess_1hfk5eg" />
    <bpmn:subProcess id="Subprocess_1hfk5eg">
      <bpmn:incoming>Flow_13xi3sc</bpmn:incoming>
      <bpmn:outgoing>Flow_0kdheys</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_1hsc96f">
        <bpmn:outgoing>Flow_1q5z2xd</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:task id="Task_02z411f">
        <bpmn:incoming>Flow_1q5z2xd</bpmn:incoming>
        <bpmn:outgoing>Flow_0530soz</bpmn:outgoing>
      </bpmn:task>
      <bpmn:sequenceFlow id="Flow_1q5z2xd" sourceRef="StartEvent_1hsc96f" targetRef="Task_02z411f" />
      <bpmn:endEvent id="EndEvent_1k3mqep">
        <bpmn:incoming>Flow_0530soz</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="Flow_0530soz" sourceRef="Task_02z411f" targetRef="EndEvent_1k3mqep" />
    </bpmn:subProcess>
    <bpmn:endEvent id="EndEvent_0n4vm09">
      <bpmn:incoming>Flow_0kdheys</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0kdheys" sourceRef="Subprocess_1hfk5eg" targetRef="EndEvent_0n4vm09" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SUBPROCESS_ERROR_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_023wwno" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:error id="error" errorCode="EXAMPLE-CODE" />
  <bpmn:process id="Process_08c5fbu" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0tszcee</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0tszcee" sourceRef="StartEvent_1" targetRef="Subprocess_18kg880" />
    <bpmn:subProcess id="Subprocess_18kg880">
      <bpmn:incoming>Flow_0tszcee</bpmn:incoming>
      <bpmn:outgoing>Flow_1o0ektt</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_007g88m">
        <bpmn:outgoing>Flow_0jlzd3z</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:task id="Task_10tcyso">
        <bpmn:incoming>Flow_0jlzd3z</bpmn:incoming>
        <bpmn:outgoing>Flow_0qsihfu</bpmn:outgoing>
      </bpmn:task>
      <bpmn:sequenceFlow id="Flow_0jlzd3z" sourceRef="StartEvent_007g88m" targetRef="Task_10tcyso" />
      <bpmn:sequenceFlow id="Flow_0qsihfu" sourceRef="Task_10tcyso" targetRef="EndEvent_0a1l09r" />
      <bpmn:endEvent id="EndEvent_0a1l09r">
        <bpmn:incoming>Flow_0qsihfu</bpmn:incoming>
        <bpmn:errorEventDefinition errorRef="error" id="ErrorEventDefinition_1cjtcz7" />
      </bpmn:endEvent>
    </bpmn:subProcess>
    <bpmn:boundaryEvent id="BoundaryEvent_024dao6" attachedToRef="Subprocess_18kg880">
      <bpmn:outgoing>Flow_1vydft5</bpmn:outgoing>
      <bpmn:errorEventDefinition errorRef="error" id="ErrorEventDefinition_1t5d49n" />
    </bpmn:boundaryEvent>
    <bpmn:task id="Task_0344sje">
      <bpmn:incoming>Flow_1vydft5</bpmn:incoming>
      <bpmn:outgoing>Flow_13z0f3n</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1vydft5" sourceRef="BoundaryEvent_024dao6" targetRef="Task_0344sje" />
    <bpmn:endEvent id="EndEvent_0nvh0en">
      <bpmn:incoming>Flow_13z0f3n</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_13z0f3n" sourceRef="Task_0344sje" targetRef="EndEvent_0nvh0en" />
    <bpmn:endEvent id="EndEvent_1eiya5o">
      <bpmn:incoming>Flow_1o0ektt</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1o0ektt" sourceRef="Subprocess_18kg880" targetRef="EndEvent_1eiya5o" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SUBPROCESS_NON_INTERRUPTING_TIMER_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0widm24" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_1ci9d2w" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1p1omlb</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1p1omlb" sourceRef="StartEvent_1" targetRef="Subprocess_1mvfe17" />
    <bpmn:subProcess id="Subprocess_1mvfe17">
      <bpmn:incoming>Flow_1p1omlb</bpmn:incoming>
      <bpmn:outgoing>Flow_0lcnd5t</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_0s64s70">
        <bpmn:outgoing>Flow_0od8cnw</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:sequenceFlow id="Flow_0od8cnw" sourceRef="StartEvent_0s64s70" targetRef="Task_09azc5q" />
      <bpmn:endEvent id="EndEvent_0zhdpvu">
        <bpmn:incoming>Flow_0edtmf1</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="Flow_0edtmf1" sourceRef="Task_09azc5q" targetRef="EndEvent_0zhdpvu" />
      <bpmn:scriptTask id="Task_09azc5q" name="Task 1">
        <bpmn:incoming>Flow_0od8cnw</bpmn:incoming>
        <bpmn:outgoing>Flow_0edtmf1</bpmn:outgoing>
        <bpmn:script>
        return new Promise((resolve) => {
          setTimeout(() => resolve(), 1500) 
        })
        </bpmn:script>
      </bpmn:scriptTask>
    </bpmn:subProcess>
    <bpmn:boundaryEvent id="BoundaryEvent_1fxt72p" cancelActivity="false" attachedToRef="Subprocess_1mvfe17">
      <bpmn:outgoing>Flow_1vrtqt2</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:boundaryEvent>
    <bpmn:sequenceFlow id="Flow_0lcnd5t" sourceRef="Subprocess_1mvfe17" targetRef="EndEvent_0zoa1gk" />
    <bpmn:endEvent id="EndEvent_0zoa1gk">
      <bpmn:incoming>Flow_0lcnd5t</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:task id="Task_0ke18vq" name="Alternative Task 1">
      <bpmn:incoming>Flow_1vrtqt2</bpmn:incoming>
      <bpmn:outgoing>Flow_1hcaka0</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1vrtqt2" sourceRef="BoundaryEvent_1fxt72p" targetRef="Task_0ke18vq" />
    <bpmn:endEvent id="EndEvent_0gwzjkj">
      <bpmn:incoming>Flow_1hcaka0</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1hcaka0" sourceRef="Task_0ke18vq" targetRef="EndEvent_0gwzjkj" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SUBPROCESS_INTERRUPTING_TIMER_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0widm24" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_1ci9d2w" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1p1omlb</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1p1omlb" sourceRef="StartEvent_1" targetRef="Subprocess_1mvfe17" />
    <bpmn:subProcess id="Subprocess_1mvfe17">
      <bpmn:incoming>Flow_1p1omlb</bpmn:incoming>
      <bpmn:outgoing>Flow_0lcnd5t</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_0s64s70">
        <bpmn:outgoing>Flow_0od8cnw</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:sequenceFlow id="Flow_0od8cnw" sourceRef="StartEvent_0s64s70" targetRef="Task_09azc5q" />
      <bpmn:endEvent id="EndEvent_0zhdpvu">
        <bpmn:incoming>Flow_0edtmf1</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="Flow_0edtmf1" sourceRef="Task_09azc5q" targetRef="EndEvent_0zhdpvu" />
      <bpmn:scriptTask id="Task_09azc5q" name="Task 1">
        <bpmn:incoming>Flow_0od8cnw</bpmn:incoming>
        <bpmn:outgoing>Flow_0edtmf1</bpmn:outgoing>
        <bpmn:script>
        return new Promise((resolve) => {
          setTimeout(() => resolve(), 1500) 
        })
        </bpmn:script>
      </bpmn:scriptTask>
    </bpmn:subProcess>
    <bpmn:boundaryEvent id="BoundaryEvent_1fxt72p" cancelActivity="true" attachedToRef="Subprocess_1mvfe17">
      <bpmn:outgoing>Flow_1vrtqt2</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:boundaryEvent>
    <bpmn:sequenceFlow id="Flow_0lcnd5t" sourceRef="Subprocess_1mvfe17" targetRef="EndEvent_0zoa1gk" />
    <bpmn:endEvent id="EndEvent_0zoa1gk">
      <bpmn:incoming>Flow_0lcnd5t</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:task id="Task_0ke18vq" name="Alternative Task 1">
      <bpmn:incoming>Flow_1vrtqt2</bpmn:incoming>
      <bpmn:outgoing>Flow_1hcaka0</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1vrtqt2" sourceRef="BoundaryEvent_1fxt72p" targetRef="Task_0ke18vq" />
    <bpmn:endEvent id="EndEvent_0gwzjkj">
      <bpmn:incoming>Flow_1hcaka0</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1hcaka0" sourceRef="Task_0ke18vq" targetRef="EndEvent_0gwzjkj" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SUBPROCESS_UNTRIGGERED_INTERRUPTING_TIMER_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0widm24" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_1ci9d2w" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1p1omlb</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1p1omlb" sourceRef="StartEvent_1" targetRef="Subprocess_1mvfe17" />
    <bpmn:subProcess id="Subprocess_1mvfe17">
      <bpmn:incoming>Flow_1p1omlb</bpmn:incoming>
      <bpmn:outgoing>Flow_0lcnd5t</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_0s64s70">
        <bpmn:outgoing>Flow_0od8cnw</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:sequenceFlow id="Flow_0od8cnw" sourceRef="StartEvent_0s64s70" targetRef="Task_09azc5q" />
      <bpmn:endEvent id="EndEvent_0zhdpvu">
        <bpmn:incoming>Flow_0edtmf1</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="Flow_0edtmf1" sourceRef="Task_09azc5q" targetRef="EndEvent_0zhdpvu" />
      <bpmn:scriptTask id="Task_09azc5q" name="Task 1">
        <bpmn:incoming>Flow_0od8cnw</bpmn:incoming>
        <bpmn:outgoing>Flow_0edtmf1</bpmn:outgoing>
        <bpmn:script>
        return new Promise((resolve) => {
          setTimeout(() => resolve(), 500) 
        })
        </bpmn:script>
      </bpmn:scriptTask>
    </bpmn:subProcess>
    <bpmn:boundaryEvent id="BoundaryEvent_1fxt72p" cancelActivity="true" attachedToRef="Subprocess_1mvfe17">
      <bpmn:outgoing>Flow_1vrtqt2</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:boundaryEvent>
    <bpmn:sequenceFlow id="Flow_0lcnd5t" sourceRef="Subprocess_1mvfe17" targetRef="EndEvent_0zoa1gk" />
    <bpmn:endEvent id="EndEvent_0zoa1gk">
      <bpmn:incoming>Flow_0lcnd5t</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:task id="Task_0ke18vq" name="Alternative Task 1">
      <bpmn:incoming>Flow_1vrtqt2</bpmn:incoming>
      <bpmn:outgoing>Flow_1hcaka0</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1vrtqt2" sourceRef="BoundaryEvent_1fxt72p" targetRef="Task_0ke18vq" />
    <bpmn:endEvent id="EndEvent_0gwzjkj">
      <bpmn:incoming>Flow_1hcaka0</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1hcaka0" sourceRef="Task_0ke18vq" targetRef="EndEvent_0gwzjkj" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_SUBPROCESS_INTERRUPTING_ESCALATION_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1iftv6a" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:escalation id="escalation" escalationCode="EXAMPLE-CODE" />  
  <bpmn:process id="Process_0v2pgjm" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0tl31nf</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0tl31nf" sourceRef="StartEvent_1" targetRef="Subprocess_1in1j2j" />
    <bpmn:subProcess id="Subprocess_1in1j2j">
      <bpmn:incoming>Flow_0tl31nf</bpmn:incoming>
      <bpmn:outgoing>Flow_19d9g57</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_190w8v7">
        <bpmn:outgoing>Flow_1jh8cfi</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:intermediateThrowEvent id="ThrowingEvent_1o2e395">
        <bpmn:incoming>Flow_0g46voo</bpmn:incoming>
        <bpmn:outgoing>Flow_00x71n1</bpmn:outgoing>
        <bpmn:escalationEventDefinition escalationRef="escalation" id="EscalationEventDefinition_1juvegz" />
      </bpmn:intermediateThrowEvent>
      <bpmn:task id="Task_1kxwma3">
        <bpmn:incoming>Flow_00x71n1</bpmn:incoming>
        <bpmn:outgoing>Flow_07kei0w</bpmn:outgoing>
      </bpmn:task>
      <bpmn:sequenceFlow id="Flow_00x71n1" sourceRef="ThrowingEvent_1o2e395" targetRef="Task_1kxwma3" />
      <bpmn:endEvent id="EndEvent_1v3hp1a">
        <bpmn:incoming>Flow_07kei0w</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="Flow_07kei0w" sourceRef="Task_1kxwma3" targetRef="EndEvent_1v3hp1a" />
      <bpmn:task id="Task_0gnmest">
        <bpmn:incoming>Flow_1jh8cfi</bpmn:incoming>
        <bpmn:outgoing>Flow_0g46voo</bpmn:outgoing>
      </bpmn:task>
      <bpmn:sequenceFlow id="Flow_1jh8cfi" sourceRef="StartEvent_190w8v7" targetRef="Task_0gnmest" />
      <bpmn:sequenceFlow id="Flow_0g46voo" sourceRef="Task_0gnmest" targetRef="ThrowingEvent_1o2e395" />
    </bpmn:subProcess>
    <bpmn:task id="Task_1wdga14">
      <bpmn:incoming>Flow_19d9g57</bpmn:incoming>
      <bpmn:outgoing>Flow_1o94lig</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_19d9g57" sourceRef="Subprocess_1in1j2j" targetRef="Task_1wdga14" />
    <bpmn:endEvent id="EndEvent_13ciwt4">
      <bpmn:incoming>Flow_1o94lig</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1o94lig" sourceRef="Task_1wdga14" targetRef="EndEvent_13ciwt4" />
    <bpmn:boundaryEvent id="BoundaryEvent_095s9sb" cancelActivity="true" attachedToRef="Subprocess_1in1j2j">
      <bpmn:outgoing>Flow_1fdpjfh</bpmn:outgoing>
      <bpmn:escalationEventDefinition escalationRef="escalation" id="EscalationEventDefinition_0x7p7j9" />
    </bpmn:boundaryEvent>
    <bpmn:task id="Task_1k7sd5q">
      <bpmn:incoming>Flow_1fdpjfh</bpmn:incoming>
      <bpmn:outgoing>Flow_0fyplyh</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1fdpjfh" sourceRef="BoundaryEvent_095s9sb" targetRef="Task_1k7sd5q" />
    <bpmn:endEvent id="EndEvent_0ynbzkf">
      <bpmn:incoming>Flow_0fyplyh</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0fyplyh" sourceRef="Task_1k7sd5q" targetRef="EndEvent_0ynbzkf" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_TERMINATED_SUBPROCESS = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1xje7gq" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <bpmn:process id="Process_0lipfqu" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1ntvjvj</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1ntvjvj" sourceRef="StartEvent_1" targetRef="Subprocess_0fg8x7f" />
    <bpmn:subProcess id="Subprocess_0fg8x7f">
      <bpmn:incoming>Flow_1ntvjvj</bpmn:incoming>
      <bpmn:outgoing>Flow_186sqz6</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_141t3ys">
        <bpmn:outgoing>Flow_0t13xb7</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:sequenceFlow id="Flow_0t13xb7" sourceRef="StartEvent_141t3ys" targetRef="ParallelGateway_10ecdd3" />
      <bpmn:parallelGateway id="ParallelGateway_10ecdd3">
        <bpmn:incoming>Flow_0t13xb7</bpmn:incoming>
        <bpmn:outgoing>Flow_012yafq</bpmn:outgoing>
        <bpmn:outgoing>Flow_1hbv0rz</bpmn:outgoing>
      </bpmn:parallelGateway>
      <bpmn:sequenceFlow id="Flow_012yafq" sourceRef="ParallelGateway_10ecdd3" targetRef="Subprocess_04qvd0i" />
      <bpmn:task id="Task_1hf4n8k">
        <bpmn:incoming>Flow_1hbv0rz</bpmn:incoming>
        <bpmn:outgoing>Flow_19vq393</bpmn:outgoing>
      </bpmn:task>
      <bpmn:sequenceFlow id="Flow_1hbv0rz" sourceRef="ParallelGateway_10ecdd3" targetRef="Task_1hf4n8k" />
      <bpmn:sequenceFlow id="Flow_19vq393" sourceRef="Task_1hf4n8k" targetRef="EndEvent_01ss3hf" />
      <bpmn:endEvent id="EndEvent_01ss3hf">
        <bpmn:incoming>Flow_19vq393</bpmn:incoming>
        <bpmn:terminateEventDefinition id="TerminateEventDefinition_03zmdsi" />
      </bpmn:endEvent>
      <bpmn:endEvent id="EndEvent_06m4mwa">
        <bpmn:incoming>Flow_0ybuwfp</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="Flow_0ybuwfp" sourceRef="Subprocess_04qvd0i" targetRef="EndEvent_06m4mwa" />
      <bpmn:subProcess id="Subprocess_04qvd0i">
        <bpmn:incoming>Flow_012yafq</bpmn:incoming>
        <bpmn:outgoing>Flow_0ybuwfp</bpmn:outgoing>
        <bpmn:startEvent id="StartEvent_0nmkq0g">
          <bpmn:outgoing>Flow_0jbq9se</bpmn:outgoing>
        </bpmn:startEvent>
        <bpmn:userTask id="UserTask_0vnq84o">
          <bpmn:incoming>Flow_0jbq9se</bpmn:incoming>
          <bpmn:outgoing>Flow_1ipfg5l</bpmn:outgoing>
        </bpmn:userTask>
        <bpmn:sequenceFlow id="Flow_0jbq9se" sourceRef="StartEvent_0nmkq0g" targetRef="Task_0vnq84o" />
        <bpmn:endEvent id="EndEvent_0f9ukxp">
          <bpmn:incoming>Flow_1ipfg5l</bpmn:incoming>
        </bpmn:endEvent>
        <bpmn:sequenceFlow id="Flow_1ipfg5l" sourceRef="Task_0vnq84o" targetRef="EndEvent_0f9ukxp" />
      </bpmn:subProcess>
    </bpmn:subProcess>
    <bpmn:task id="Task_0gw0mdy">
      <bpmn:incoming>Flow_186sqz6</bpmn:incoming>
      <bpmn:outgoing>Flow_14ai8no</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_186sqz6" sourceRef="Subprocess_0fg8x7f" targetRef="Task_0gw0mdy" />
    <bpmn:endEvent id="EndEvent_0j72evn">
      <bpmn:incoming>Flow_14ai8no</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_14ai8no" sourceRef="Task_0gw0mdy" targetRef="EndEvent_0j72evn" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_TERMINATED_SUBPROCESS_TRIPLE_SPLIT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0nq66k1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="3.6.0">
  <bpmn:process id="Process_01re2kk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_0u0ebzc</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:subProcess id="SubProcess_0phiv1k">
      <bpmn:incoming>SequenceFlow_0u0ebzc</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0k92bg2</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_1og4rvt">
        <bpmn:outgoing>SequenceFlow_1c2jx0i</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:sequenceFlow id="SequenceFlow_1c2jx0i" sourceRef="StartEvent_1og4rvt" targetRef="ExclusiveGateway_1i43e7i" />
      <bpmn:parallelGateway id="ExclusiveGateway_1i43e7i">
        <bpmn:incoming>SequenceFlow_1c2jx0i</bpmn:incoming>
        <bpmn:outgoing>SequenceFlow_1fiv3is</bpmn:outgoing>
        <bpmn:outgoing>SequenceFlow_0l7asdj</bpmn:outgoing>
        <bpmn:outgoing>SequenceFlow_1k0vqr2</bpmn:outgoing>
      </bpmn:parallelGateway>
      <bpmn:sequenceFlow id="SequenceFlow_1fiv3is" sourceRef="ExclusiveGateway_1i43e7i" targetRef="Task_0c7ed5k" />
      <bpmn:sequenceFlow id="SequenceFlow_0l7asdj" sourceRef="ExclusiveGateway_1i43e7i" targetRef="Task_0if8lmi" />
      <bpmn:sequenceFlow id="SequenceFlow_1k0vqr2" sourceRef="ExclusiveGateway_1i43e7i" targetRef="EndEvent_1npa2ar" />
      <bpmn:endEvent id="EndEvent_1kzty3o">
        <bpmn:incoming>SequenceFlow_1a9l1s1</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="SequenceFlow_1a9l1s1" sourceRef="Task_0c7ed5k" targetRef="EndEvent_1kzty3o" />
      <bpmn:endEvent id="EndEvent_1pvo3r1">
        <bpmn:incoming>SequenceFlow_06ku3z9</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="SequenceFlow_06ku3z9" sourceRef="Task_0if8lmi" targetRef="EndEvent_1pvo3r1" />
      <bpmn:endEvent id="EndEvent_1npa2ar">
        <bpmn:incoming>SequenceFlow_1k0vqr2</bpmn:incoming>
        <bpmn:terminateEventDefinition id="TerminateEventDefinition_0tsgrrg" />
      </bpmn:endEvent>
      <bpmn:userTask id="Task_0c7ed5k">
        <bpmn:incoming>SequenceFlow_1fiv3is</bpmn:incoming>
        <bpmn:outgoing>SequenceFlow_1a9l1s1</bpmn:outgoing>
      </bpmn:userTask>
      <bpmn:userTask id="Task_0if8lmi">
        <bpmn:incoming>SequenceFlow_0l7asdj</bpmn:incoming>
        <bpmn:outgoing>SequenceFlow_06ku3z9</bpmn:outgoing>
      </bpmn:userTask>
    </bpmn:subProcess>
    <bpmn:sequenceFlow id="SequenceFlow_0u0ebzc" sourceRef="StartEvent_1" targetRef="SubProcess_0phiv1k" />
    <bpmn:endEvent id="EndEvent_0wsgk5o">
      <bpmn:incoming>SequenceFlow_0k92bg2</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0k92bg2" sourceRef="SubProcess_0phiv1k" targetRef="EndEvent_0wsgk5o" />
  </bpmn:process>
</bpmn:definitions>
`;

export const BPMN_XML_EMBEDDED_SUBPROCESS_DOUBLE_SPLIT = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0nq66k1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="3.6.0">
  <bpmn:process id="Process_01re2kk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_0u0ebzc</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:subProcess id="SubProcess_0phiv1k">
      <bpmn:incoming>SequenceFlow_0u0ebzc</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0k92bg2</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_1og4rvt">
        <bpmn:outgoing>SequenceFlow_1c2jx0i</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:sequenceFlow id="SequenceFlow_1c2jx0i" sourceRef="StartEvent_1og4rvt" targetRef="ExclusiveGateway_1i43e7i" />
      <bpmn:parallelGateway id="ExclusiveGateway_1i43e7i">
        <bpmn:incoming>SequenceFlow_1c2jx0i</bpmn:incoming>
        <bpmn:outgoing>SequenceFlow_1fiv3is</bpmn:outgoing>
        <bpmn:outgoing>SequenceFlow_0l7asdj</bpmn:outgoing>
      </bpmn:parallelGateway>
      <bpmn:sequenceFlow id="SequenceFlow_1fiv3is" sourceRef="ExclusiveGateway_1i43e7i" targetRef="Task_0c7ed5k" />
      <bpmn:sequenceFlow id="SequenceFlow_0l7asdj" sourceRef="ExclusiveGateway_1i43e7i" targetRef="Task_0if8lmi" />
      <bpmn:endEvent id="EndEvent_1kzty3o">
        <bpmn:incoming>SequenceFlow_1a9l1s1</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="SequenceFlow_1a9l1s1" sourceRef="Task_0c7ed5k" targetRef="EndEvent_1kzty3o" />
      <bpmn:endEvent id="EndEvent_1pvo3r1">
        <bpmn:incoming>SequenceFlow_06ku3z9</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="SequenceFlow_06ku3z9" sourceRef="Task_0if8lmi" targetRef="EndEvent_1pvo3r1" />
      <bpmn:userTask id="Task_0c7ed5k">
        <bpmn:incoming>SequenceFlow_1fiv3is</bpmn:incoming>
        <bpmn:outgoing>SequenceFlow_1a9l1s1</bpmn:outgoing>
      </bpmn:userTask>
      <bpmn:userTask id="Task_0if8lmi">
        <bpmn:incoming>SequenceFlow_0l7asdj</bpmn:incoming>
        <bpmn:outgoing>SequenceFlow_06ku3z9</bpmn:outgoing>
      </bpmn:userTask>
    </bpmn:subProcess>
    <bpmn:sequenceFlow id="SequenceFlow_0u0ebzc" sourceRef="StartEvent_1" targetRef="SubProcess_0phiv1k" />
    <bpmn:endEvent id="EndEvent_0wsgk5o">
      <bpmn:incoming>SequenceFlow_0k92bg2</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_0k92bg2" sourceRef="SubProcess_0phiv1k" targetRef="EndEvent_0wsgk5o" />
  </bpmn:process>
</bpmn:definitions>
`;

export const BPMN_XML_MIGRATION_INITIAL = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:proceed="https://docs.proceed-labs.org/BPMN" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_d3d75af4-7075-46cc-bec5-4a7696633154" name="Test" targetNamespace="https://docs.proceed-labs.org/_d3d75af4-7075-46cc-bec5-4a7696633154" expressionLanguage="https://ecma-international.org/ecma-262/8.0/" typeLanguage="https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" exporter="PROCEED Management System" exporterVersion="1.0.0" proceed:creatorEnvironmentId="" proceed:creatorEnvironmentName="" xsi:schemaLocation="https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd">
  <process id="Process_05bdhx0" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <startEvent id="StartEvent_0kbrms3">
      <outgoing>Flow_1rhengj</outgoing>
    </startEvent>
    <task id="Activity_16tbasu">
      <incoming>Flow_1rhengj</incoming>
      <outgoing>Flow_0ucazbk</outgoing>
    </task>
    <sequenceFlow id="Flow_1rhengj" sourceRef="StartEvent_0kbrms3" targetRef="Activity_16tbasu" />
    <endEvent id="Event_1s7fpk8">
      <incoming>Flow_0ucazbk</incoming>
    </endEvent>
    <sequenceFlow id="Flow_0ucazbk" sourceRef="Activity_16tbasu" targetRef="Event_1s7fpk8" />
  </process>
</definitions>
`;

export const BPMN_XML_MIGRATION_EXTENDED = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:proceed="https://docs.proceed-labs.org/BPMN" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_d3d75af4-7075-46cc-bec5-4a7696633154" name="Test" targetNamespace="https://docs.proceed-labs.org/_d3d75af4-7075-46cc-bec5-4a7696633154" expressionLanguage="https://ecma-international.org/ecma-262/8.0/" typeLanguage="https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" exporter="PROCEED Management System" exporterVersion="1.0.0" proceed:creatorEnvironmentId="" proceed:creatorEnvironmentName="" xsi:schemaLocation="https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd">
  <process id="Process_05bdhx0" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <startEvent id="StartEvent_0kbrms3">
      <outgoing>Flow_1rhengj</outgoing>
    </startEvent>
    <task id="Activity_16tbasu">
      <incoming>Flow_1rhengj</incoming>
      <outgoing>Flow_0fxl630</outgoing>
    </task>
    <sequenceFlow id="Flow_1rhengj" sourceRef="StartEvent_0kbrms3" targetRef="Activity_16tbasu" />
    <endEvent id="Event_1s7fpk8">
      <incoming>Flow_0gia3hu</incoming>
    </endEvent>
    <task id="Activity_0y4lned">
      <incoming>Flow_0fxl630</incoming>
      <outgoing>Flow_0gia3hu</outgoing>
    </task>
    <sequenceFlow id="Flow_0fxl630" sourceRef="Activity_16tbasu" targetRef="Activity_0y4lned" />
    <sequenceFlow id="Flow_0gia3hu" sourceRef="Activity_0y4lned" targetRef="Event_1s7fpk8" />
  </process>
</definitions>`;

export const BPMN_XML_MIGRATION_REPLACEMENT = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:proceed="https://docs.proceed-labs.org/BPMN" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_d3d75af4-7075-46cc-bec5-4a7696633154" name="Test" targetNamespace="https://docs.proceed-labs.org/_d3d75af4-7075-46cc-bec5-4a7696633154" expressionLanguage="https://ecma-international.org/ecma-262/8.0/" typeLanguage="https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" exporter="PROCEED Management System" exporterVersion="1.0.0" proceed:creatorEnvironmentId="" proceed:creatorEnvironmentName="" xsi:schemaLocation="https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd">
  <process id="Process_05bdhx0" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <startEvent id="StartEvent_0kbrms3">
      <outgoing>Flow_1stp2tv</outgoing>
    </startEvent>
    <endEvent id="Event_1s7fpk8">
      <incoming>Flow_0uvbcbq</incoming>
    </endEvent>
    <sequenceFlow id="Flow_1stp2tv" sourceRef="StartEvent_0kbrms3" targetRef="Activity_0l50qj6" />
    <sequenceFlow id="Flow_0uvbcbq" sourceRef="Activity_0l50qj6" targetRef="Event_1s7fpk8" />
    <userTask id="Activity_0l50qj6">
      <incoming>Flow_1stp2tv</incoming>
      <outgoing>Flow_0uvbcbq</outgoing>
    </userTask>
  </process>
</definitions>
`;

export const BPMN_XML_MIGRATION_TYPE_CHANGE = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:proceed="https://docs.proceed-labs.org/BPMN" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_d3d75af4-7075-46cc-bec5-4a7696633154" name="Test" targetNamespace="https://docs.proceed-labs.org/_d3d75af4-7075-46cc-bec5-4a7696633154" expressionLanguage="https://ecma-international.org/ecma-262/8.0/" typeLanguage="https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" exporter="PROCEED Management System" exporterVersion="1.0.0" proceed:creatorEnvironmentId="" proceed:creatorEnvironmentName="" xsi:schemaLocation="https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd">
  <process id="Process_05bdhx0" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <startEvent id="StartEvent_0kbrms3">
      <outgoing>Flow_1rhengj</outgoing>
    </startEvent>
    <userTask id="Activity_16tbasu">
      <incoming>Flow_1rhengj</incoming>
      <outgoing>Flow_0ucazbk</outgoing>
    </userTask>
    <sequenceFlow id="Flow_1rhengj" sourceRef="StartEvent_0kbrms3" targetRef="Activity_16tbasu" />
    <endEvent id="Event_1s7fpk8">
      <incoming>Flow_0ucazbk</incoming>
    </endEvent>
    <sequenceFlow id="Flow_0ucazbk" sourceRef="Activity_16tbasu" targetRef="Event_1s7fpk8" />
  </process>
</definitions>
`;

export const BPMN_XML_MIGRATION_EXTENDED_PARALLELIZATION = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:proceed="https://docs.proceed-labs.org/BPMN" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_d3d75af4-7075-46cc-bec5-4a7696633154" name="Test" targetNamespace="https://docs.proceed-labs.org/_d3d75af4-7075-46cc-bec5-4a7696633154" expressionLanguage="https://ecma-international.org/ecma-262/8.0/" typeLanguage="https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" exporter="PROCEED Management System" exporterVersion="1.0.0" proceed:creatorEnvironmentId="" proceed:creatorEnvironmentName="" xsi:schemaLocation="https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd">
  <process id="Process_05bdhx0" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <startEvent id="StartEvent_0kbrms3">
      <outgoing>Flow_0anlk8b</outgoing>
    </startEvent>
    <task id="Activity_16tbasu">
      <incoming>Flow_1uq67qt</incoming>
      <outgoing>Flow_07ry6wu</outgoing>
    </task>
    <endEvent id="Event_1s7fpk8">
      <incoming>Flow_0z90co4</incoming>
    </endEvent>
    <task id="Activity_0y4lned">
      <incoming>Flow_010iask</incoming>
      <outgoing>Flow_0pldfeh</outgoing>
    </task>
    <sequenceFlow id="Flow_0anlk8b" sourceRef="StartEvent_0kbrms3" targetRef="Gateway_1xlnlqm" />
    <sequenceFlow id="Flow_1uq67qt" sourceRef="Gateway_1xlnlqm" targetRef="Activity_16tbasu" />
    <sequenceFlow id="Flow_010iask" sourceRef="Gateway_1xlnlqm" targetRef="Activity_0y4lned" />
    <sequenceFlow id="Flow_0pldfeh" sourceRef="Activity_0y4lned" targetRef="Gateway_0f6s2kq" />
    <sequenceFlow id="Flow_07ry6wu" sourceRef="Activity_16tbasu" targetRef="Gateway_0f6s2kq" />
    <sequenceFlow id="Flow_0z90co4" sourceRef="Gateway_0f6s2kq" targetRef="Event_1s7fpk8" />
    <parallelGateway id="Gateway_0f6s2kq">
      <incoming>Flow_0pldfeh</incoming>
      <incoming>Flow_07ry6wu</incoming>
      <outgoing>Flow_0z90co4</outgoing>
    </parallelGateway>
    <parallelGateway id="Gateway_1xlnlqm">
      <incoming>Flow_0anlk8b</incoming>
      <outgoing>Flow_1uq67qt</outgoing>
      <outgoing>Flow_010iask</outgoing>
    </parallelGateway>
  </process>
</definitions>
`;

export const BPMN_XML_MIGRATION_WITHOUT_START_EVENT_TYPE_CHANGE = `
<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1dzf93e" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:userTask id="Task_175vb4p" name="Activity 1">
      <bpmn:outgoing>SequenceFlow_12bv6av</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:endEvent id="EndEvent_1umdfcw">
      <bpmn:incoming>SequenceFlow_12bv6av</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_12bv6av" sourceRef="Task_175vb4p" targetRef="EndEvent_1umdfcw" />
  </bpmn:process>
</bpmn:definitions>`;

export const BPMN_XML_MIGRATION_SUBPROCESS_INITIAL = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:proceed="https://docs.proceed-labs.org/BPMN" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_43f6cae5-507f-4b54-a88e-d589da164687" name="Test" targetNamespace="https://docs.proceed-labs.org/_43f6cae5-507f-4b54-a88e-d589da164687" exporter="PROCEED Management System" exporterVersion="1.0.0">
  <process id="Process_0kagopf" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <startEvent id="Event_1cilees">
      <outgoing>Flow_1evmi3x</outgoing>
    </startEvent>
    <subProcess id="Activity_0cl2rre">
      <incoming>Flow_1evmi3x</incoming>
      <outgoing>Flow_0n13ws7</outgoing>
      <startEvent id="Event_1c3mcer">
        <outgoing>Flow_1r5xfcd</outgoing>
      </startEvent>
      <sequenceFlow id="Flow_1r5xfcd" sourceRef="Event_1c3mcer" targetRef="Activity_017604y" />
      <endEvent id="Event_0xoyqmm">
        <incoming>Flow_1qg7njb</incoming>
      </endEvent>
      <sequenceFlow id="Flow_1qg7njb" sourceRef="Activity_017604y" targetRef="Event_0xoyqmm" />
      <userTask id="Activity_017604y">
        <incoming>Flow_1r5xfcd</incoming>
        <outgoing>Flow_1qg7njb</outgoing>
      </userTask>
    </subProcess>
    <sequenceFlow id="Flow_1evmi3x" sourceRef="Event_1cilees" targetRef="Activity_0cl2rre" />
    <endEvent id="Event_0sldb7w">
      <incoming>Flow_0n13ws7</incoming>
    </endEvent>
    <sequenceFlow id="Flow_0n13ws7" sourceRef="Activity_0cl2rre" targetRef="Event_0sldb7w" />
  </process>
</definitions>`;

export const BPMN_XML_MIGRATION_SUBPROCESS_ADDED_TASK = `
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:proceed="https://docs.proceed-labs.org/BPMN" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_43f6cae5-507f-4b54-a88e-d589da164687" name="Test" targetNamespace="https://docs.proceed-labs.org/_43f6cae5-507f-4b54-a88e-d589da164687" expressionLanguage="https://ecma-international.org/ecma-262/8.0/" typeLanguage="https://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" exporter="PROCEED Management System" exporterVersion="1.0.0" proceed:version="1671629408848" proceed:versionName="b" proceed:versionDescription="b" proceed:versionBasedOn="1671377686408" proceed:creatorEnvironmentId="" proceed:creatorEnvironmentName="" xsi:schemaLocation="https://docs.proceed-labs.org/BPMN https://docs.proceed-labs.org/xsd/XSD-PROCEED.xsd http://www.omg.org/spec/BPMN/20100524/MODEL https://www.omg.org/spec/BPMN/20100501/BPMN20.xsd">
  <process id="Process_0kagopf" name="PROCEED Main Process" processType="Private" isExecutable="true">
    <startEvent id="Event_1cilees">
      <outgoing>Flow_1evmi3x</outgoing>
    </startEvent>
    <subProcess id="Activity_0cl2rre">
      <incoming>Flow_1evmi3x</incoming>
      <outgoing>Flow_0n13ws7</outgoing>
      <startEvent id="Event_1c3mcer">
        <outgoing>Flow_1r5xfcd</outgoing>
      </startEvent>
      <sequenceFlow id="Flow_1r5xfcd" sourceRef="Event_1c3mcer" targetRef="Activity_017604y" />
      <endEvent id="Event_0xoyqmm">
        <incoming>Flow_0qbicyr</incoming>
      </endEvent>
      <userTask id="Activity_017604y" name="A">
        <incoming>Flow_1r5xfcd</incoming>
        <outgoing>Flow_1szu93y</outgoing>
      </userTask>
      <sequenceFlow id="Flow_1szu93y" sourceRef="Activity_017604y" targetRef="Activity_19zpusa" />
      <sequenceFlow id="Flow_0qbicyr" sourceRef="Activity_19zpusa" targetRef="Event_0xoyqmm" />
      <userTask id="Activity_19zpusa" name="B">
        <incoming>Flow_1szu93y</incoming>
        <outgoing>Flow_0qbicyr</outgoing>
      </userTask>
    </subProcess>
    <sequenceFlow id="Flow_1evmi3x" sourceRef="Event_1cilees" targetRef="Activity_0cl2rre" />
    <endEvent id="Event_0sldb7w">
      <incoming>Flow_0n13ws7</incoming>
    </endEvent>
    <sequenceFlow id="Flow_0n13ws7" sourceRef="Activity_0cl2rre" targetRef="Event_0sldb7w" />
  </process>
</definitions>
`;
