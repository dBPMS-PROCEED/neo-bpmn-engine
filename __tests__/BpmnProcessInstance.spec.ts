import { BPMN_XML_SCRIPT_TASK_MULTIPLE_VAR, BPMN_XML_SINGLE_NONE_START_EVENT } from './fixtures/bpmnXml';

import BpmnProcess from '../src/BpmnProcess';
import BpmnProcessInstance from '../src/BpmnProcessInstance';

import { Definitions } from 'bpmn-moddle';
import BpmnModdle from '../src/util/BpmnModdle';

function toModdle(xml: string): Promise<Definitions> {
  return new Promise((resolve, reject) => {
    new BpmnModdle().fromXML(xml, (err, definitions) => {
      if (err) {
        reject(err);
        return;
      }

      resolve(definitions);
    });
  });
}

let mockNoneStartDefinitions: Definitions;

beforeAll(async () => {
  mockNoneStartDefinitions = await toModdle(BPMN_XML_SINGLE_NONE_START_EVENT);
});

let mockNoneStartInstance: BpmnProcessInstance;
let mockFlowNodeActivationHook = jest.fn();
let mockShouldPassHook = jest.fn();
let mockOnEnded = jest.fn();

beforeEach(async () => {
  mockNoneStartInstance = BpmnProcessInstance.fromModdleDefinitions({
    processId: 'mockProcess',
    id: 'mockInstance',
    globalStartTime: 0,
    definitions: mockNoneStartDefinitions,
    variables: {},
    log: [],
    tokens: [],
    shouldActivateFlowNodeHook: mockFlowNodeActivationHook,
    shouldPassTokenHook: mockShouldPassHook
  });
  mockNoneStartInstance.onEnded(mockOnEnded);
  mockFlowNodeActivationHook.mockReset();
  mockShouldPassHook.mockReset();
  mockFlowNodeActivationHook.mockImplementation(async () => true);
  mockShouldPassHook.mockImplementation(async () => true);
  mockOnEnded.mockReset();
});

describe('Flow Node State Functions', () => {
  describe('getSequenceFlowId', () => {
    it('returns the id of a sequence flow between two flow nodes', () => {
      const sequenceId = mockNoneStartInstance.getSequenceFlowId('StartEvent_1', 'Task_175vb4p');
      expect(sequenceId).toBe('SequenceFlow_07so23a');
    });
  });

  describe('ending an activity through the API', () => {
    beforeEach(() => {
      mockFlowNodeActivationHook.mockImplementation(async () => false);
      mockNoneStartInstance.placeTokenAt('Task_175vb4p', { tokenId: 'mockToken' });
      expect(mockNoneStartInstance.getOpenElements('bpmn:Task').length).toBe(1);
    });

    describe('completeActivity', () => {
      it('should not do anything if token was not found', done => {
        mockNoneStartInstance.completeActivity('Task_175vb4p', 'nonexistant-token');

        setTimeout(() => {
          const openTasks = mockNoneStartInstance.getOpenElements('bpmn:Task');
          expect(openTasks.length).toEqual(1);

          expect(mockOnEnded).toHaveBeenCalledTimes(0);

          done();
        }, 10);
      });

      it('should complete the activity that was open', done => {
        mockShouldPassHook.mockImplementation(async () => {
          expect(mockNoneStartInstance.getState().tokens[0]).toEqual({
            tokenId: 'mockToken',
            state: 'READY',
            localStartTime: expect.any(Number),
            localExecutionTime: expect.any(Number),
            currentFlowElementId: 'SequenceFlow_12bv6av',
            currentFlowElementStartTime: expect.any(Number),
            previousFlowElementId: 'Task_175vb4p',
            intermediateVariablesState: null
          });

          expect(mockOnEnded).toHaveBeenCalledTimes(0);

          done();
        });

        mockNoneStartInstance.completeActivity('Task_175vb4p', 'mockToken');
      });

      it('should apply optional variable changes', done => {
        expect(mockNoneStartInstance.getVariables()).toEqual({});

        setTimeout(() => {
          expect(mockNoneStartInstance.getVariables()).toEqual({
            test: 123
          });

          done();
        }, 10);

        mockNoneStartInstance.completeActivity('Task_175vb4p', 'mockToken', { test: 123 });
      });
    });

    describe('terminateActivity', () => {
      it('should not do anything if token was not found', done => {
        mockNoneStartInstance.terminateActivity('Task_175vb4p', 'nonexistant-token');

        setTimeout(() => {
          const openTasks = mockNoneStartInstance.getOpenElements('bpmn:Task');
          expect(openTasks.length).toEqual(1);

          expect(mockOnEnded).toHaveBeenCalledTimes(0);

          done();
        }, 10);
      });

      it('should terminate the activity that was open', done => {
        mockNoneStartInstance.terminateActivity('Task_175vb4p', 'mockToken');

        setTimeout(() => {
          const openTasks = mockNoneStartInstance.getOpenElements('bpmn:Task');
          expect(openTasks.length).toEqual(0);
          expect(mockNoneStartInstance.getState().tokens[0]).toEqual({
            tokenId: 'mockToken',
            state: 'TERMINATED',
            localStartTime: expect.any(Number),
            localExecutionTime: expect.any(Number),
            currentFlowElementId: 'Task_175vb4p',
            currentFlowNodeState: 'TERMINATED',
            currentFlowElementStartTime: expect.any(Number),
            intermediateVariablesState: null
          });

          expect(mockOnEnded).toHaveBeenCalledTimes(1);

          done();
        }, 10);
      });
    });

    describe('failActivity', () => {
      it('should not do anything if token was not found', done => {
        mockNoneStartInstance.failActivity('Task_175vb4p', 'nonexistant-token');

        setTimeout(() => {
          const openTasks = mockNoneStartInstance.getOpenElements('bpmn:Task');
          expect(openTasks.length).toEqual(1);

          expect(mockOnEnded).toHaveBeenCalledTimes(0);

          done();
        }, 10);
      });

      it('should abort the activity that was open', done => {
        mockNoneStartInstance.failActivity('Task_175vb4p', 'mockToken');

        setTimeout(() => {
          const openTasks = mockNoneStartInstance.getOpenElements('bpmn:Task');
          expect(openTasks.length).toEqual(0);
          expect(mockNoneStartInstance.getState().tokens[0]).toEqual({
            tokenId: 'mockToken',
            state: 'FAILED',
            localStartTime: expect.any(Number),
            localExecutionTime: expect.any(Number),
            currentFlowElementId: 'Task_175vb4p',
            currentFlowNodeState: 'FAILED',
            currentFlowElementStartTime: expect.any(Number),
            intermediateVariablesState: null
          });

          expect(mockOnEnded).toHaveBeenCalledTimes(1);

          done();
        }, 10);
      });
      // TODO: Test Error Propagation
    });
  });

  describe('setFlowNodeState', () => {
    it('changes the state of a flowNode to a user provided state', done => {
      mockFlowNodeActivationHook.mockImplementation(async () => {
        const [token] = mockNoneStartInstance.getState().tokens;

        expect(token.currentFlowNodeState).toBe('READY');

        mockNoneStartInstance.setFlowNodeState(token.tokenId, 'SOME STATE');

        setTimeout(() => {
          const [changedToken] = mockNoneStartInstance.getState().tokens;

          expect(changedToken).toStrictEqual({
            ...token,
            currentFlowNodeState: 'SOME STATE'
          });

          done();
        }, 10);

        return false;
      });

      mockNoneStartInstance.placeTokenAt('SequenceFlow_07so23a', { tokenId: 'mockToken' });
    });
  });
});

describe('variable state', () => {
  let singleTaskProcess: BpmnProcess;
  let multiVarProcess: BpmnProcess;
  let intermediateVarProcess: BpmnProcess;

  let singleTaskActivateFlowNodeHook = jest.fn();

  beforeAll(async () => {
    singleTaskProcess = await BpmnProcess.fromXml('testProcess', BPMN_XML_SINGLE_NONE_START_EVENT, {
      shouldActivateFlowNodeHook: singleTaskActivateFlowNodeHook
    });

    singleTaskProcess.deploy();

    multiVarProcess = await BpmnProcess.fromXml('multiVar', BPMN_XML_SCRIPT_TASK_MULTIPLE_VAR);
    multiVarProcess.deploy();

    intermediateVarProcess = await BpmnProcess.fromXml(
      'intermediateVar',
      `
    <?xml version="1.0" encoding="UTF-8"?>
    <definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="_3fc415fc-fa49-44f3-9f5b-f459cc55fd08" name="Test" targetNamespace="https://docs.proceed-labs.org/_3fc415fc-fa49-44f3-9f5b-f459cc55fd08" exporter="PROCEED Management System" exporterVersion="1.0.0">
      <process id="Process_1gcn08i" name="PROCEED Main Process" processType="Private" isExecutable="true">
        <startEvent id="StartEvent_0knyffd">
          <outgoing>Flow_1kzpfjw</outgoing>
        </startEvent>
        <sequenceFlow id="Flow_1kzpfjw" sourceRef="StartEvent_0knyffd" targetRef="Activity_045n21g" />
        <sequenceFlow id="Flow_152imfp" sourceRef="Activity_045n21g" targetRef="Activity_0r4rf4b" />
        <endEvent id="Event_0xk1gqb">
          <incoming>Flow_0kp5lbh</incoming>
        </endEvent>
        <sequenceFlow id="Flow_0kp5lbh" sourceRef="Activity_0r4rf4b" targetRef="Event_0xk1gqb" />
        <scriptTask id="Activity_045n21g" scriptFormat="application/javascript">
          <incoming>Flow_1kzpfjw</incoming>
          <outgoing>Flow_152imfp</outgoing>
    <script>
      variable.set('a', 10);
    </script>
        </scriptTask>
        <scriptTask id="Activity_0r4rf4b" scriptFormat="application/javascript">
          <incoming>Flow_152imfp</incoming>
          <outgoing>Flow_0kp5lbh</outgoing>
    <script>
      variable.set('a', variable.get('a') + 10);
      await new Promise((resolve) => setTimeout(resolve, 2000));
      variable.set('b', 'Test');
    </script>
        </scriptTask>
      </process>
    </definitions>`
    );
    intermediateVarProcess.deploy();
  });

  beforeEach(() => {
    singleTaskActivateFlowNodeHook.mockReset();
    singleTaskActivateFlowNodeHook.mockResolvedValue(true);
  });

  describe('getVariables()', () => {
    it('returns the values of all variables in the instance', done => {
      multiVarProcess.getInstance$().subscribe(instance => {
        instance.onEnded(() => {
          const variables = instance.getVariables();

          expect(variables).toStrictEqual({
            a: 20,
            b: 'Test',
            c: ['initial', 'variable']
          });

          done();
        });
      });

      multiVarProcess.start({
        variables: { c: { value: ['initial', 'variable'] } }
      });
    });

    it('will merge the instance variables with the intermediate variable values of a token if given a token id', done => {
      let instance: BpmnProcessInstance;
      let tokenId: string;

      intermediateVarProcess.getInstance$().subscribe(inst => {
        instance = inst;
        ({ tokenId } = instance.getState().tokens[0]);

        setTimeout(() => {
          const variables = instance.getVariables(tokenId);

          const state = instance.getState();

          expect(state.variables).toStrictEqual({
            a: { value: 10, log: [{ changedTime: expect.any(Number), changedBy: 'Activity_045n21g' }] },
            c: { value: ['initial', 'variable'], log: [{ changedTime: expect.any(Number) }] }
          });
          //console.log(state);
          expect(state.tokens[0].intermediateVariablesState).toStrictEqual({
            a: 20
          });

          expect(variables).toStrictEqual({
            a: 20,
            c: ['initial', 'variable']
          });

          done();
        }, 1000);
      });

      intermediateVarProcess.start({
        variables: { c: { value: ['initial', 'variable'] } }
      });
    });
  });

  describe('getVariablesWithLogs()', () => {
    it('returns the values and logging information for all variables in the instance', done => {
      multiVarProcess.getInstance$().subscribe(instance => {
        instance.onEnded(() => {
          const variables = instance.getVariablesWithLogs();

          expect(variables).toStrictEqual({
            a: {
              value: 20,
              log: [
                { changedTime: expect.any(Number), changedBy: 'Activity_045n21g' },
                { changedTime: expect.any(Number), changedBy: 'Activity_0r4rf4b', oldValue: 10 }
              ]
            },
            b: { value: 'Test', log: [{ changedTime: expect.any(Number), changedBy: 'Activity_0r4rf4b' }] },
            c: { value: ['initial', 'variable'], log: [{ changedTime: 111, changedBy: 'some activity' }] }
          });

          done();
        });
      });

      multiVarProcess.start({
        variables: { c: { value: ['initial', 'variable'], log: [{ changedTime: 111, changedBy: 'some activity' }] } }
      });
    });

    it('will merge the instance variables with the intermediate variable values of a token if given a token id', done => {
      let instance: BpmnProcessInstance;
      let tokenId: string;

      intermediateVarProcess.getInstance$().subscribe(inst => {
        instance = inst;
        ({ tokenId } = instance.getState().tokens[0]);

        instance.onEnded(done);

        setTimeout(() => {
          const variables = instance.getVariablesWithLogs(tokenId);

          const state = instance.getState();

          expect(state.variables).toStrictEqual({
            a: { value: 10, log: [{ changedTime: expect.any(Number), changedBy: 'Activity_045n21g' }] },
            c: { value: ['initial', 'variable'], log: [{ changedTime: expect.any(Number) }] }
          });

          expect(state.tokens[0].intermediateVariablesState).toStrictEqual({
            a: 20
          });

          expect(variables).toStrictEqual({
            a: {
              value: 20,
              log: [
                { changedTime: expect.any(Number), changedBy: 'Activity_0r4rf4b', oldValue: 10 },
                { changedTime: expect.any(Number), changedBy: 'Activity_045n21g' }
              ]
            },
            c: { value: ['initial', 'variable'], log: [{ changedTime: expect.any(Number) }] }
          });
        }, 1000);
      });

      intermediateVarProcess.start({
        variables: { c: { value: ['initial', 'variable'] } }
      });
    });
  });

  describe('setVariable()', () => {
    it('allows to change a variable in the name of a flow element', async done => {
      singleTaskActivateFlowNodeHook.mockImplementation(async (processId, processInstanceId, tokenId, flowNode) => {
        if (flowNode.id === 'Task_175vb4p') {
          const instance = singleTaskProcess.getInstanceById(processInstanceId);

          instance.setVariable(tokenId, 'var', 'test');

          setTimeout(() => {
            const variables = instance.getVariablesWithLogs();

            expect(variables).toStrictEqual({
              var: { value: 'test', log: [{ changedTime: expect.any(Number), changedBy: flowNode.id }] }
            });

            done();
          }, 10);
        }

        return true;
      });

      singleTaskProcess.start();
    });

    it('will write the new value into the tokens intermediateVariablesState', async done => {
      singleTaskActivateFlowNodeHook.mockImplementation(async (processId, processInstanceId, tokenId, flowNode) => {
        if (flowNode.id === 'Task_175vb4p') {
          const instance = singleTaskProcess.getInstanceById(processInstanceId);

          instance.setVariable(tokenId, 'var', 'test');

          setTimeout(() => {
            expect(instance.getState().tokens[0].intermediateVariablesState).toStrictEqual({
              var: 'test'
            });

            const variables = instance.getVariablesWithLogs();
            expect(variables).toStrictEqual({});

            done();
          }, 10);

          return false;
        }

        return true;
      });

      singleTaskProcess.start();
    });

    it('will write the new value directly into the global instance state if the global flag is set', async done => {
      singleTaskActivateFlowNodeHook.mockImplementation(async (processId, processInstanceId, tokenId, flowNode) => {
        if (flowNode.id === 'Task_175vb4p') {
          const instance = singleTaskProcess.getInstanceById(processInstanceId);

          instance.setVariable(tokenId, 'var', 'test', true);

          setTimeout(() => {
            expect(instance.getState().tokens[0].intermediateVariablesState).toStrictEqual({});

            const variables = instance.getVariablesWithLogs();
            expect(variables).toStrictEqual({
              var: { value: 'test', log: [{ changedBy: 'Task_175vb4p', changedTime: expect.any(Number) }] }
            });

            done();
          }, 10);

          return false;
        }

        return true;
      });

      singleTaskProcess.start();
    });
  });
});
