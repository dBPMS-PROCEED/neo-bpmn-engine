import { Definitions } from 'bpmn-moddle';
import BpmnProcess from '../../src/BpmnProcess';
import BpmnProcessInstance from '../../src/BpmnProcessInstance';
import { AdaptationType } from '../../src/state/reducers/adaptationLog';
import { ExecutionState } from '../../src/state/reducers/log';
import { FlowNodeState, TokenState } from '../../src/state/reducers/tokens';
import BpmnModdle from '../../src/util/BpmnModdle';

import {
  BPMN_XML_MIGRATION_INITIAL,
  BPMN_XML_MIGRATION_EXTENDED,
  BPMN_XML_MIGRATION_EXTENDED_PARALLELIZATION,
  BPMN_XML_MIGRATION_REPLACEMENT,
  BPMN_XML_MIGRATION_TYPE_CHANGE,
  BPMN_XML_MIGRATION_SUBPROCESS_INITIAL,
  BPMN_XML_MIGRATION_SUBPROCESS_ADDED_TASK
} from '../fixtures/bpmnXml';

const moddle = new BpmnModdle();

function toModdle(xml: string): Promise<Definitions> {
  return new Promise((resolve, reject) => {
    moddle.fromXML(xml, (err, definitions) => {
      if (err) {
        reject(err);
        return;
      }

      resolve(definitions);
    });
  });
}

describe('BpmnProcessInstance.migrate()', () => {
  it('should allow migration to a new process model while running', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model contains an additional task following the currently active one
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const newModdle = await toModdle(BPMN_XML_MIGRATION_EXTENDED);

          expect(() => instance.migrate('otherProcess', { moddleDefinitions: newModdle })).not.toThrow();

          setTimeout(() => {
            const state = instance.getState();

            expect(instance.isEnded()).toBeTruthy();

            expect(state.processId).toBe('otherProcess');

            expect(state.tokens.length).toBe(1);
            expect(state.tokens).toEqual([
              {
                tokenId: tId,
                state: TokenState.ENDED,
                currentFlowElementId: 'Event_1s7fpk8',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0gia3hu',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log.length).toBe(4);
            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_0kbrms3',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_16tbasu',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_0y4lned',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Event_1s7fpk8',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.adaptationLog).toEqual([
              {
                type: AdaptationType.MIGRATION,
                time: expect.any(Number),
                from: 'simpleProcess',
                to: 'otherProcess'
              }
            ]);

            done();
          }, 30);

          await new Promise(resolve => setTimeout(resolve, 10));
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  it('should apply desired token move alongside the change of the moddleDefinitions', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model contains a new task instead of the current one
      // this requires an additional token mapping to start the new task instead of the currently running one
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        const instance = simpleProcess.getInstanceById(instanceId);
        if (flowNode.id === 'Activity_16tbasu') {
          const newModdle = await toModdle(BPMN_XML_MIGRATION_REPLACEMENT);

          expect(() =>
            instance.migrate('otherProcess', {
              moddleDefinitions: newModdle,
              tokenMapping: {
                move: [{ targetFlowElementId: 'Flow_1stp2tv', tokenId: tId }]
              }
            })
          ).not.toThrow();
        }

        // we expect the user task that is replacing the task to become active after the instance was migrated with the given token mapping
        if (flowNode.id === 'Activity_0l50qj6') {
          const state = instance.getState();

          expect(state.processId).toBe('otherProcess');

          expect(state.tokens.length).toBe(1);
          expect(state.tokens).toEqual([
            {
              tokenId: tId,
              state: TokenState.RUNNING,
              currentFlowElementId: 'Activity_0l50qj6',
              currentFlowNodeState: FlowNodeState.READY,
              currentFlowElementStartTime: expect.any(Number),
              previousFlowElementId: 'Flow_1stp2tv',
              localStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              intermediateVariablesState: {}
            }
          ]);

          expect(state.log.length).toBe(2);
          expect(state.log).toEqual([
            {
              flowElementId: 'StartEvent_0kbrms3',
              tokenId: tId,
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'Activity_16tbasu',
              tokenId: tId,
              executionState: ExecutionState.SKIPPED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            }
          ]);

          expect(state.adaptationLog).toEqual([
            {
              type: AdaptationType.MIGRATION,
              time: expect.any(Number),
              from: 'simpleProcess',
              to: 'otherProcess'
            },
            {
              type: AdaptationType.TOKEN_MOVE,
              time: expect.any(Number),
              tokenId: tId,
              currentFlowElementId: 'Activity_16tbasu',
              targetFlowElementId: 'Flow_1stp2tv'
            }
          ]);

          done();
          return false;
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  it('should apply desired token addition alongside the change of the moddleDefinitions', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_EXTENDED, {
      // migrate the instance on activation of the task; the new model has both tasks in parallel
      // this requires an additional token mapping to place a new token in the inactive parallel path to prevent a deadlock of the process
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const newModdle = await toModdle(BPMN_XML_MIGRATION_EXTENDED_PARALLELIZATION);

          expect(() =>
            instance.migrate('otherProcess', {
              moddleDefinitions: newModdle,
              tokenMapping: {
                add: [{ targetFlowElementId: 'Flow_010iask' }]
              }
            })
          ).not.toThrow();

          setTimeout(() => {
            const state = instance.getState();

            expect(instance.isEnded()).toBeTruthy();

            expect(state.processId).toBe('otherProcess');

            expect(state.tokens.length).toBe(1);
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                state: TokenState.ENDED,
                currentFlowElementId: 'Event_1s7fpk8',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0z90co4',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log.length).toBe(5);
            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_0kbrms3',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_0y4lned',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_16tbasu',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Gateway_0f6s2kq',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Event_1s7fpk8',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.adaptationLog).toEqual([
              {
                type: AdaptationType.MIGRATION,
                time: expect.any(Number),
                from: 'simpleProcess',
                to: 'otherProcess'
              },
              {
                type: AdaptationType.TOKEN_ADD,
                time: expect.any(Number),
                tokenId: expect.any(String),
                targetFlowElementId: 'Flow_010iask'
              }
            ]);

            done();
          }, 30);

          await new Promise(resolve => setTimeout(resolve, 10));
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  it('should apply desired token removal and move alongside the change of the moddleDefinitions', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_EXTENDED_PARALLELIZATION, {
      // migrate the instance on activation of the task; the new model does not contain one of the parallel paths
      // this requires the removal of the token in that path and a mapping of the other token behind the removed parallel merge
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_0y4lned') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const newModdle = await toModdle(BPMN_XML_MIGRATION_INITIAL);

          await new Promise(resolve => setTimeout(resolve, 10));

          const otherToken = instance.getState().tokens.find(token => token.tokenId !== tId)!;

          expect(() =>
            instance.migrate('otherProcess', {
              moddleDefinitions: newModdle,
              tokenMapping: {
                move: [{ tokenId: otherToken.tokenId, targetFlowElementId: 'Flow_0ucazbk' }],
                remove: [tId]
              }
            })
          ).not.toThrow();

          setTimeout(() => {
            const state = instance.getState();

            expect(instance.isEnded()).toBeTruthy();

            expect(state.processId).toBe('otherProcess');

            expect(state.tokens.length).toBe(1);
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                state: TokenState.ENDED,
                currentFlowElementId: 'Event_1s7fpk8',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0ucazbk',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log.length).toBe(6);
            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_0kbrms3',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Gateway_1xlnlqm',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_16tbasu',
                tokenId: otherToken.tokenId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_0y4lned',
                tokenId: tId,
                executionState: ExecutionState.SKIPPED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Gateway_0f6s2kq',
                tokenId: otherToken.tokenId,
                executionState: ExecutionState.SKIPPED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Event_1s7fpk8',
                tokenId: otherToken.tokenId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.adaptationLog).toEqual([
              {
                type: AdaptationType.MIGRATION,
                time: expect.any(Number),
                from: 'simpleProcess',
                to: 'otherProcess'
              },
              {
                type: AdaptationType.TOKEN_REMOVE,
                time: expect.any(Number),
                tokenId: tId,
                currentFlowElementId: 'Activity_0y4lned'
              },
              {
                type: AdaptationType.TOKEN_MOVE,
                time: expect.any(Number),
                tokenId: otherToken.tokenId,
                currentFlowElementId: 'Gateway_0f6s2kq',
                targetFlowElementId: 'Flow_0ucazbk'
              }
            ]);

            done();
          }, 30);

          await new Promise(resolve => setTimeout(resolve, 10));
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  it('will inform the user about a task being interrupted when it was removed during migration (token remove)', async done => {
    const onTaskInterrupt = jest.fn();
    const onEnded = jest.fn();
    const onStateChange = jest.fn();

    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model does not contain the current task
      // this will lead to the task being interrupted and the token being removed
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const newModdle = await toModdle(BPMN_XML_MIGRATION_REPLACEMENT);

          await new Promise(resolve => setTimeout(resolve, 10));

          expect(() =>
            instance.migrate('otherProcess', {
              moddleDefinitions: newModdle,
              tokenMapping: {
                add: [{ targetFlowElementId: 'Flow_1stp2tv' }],
                remove: [tId]
              }
            })
          ).not.toThrow();

          setTimeout(() => {
            expect(instance.getState().tokens).toEqual([
              {
                tokenId: expect.any(String),
                state: TokenState.RUNNING,
                currentFlowElementId: 'Activity_0l50qj6',
                currentFlowNodeState: FlowNodeState.ACTIVE,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1stp2tv',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              }
            ]);

            expect(instance.getState().log).toContainEqual({
              flowElementId: 'Activity_16tbasu',
              tokenId: tId,
              executionState: ExecutionState.SKIPPED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            });

            expect(onTaskInterrupt).toHaveBeenCalledTimes(1);

            // When the only token in the instance is removed the instance should normally be stopped
            // but we don't want this to happen when it is only an intermediate state during the migration during which another token will be added
            expect(onEnded).not.toHaveBeenCalled();
            expect(onStateChange).not.toHaveBeenCalledWith(['STOPPED']);

            done();
          }, 30);

          await new Promise(resolve => setTimeout(resolve, 10));
        }

        return true;
      }
    });

    simpleProcess.getInstance$().subscribe(instance => {
      instance.onActivityInterrupted('bpmn:Task', onTaskInterrupt);
      instance.onEnded(onEnded);
      instance.onAborted(onEnded);
      instance.onInstanceStateChange(onStateChange);
    });

    // start an instance
    simpleProcess.deployAndStart();
  });

  it('will stop the instance if no token is left after migration', async done => {
    const onTaskInterrupt = jest.fn();
    const onEnded = jest.fn();
    const onStateChange = jest.fn();

    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model does not contain the current task
      // this will lead to the task being interrupted and the token being removed
      // since no token is left the instance will be stopped
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const newModdle = await toModdle(BPMN_XML_MIGRATION_REPLACEMENT);

          await new Promise(resolve => setTimeout(resolve, 10));

          expect(() =>
            instance.migrate('otherProcess', {
              moddleDefinitions: newModdle,
              tokenMapping: {
                remove: [tId]
              }
            })
          ).not.toThrow();

          setTimeout(() => {
            expect(instance.getState().tokens).toEqual([]);
            expect(instance.getState().log).toContainEqual({
              flowElementId: 'Activity_16tbasu',
              tokenId: tId,
              executionState: ExecutionState.SKIPPED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            });

            expect(onTaskInterrupt).toHaveBeenCalledTimes(1);

            // When the only token in the instance is removed the instance should be stopped
            expect(instance.getState().instanceState).toEqual(['STOPPED']);

            done();
          }, 30);

          await new Promise(resolve => setTimeout(resolve, 10));
        }

        return true;
      }
    });

    simpleProcess.getInstance$().subscribe(instance => {
      instance.onActivityInterrupted('bpmn:Task', onTaskInterrupt);
      instance.onEnded(onEnded);
      instance.onAborted(onEnded);
      instance.onInstanceStateChange(onStateChange);
    });

    // start an instance
    simpleProcess.deployAndStart();
  });

  it('will inform the user about a task being interrupted when it was removed during migration (token move)', async done => {
    const onTaskInterrupt = jest.fn();
    const onEnded = jest.fn();
    const onStateChange = jest.fn();

    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model does not contain the current task
      // this will lead to the task being interrupted and the token being removed
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const newModdle = await toModdle(BPMN_XML_MIGRATION_REPLACEMENT);

          await new Promise(resolve => setTimeout(resolve, 10));

          expect(() =>
            instance.migrate('otherProcess', {
              moddleDefinitions: newModdle,
              tokenMapping: {
                move: [{ tokenId: tId, targetFlowElementId: 'Flow_1stp2tv' }]
              }
            })
          ).not.toThrow();

          setTimeout(() => {
            expect(instance.getState().tokens).toEqual([
              {
                tokenId: expect.any(String),
                state: TokenState.RUNNING,
                currentFlowElementId: 'Activity_0l50qj6',
                currentFlowNodeState: FlowNodeState.ACTIVE,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1stp2tv',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              }
            ]);

            expect(instance.getState().log).toContainEqual({
              flowElementId: 'Activity_16tbasu',
              tokenId: tId,
              executionState: ExecutionState.SKIPPED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            });

            expect(onTaskInterrupt).toHaveBeenCalledTimes(1);

            done();
          }, 30);

          await new Promise(resolve => setTimeout(resolve, 10));
        }

        return true;
      }
    });

    simpleProcess.getInstance$().subscribe(instance => {
      instance.onActivityInterrupted('bpmn:Task', onTaskInterrupt);
      instance.onEnded(onEnded);
      instance.onAborted(onEnded);
      instance.onInstanceStateChange(onStateChange);
    });

    // start an instance
    simpleProcess.deployAndStart();
  });
});

describe('BpmnProcess.migrate()', () => {
  it('allows the migration of an instance of one deployed process to another process', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model contains an additional task following the currently active one
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const otherProcess = await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_EXTENDED);

          expect(() => BpmnProcess.migrate('simpleProcess', 'otherProcess', [instanceId], {})).not.toThrow();

          setTimeout(() => {
            const state = instance.getState();

            expect(instance.isEnded()).toBeTruthy();

            expect(state.processId).toBe('otherProcess');

            expect(state.tokens.length).toBe(1);
            expect(state.tokens).toEqual([
              {
                tokenId: tId,
                state: TokenState.ENDED,
                currentFlowElementId: 'Event_1s7fpk8',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0gia3hu',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log.length).toBe(4);
            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_0kbrms3',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_16tbasu',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_0y4lned',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Event_1s7fpk8',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.adaptationLog).toEqual([
              {
                type: AdaptationType.MIGRATION,
                time: expect.any(Number),
                from: 'simpleProcess',
                to: 'otherProcess'
              }
            ]);

            // should have added the instance to the new process and removed it from the old one
            expect(simpleProcess.getInstanceById(instanceId)).toBeFalsy();
            expect(otherProcess.getInstanceById(instanceId)).toBe(instance);

            done();
          }, 30);

          await new Promise(resolve => setTimeout(resolve, 10));
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  it('allows the migration of multiple instances of the same deployed process to another process', async done => {
    const instances: BpmnProcessInstance[] = [];
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instances on activation of the task; the new model contains an additional task following the currently active one
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);
          instances.push(instance);

          if (instances.length === 2) {
            await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_EXTENDED);
            const instanceIds = instances.map(instance => instance.getState().processInstanceId);
            expect(() => BpmnProcess.migrate('simpleProcess', 'otherProcess', instanceIds, {})).not.toThrow();
          }
          await new Promise(resolve => setTimeout(resolve, 100));

          setTimeout(() => {
            const state = instance.getState();

            expect(instance.isEnded()).toBeTruthy();

            expect(state.processId).toBe('otherProcess');

            expect(state.tokens.length).toBe(1);
            expect(state.tokens).toEqual([
              {
                tokenId: tId,
                state: TokenState.ENDED,
                currentFlowElementId: 'Event_1s7fpk8',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0gia3hu',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log.length).toBe(4);
            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_0kbrms3',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_16tbasu',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_0y4lned',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Event_1s7fpk8',
                tokenId: tId,
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.adaptationLog).toEqual([
              {
                type: AdaptationType.MIGRATION,
                time: expect.any(Number),
                from: 'simpleProcess',
                to: 'otherProcess'
              }
            ]);

            done();
          }, 30);
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
    simpleProcess.start();
  });

  it('will automatically remove tokens when their current position is removed in the new model', async done => {
    // create the process to run the test on
    const extendedProcess = await BpmnProcess.fromXml('extendedProcess', BPMN_XML_MIGRATION_EXTENDED, {
      // migrate the instance on activation of the task; the new model does not contain the currently active task so the token is removed
      shouldActivateFlowNodeHook: async (_pId, instanceId, tokenId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_0y4lned') {
          const instance = extendedProcess.getInstanceById(instanceId);
          const migrationWatcher = jest.spyOn(instance, 'migrate');
          await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_INITIAL);

          expect(() => BpmnProcess.migrate('extendedProcess', 'otherProcess', [instanceId], {})).not.toThrow();

          expect(migrationWatcher).toHaveBeenCalledWith('otherProcess', {
            moddleDefinitions: expect.any(Object),
            tokenMapping: {
              remove: [tokenId]
            }
          });

          done();
        }

        return true;
      }
    });
    // start an instance
    extendedProcess.deployAndStart();
  });

  it('will not try to remove the token twice if the token is already marked for deletion', async done => {
    // create the process to run the test on
    const extendedProcess = await BpmnProcess.fromXml('extendedProcess', BPMN_XML_MIGRATION_EXTENDED, {
      // migrate the instance on activation of the task; the new model does not contain the currently active task so the token is removed
      shouldActivateFlowNodeHook: async (_pId, instanceId, tokenId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_0y4lned') {
          const instance = extendedProcess.getInstanceById(instanceId);
          const migrationWatcher = jest.spyOn(instance, 'migrate');
          await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_INITIAL);

          expect(() =>
            BpmnProcess.migrate('extendedProcess', 'otherProcess', [instanceId], {
              tokenMapping: { remove: [tokenId] }
            })
          ).not.toThrow();

          expect(migrationWatcher).toHaveBeenCalledWith('otherProcess', {
            moddleDefinitions: expect.any(Object),
            tokenMapping: {
              remove: [tokenId]
            }
          });

          done();
        }

        return true;
      }
    });
    // start an instance
    extendedProcess.deployAndStart();
  });

  it('should use the given flowElementMapping to create a token move mapping for the instance to migrate', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model contains an additional task which the token should be moved to
      shouldActivateFlowNodeHook: async (_pId, instanceId, tokenId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const migrationWatcher = jest.spyOn(instance, 'migrate').mockImplementation(() => {});
          await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_EXTENDED);
          expect(() =>
            BpmnProcess.migrate('simpleProcess', 'otherProcess', [instanceId], {
              flowElementMapping: {
                Activity_16tbasu: ['Activity_0y4lned']
              }
            })
          ).not.toThrow();

          expect(migrationWatcher).toHaveBeenCalledWith('otherProcess', {
            moddleDefinitions: expect.any(Object),
            tokenMapping: {
              move: [
                {
                  tokenId,
                  targetFlowElementId: 'Activity_0y4lned'
                }
              ]
            }
          });

          done();
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  it('should not remove a token that was mapped to a valid element from a removed one', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model contains a new task instead of the currently active one
      // the flowElementMapping should prevent the token from being removed
      shouldActivateFlowNodeHook: async (_pId, instanceId, tokenId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const migrationWatcher = jest.spyOn(instance, 'migrate').mockImplementation(() => {});
          await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_REPLACEMENT);
          expect(() =>
            BpmnProcess.migrate('simpleProcess', 'otherProcess', [instanceId], {
              flowElementMapping: {
                Activity_16tbasu: ['Activity_0l50qj6']
              }
            })
          ).not.toThrow();

          expect(migrationWatcher).toHaveBeenCalledWith('otherProcess', {
            moddleDefinitions: expect.any(Object),
            tokenMapping: {
              move: [
                {
                  tokenId,
                  targetFlowElementId: 'Activity_0l50qj6'
                }
              ]
            }
          });

          done();
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  it('should allow a one to many flowElementMapping to create additional tokens', async done => {
    // create the process to run the test on
    const extendedProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_EXTENDED, {
      // migrate the instance on activation of the task; the new model parallelized the previously sequential tasks
      // we need to add a token to prevent a deadlock
      shouldActivateFlowNodeHook: async (_pId, instanceId, tokenId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = extendedProcess.getInstanceById(instanceId);
          const migrationWatcher = jest.spyOn(instance, 'migrate').mockImplementation(() => {});
          await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_EXTENDED_PARALLELIZATION);
          expect(() =>
            BpmnProcess.migrate('simpleProcess', 'otherProcess', [instanceId], {
              flowElementMapping: {
                Activity_16tbasu: ['Activity_16tbasu', 'Activity_0y4lned']
              }
            })
          ).not.toThrow();

          expect(migrationWatcher).toHaveBeenCalledWith('otherProcess', {
            moddleDefinitions: expect.any(Object),
            tokenMapping: {
              add: [
                {
                  targetFlowElementId: 'Activity_0y4lned'
                }
              ]
            }
          });

          done();
        }

        return true;
      }
    });
    // start an instance
    extendedProcess.deployAndStart();
  });

  it('should automatically restart a task if its type changes with the migration (by token move)', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_INITIAL, {
      // migrate the instance on activation of the task; the new model contains the same task but with a different type
      // the task should be reinvoked by putting its token on the incoming sequence flow
      shouldActivateFlowNodeHook: async (_pId, instanceId, tokenId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_16tbasu') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const migrationWatcher = jest.spyOn(instance, 'migrate').mockImplementation(() => {});
          await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_TYPE_CHANGE);
          expect(() => BpmnProcess.migrate('simpleProcess', 'otherProcess', [instanceId], {})).not.toThrow();

          expect(migrationWatcher).toHaveBeenCalledWith('otherProcess', {
            moddleDefinitions: expect.any(Object),
            tokenMapping: {
              move: [
                {
                  tokenId,
                  targetFlowElementId: 'Activity_16tbasu'
                }
              ]
            }
          });

          done();
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  it('should allow a migration where a task is being added inside a subprocess', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_MIGRATION_SUBPROCESS_INITIAL, {
      // migrate the instance on activation of the task; the new model contains an additional task following the currently active one inside the subprocess
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'Activity_017604y') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const otherProcess = await BpmnProcess.fromXml('otherProcess', BPMN_XML_MIGRATION_SUBPROCESS_ADDED_TASK);

          expect(() => BpmnProcess.migrate('simpleProcess', 'otherProcess', [instanceId], {})).not.toThrow();

          setTimeout(() => {
            const state = instance.getState();

            expect(instance.isEnded()).toBeFalsy();

            expect(state.processId).toBe('otherProcess');

            expect(state.tokens.length).toBe(2);
            expect(state.tokens).toEqual([
              {
                tokenId: tId.split('#')[0],
                state: TokenState.RUNNING,
                currentFlowElementId: 'Activity_0cl2rre',
                currentFlowNodeState: FlowNodeState.ACTIVE,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1evmi3x',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              },
              {
                tokenId: tId,
                state: TokenState.RUNNING,
                currentFlowElementId: 'Activity_017604y',
                currentFlowNodeState: FlowNodeState.ACTIVE,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1r5xfcd',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              }
            ]);

            expect(state.adaptationLog).toEqual([
              {
                type: AdaptationType.MIGRATION,
                time: expect.any(Number),
                from: 'simpleProcess',
                to: 'otherProcess'
              }
            ]);

            done();
          }, 30);

          await new Promise(resolve => setTimeout(resolve, 10));
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });
});
