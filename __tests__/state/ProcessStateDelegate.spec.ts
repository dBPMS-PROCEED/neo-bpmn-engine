import { Definitions } from 'bpmn-moddle';
import { ProcessStateDelegate } from '../../src/state/ProcessStateDelegate';
import { DEFAULT_PROCESS_STATE } from '../../src/state/reducers';
import BpmnModdle from '../../src/util/BpmnModdle';
import { BPMN_XML_SINGLE_NONE_START_EVENT } from '../fixtures/bpmnXml';

let moddleDefinitions: Definitions | null = null;
beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_SINGLE_NONE_START_EVENT, (_, definitions) => {
    moddleDefinitions = definitions;
    done();
  });
});

describe('object construction', () => {
  it('should be able to construct with default state when not supplied', () => {
    const delegate = ProcessStateDelegate.fromDefaultState({ moddleDefinitions: moddleDefinitions! });
    expect(delegate.getState()).toEqual({ ...DEFAULT_PROCESS_STATE, moddleDefinitions });
  });

  it('should be able to construct from given state', () => {
    const delegate = ProcessStateDelegate.fromState({
      state: {
        ...DEFAULT_PROCESS_STATE,
        variables: { a: { value: 5, log: [{ changedTime: 1 }] } }
      },
      moddleDefinitions: moddleDefinitions!
    });
    expect(delegate.getState()).toEqual({
      ...DEFAULT_PROCESS_STATE,
      moddleDefinitions,
      variables: { a: { value: 5, log: [{ changedTime: 1 }] } }
    });
  });
});
