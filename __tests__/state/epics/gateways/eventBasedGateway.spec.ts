import {
  activateFlowNodeAction,
  receiveMessageAction,
  receiveSignalAction,
  endFlowNodeAction,
  updateTokenAction,
  interruptAction,
  endTokenAction,
  pauseTokenAction
} from '../../../../src/state/actions';
import { eventBasedGatewayActivateEpic } from '../../../../src/state/epics/gateway/eventBasedGatewayEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { ExecutionState } from '../../../../src/state/reducers/log';
import { TokenState } from '../../../../src/state/reducers/tokens';

const BPMN_XML_EVENT_BASED_GATEWAY = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0xgjfu8" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:message id="Message_0io414n" name="Message_24beroh" />
  <bpmn:signal id="Signal_0bz6prz" name="Signal_36r3o14" />
  <bpmn:process id="Process_0ozffmp" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1jsggjz</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1jsggjz" sourceRef="StartEvent_1" targetRef="EventBasedGateway_11p9hof" />
    <bpmn:eventBasedGateway id="EventBasedGateway_11p9hof">
      <bpmn:incoming>Flow_1jsggjz</bpmn:incoming>
      <bpmn:outgoing>Flow_1tsifsy</bpmn:outgoing>
      <bpmn:outgoing>Flow_0fw4edc</bpmn:outgoing>
      <bpmn:outgoing>Flow_0g868k9</bpmn:outgoing>
    </bpmn:eventBasedGateway>
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_1eig9pq">
      <bpmn:incoming>Flow_1tsifsy</bpmn:incoming>
      <bpmn:outgoing>Flow_0sb10hf</bpmn:outgoing>
      <bpmn:messageEventDefinition messageRef="Message_0io414n" />
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="Flow_1tsifsy" sourceRef="EventBasedGateway_11p9hof" targetRef="IntermediateCatchEvent_1eig9pq" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_17hey5p">
      <bpmn:incoming>Flow_0fw4edc</bpmn:incoming>
      <bpmn:outgoing>Flow_1mb1toa</bpmn:outgoing>
      <bpmn:signalEventDefinition signalRef="Signal_0bz6prz" />
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="Flow_0fw4edc" sourceRef="EventBasedGateway_11p9hof" targetRef="IntermediateCatchEvent_17hey5p" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_1mpqeif">
      <bpmn:incoming>Flow_0g868k9</bpmn:incoming>
      <bpmn:outgoing>Flow_1js19w8</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT2S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:intermediateCatchEvent>
    <bpmn:sequenceFlow id="Flow_0g868k9" sourceRef="EventBasedGateway_11p9hof" targetRef="IntermediateCatchEvent_1mpqeif" />
    <bpmn:endEvent id="EndEvent_0dd9ysh">
      <bpmn:incoming>Flow_0sb10hf</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0sb10hf" sourceRef="IntermediateCatchEvent_1eig9pq" targetRef="EndEvent_0dd9ysh" />
    <bpmn:endEvent id="EndEvent_1k8vrd7">
      <bpmn:incoming>Flow_1mb1toa</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1mb1toa" sourceRef="IntermediateCatchEvent_17hey5p" targetRef="EndEvent_1k8vrd7" />
    <bpmn:endEvent id="EndEvent_0digcar">
      <bpmn:incoming>Flow_1js19w8</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1js19w8" sourceRef="IntermediateCatchEvent_1mpqeif" targetRef="EndEvent_0digcar" />
  </bpmn:process>
  </bpmn:definitions>`;

let baseState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_EVENT_BASED_GATEWAY, (_, definitions) => {
    baseState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('EVENTBASED GATEWAY', () => {
  it('should not respond to other bpmn element types', async () => {
    const epic = eventBasedGatewayActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'EndEvent_1ssivqu',
        tokenId: 'xyz'
      })
    ]);

    expect(actions).toEqual([]);
  });

  it('should end execution and pass token from duration 1s to task if no other catch activates', async () => {
    const epic = eventBasedGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EventBasedGateway_11p9hof',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EventBasedGateway_11p9hof',
          tokenId: 'abc'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'abc',
        state: TokenState.READY
      }),
      endFlowNodeAction({
        flowNodeId: 'EventBasedGateway_11p9hof',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          passTokenTo: 'Flow_1js19w8'
        },
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should end execution and pass token from duration 1s to task if caught event has different refId', async () => {
    const epic = eventBasedGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EventBasedGateway_11p9hof',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EventBasedGateway_11p9hof',
          tokenId: 'abc'
        }),
        receiveMessageAction({ id: 'Message-different-refId' }), // won't be caught
        receiveSignalAction({ id: 'Signal-different-refId' }) // won't be caught
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'abc',
        state: TokenState.READY
      }),
      endFlowNodeAction({
        flowNodeId: 'EventBasedGateway_11p9hof',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          passTokenTo: 'Flow_1js19w8'
        },
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should end execution and pass token from message to task', async () => {
    const epic = eventBasedGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EventBasedGateway_11p9hof',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EventBasedGateway_11p9hof',
          tokenId: 'abc'
        }),
        receiveMessageAction({ id: 'Message_0io414n' }),
        receiveSignalAction({ id: 'Signal_124dvwx' }) // won't be caught
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'abc',
        state: TokenState.READY
      }),
      endFlowNodeAction({
        flowNodeId: 'EventBasedGateway_11p9hof',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          passTokenTo: 'Flow_0sb10hf'
        },
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should end execution and pass token from signal to task', async () => {
    const epic = eventBasedGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EventBasedGateway_11p9hof',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EventBasedGateway_11p9hof',
          tokenId: 'abc'
        }),
        receiveSignalAction({ id: 'Signal_0bz6prz' }),
        receiveMessageAction({ id: 'Message_0io414n' }) // wont be caught
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'abc',
        state: TokenState.READY
      }),
      endFlowNodeAction({
        flowNodeId: 'EventBasedGateway_11p9hof',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          passTokenTo: 'Flow_1mb1toa'
        },
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should not pass token if flowNode was paused', async () => {
    const epic = eventBasedGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EventBasedGateway_11p9hof',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EventBasedGateway_11p9hof',
          tokenId: 'abc'
        }),
        pauseTokenAction({
          tokenId: 'abc'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'abc',
        state: TokenState.READY
      })
    ]);
  });

  it('should not pass token if flowNode was interrupted', async () => {
    const epic = eventBasedGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EventBasedGateway_11p9hof',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EventBasedGateway_11p9hof',
          tokenId: 'abc'
        }),
        interruptAction({
          flowNodeId: 'EventBasedGateway_11p9hof',
          tokenId: 'abc'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'abc',
        state: TokenState.READY
      })
    ]);
  });

  it('should not pass token if token is ended', async () => {
    const epic = eventBasedGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EventBasedGateway_11p9hof',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EventBasedGateway_11p9hof',
          tokenId: 'abc'
        }),
        endTokenAction({
          tokenId: 'abc',
          state: 'ABORTED'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'abc',
        state: TokenState.READY
      })
    ]);
  });
});
