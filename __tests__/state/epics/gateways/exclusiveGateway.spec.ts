import {
  activateFlowNodeAction,
  willPassTokenAction,
  createTokenAction,
  endFlowNodeAction,
  interruptAction,
  endTokenAction
} from '../../../../src/state/actions';
import { exclusiveGatewayActivateEpic } from '../../../../src/state/epics/gateway/exclusiveGatewayEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { ExecutionState } from '../../../../src/state/reducers/log';
import { TokenState } from '../../../../src/state/reducers/tokens';
import { BPMN_XML_EXCLUSIVE_XOR_GATEWAY_SIMPLE_LOOP } from '../../../fixtures/bpmnXml';

const BPMN_XML_BINARY_EXCLUSIVE_GATEWAY = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0xgjfu8" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
<bpmn:process id="Process_0ozffmp" isExecutable="true">
  <bpmn:startEvent id="StartEvent_1">
    <bpmn:outgoing>Flow_07qc5ea</bpmn:outgoing>
  </bpmn:startEvent>
  <bpmn:exclusiveGateway id="ExclusiveGateway_11p9hof" default="Flow_08cu877">
    <bpmn:incoming>Flow_07qc5ea</bpmn:incoming>
    <bpmn:outgoing>Flow_0lx194b</bpmn:outgoing>
    <bpmn:outgoing>Flow_08cu877</bpmn:outgoing>
  </bpmn:exclusiveGateway>
  <bpmn:sequenceFlow id="Flow_07qc5ea" sourceRef="StartEvent_1" targetRef="ExclusiveGateway_11p9hof" />
  <bpmn:task id="Task_0zwm0uw">
    <bpmn:incoming>Flow_0lx194b</bpmn:incoming>
    <bpmn:outgoing>Flow_06euut9</bpmn:outgoing>
  </bpmn:task>
  <bpmn:sequenceFlow id="Flow_0lx194b" sourceRef="ExclusiveGateway_11p9hof" targetRef="Task_0zwm0uw">
    <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">hasGermanPassport</bpmn:conditionExpression>
  </bpmn:sequenceFlow>
  <bpmn:task id="Task_0b397h8">
    <bpmn:incoming>Flow_08cu877</bpmn:incoming>
    <bpmn:outgoing>Flow_0bgzo8z</bpmn:outgoing>
  </bpmn:task>
  <bpmn:sequenceFlow id="Flow_08cu877" sourceRef="ExclusiveGateway_11p9hof" targetRef="Task_0b397h8" />
  <bpmn:exclusiveGateway id="ExclusiveGateway_000jiv7">
    <bpmn:incoming>Flow_06euut9</bpmn:incoming>
    <bpmn:incoming>Flow_0bgzo8z</bpmn:incoming>
    <bpmn:outgoing>Flow_1vief06</bpmn:outgoing>
  </bpmn:exclusiveGateway>
  <bpmn:sequenceFlow id="Flow_06euut9" sourceRef="Task_0zwm0uw" targetRef="ExclusiveGateway_000jiv7" />
  <bpmn:sequenceFlow id="Flow_0bgzo8z" sourceRef="Task_0b397h8" targetRef="ExclusiveGateway_000jiv7" />
  <bpmn:endEvent id="EndEvent_02vlyn3">
    <bpmn:incoming>Flow_1vief06</bpmn:incoming>
  </bpmn:endEvent>
  <bpmn:sequenceFlow id="Flow_1vief06" sourceRef="ExclusiveGateway_000jiv7" targetRef="EndEvent_02vlyn3" />
</bpmn:process>
</bpmn:definitions>`;

let binaryState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_BINARY_EXCLUSIVE_GATEWAY, (_, definitions) => {
    binaryState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('EXCLUSIVE GATEWAY', () => {
  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = exclusiveGatewayActivateEpic();
    const actions = await testEpic(epic, binaryState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element is of incorrect type', async () => {
    const epic = exclusiveGatewayActivateEpic();
    const actions = await testEpic(epic, binaryState, [
      activateFlowNodeAction({
        flowNodeId: 'Task_0zwm0uw',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  describe('binary split', () => {
    it('should end execution and pass token to the first outgoing sequence evaluating to true - 1', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...binaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ExclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            hasGermanPassport: {
              value: true,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'ExclusiveGateway_11p9hof',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: expect.any(Number)
        }),
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'ExclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        willPassTokenAction({
          sequenceId: 'Flow_0lx194b',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        })
      ]);
    });

    it('should end execution and pass token to the first outgoing sequence evaluating to true - 2', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...binaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ExclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            hasGermanPassport: {
              value: false,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'ExclusiveGateway_11p9hof',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: expect.any(Number)
        }),
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'ExclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        willPassTokenAction({
          sequenceId: 'Flow_08cu877',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        })
      ]);
    });

    it('should not pass token if flowNode was interrupted', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...binaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ExclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            hasGermanPassport: {
              value: true,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          }),
          interruptAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([]);
    });
  });

  describe('ternary split', () => {
    const BPMN_XML_TERNARY_EXCLUSIVE_GATEWAY = `
    <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0xgjfu8" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0ozffmp" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1jsggjz</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:exclusiveGateway id="ExclusiveGateway_11p9hof" default="Flow_1qnmfrw">
      <bpmn:incoming>Flow_1jsggjz</bpmn:incoming>
      <bpmn:outgoing>Flow_0n4ewop</bpmn:outgoing>
      <bpmn:outgoing>Flow_04wz8jn</bpmn:outgoing>
      <bpmn:outgoing>Flow_1qnmfrw</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:exclusiveGateway id="ExclusiveGateway_000jiv7">
      <bpmn:incoming>Flow_0x4hio1</bpmn:incoming>
      <bpmn:incoming>Flow_1dopqkn</bpmn:incoming>
      <bpmn:incoming>Flow_0ul18jr</bpmn:incoming>
      <bpmn:outgoing>Flow_1vief06</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:endEvent id="EndEvent_02vlyn3">
      <bpmn:incoming>Flow_1vief06</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1vief06" sourceRef="ExclusiveGateway_000jiv7" targetRef="EndEvent_02vlyn3" />
    <bpmn:sequenceFlow id="Flow_1jsggjz" sourceRef="StartEvent_1" targetRef="ExclusiveGateway_11p9hof" />
    <bpmn:task id="Task_07p039k">
      <bpmn:incoming>Flow_0n4ewop</bpmn:incoming>
      <bpmn:outgoing>Flow_1dopqkn</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0n4ewop" sourceRef="ExclusiveGateway_11p9hof" targetRef="Task_07p039k">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">age &lt; 18</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0hpzucb">
      <bpmn:incoming>Flow_04wz8jn</bpmn:incoming>
      <bpmn:outgoing>Flow_0x4hio1</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_04wz8jn" sourceRef="ExclusiveGateway_11p9hof" targetRef="Task_0hpzucb">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">age &lt; 60</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_1px3xn7">
      <bpmn:incoming>Flow_1qnmfrw</bpmn:incoming>
      <bpmn:outgoing>Flow_0ul18jr</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1qnmfrw" sourceRef="ExclusiveGateway_11p9hof" targetRef="Task_1px3xn7" />
    <bpmn:sequenceFlow id="Flow_0x4hio1" sourceRef="Task_0hpzucb" targetRef="ExclusiveGateway_000jiv7" />
    <bpmn:sequenceFlow id="Flow_1dopqkn" sourceRef="Task_07p039k" targetRef="ExclusiveGateway_000jiv7" />
    <bpmn:sequenceFlow id="Flow_0ul18jr" sourceRef="Task_1px3xn7" targetRef="ExclusiveGateway_000jiv7" />
  </bpmn:process>
  </bpmn:definitions>`;

    let ternaryState: ProcessState;

    beforeAll(done => {
      new BpmnModdle().fromXML(BPMN_XML_TERNARY_EXCLUSIVE_GATEWAY, (_, definitions) => {
        ternaryState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end execution and pass token to the first outgoing sequence evaluating to true - 1', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...ternaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ExclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            age: {
              value: 17,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'ExclusiveGateway_11p9hof',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: expect.any(Number)
        }),
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'ExclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        willPassTokenAction({
          sequenceId: 'Flow_0n4ewop',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        })
      ]);
    });

    it('should end execution and pass token to the first outgoing sequence evaluating to true - 2', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...ternaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ExclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            age: {
              value: 23,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'ExclusiveGateway_11p9hof',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: expect.any(Number)
        }),
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'ExclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        willPassTokenAction({
          sequenceId: 'Flow_04wz8jn',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        })
      ]);
    });

    it('should end execution and pass token to the first outgoing sequence evaluating to true - 3', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...ternaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ExclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            age: {
              value: 70,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'ExclusiveGateway_11p9hof',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: expect.any(Number)
        }),
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'ExclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        willPassTokenAction({
          sequenceId: 'Flow_1qnmfrw',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        })
      ]);
    });

    it('should not pass token if flowNode was interrupted', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...ternaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ExclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            age: {
              value: 70,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          }),
          interruptAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([]);
    });

    it('should not pass token if token was ended', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...ternaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ExclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            age: {
              value: 70,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'ExclusiveGateway_11p9hof',
            tokenId: 'xyz'
          }),
          endTokenAction({
            tokenId: 'xyz',
            state: 'ABORTED'
          })
        ]
      );

      expect(actions).toEqual([]);
    });
  });

  describe('loop', () => {
    let loopState: ProcessState;

    beforeAll(done => {
      new BpmnModdle().fromXML(BPMN_XML_EXCLUSIVE_XOR_GATEWAY_SIMPLE_LOOP, (_, definitions) => {
        loopState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should pass the incoming token without removing or adding tokens if it is a root token', async () => {
      const epic = exclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...loopState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'Gateway_1ydo9ch',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ]
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'Gateway_1ydo9ch',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'Gateway_1ydo9ch',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          endTime: expect.any(Number)
        }),
        willPassTokenAction({
          sequenceId: 'Flow_0yvqccu',
          tokenId: 'xyz'
        })
      ]);
    });
  });
});
