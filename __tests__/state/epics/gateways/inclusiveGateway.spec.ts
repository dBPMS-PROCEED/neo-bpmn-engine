import {
  activateFlowNodeAction,
  willPassTokenAction,
  updateTokenAction,
  createTokenAction,
  endFlowNodeAction,
  interruptAction,
  endTokenAction
} from '../../../../src/state/actions';
import { inclusiveGatewayActivateEpic } from '../../../../src/state/epics/gateway/inclusiveGatewayEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { ExecutionState } from '../../../../src/state/reducers/log';
import { TokenState } from '../../../../src/state/reducers/tokens';

const BPMN_XML_INCLUSIVE_GATEWAY = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0xgjfu8" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0ozffmp" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1jsggjz</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:endEvent id="EndEvent_02vlyn3">
      <bpmn:incoming>Flow_1vief06</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1vief06" sourceRef="InclusiveGateway_000jiv7" targetRef="EndEvent_02vlyn3" />
    <bpmn:sequenceFlow id="Flow_1jsggjz" sourceRef="StartEvent_1" targetRef="InclusiveGateway_11p9hof" />
    <bpmn:task id="Task_07p039k">
      <bpmn:incoming>Flow_0n4ewop</bpmn:incoming>
      <bpmn:outgoing>Flow_1dopqkn</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0n4ewop" sourceRef="InclusiveGateway_11p9hof" targetRef="Task_07p039k">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">var1</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0hpzucb">
      <bpmn:incoming>Flow_04wz8jn</bpmn:incoming>
      <bpmn:outgoing>Flow_0x4hio1</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_04wz8jn" sourceRef="InclusiveGateway_11p9hof" targetRef="Task_0hpzucb">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">var2</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_1px3xn7">
      <bpmn:incoming>Flow_1qnmfrw</bpmn:incoming>
      <bpmn:outgoing>Flow_0ul18jr</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1qnmfrw" sourceRef="InclusiveGateway_11p9hof" targetRef="Task_1px3xn7" />
    <bpmn:sequenceFlow id="Flow_0x4hio1" sourceRef="Task_0hpzucb" targetRef="InclusiveGateway_000jiv7" />
    <bpmn:sequenceFlow id="Flow_1dopqkn" sourceRef="Task_07p039k" targetRef="InclusiveGateway_000jiv7" />
    <bpmn:sequenceFlow id="Flow_0ul18jr" sourceRef="Task_1px3xn7" targetRef="InclusiveGateway_000jiv7" />
    <bpmn:inclusiveGateway id="InclusiveGateway_11p9hof" default="Flow_1qnmfrw">
      <bpmn:incoming>Flow_1jsggjz</bpmn:incoming>
      <bpmn:outgoing>Flow_0n4ewop</bpmn:outgoing>
      <bpmn:outgoing>Flow_04wz8jn</bpmn:outgoing>
      <bpmn:outgoing>Flow_1qnmfrw</bpmn:outgoing>
    </bpmn:inclusiveGateway>
    <bpmn:inclusiveGateway id="InclusiveGateway_000jiv7">
      <bpmn:incoming>Flow_0x4hio1</bpmn:incoming>
      <bpmn:incoming>Flow_1dopqkn</bpmn:incoming>
      <bpmn:incoming>Flow_0ul18jr</bpmn:incoming>
      <bpmn:outgoing>Flow_1vief06</bpmn:outgoing>
    </bpmn:inclusiveGateway>
  </bpmn:process>
  </bpmn:definitions>`;

let baseState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_INCLUSIVE_GATEWAY, (_, definitions) => {
    baseState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('INCLUSIVE GATEWAY', () => {
  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = inclusiveGatewayActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element is of incorrect type', async () => {
    const epic = inclusiveGatewayActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'Task_0hpzucb',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should set state of token to READY when other tokens exist in incoming paths', async () => {
    const epic = inclusiveGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'InclusiveGateway_000jiv7',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Task_0hpzucb',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|789',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'InclusiveGateway_000jiv7',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'InclusiveGateway_000jiv7',
          tokenId: 'xyz|789'
        })
      ]
    );
    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz|789',
        state: TokenState.READY
      })
    ]);
  });

  it('should not pass token if flowNode was interrupted', async () => {
    const epic = inclusiveGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'InclusiveGateway_11p9hof',
              startTime: 100
            },
            localStartTime: 100,
            localExecutionTime: 100
          }
        ],
        variables: {
          var1: {
            value: true,
            log: [
              {
                changedTime: 100
              }
            ]
          },
          var2: {
            value: false,
            log: [
              {
                changedTime: 100
              }
            ]
          }
        }
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'InclusiveGateway_11p9hof',
          tokenId: 'xyz'
        }),
        interruptAction({
          flowNodeId: 'InclusiveGateway_11p9hof',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not pass token if token is ended', async () => {
    const epic = inclusiveGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'InclusiveGateway_11p9hof',
              startTime: 100
            },
            localStartTime: 100,
            localExecutionTime: 100
          }
        ],
        variables: {
          var1: {
            value: true,
            log: [
              {
                changedTime: 100
              }
            ]
          },
          var2: {
            value: false,
            log: [
              {
                changedTime: 100
              }
            ]
          }
        }
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'InclusiveGateway_11p9hof',
          tokenId: 'xyz'
        }),
        endTokenAction({
          tokenId: 'xyz',
          state: 'ABORTED'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  describe('one true path', () => {
    it('should end execution and emit pass token to the sequences evaluating to true', async () => {
      const epic = inclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...baseState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'InclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            var1: {
              value: true,
              log: [
                {
                  changedTime: 100
                }
              ]
            },
            var2: {
              value: false,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'InclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'InclusiveGateway_11p9hof',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: expect.any(Number)
        }),
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'InclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        willPassTokenAction({
          sequenceId: 'Flow_0n4ewop',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        })
      ]);
    });
  });

  describe('two true paths', () => {
    it('should end execution and emit pass token to the sequences evaluating to true', async () => {
      const epic = inclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...baseState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'InclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            var1: {
              value: true,
              log: [
                {
                  changedTime: 100
                }
              ]
            },
            var2: {
              value: true,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'InclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'InclusiveGateway_11p9hof',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: expect.any(Number)
        }),

        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'InclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'InclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        willPassTokenAction({
          sequenceId: 'Flow_0n4ewop',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        }),
        willPassTokenAction({
          sequenceId: 'Flow_04wz8jn',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        })
      ]);
    });
  });

  describe('default true path', () => {
    it('should end execution and emit pass token to the default sequence flow', async () => {
      const epic = inclusiveGatewayActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...baseState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'InclusiveGateway_11p9hof',
                startTime: 100
              },
              localStartTime: 100,
              localExecutionTime: 100
            }
          ],
          variables: {
            var1: {
              value: false,
              log: [
                {
                  changedTime: 100
                }
              ]
            },
            var2: {
              value: false,
              log: [
                {
                  changedTime: 100
                }
              ]
            }
          }
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'InclusiveGateway_11p9hof',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'InclusiveGateway_11p9hof',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: expect.any(Number)
        }),
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+/),
          state: TokenState.READY,
          currentFlowElement: { id: 'InclusiveGateway_11p9hof' },
          localStartTime: 100,
          localExecutionTime: 100
        }),
        willPassTokenAction({
          sequenceId: 'Flow_1qnmfrw',
          tokenId: expect.stringMatching(/^xyz\|.+/)
        })
      ]);
    });
  });
});
