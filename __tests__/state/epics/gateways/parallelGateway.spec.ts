import {
  activateFlowNodeAction,
  willPassTokenAction,
  removeTokenAction,
  updateTokenAction,
  createTokenAction,
  endFlowNodeAction
} from '../../../../src/state/actions';
import { parallelGatewayActivateEpic } from '../../../../src/state/epics/gateway/parallelGatewayEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { TokenState } from '../../../../src/state/reducers/tokens';
import { ExecutionState } from '../../../../src/state/reducers/log';

const BPMN_XML_PARALLEL_GATEWAY = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1b6y0fm" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="2.2.4">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>SequenceFlow_01das5j</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:endEvent id="EndEvent_02qfml5">
      <bpmn:incoming>SequenceFlow_0ssjqj4</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:parallelGateway id="Gateway_0mbjaes" />
    <bpmn:sequenceFlow id="SequenceFlow_01das5j" sourceRef="StartEvent_1" targetRef="ParallelGateway_1gb46mc" />
    <bpmn:parallelGateway id="ParallelGateway_1gb46mc">
      <bpmn:incoming>SequenceFlow_01das5j</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0zcellh</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0glegwn</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_07jqmiu</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:task id="Task_1p4tmqj" name="Activity 1">
      <bpmn:incoming>SequenceFlow_0zcellh</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0fsi00m</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0zcellh" sourceRef="ParallelGateway_1gb46mc" targetRef="Task_1p4tmqj" />
    <bpmn:sequenceFlow id="SequenceFlow_0fsi00m" sourceRef="Task_1p4tmqj" targetRef="ParallelGateway_1u9g4km" />
    <bpmn:parallelGateway id="ParallelGateway_1u9g4km">
      <bpmn:incoming>SequenceFlow_0fsi00m</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0u19mqy</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_10n4zv7</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0ssjqj4</bpmn:outgoing>
    </bpmn:parallelGateway>
    <bpmn:task id="Task_0j6jido" name="Activity 2">
      <bpmn:incoming>SequenceFlow_0glegwn</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0u19mqy</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0glegwn" sourceRef="ParallelGateway_1gb46mc" targetRef="Task_0j6jido" />
    <bpmn:sequenceFlow id="SequenceFlow_0u19mqy" sourceRef="Task_0j6jido" targetRef="ParallelGateway_1u9g4km" />
    <bpmn:task id="Task_0l2ayv1" name="Activity 3">
      <bpmn:incoming>SequenceFlow_07jqmiu</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_10n4zv7</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_07jqmiu" sourceRef="ParallelGateway_1gb46mc" targetRef="Task_0l2ayv1" />
    <bpmn:sequenceFlow id="SequenceFlow_10n4zv7" sourceRef="Task_0l2ayv1" targetRef="ParallelGateway_1u9g4km" />
    <bpmn:sequenceFlow id="SequenceFlow_0ssjqj4" sourceRef="ParallelGateway_1u9g4km" targetRef="EndEvent_02qfml5" />
  </bpmn:process>
</bpmn:definitions>`;

let baseState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_PARALLEL_GATEWAY, (_, definitions) => {
    baseState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('PARALLEL GATEWAY', () => {
  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = parallelGatewayActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element is of incorrect type', async () => {
    const epic = parallelGatewayActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'Task_1p4tmqj',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should set token state to READY when an insufficient number of tokens are at the gateway', async () => {
    const epic = parallelGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0zcellh'
          },
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0u19mqy'
          },
          {
            tokenId: 'xyz|789',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_10n4zv7'
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ParallelGateway_1u9g4km',
          tokenId: 'xyz|789'
        })
      ]
    );
    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz|789',
        state: TokenState.READY
      })
    ]);
  });

  it('should set token state to READY when one incoming sequence flow does not have a token', async () => {
    const epic = parallelGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0u19mqy'
          },
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0u19mqy'
          },
          {
            tokenId: 'xyz|789',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_10n4zv7'
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ParallelGateway_1u9g4km',
          tokenId: 'xyz|789'
        })
      ]
    );
    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz|789',
        state: TokenState.READY
      })
    ]);
  });

  it('should end execution and merge parent tokens when outgoing is a single sequence', async () => {
    const epic = parallelGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0fsi00m',
            localExecutionTime: 100
          },
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0u19mqy',
            localExecutionTime: 100
          },
          {
            tokenId: 'xyz|789',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_10n4zv7',
            localExecutionTime: 100
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ParallelGateway_1u9g4km',
          tokenId: 'xyz|789'
        })
      ]
    );

    expect(actions).toEqual([
      endFlowNodeAction({
        flowNodeId: 'ParallelGateway_1u9g4km',
        tokenId: 'xyz|789',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          removeToken: true
        },
        endTime: expect.any(Number)
      }),
      createTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY,
        currentFlowElement: { id: 'ParallelGateway_1u9g4km' },
        previousFlowElementId: undefined,
        localStartTime: 100,
        localExecutionTime: 100
      }),
      removeTokenAction({ tokenId: 'xyz|123' }),
      removeTokenAction({ tokenId: 'xyz|456' }),
      willPassTokenAction({
        sequenceId: 'SequenceFlow_0ssjqj4',
        tokenId: 'xyz'
      })
    ]);
  });

  it('should only take one token from each flow', async () => {
    const epic = parallelGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0fsi00m',
            localExecutionTime: 100
          },
          {
            tokenId: 'abc|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0fsi00m',
            localExecutionTime: 100
          },
          {
            tokenId: 'abc|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0u19mqy',
            localExecutionTime: 100
          },
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0u19mqy',
            localExecutionTime: 100
          },
          {
            tokenId: 'xyz|789',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_10n4zv7',
            localExecutionTime: 100
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ParallelGateway_1u9g4km',
          tokenId: 'abc|123'
        })
      ]
    );

    expect(actions).toEqual([
      endFlowNodeAction({
        flowNodeId: 'ParallelGateway_1u9g4km',
        tokenId: 'abc|123',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          removeToken: true
        },
        endTime: expect.any(Number)
      }),
      createTokenAction({
        tokenId: 'abc|123_abc|456_xyz|789',
        state: TokenState.READY,
        currentFlowElement: { id: 'ParallelGateway_1u9g4km' },
        previousFlowElementId: undefined,
        localStartTime: 100,
        localExecutionTime: 100
      }),
      removeTokenAction({ tokenId: 'abc|456' }),
      removeTokenAction({ tokenId: 'xyz|789' }),
      willPassTokenAction({
        sequenceId: 'SequenceFlow_0ssjqj4',
        tokenId: 'abc|123_abc|456_xyz|789'
      })
    ]);
  });

  it('should prefer merging tokens that came from the same parent token', async () => {
    const epic = parallelGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0fsi00m',
            localExecutionTime: 100
          },
          {
            tokenId: 'abc|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0fsi00m',
            localExecutionTime: 100
          },
          {
            tokenId: 'abc|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0u19mqy',
            localExecutionTime: 100
          },
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_0u19mqy',
            localExecutionTime: 100
          },
          {
            tokenId: 'xyz|789',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1u9g4km',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_10n4zv7',
            localExecutionTime: 100
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ParallelGateway_1u9g4km',
          tokenId: 'xyz|789'
        })
      ]
    );

    expect(actions).toEqual([
      endFlowNodeAction({
        flowNodeId: 'ParallelGateway_1u9g4km',
        tokenId: 'xyz|789',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          removeToken: true
        },
        endTime: expect.any(Number)
      }),
      createTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY,
        currentFlowElement: { id: 'ParallelGateway_1u9g4km' },
        previousFlowElementId: undefined,
        localStartTime: 100,
        localExecutionTime: 100
      }),
      removeTokenAction({ tokenId: 'xyz|123' }),
      removeTokenAction({ tokenId: 'xyz|456' }),
      willPassTokenAction({
        sequenceId: 'SequenceFlow_0ssjqj4',
        tokenId: 'xyz'
      })
    ]);
  });

  it('should end execution and pass token to each outgoing sequence', async () => {
    const epic = parallelGatewayActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'ParallelGateway_1gb46mc',
              startTime: 100
            },
            previousFlowElementId: 'SequenceFlow_01das5j',
            localExecutionTime: 100
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ParallelGateway_1gb46mc',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([
      endFlowNodeAction({
        flowNodeId: 'ParallelGateway_1gb46mc',
        tokenId: 'xyz',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          removeToken: true
        },
        endTime: expect.any(Number)
      }),
      createTokenAction({
        tokenId: expect.stringMatching(/^xyz\|.+/),
        state: TokenState.READY,
        currentFlowElement: { id: 'ParallelGateway_1gb46mc' },
        previousFlowElementId: undefined,
        localStartTime: 100,
        localExecutionTime: 100
      }),
      createTokenAction({
        tokenId: expect.stringMatching(/^xyz\|.+/),
        state: TokenState.READY,
        currentFlowElement: { id: 'ParallelGateway_1gb46mc' },
        previousFlowElementId: undefined,
        localStartTime: 100,
        localExecutionTime: 100
      }),
      createTokenAction({
        tokenId: expect.stringMatching(/^xyz\|.+/),
        state: TokenState.READY,
        currentFlowElement: { id: 'ParallelGateway_1gb46mc' },
        previousFlowElementId: undefined,
        localStartTime: 100,
        localExecutionTime: 100
      }),
      willPassTokenAction({
        sequenceId: 'SequenceFlow_0zcellh',
        tokenId: expect.stringMatching(/^xyz\|.+/)
      }),
      willPassTokenAction({
        sequenceId: 'SequenceFlow_0glegwn',
        tokenId: expect.stringMatching(/^xyz\|.+/)
      }),
      willPassTokenAction({
        sequenceId: 'SequenceFlow_07jqmiu',
        tokenId: expect.stringMatching(/^xyz\|.+/)
      })
    ]);
    // outgoing tokens should be different
    const outgoingTokens = actions
      .filter(a => a.payload.to && a.payload.to.startsWith('Task_'))
      .map(a => a.payload.tokenId);
    expect(outgoingTokens.length).toEqual(new Set(outgoingTokens).size);
  });
});
