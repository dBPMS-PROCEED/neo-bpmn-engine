import {
  initFlowNodeAction,
  listenBoundaryEventAction,
  createTokenAction,
  initBoundaryEventAction,
  respondThrowingEventAction,
  endFlowNodeAction,
  interruptAction,
  pauseTokenAction,
  endTokenAction
} from '../../../../src/state/actions';
import { boundaryEventInitEpic, boundaryEventListenEpic } from '../../../../src/state/epics/event/boundaryEventEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { createMockLogger } from '../../../helpers/mockLogger';
import { ExecutionState } from '../../../../src/state/reducers/log';
import { TokenState } from '../../../../src/state/reducers/tokens';

const mockLogger = createMockLogger();

beforeEach(() => {
  mockLogger.reset();
});

const BPMN_XML_INTERRUPTING_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0ajmgwk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:escalation id="escalation" escalationCode="EXAMPLE-CODE" />
  <bpmn:process id="Process_063mnrk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1y2mo4x</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1t6iujt">
      <bpmn:incoming>Flow_1y2mo4x</bpmn:incoming>
      <bpmn:outgoing>Flow_12mymq5</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1y2mo4x" sourceRef="StartEvent_1" targetRef="Task_1t6iujt" />
    <bpmn:endEvent id="EndEvent_0m6s6vb">
      <bpmn:incoming>Flow_12mymq5</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_12mymq5" sourceRef="Task_1t6iujt" targetRef="EndEvent_0m6s6vb" />
    <bpmn:boundaryEvent id="BoundaryEvent_1wr293r" attachedToRef="Task_1t6iujt">
      <bpmn:outgoing>Flow_00p4puy</bpmn:outgoing>
      <bpmn:escalationEventDefinition escalationRef="escalation" id="EscalationEventDefinition_0v9v0tx" />
    </bpmn:boundaryEvent>
    <bpmn:endEvent id="EndEvent_1r91pqr">
      <bpmn:incoming>Flow_00p4puy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_00p4puy" sourceRef="BoundaryEvent_1wr293r" targetRef="EndEvent_1r91pqr" />
  </bpmn:process>
  </bpmn:definitions>`;

const BPMN_XML_NON_INTERRUPTING_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0ajmgwk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:escalation id="escalation" escalationCode="EXAMPLE-CODE" />
  <bpmn:process id="Process_063mnrk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1y2mo4x</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1t6iujt">
      <bpmn:incoming>Flow_1y2mo4x</bpmn:incoming>
      <bpmn:outgoing>Flow_12mymq5</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1y2mo4x" sourceRef="StartEvent_1" targetRef="Task_1t6iujt" />
    <bpmn:endEvent id="EndEvent_0m6s6vb">
      <bpmn:incoming>Flow_12mymq5</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_12mymq5" sourceRef="Task_1t6iujt" targetRef="EndEvent_0m6s6vb" />
    <bpmn:endEvent id="EndEvent_1r91pqr">
      <bpmn:incoming>Flow_00p4puy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_00p4puy" sourceRef="BoundaryEvent_1wr293r" targetRef="EndEvent_1r91pqr" />
    <bpmn:boundaryEvent id="BoundaryEvent_1wr293r" cancelActivity="false" attachedToRef="Task_1t6iujt">
      <bpmn:outgoing>Flow_00p4puy</bpmn:outgoing>
      <bpmn:escalationEventDefinition escalationRef="escalation" id="EscalationEventDefinition_0v9v0tx" />
    </bpmn:boundaryEvent>
  </bpmn:process>
</bpmn:definitions>`;

const BPMN_XML_TIMER_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0ajmgwk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_063mnrk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1y2mo4x</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1t6iujt">
      <bpmn:incoming>Flow_1y2mo4x</bpmn:incoming>
      <bpmn:outgoing>Flow_12mymq5</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1y2mo4x" sourceRef="StartEvent_1" targetRef="Task_1t6iujt" />
    <bpmn:endEvent id="EndEvent_0m6s6vb">
      <bpmn:incoming>Flow_12mymq5</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_12mymq5" sourceRef="Task_1t6iujt" targetRef="EndEvent_0m6s6vb" />
    <bpmn:endEvent id="EndEvent_1r91pqr">
      <bpmn:incoming>Flow_00p4puy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_00p4puy" sourceRef="BoundaryEvent_1wr293r" targetRef="EndEvent_1r91pqr" />
    <bpmn:boundaryEvent id="BoundaryEvent_1wr293r" attachedToRef="Task_1t6iujt">
      <bpmn:outgoing>Flow_00p4puy</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDuration xsi:type="bpmn:tFormalExpression">PT1S</bpmn:timeDuration>
      </bpmn:timerEventDefinition>
    </bpmn:boundaryEvent>
  </bpmn:process>
</bpmn:definitions>`;

const BPMN_XML_ERROR_BOUNDARY_EVENT = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0ajmgwk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:error id="error" escalationCode="EXAMPLE-CODE" /> 
  <bpmn:process id="Process_063mnrk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1y2mo4x</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1t6iujt">
      <bpmn:incoming>Flow_1y2mo4x</bpmn:incoming>
      <bpmn:outgoing>Flow_12mymq5</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1y2mo4x" sourceRef="StartEvent_1" targetRef="Task_1t6iujt" />
    <bpmn:endEvent id="EndEvent_0m6s6vb">
      <bpmn:incoming>Flow_12mymq5</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_12mymq5" sourceRef="Task_1t6iujt" targetRef="EndEvent_0m6s6vb" />
    <bpmn:endEvent id="EndEvent_1r91pqr">
      <bpmn:incoming>Flow_00p4puy</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_00p4puy" sourceRef="BoundaryEvent_1wr293r" targetRef="EndEvent_1r91pqr" />
    <bpmn:boundaryEvent id="BoundaryEvent_1wr293r" attachedToRef="Task_1t6iujt">
      <bpmn:outgoing>Flow_00p4puy</bpmn:outgoing>
      <bpmn:errorEventDefinition errorRef="error" id="ErrorEventDefinition_1204wys" />
    </bpmn:boundaryEvent>
  </bpmn:process>
</bpmn:definitions>`;

let errorBoundaryState: ProcessState;
let interruptingBoundaryState: ProcessState;
let nonInterruptingBoundaryState: ProcessState;
let timerBoundaryState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_ERROR_BOUNDARY_EVENT, (_, definitions) => {
    errorBoundaryState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_INTERRUPTING_BOUNDARY_EVENT, (_, definitions) => {
    interruptingBoundaryState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_NON_INTERRUPTING_BOUNDARY_EVENT, (_, definitions) => {
    nonInterruptingBoundaryState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_TIMER_BOUNDARY_EVENT, (_, definitions) => {
    timerBoundaryState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('LISTEN', () => {
  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = boundaryEventListenEpic();
    const actions = await testEpic(epic, timerBoundaryState, [
      initFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  describe('TIMER EVENT', () => {
    it('should INIT if timer duration is passed', async () => {
      const epic = boundaryEventListenEpic();
      const actions = await testEpic(
        epic,
        {
          ...timerBoundaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              localStartTime: 100,
              localExecutionTime: 100,
              currentFlowElement: {
                id: 'Task_1t6iujt',
                startTime: Date.now()
              }
            }
          ]
        },
        [
          listenBoundaryEventAction({
            flowNodeId: 'BoundaryEvent_1wr293r',
            tokenId: 'xyz'
          })
        ],
        2000
      );
      expect(actions).toEqual([
        createTokenAction({
          tokenId: expect.stringMatching(/^xyz\|.+-x-.+/),
          state: TokenState.READY,
          localStartTime: 100,
          localExecutionTime: 100,
          currentFlowElement: {
            id: 'BoundaryEvent_1wr293r'
          }
        }),
        initBoundaryEventAction({
          flowNodeId: 'BoundaryEvent_1wr293r',
          tokenId: expect.stringMatching(/^xyz\|.+-x-.+/)
        })
      ]);
    });

    it('should not INIT if attached activity already ended before timer passed', async () => {
      const epic = boundaryEventListenEpic();
      const actions = await testEpic(
        epic,
        {
          ...timerBoundaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              localStartTime: 100,
              localExecutionTime: 100,
              currentFlowElement: {
                id: 'Task_1t6iujt',
                startTime: Date.now()
              }
            }
          ]
        },
        [
          listenBoundaryEventAction({
            flowNodeId: 'BoundaryEvent_1wr293r',
            tokenId: 'xyz'
          }),
          endFlowNodeAction({
            flowNodeId: 'Task_1t6iujt',
            tokenId: 'xyz',
            state: ExecutionState.COMPLETED,
            endTime: 200
          })
        ],
        2000
      );
      expect(actions).toEqual([]);
    });

    it('should not INIT if attached activity is interrupted before timer passed', async () => {
      const epic = boundaryEventListenEpic();
      const actions = await testEpic(
        epic,
        {
          ...timerBoundaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              localStartTime: 100,
              localExecutionTime: 100,
              currentFlowElement: {
                id: 'Task_1t6iujt',
                startTime: Date.now()
              }
            }
          ]
        },
        [
          listenBoundaryEventAction({
            flowNodeId: 'BoundaryEvent_1wr293r',
            tokenId: 'xyz'
          }),
          interruptAction({
            flowNodeId: 'Task_1t6iujt',
            tokenId: 'xyz'
          })
        ],
        2000
      );
      expect(actions).toEqual([]);
    });

    it('should not INIT if attached activity is paused before timer passed', async () => {
      const epic = boundaryEventListenEpic();
      const actions = await testEpic(
        epic,
        {
          ...timerBoundaryState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              localStartTime: 100,
              localExecutionTime: 100,
              currentFlowElement: {
                id: 'Task_1t6iujt',
                startTime: Date.now()
              }
            }
          ]
        },
        [
          listenBoundaryEventAction({
            flowNodeId: 'BoundaryEvent_1wr293r',
            tokenId: 'xyz'
          }),
          pauseTokenAction({
            tokenId: 'xyz'
          })
        ],
        2000
      );
      expect(actions).toEqual([]);
    });
  });
});

describe('INIT', () => {
  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = boundaryEventInitEpic(mockLogger);
    const actions = await testEpic(epic, timerBoundaryState, [
      initFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = boundaryEventInitEpic(mockLogger);
    const actions = await testEpic(epic, timerBoundaryState, [
      initFlowNodeAction({
        flowNodeId: 'Task_1t6iujt',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  describe('NON INTERRUPTING', () => {
    it('should end execution, pass token and emit respondThrowingEventAction', async () => {
      const epic = boundaryEventInitEpic(mockLogger);
      const actions = await testEpic(
        epic,
        {
          ...nonInterruptingBoundaryState,
          tokens: [
            {
              tokenId: 'xyz|abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'BoundaryEvent_1wr293r',
                startTime: 100
              }
            }
          ]
        },
        [
          initBoundaryEventAction({
            flowNodeId: 'BoundaryEvent_1wr293r',
            tokenId: 'xyz|abc',
            eventOrigin: {
              flowNodeId: 'Task_1t6iujt',
              tokenId: 'xyz'
            }
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'BoundaryEvent_1wr293r',
          tokenId: 'xyz|abc',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'Flow_00p4puy'
          },
          endTime: expect.any(Number)
        }),
        respondThrowingEventAction({
          flowNodeId: 'Task_1t6iujt',
          tokenId: 'xyz',
          caught: true,
          continueExecution: true
        })
      ]);
    });
  });

  describe('INTERRUPTING', () => {
    it('should end execution, pass token, emit respondThrowingEventAction and terminate attached Activity', async () => {
      const epic = boundaryEventInitEpic(mockLogger);
      const actions = await testEpic(
        epic,
        {
          ...interruptingBoundaryState,
          tokens: [
            {
              tokenId: 'xyz|abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'BoundaryEvent_1wr293r',
                startTime: 100
              }
            },
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'Task_1t6iujt',
                startTime: 100
              }
            }
          ]
        },
        [
          initBoundaryEventAction({
            flowNodeId: 'BoundaryEvent_1wr293r',
            tokenId: 'xyz|abc',
            eventOrigin: {
              flowNodeId: 'Task_1t6iujt',
              tokenId: 'xyz'
            }
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'BoundaryEvent_1wr293r',
          tokenId: 'xyz|abc',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'Flow_00p4puy'
          },
          endTime: expect.any(Number)
        }),
        endTokenAction({
          tokenId: 'xyz',
          state: ExecutionState.TERMINATED,
          endTime: expect.any(Number),
          removeAfterEnding: true
        }),
        respondThrowingEventAction({
          flowNodeId: 'Task_1t6iujt',
          tokenId: 'xyz',
          caught: true,
          continueExecution: false
        })
      ]);
    });
  });

  describe('ERROR EVENT', () => {
    it('should end execution, pass token, emit respondThrowingEventAction and fail attached Activity', async () => {
      const epic = boundaryEventInitEpic(mockLogger);
      const actions = await testEpic(
        epic,
        {
          ...errorBoundaryState,
          tokens: [
            {
              tokenId: 'xyz|abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'BoundaryEvent_1wr293r',
                startTime: 100
              }
            },
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'Task_1t6iujt',
                startTime: 100
              }
            }
          ]
        },
        [
          initBoundaryEventAction({
            flowNodeId: 'BoundaryEvent_1wr293r',
            tokenId: 'xyz|abc',
            eventOrigin: {
              flowNodeId: 'Task_1t6iujt',
              tokenId: 'xyz'
            }
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'BoundaryEvent_1wr293r',
          tokenId: 'xyz|abc',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'Flow_00p4puy'
          },
          endTime: expect.any(Number)
        }),
        endTokenAction({
          tokenId: 'xyz',
          state: ExecutionState.FAILED,
          endTime: expect.any(Number),
          removeAfterEnding: true
        }),
        respondThrowingEventAction({
          flowNodeId: 'Task_1t6iujt',
          tokenId: 'xyz',
          caught: true,
          continueExecution: false
        })
      ]);
    });
  });
});
