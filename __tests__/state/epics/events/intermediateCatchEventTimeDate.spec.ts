import { Definitions } from 'bpmn-moddle';
import {
  activateFlowNodeAction,
  interruptAction,
  updateTokenAction,
  endFlowNodeAction,
  endTokenAction,
  pauseTokenAction
} from '../../../../src/state/actions';
import intermediateCatchEventActivateEpic from '../../../../src/state/epics/event/intermediateCatchEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { TokenState } from '../../../../src/state/reducers/tokens';
import { ExecutionState } from '../../../../src/state/reducers/log';

let moddleDefinitions: Definitions;

const BPMN_XML_INTERMEDIATE_CATCH_TIMEDATE = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0ajmgwk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_063mnrk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0kuj0tn</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0kuj0tn" sourceRef="StartEvent_1" targetRef="IntermediateCatchEvent_08v7ysb" />
    <bpmn:endEvent id="EndEvent_12pdhek">
      <bpmn:incoming>Flow_0tiauv0</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0tiauv0" sourceRef="IntermediateCatchEvent_08v7ysb" targetRef="EndEvent_12pdhek" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_08v7ysb">
      <bpmn:incoming>Flow_0kuj0tn</bpmn:incoming>
      <bpmn:outgoing>Flow_0tiauv0</bpmn:outgoing>
      <bpmn:timerEventDefinition>
        <bpmn:timeDate>2022-06-14T20:40:40</bpmn:timeDate>
      </bpmn:timerEventDefinition>
    </bpmn:intermediateCatchEvent>
  </bpmn:process>
  </bpmn:definitions>`;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_INTERMEDIATE_CATCH_TIMEDATE, (_, definitions) => {
    moddleDefinitions = definitions;
    done();
  });
});

let initialProcessState: ProcessState;

beforeEach(() => {
  // mock current Date time -> 2 seconds before specified time date
  Date.now = jest.fn().mockReturnValue(new Date('2022-06-14T20:40:38'));

  initialProcessState = {
    ...DEFAULT_PROCESS_STATE,
    moddleDefinitions
  };
});

describe('Activate', () => {
  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(epic, initialProcessState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(epic, initialProcessState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should wait till time date reached', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...initialProcessState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        })
      ],
      1500
    );

    // update token to READY, expect no other action until timer fires
    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      })
    ]);
  });

  it('should end execution and pass token once time date reached', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...initialProcessState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      }),
      endFlowNodeAction({
        flowNodeId: 'IntermediateCatchEvent_08v7ysb',
        tokenId: 'xyz',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          passTokenTo: 'Flow_0tiauv0'
        },
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should end execution immediately if specified date is already passed', async () => {
    // mock current Date time -> 2 seconds AFTER specified time date
    Date.now = jest.fn().mockReturnValue(new Date('2022-06-14T20:40:42'));
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...initialProcessState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        })
      ],
      100
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      }),
      endFlowNodeAction({
        flowNodeId: 'IntermediateCatchEvent_08v7ysb',
        tokenId: 'xyz',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          passTokenTo: 'Flow_0tiauv0'
        },
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should not emit if interrupted before time date reached', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...initialProcessState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        }),
        interruptAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      })
    ]);
  });

  it('should not emit if paused', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...initialProcessState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        }),
        pauseTokenAction({
          tokenId: 'xyz'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      })
    ]);
  });

  it('should not emit if token ended before time date reached', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...initialProcessState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        }),
        endTokenAction({
          tokenId: 'xyz',
          state: 'ABORTED'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      })
    ]);
  });
});
