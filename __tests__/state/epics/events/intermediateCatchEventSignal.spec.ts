import {
  activateFlowNodeAction,
  interruptAction,
  receiveSignalAction,
  willPassTokenAction,
  updateTokenAction,
  endFlowNodeAction,
  endTokenAction
} from '../../../../src/state/actions';
import intermediateCatchEventActivateEpic from '../../../../src/state/epics/event/intermediateCatchEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { TokenState } from '../../../../src/state/reducers/tokens';
import { ExecutionState } from '../../../../src/state/reducers/log';

const BPMN_XML_INTERMEDIATE_CATCH_SIGNAL = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0ajmgwk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:signal id="Signal_1gcpp7v" name="signal1" />
  <bpmn:process id="Process_063mnrk" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0kuj0tn</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0kuj0tn" sourceRef="StartEvent_1" targetRef="IntermediateCatchEvent_08v7ysb" />
    <bpmn:endEvent id="EndEvent_12pdhek">
      <bpmn:incoming>Flow_0tiauv0</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0tiauv0" sourceRef="IntermediateCatchEvent_08v7ysb" targetRef="EndEvent_12pdhek" />
    <bpmn:intermediateCatchEvent id="IntermediateCatchEvent_08v7ysb">
      <bpmn:incoming>Flow_0kuj0tn</bpmn:incoming>
      <bpmn:outgoing>Flow_0tiauv0</bpmn:outgoing>
      <bpmn:signalEventDefinition signalRef="Signal_1gcpp7v" />
    </bpmn:intermediateCatchEvent>
  </bpmn:process>
  </bpmn:definitions>`;

let baseState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_INTERMEDIATE_CATCH_SIGNAL, (_, definitions) => {
    baseState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('Activate', () => {
  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should wait till signal arrives', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        })
      ]
    );

    // update token to READY, expect no other action until signal event arrives
    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      })
    ]);
  });

  it('should end execution and pass token once signal arrives', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        }),
        receiveSignalAction({
          id: 'Signal_1gcpp7v'
        })
      ]
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      }),
      endFlowNodeAction({
        flowNodeId: 'IntermediateCatchEvent_08v7ysb',
        tokenId: 'xyz',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          passTokenTo: 'Flow_0tiauv0'
        },
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should not emit if interrupted before signal', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        }),
        interruptAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        }),
        receiveSignalAction({
          id: 'Signal_1gcpp7v'
        })
      ]
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      })
    ]);
  });

  it('should not emit if token ended before signal', async () => {
    const epic = intermediateCatchEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateCatchEvent_08v7ysb',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateCatchEvent_08v7ysb',
          tokenId: 'xyz'
        }),
        endTokenAction({
          tokenId: 'xyz',
          state: 'ABORTED'
        }),
        receiveSignalAction({
          id: 'Signal_1gcpp7v'
        })
      ],
      2500
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      })
    ]);
  });
});
