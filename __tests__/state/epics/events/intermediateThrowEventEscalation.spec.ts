import {
  activateFlowNodeAction,
  willPassTokenAction,
  propagateEscalationAction,
  respondThrowingEventAction,
  endTokenAction,
  endFlowNodeAction
} from '../../../../src/state/actions';
import intermediateThrowEventActivateEpic from '../../../../src/state/epics/event/intermediateThrowEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { ExecutionState } from '../../../../src/state/reducers/log';
import { TokenState } from '../../../../src/state/reducers/tokens';

const BPMN_XML_INTERMEDIATE_THROW_ESCALATION = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0puwgwk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0pu4dy7" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1xzgot0</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1xzgot0" sourceRef="StartEvent_1" targetRef="IntermediateThrowEvent_0kl15gm" />
    <bpmn:endEvent id="EndEvent_1e43kb4">
      <bpmn:incoming>Flow_1vpz7qi</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1vpz7qi" sourceRef="IntermediateThrowEvent_0kl15gm" targetRef="EndEvent_1e43kb4" />
    <bpmn:intermediateThrowEvent id="IntermediateThrowEvent_0kl15gm">
      <bpmn:incoming>Flow_1xzgot0</bpmn:incoming>
      <bpmn:outgoing>Flow_1vpz7qi</bpmn:outgoing>
      <bpmn:escalationEventDefinition escalationRef="escalation" id="EscalationEventDefinition_0ms9ej1" />
    </bpmn:intermediateThrowEvent>
  </bpmn:process>
</bpmn:definitions>`;

let baseState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_INTERMEDIATE_THROW_ESCALATION, (_, definitions) => {
    baseState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('Activate', () => {
  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = intermediateThrowEventActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element is of incorrect type', async () => {
    const epic = intermediateThrowEventActivateEpic();
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should end execution and pass token if thrown escalation was caught as non-interrupting', async () => {
    const epic = intermediateThrowEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateThrowEvent_0kl15gm',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateThrowEvent_0kl15gm',
          tokenId: 'xyz'
        }),
        respondThrowingEventAction({
          flowNodeId: 'IntermediateThrowEvent_0kl15gm',
          tokenId: 'xyz',
          caught: true,
          continueExecution: true
        })
      ]
    );

    expect(actions).toEqual([
      propagateEscalationAction({
        flowNodeId: 'IntermediateThrowEvent_0kl15gm',
        tokenId: 'xyz'
      }),
      endFlowNodeAction({
        flowNodeId: 'IntermediateThrowEvent_0kl15gm',
        tokenId: 'xyz',
        state: ExecutionState.COMPLETED,
        tokenHandling: {
          passTokenTo: 'Flow_1vpz7qi'
        },
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should emit COMPLETED after throwing escalation if thrown escalation was not caught', async () => {
    const epic = intermediateThrowEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'IntermediateThrowEvent_0kl15gm',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'IntermediateThrowEvent_0kl15gm',
          tokenId: 'xyz'
        }),
        respondThrowingEventAction({
          flowNodeId: 'IntermediateThrowEvent_0kl15gm',
          tokenId: 'xyz',
          caught: false,
          continueExecution: false
        })
      ]
    );

    expect(actions).toEqual([
      propagateEscalationAction({
        flowNodeId: 'IntermediateThrowEvent_0kl15gm',
        tokenId: 'xyz'
      }),
      endTokenAction({
        tokenId: 'xyz',
        state: TokenState.ERROR_SEMANTIC,
        errorMessage: 'Thrown Escalation undefined was not caught. Execution of Token is ended.',
        endTime: expect.any(Number)
      })
    ]);
  });
});
