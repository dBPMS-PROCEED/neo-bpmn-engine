import {
  activateFlowNodeAction,
  sendMessageAction,
  sendSignalAction,
  propagateErrorAction,
  propagateEscalationAction,
  respondThrowingEventAction,
  endTokenAction,
  endActivityAction
} from '../../../../src/state/actions';
import { endEventActivateEpic, endEventResponseEpic } from '../../../../src/state/epics/event/endEventEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { TokenState } from '../../../../src/state/reducers/tokens';
import { ExecutionState } from '../../../../src/state/reducers/log';
import { registerCustomTokenEndStates } from '../../../../src/Configuration';

describe('none end event', () => {
  const BPMN_XML_NONE_ENDEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
    <bpmn:process id="Process_1h320oz" isExecutable="true">
      <bpmn:startEvent id="StartEvent_1">
        <bpmn:outgoing>Flow_0vwkmpk</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:endEvent id="EndEvent_09vstqz">
        <bpmn:incoming>Flow_0vwkmpk</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="Flow_0vwkmpk" sourceRef="StartEvent_1" targetRef="EndEvent_09vstqz" />
    </bpmn:process>
  </bpmn:definitions>`;

  let endEventState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_NONE_ENDEVENT, (_, definitions) => {
      endEventState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, endEventState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, endEventState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should end token', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...endEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EndEvent_09vstqz',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EndEvent_09vstqz',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([endTokenAction({ tokenId: 'xyz', state: TokenState.ENDED, endTime: expect.any(Number) })]);
  });
});

describe('message end event', () => {
  const BPMN_XML_MESSAGE_ENDEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:message id="Message_0rktjyo" name="Message_1et2nam" />
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0vwkmpk</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0vwkmpk" sourceRef="StartEvent_1" targetRef="MessageEndEvent_09vstqz" />
    <bpmn:endEvent id="MessageEndEvent_09vstqz">
      <bpmn:incoming>Flow_0vwkmpk</bpmn:incoming>
      <bpmn:messageEventDefinition messageRef="Message_0rktjyo" />
    </bpmn:endEvent>
  </bpmn:process>
  </bpmn:definitions>`;

  let messageEventState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_MESSAGE_ENDEVENT, (_, definitions) => {
      messageEventState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, messageEventState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, messageEventState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should end token and emit message', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...messageEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'MessageEndEvent_09vstqz',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'MessageEndEvent_09vstqz',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([
      endTokenAction({ tokenId: 'xyz', state: TokenState.ENDED, endTime: expect.any(Number) }),
      sendMessageAction({ id: 'Message_0rktjyo' })
    ]);
  });
});

describe('signal end event', () => {
  const BPMN_XML_SIGNAL_ENDEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:signal id="Signal_12cv5fk" name="Signal_2j8p7pq" />
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0vwkmpk</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0vwkmpk" sourceRef="StartEvent_1" targetRef="SignalEndEvent_09vstqz" />
    <bpmn:endEvent id="SignalEndEvent_09vstqz">
      <bpmn:incoming>Flow_0vwkmpk</bpmn:incoming>
      <bpmn:signalEventDefinition signalRef="Signal_12cv5fk" />
    </bpmn:endEvent>
  </bpmn:process>
  </bpmn:definitions>`;

  let signalEventState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_SIGNAL_ENDEVENT, (_, definitions) => {
      signalEventState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, signalEventState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, signalEventState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should end token and emit signal', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...signalEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'SignalEndEvent_09vstqz',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'SignalEndEvent_09vstqz',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([
      endTokenAction({ tokenId: 'xyz', state: TokenState.ENDED, endTime: expect.any(Number) }),
      sendSignalAction({ id: 'Signal_12cv5fk' })
    ]);
  });
});

describe('error end event', () => {
  const BPMN_XML_ERROR_ENDEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:error id="Error_1mf2gpw" name="Error_0c9a62j" />
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0vwkmpk</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0vwkmpk" sourceRef="StartEvent_1" targetRef="ErrorEndEvent_09vstqz" />
    <bpmn:endEvent id="ErrorEndEvent_09vstqz">
      <bpmn:incoming>Flow_0vwkmpk</bpmn:incoming>
      <bpmn:errorEventDefinition errorRef="Error_1mf2gpw" />
    </bpmn:endEvent>
  </bpmn:process>
  </bpmn:definitions>`;

  let errorEventState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_ERROR_ENDEVENT, (_, definitions) => {
      errorEventState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, errorEventState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, errorEventState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });
  it('should propagate error', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...errorEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ErrorEndEvent_09vstqz',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ErrorEndEvent_09vstqz',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([
      propagateErrorAction({ flowNodeId: 'ErrorEndEvent_09vstqz', tokenId: 'xyz', refId: 'Error_1mf2gpw' })
    ]);
  });

  it('should end token with state ERROR-SEMANTIC if thrown error event was not caught', async () => {
    const epic = endEventResponseEpic();
    const actions = await testEpic(
      epic,
      {
        ...errorEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ErrorEndEvent_09vstqz',
              startTime: 100
            }
          }
        ]
      },
      [
        respondThrowingEventAction({
          flowNodeId: 'ErrorEndEvent_09vstqz',
          tokenId: 'xyz',
          caught: false,
          continueExecution: false
        })
      ]
    );

    expect(actions).toEqual([
      endTokenAction({
        tokenId: 'xyz',
        state: TokenState.ERROR_SEMANTIC,
        errorMessage: 'Thrown Event was not caught. Execution of Token is ended.',
        endTime: expect.any(Number)
      })
    ]);
  });
});

describe('escalation end event', () => {
  const BPMN_XML_ESCALATION_ENDEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:escalation id="Escalation_1cyxu5c" name="Escalation_9szb78" />
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0vwkmpk</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0vwkmpk" sourceRef="StartEvent_1" targetRef="EscalationEndEvent_09vstqz" />
    <bpmn:endEvent id="EscalationEndEvent_09vstqz">
      <bpmn:incoming>Flow_0vwkmpk</bpmn:incoming>
      <bpmn:escalationEventDefinition escalationRef="Escalation_1cyxu5c"" />
    </bpmn:endEvent>
  </bpmn:process>
  </bpmn:definitions>`;

  let escalationEventState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_ESCALATION_ENDEVENT, (_, definitions) => {
      escalationEventState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, escalationEventState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, escalationEventState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should propagate escalation', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...escalationEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EscalationEndEvent_09vstqz',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'EscalationEndEvent_09vstqz',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([propagateEscalationAction({ flowNodeId: 'EscalationEndEvent_09vstqz', tokenId: 'xyz' })]);
  });

  it('should end token with state ERROR-SEMANTIC if thrown escalation event was not caught', async () => {
    const epic = endEventResponseEpic();
    const actions = await testEpic(
      epic,
      {
        ...escalationEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'EscalationEndEvent_09vstqz',
              startTime: 100
            }
          }
        ]
      },
      [
        respondThrowingEventAction({
          flowNodeId: 'EscalationEndEvent_09vstqz',
          tokenId: 'xyz',
          caught: false,
          continueExecution: false
        })
      ]
    );

    expect(actions).toEqual([
      endTokenAction({
        tokenId: 'xyz',
        state: TokenState.ERROR_SEMANTIC,
        errorMessage: 'Thrown Event was not caught. Execution of Token is ended.',
        endTime: expect.any(Number)
      })
    ]);
  });
});

describe('terminate end event', () => {
  const BPMN_XML_TERMINATE_ENDEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0vwkmpk</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0vwkmpk" sourceRef="StartEvent_1" targetRef="TerminateEndEvent_09vstqz" />
    <bpmn:endEvent id="TerminateEndEvent_09vstqz">
      <bpmn:incoming>Flow_0vwkmpk</bpmn:incoming>
      <bpmn:terminateEventDefinition id="TerminateEventDefinition_07bwbd7" />
    </bpmn:endEvent>
  </bpmn:process>
  </bpmn:definitions>`;

  let terminateEventState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_TERMINATE_ENDEVENT, (_, definitions) => {
      terminateEventState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should ignore if bpmn element id does not exist in context', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, terminateEventState, [
      activateFlowNodeAction({
        flowNodeId: 'some-non-existent',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should ignore if bpmn element id points to incorrect type', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(epic, terminateEventState, [
      activateFlowNodeAction({
        flowNodeId: 'Task_0a5ear3',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });
  it('should end running tokens', async () => {
    const epic = endEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...terminateEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'TerminateEndEvent_09vstqz',
              startTime: 100
            }
          },
          {
            tokenId: 'pqr',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'TerminateEndEvent_09vstqz',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([
      endTokenAction({ tokenId: 'xyz', state: TokenState.ENDED, endTime: expect.any(Number) }),
      endTokenAction({ tokenId: 'pqr', state: TokenState.ABORTED, endTime: expect.any(Number) })
    ]);
  });
  it('should not end tokens that have already ended', async () => {
    registerCustomTokenEndStates('CUSTOM-ENDED');

    const epic = endEventActivateEpic();
    const actions = await testEpic(
      epic,
      {
        ...terminateEventState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'TerminateEndEvent_09vstqz',
              startTime: 100
            }
          },
          {
            tokenId: 'abc',
            state: TokenState.ENDED,
            currentFlowElement: {
              id: 'Activity_1',
              startTime: 100
            }
          },
          {
            tokenId: 'def',
            state: 'CUSTOM-ENDED', // This custom end state should be handled like any default end state
            currentFlowElement: {
              id: 'Activity_2',
              startTime: 100
            }
          },
          {
            tokenId: 'pqr',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'TerminateEndEvent_09vstqz',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([
      endTokenAction({ tokenId: 'xyz', state: TokenState.ENDED, endTime: expect.any(Number) }),
      endTokenAction({ tokenId: 'pqr', state: TokenState.ABORTED, endTime: expect.any(Number) })
    ]);
  });
  describe('subprocess', () => {
    const BPMN_XML_SUBPROCESS_TERMINATE_ENDEVENT = `
    <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0mjrccn</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0mjrccn" sourceRef="StartEvent_1" targetRef="Subprocess_0ubfvzi" />
    <bpmn:subProcess id="Subprocess_0ubfvzi">
      <bpmn:incoming>Flow_0mjrccn</bpmn:incoming>
      <bpmn:outgoing>Flow_1darpax</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_1xk6p4m">
        <bpmn:outgoing>Flow_18pnhky</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:sequenceFlow id="Flow_18pnhky" sourceRef="StartEvent_1xk6p4m" targetRef="TerminateEndEvent_0hcdfs1" />
      <bpmn:endEvent id="TerminateEndEvent_0hcdfs1">
        <bpmn:incoming>Flow_18pnhky</bpmn:incoming>
        <bpmn:terminateEventDefinition id="TerminateEventDefinition_0ukt471" />
      </bpmn:endEvent>
    </bpmn:subProcess>
    <bpmn:endEvent id="EndEvent_1mlks29">
      <bpmn:incoming>Flow_1darpax</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1darpax" sourceRef="Subprocess_0ubfvzi" targetRef="EndEvent_1mlks29" />
  </bpmn:process>
  </bpmn:definitions>`;

    let subProcessState: ProcessState;

    beforeAll(done => {
      new BpmnModdle().fromXML(BPMN_XML_SUBPROCESS_TERMINATE_ENDEVENT, (_, definitions) => {
        subProcessState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token and emit endActivityAction for the subprocess', async () => {
      const epic = endEventActivateEpic();
      const actions = await testEpic(
        epic,
        {
          ...subProcessState,
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'Subprocess_0ubfvzi',
                startTime: 100
              }
            },
            {
              tokenId: 'xyz#123|abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'TerminateEndEvent_0hcdfs1',
                startTime: 100
              }
            }
          ]
        },
        [
          activateFlowNodeAction({
            flowNodeId: 'TerminateEndEvent_0hcdfs1',
            tokenId: 'xyz#123|abc'
          })
        ]
      );

      expect(actions).toEqual([
        endTokenAction({ tokenId: 'xyz#123|abc', state: TokenState.ENDED, endTime: expect.any(Number) }),
        endActivityAction({ tokenId: 'xyz', flowNodeId: 'Subprocess_0ubfvzi', state: ExecutionState.ABORTED })
      ]);
    });
  });
});
