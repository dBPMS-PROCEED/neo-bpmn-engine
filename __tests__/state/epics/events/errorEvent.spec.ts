import {
  propagateErrorAction,
  createTokenAction,
  respondThrowingEventAction,
  initBoundaryEventAction
} from '../../../../src/state/actions';
import { errorEventEpic } from '../../../../src/state/epics/event/errorEventEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import BpmnModdle from '../../../../src/util/BpmnModdle';
import { testEpic } from '../../../helpers/epicTester';
import { createMockLogger } from '../../../helpers/mockLogger';
import { TokenState } from '../../../../src/state/reducers/tokens';

const mockLogger = createMockLogger();

beforeEach(() => {
  mockLogger.reset();
});

describe('PROCESS INSTANCE LEVEL', () => {
  const BPMN_XML_TASK_WITHOUT_BOUNDARYEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1ohebts</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_192c4km">
      <bpmn:incoming>Flow_1ohebts</bpmn:incoming>
      <bpmn:outgoing>Flow_0bw1s7w</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1ohebts" sourceRef="StartEvent_1" targetRef="Task_192c4km" />
    <bpmn:endEvent id="EndEvent_1nkb6ph">
      <bpmn:incoming>Flow_0bw1s7w</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0bw1s7w" sourceRef="Task_192c4km" targetRef="EndEvent_1nkb6ph" />
  </bpmn:process>
  </bpmn:definitions>`;

  let noBoundaryState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_TASK_WITHOUT_BOUNDARYEVENT, (_, definitions) => {
      noBoundaryState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should end token if activity does not have error boundary event', async () => {
    const epic = errorEventEpic();
    const actions = await testEpic(
      epic,
      {
        ...noBoundaryState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Task_192c4km',
              startTime: 100
            }
          }
        ]
      },
      [
        propagateErrorAction({
          flowNodeId: 'Task_192c4km',
          tokenId: 'xyz',
          refId: 'error'
        })
      ]
    );
    expect(actions).toEqual([
      respondThrowingEventAction({
        flowNodeId: 'Task_192c4km',
        tokenId: 'xyz',
        caught: false,
        continueExecution: false
      })
    ]);
  });

  const BPMN_XML_TASK_WITH_BOUNDARYEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:error id="error" errorCode="EXAMPLE-CODE" /> 
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1ohebts</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_192c4km">
      <bpmn:incoming>Flow_1ohebts</bpmn:incoming>
      <bpmn:outgoing>Flow_0bw1s7w</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_1ohebts" sourceRef="StartEvent_1" targetRef="Task_192c4km" />
    <bpmn:endEvent id="EndEvent_1nkb6ph">
      <bpmn:incoming>Flow_0bw1s7w</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0bw1s7w" sourceRef="Task_192c4km" targetRef="EndEvent_1nkb6ph" />
    <bpmn:endEvent id="Event_19qisx7">
      <bpmn:incoming>Flow_186n5lp</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_186n5lp" sourceRef="BoundaryEvent_1utagnk" targetRef="Event_19qisx7" />
    <bpmn:boundaryEvent id="BoundaryEvent_1utagnk" attachedToRef="Task_192c4km">
      <bpmn:outgoing>Flow_186n5lp</bpmn:outgoing>
      <bpmn:errorEventDefinition errorRef="error" />
    </bpmn:boundaryEvent>
  </bpmn:process>
  </bpmn:definitions>`;

  let boundaryState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_TASK_WITH_BOUNDARYEVENT, (_, definitions) => {
      boundaryState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should end token if thrown error can not be matched with boundary event', async () => {
    const epic = errorEventEpic();
    const actions = await testEpic(
      epic,
      {
        ...boundaryState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Task_192c4km',
              startTime: 100
            },
            localStartTime: 100
          }
        ]
      },
      [
        propagateErrorAction({
          flowNodeId: 'Task_192c4km',
          tokenId: 'xyz',
          refId: 'different-error'
        })
      ]
    );
    expect(actions).toEqual([
      respondThrowingEventAction({
        flowNodeId: 'Task_192c4km',
        tokenId: 'xyz',
        caught: false,
        continueExecution: false
      })
    ]);
  });

  it('should INIT boundary event at task if boundary event has correct errorRef', async () => {
    const epic = errorEventEpic();
    const actions = await testEpic(
      epic,
      {
        ...boundaryState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Task_192c4km',
              startTime: 100
            },
            localStartTime: 100,
            localExecutionTime: 100
          }
        ]
      },
      [
        propagateErrorAction({
          flowNodeId: 'Task_192c4km',
          tokenId: 'xyz',
          refId: 'error'
        })
      ]
    );
    expect(actions).toEqual([
      createTokenAction({
        tokenId: expect.stringMatching(/^xyz\|.+/),
        state: TokenState.READY,
        currentFlowElement: { id: 'BoundaryEvent_1utagnk' },
        localStartTime: 100,
        localExecutionTime: 100
      }),
      initBoundaryEventAction({
        flowNodeId: 'BoundaryEvent_1utagnk',
        tokenId: expect.stringMatching(/^xyz\|.+/),
        eventOrigin: {
          tokenId: 'xyz',
          flowNodeId: 'Task_192c4km'
        }
      })
    ]);
  });

  it('should INIT boundary event at task if boundary event has correct errorCode', async () => {
    const epic = errorEventEpic();
    const actions = await testEpic(
      epic,
      {
        ...boundaryState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Task_192c4km',
              startTime: 100
            },
            localStartTime: 100,
            localExecutionTime: 100
          }
        ]
      },
      [
        propagateErrorAction({
          flowNodeId: 'Task_192c4km',
          tokenId: 'xyz',
          refId: 'EXAMPLE-CODE'
        })
      ]
    );
    expect(actions).toEqual([
      createTokenAction({
        tokenId: expect.stringMatching(/^xyz\|.+/),
        state: TokenState.READY,
        currentFlowElement: { id: 'BoundaryEvent_1utagnk' },
        localStartTime: 100,
        localExecutionTime: 100
      }),
      initBoundaryEventAction({
        flowNodeId: 'BoundaryEvent_1utagnk',
        tokenId: expect.stringMatching(/^xyz\|.+/),
        eventOrigin: {
          tokenId: 'xyz',
          flowNodeId: 'Task_192c4km'
        }
      })
    ]);
  });
});

describe('SUBPROCESS LEVEL', () => {
  const BPMN_XML_SUBPROCESS_ERROR_BOUNDARYEVENT = `
  <bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_10u36kv" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_1h320oz" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1w1f92n</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1w1f92n" sourceRef="StartEvent_1" targetRef="Subprocess_195ej78" />
    <bpmn:subProcess id="Subprocess_195ej78">
      <bpmn:incoming>Flow_1w1f92n</bpmn:incoming>
      <bpmn:outgoing>Flow_0hj1b7s</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_15kcnfh">
        <bpmn:outgoing>Flow_1gpr49n</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:sequenceFlow id="Flow_1gpr49n" sourceRef="StartEvent_15kcnfh" targetRef="ErrorEndEvent_09aioji" />
      <bpmn:endEvent id="ErrorEndEvent_09aioji">
        <bpmn:incoming>Flow_1gpr49n</bpmn:incoming>
        <bpmn:errorEventDefinition id="ErrorEventDefinition_1ivzhuh" />
      </bpmn:endEvent>
    </bpmn:subProcess>
    <bpmn:endEvent id="EndEvent_1p6jc43">
      <bpmn:incoming>Flow_0hj1b7s</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_0hj1b7s" sourceRef="Subprocess_195ej78" targetRef="EndEvent_1p6jc43" />
    <bpmn:boundaryEvent id="BoundaryEvent_186rr0a" attachedToRef="Subprocess_195ej78">
      <bpmn:outgoing>Flow_1h9e787</bpmn:outgoing>
      <bpmn:errorEventDefinition id="ErrorEventDefinition_0zx3lin" />
    </bpmn:boundaryEvent>
    <bpmn:endEvent id="EndEvent_12941zr">
      <bpmn:incoming>Flow_1h9e787</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1h9e787" sourceRef="BoundaryEvent_186rr0a" targetRef="EndEvent_12941zr" />
  </bpmn:process>
  </bpmn:definitions>`;

  let subprocessState: ProcessState;

  beforeAll(done => {
    new BpmnModdle().fromXML(BPMN_XML_SUBPROCESS_ERROR_BOUNDARYEVENT, (_, definitions) => {
      subprocessState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
      done();
    });
  });

  it('should propagate event to upper scope and INIT boundary event', async () => {
    const epic = errorEventEpic();
    const actions = await testEpic(
      epic,
      {
        ...subprocessState,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Subprocess_195ej78', // subprocess
              startTime: 100
            },
            localStartTime: 100,
            localExecutionTime: 100
          },
          {
            tokenId: 'xyz#abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ErrorEndEvent_09aioji', // throwing error event inside subprocess
              startTime: 100
            },
            localStartTime: 200,
            localExecutionTime: 100
          }
        ]
      },
      [
        propagateErrorAction({
          flowNodeId: 'ErrorEndEvent_09aioji',
          tokenId: 'xyz#abc',
          refId: 'error'
        })
      ]
    );
    expect(actions).toEqual([
      createTokenAction({
        tokenId: expect.stringMatching(/^xyz\|.+/),
        state: TokenState.READY,
        currentFlowElement: { id: 'BoundaryEvent_186rr0a' },
        localStartTime: 100,
        localExecutionTime: 100
      }),
      initBoundaryEventAction({
        flowNodeId: 'BoundaryEvent_186rr0a',
        tokenId: expect.stringMatching(/^xyz\|.+/),
        eventOrigin: {
          flowNodeId: 'ErrorEndEvent_09aioji',
          tokenId: 'xyz#abc'
        }
      })
    ]);
  });
});
