import { ActionsObservable, StateObservable } from 'redux-observable';
import { merge, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import {
  initFlowNodeAction,
  pauseProcessAction,
  ProcessAction,
  ProcessActionType,
  resumeProcessAction,
  tearDownProcessAction
} from '../../../src/state/actions';
import rootEpic from '../../../src/state/epics/rootEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../src/state/reducers';
import { DEFAULT_STATUS_STATE, ProcessStatus } from '../../../src/state/reducers/status';
import { testEpic } from '../../helpers/epicTester';
import { TokenState } from '../../../src/state/reducers/tokens';
import { ExecutionState } from '../../../src/state/reducers/log';

describe('root epic', () => {
  it('should pass thorugh actions when unpaused ', async () => {
    const actions = [
      initFlowNodeAction({ flowNodeId: '1', tokenId: 'abc' }),
      initFlowNodeAction({ flowNodeId: '2', tokenId: 'pqr' })
    ];
    const STATE = {
      ...DEFAULT_PROCESS_STATE,
      status: DEFAULT_STATUS_STATE
    };

    const myEpic = (action$: ActionsObservable<ProcessAction>) =>
      action$.ofType(ProcessActionType.FlowNodeInit).pipe(
        map(a => ({
          type: 'NONE',
          originalAction: { ...a }
        }))
      );

    const epic = rootEpic(myEpic as any);

    expect(await testEpic(epic, STATE, actions)).toEqual([
      { type: 'NONE', originalAction: initFlowNodeAction({ flowNodeId: '1', tokenId: 'abc' }) },
      { type: 'NONE', originalAction: initFlowNodeAction({ flowNodeId: '2', tokenId: 'pqr' }) }
    ]);
  });

  it('should not pass any even while paused', async () => {
    const actions = [
      initFlowNodeAction({ flowNodeId: '1', tokenId: 'abc' }),
      initFlowNodeAction({ flowNodeId: '2', tokenId: 'pqr' })
    ];
    const STATE = {
      ...DEFAULT_PROCESS_STATE,
      status: ProcessStatus.PAUSED
    };

    const myEpic = (action$: ActionsObservable<ProcessAction>) =>
      action$.ofType(ProcessActionType.FlowNodeInit).pipe(
        map(a => ({
          type: 'NONE',
          originalAction: { ...a }
        }))
      );

    const epic = rootEpic(myEpic as any);

    expect(await testEpic(epic, STATE, actions)).toEqual([]);
  });

  it('should release buffered events after resume', async () => {
    const action$ = merge(
      of(pauseProcessAction()).pipe(delay(0)),
      of(initFlowNodeAction({ flowNodeId: '1', tokenId: 'abc' })).pipe(delay(50)),
      of(initFlowNodeAction({ flowNodeId: '2', tokenId: 'pqr' })).pipe(delay(100)),
      of(resumeProcessAction()).pipe(delay(150)),
      of(initFlowNodeAction({ flowNodeId: '3', tokenId: 'xyz' })).pipe(delay(200))
    ) as ActionsObservable<ProcessAction>;

    const INIT_STATE = {
      ...DEFAULT_PROCESS_STATE,
      status: ProcessStatus.PAUSED
    };

    const state$ = merge(
      of(INIT_STATE),
      of({
        ...DEFAULT_PROCESS_STATE,
        status: DEFAULT_STATUS_STATE
      }).pipe(delay(150))
    ) as StateObservable<ProcessState>;

    const myEpic = (action$: ActionsObservable<ProcessAction>) =>
      action$.ofType(ProcessActionType.FlowNodeInit).pipe(
        map(a => ({
          type: 'NONE',
          originalAction: { ...a }
        }))
      );

    const epic = rootEpic(myEpic as any);

    const retActions = await testEpic(epic, INIT_STATE, action$, 250, state$);

    expect(retActions).toEqual([
      { type: 'NONE', originalAction: initFlowNodeAction({ flowNodeId: '1', tokenId: 'abc' }) },
      { type: 'NONE', originalAction: initFlowNodeAction({ flowNodeId: '2', tokenId: 'pqr' }) },
      { type: 'NONE', originalAction: initFlowNodeAction({ flowNodeId: '3', tokenId: 'xyz' }) }
    ]);
  });

  it('should not emit events after end', async () => {
    const action$ = merge(
      of(initFlowNodeAction({ flowNodeId: '1', tokenId: 'abc' })).pipe(delay(50)),
      of(initFlowNodeAction({ flowNodeId: '2', tokenId: 'pqr' })).pipe(delay(100)),
      of(tearDownProcessAction()).pipe(delay(150)),
      of(initFlowNodeAction({ flowNodeId: '3', tokenId: 'xyz' })).pipe(delay(200))
    ) as ActionsObservable<ProcessAction>;

    const INIT_STATE = {
      ...DEFAULT_PROCESS_STATE,
      status: DEFAULT_STATUS_STATE
    };

    const state$ = merge(
      of(INIT_STATE),
      of({
        ...DEFAULT_PROCESS_STATE,
        status: ProcessStatus.ENDED
      }).pipe(delay(150))
    ) as StateObservable<ProcessState>;

    const myEpic = (action$: ActionsObservable<ProcessAction>) =>
      action$.ofType(ProcessActionType.FlowNodeInit).pipe(
        map(a => ({
          type: 'NONE',
          originalAction: { ...a }
        }))
      );

    const epic = rootEpic(myEpic as any);

    const retActions = await testEpic(epic, INIT_STATE, action$, 250, state$);

    expect(retActions).toEqual([
      { type: 'NONE', originalAction: initFlowNodeAction({ flowNodeId: '1', tokenId: 'abc' }) },
      { type: 'NONE', originalAction: initFlowNodeAction({ flowNodeId: '2', tokenId: 'pqr' }) }
    ]);
  });

  it('should abort the process if there is a runtime error during its execution', async () => {
    const actions = [initFlowNodeAction({ flowNodeId: '1', tokenId: 'abc' })];
    const STATE = {
      ...DEFAULT_PROCESS_STATE,
      status: DEFAULT_STATUS_STATE,
      tokens: [
        {
          currentFlowElement: { id: '1' },
          tokenId: 'abc',
          state: TokenState.READY,
          localStartTime: 0,
          localExecutionTime: 0
        }
      ]
    };

    const myEpic = (action$: ActionsObservable<ProcessAction>) =>
      action$.ofType(ProcessActionType.FlowNodeInit).pipe(
        map(_ => {
          throw new Error();
        })
      );

    const epic = rootEpic(myEpic as any);
    const retActions = await testEpic(epic, STATE, actions);

    const abortFlowNode = retActions.find(a => a.type === ProcessActionType.FlowNodeEnd);
    expect(abortFlowNode).toBeDefined();
    expect(abortFlowNode?.payload?.flowNodeId).toBe('1');
    expect(abortFlowNode?.payload?.state).toBe(ExecutionState.ERROR_UNKNOWN);

    const abortToken = retActions.find(a => a.type === ProcessActionType.UpdateToken);
    expect(abortToken).toBeDefined();
    expect(abortToken?.payload?.tokenId).toBe('abc');
    expect(abortToken?.payload?.state).toBe(TokenState.ERROR_UNKNOWN);

    const endProcess = retActions.find(a => a.type === ProcessActionType.EndProcess);
    expect(endProcess).toBeDefined();
    expect(endProcess?.payload?.aborted).toBe(true);
  });
});
