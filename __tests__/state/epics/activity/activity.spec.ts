import { endFlowNodeAction, endActivityAction } from '../../../../src/state/actions';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../src/state/reducers';
import { TokenState } from '../../../../src/state/reducers/tokens';
import { ExecutionState } from '../../../../src/state/reducers/log';
import { endActivityEpic } from '../../../../src/state/epics/activity/activityEpic';

import { testEpic } from '../../../helpers/epicTester';
import BpmnModdle from '../../../../src/util/BpmnModdle';

const BPMN_XML_ABSTRACT_TASK = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1by602o" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0u68iyx" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0ogkvto</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_13g2n7k">
      <bpmn:incoming>Flow_0ogkvto</bpmn:incoming>
      <bpmn:outgoing>Flow_17bkkgc</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0ogkvto" sourceRef="StartEvent_1" targetRef="Task_13g2n7k" />
    <bpmn:endEvent id="EndEvent_1txooty">
      <bpmn:incoming>Flow_17bkkgc</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_17bkkgc" sourceRef="Task_13g2n7k" targetRef="EndEvent_1txooty" />
  </bpmn:process>
</bpmn:definitions>`;

const BPMN_XML_SCRIPT_TASK = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1by602o" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0u68iyx" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0ogkvto</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0ogkvto" sourceRef="StartEvent_1" targetRef="ScriptTask_13g2n7k" />
    <bpmn:endEvent id="EndEvent_1txooty">
      <bpmn:incoming>Flow_17bkkgc</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_17bkkgc" sourceRef="ScriptTask_13g2n7k" targetRef="EndEvent_1txooty" />
    <bpmn:scriptTask id="ScriptTask_13g2n7k">
      <bpmn:incoming>Flow_0ogkvto</bpmn:incoming>
      <bpmn:outgoing>Flow_17bkkgc</bpmn:outgoing>
      <bpmn:script>
      log.info('Hello World');
      </bpmn:script>
    </bpmn:scriptTask>
  </bpmn:process>
</bpmn:definitions>`;

const BPMN_XML_USER_TASK = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1by602o" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0u68iyx" isExecutable="true">

    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0ogkvto</bpmn:outgoing>
    </bpmn:startEvent>

    <bpmn:sequenceFlow id="Flow_0ogkvto" sourceRef="StartEvent_1" targetRef="UserTask_13g2n7k" />

    <bpmn:endEvent id="EndEvent_1txooty">
      <bpmn:incoming>Flow_17bkkgc</bpmn:incoming>
    </bpmn:endEvent>

    <bpmn:sequenceFlow id="Flow_17bkkgc" sourceRef="UserTask_13g2n7k" targetRef="EndEvent_1txooty" />

    <bpmn:userTask id="UserTask_13g2n7k">
      <bpmn:incoming>Flow_0ogkvto</bpmn:incoming>
      <bpmn:outgoing>Flow_17bkkgc</bpmn:outgoing>
    </bpmn:userTask>

  </bpmn:process>
</bpmn:definitions>`;

const BPMN_XML_SUBPROCESS = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0i6szkk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0kk744j" isExecutable="true">
    
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_00oflkn</bpmn:outgoing>
    </bpmn:startEvent>

    <bpmn:sequenceFlow id="Flow_00oflkn" sourceRef="StartEvent_1" targetRef="Subprocess_1jk1d2r" />

    <bpmn:subProcess id="Subprocess_1jk1d2r">
      <bpmn:incoming>Flow_00oflkn</bpmn:incoming>
      <bpmn:outgoing>Flow_1gj7cwl</bpmn:outgoing>

      <bpmn:startEvent id="StartEvent_1n4nikm">
        <bpmn:outgoing>Flow_0q2atb3</bpmn:outgoing>
      </bpmn:startEvent>

      <bpmn:task id="Task_1bq3ax5">
        <bpmn:incoming>Flow_0q2atb3</bpmn:incoming>
        <bpmn:outgoing>Flow_05idzmy</bpmn:outgoing>
      </bpmn:task>

      <bpmn:sequenceFlow id="Flow_0q2atb3" sourceRef="StartEvent_1n4nikm" targetRef="Task_1bq3ax5" />

      <bpmn:endEvent id="EndEvent_1sh2sct">
        <bpmn:incoming>Flow_05idzmy</bpmn:incoming>
      </bpmn:endEvent>

      <bpmn:sequenceFlow id="Flow_05idzmy" sourceRef="Task_1bq3ax5" targetRef="EndEvent_1sh2sct" />

    </bpmn:subProcess>

    <bpmn:endEvent id="EndEvent_15nr0en">
      <bpmn:incoming>Flow_1gj7cwl</bpmn:incoming>
    </bpmn:endEvent>

    <bpmn:sequenceFlow id="Flow_1gj7cwl" sourceRef="Subprocess_1jk1d2r" targetRef="EndEvent_15nr0en" />

  </bpmn:process>
</bpmn:definitions>`;

const BPMN_XML_CALL_ACTIVITY = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:p33c24="http://docs.snet.tu-berlin.de/proceed/_17c6fed0-8a8c-4722-a62f-86ebf1324c33" id="_e292e6c4-4d7f-4aff-b91f-c102d5ea4ae8" name="Global Task Process" targetNamespace="http://docs.snet.tu-berlin.de/proceed/_e292e6c4-4d7f-4aff-b91f-c102d5ea4ae8" exporter="Camunda Modeler" exporterVersion="4.0.0">
  <import namespace="http://docs.snet.tu-berlin.de/proceed/_17c6fed0-8a8c-4722-a62f-86ebf1324c33" location="Other-Process-17c6fed0" importType="http://www.omg.org/spec/BPMN/20100524/MODEL" />
  <bpmn:process id="Process_1au95he" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_1t2jdbt</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_1t2jdbt" sourceRef="StartEvent_1" targetRef="CallActivity_1mcfm7t" />
    <bpmn:endEvent id="EndEvent_1snnjzn">
      <bpmn:incoming>Flow_1ydrl4y</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1ydrl4y" sourceRef="CallActivity_1mcfm7t" targetRef="EndEvent_1snnjzn" />
    <bpmn:callActivity id="CallActivity_1mcfm7t" name="Call the global process" calledElement="p33c24:_e069937f-27b6-464b-b397-b88a2599f1b9">
      <bpmn:incoming>Flow_1t2jdbt</bpmn:incoming>
      <bpmn:outgoing>Flow_1ydrl4y</bpmn:outgoing>
    </bpmn:callActivity>
  </bpmn:process>
</bpmn:definitions>`;

let moddle = new BpmnModdle();

let baseState: ProcessState;

beforeAll(done => {
  moddle.fromXML(BPMN_XML_USER_TASK, (_, definitions) => {
    baseState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('COMPLETED', () => {
  it('should not respond to other bpmn element type', async () => {
    const epic = endActivityEpic();
    const actions = await testEpic(epic, baseState, [
      endActivityAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED
      })
    ]);

    expect(actions).toEqual([]);
  });

  describe('Task', () => {
    let taskState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_ABSTRACT_TASK, (_, definitions) => {
        taskState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end execution and emit pass token event', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...taskState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'Task_13g2n7k',
                startTime: 100
              }
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'Task_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.COMPLETED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'Task_13g2n7k',
          tokenId: 'abc',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'Flow_17bkkgc'
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('ScriptTask', () => {
    let scriptState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_SCRIPT_TASK, (_, definitions) => {
        scriptState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end execution and emit pass token event', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...scriptState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'ScriptTask_13g2n7k',
                startTime: 100
              }
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'ScriptTask_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.COMPLETED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'Flow_17bkkgc'
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('UserTask', () => {
    let userState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_USER_TASK, (_, definitions) => {
        userState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end execution of user task and emit pass token event', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...userState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'UserTask_13g2n7k',
                startTime: 100
              }
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'UserTask_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.COMPLETED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'UserTask_13g2n7k',
          tokenId: 'abc',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'Flow_17bkkgc'
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('SubProcess', () => {
    let subprocessState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_SUBPROCESS, (_, definitions) => {
        subprocessState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end execution if tokens inside ended and emit pass token event', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...subprocessState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'Subprocess_1jk1d2r',
                startTime: 100
              }
            },
            {
              tokenId: 'abc#123',
              state: TokenState.ENDED,
              currentFlowElement: {
                id: 'EndEvent_1sh2sct',
                startTime: 100
              }
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'Subprocess_1jk1d2r',
            tokenId: 'abc',
            state: ExecutionState.COMPLETED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'Subprocess_1jk1d2r',
          tokenId: 'abc',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'Flow_1gj7cwl'
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('callActivity', () => {
    let callActivityState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_CALL_ACTIVITY, (_, definitions) => {
        callActivityState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end execution and emit pass token event', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...callActivityState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              currentFlowElement: {
                id: 'CallActivity_1mcfm7t',
                startTime: 100
              }
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'CallActivity_1mcfm7t',
            tokenId: 'abc',
            state: ExecutionState.COMPLETED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          flowNodeId: 'CallActivity_1mcfm7t',
          tokenId: 'abc',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'Flow_1ydrl4y'
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });
});

describe('TERMINATED', () => {
  it('should not respond to other bpmn element type', async () => {
    const epic = endActivityEpic();
    const actions = await testEpic(epic, baseState, [
      endActivityAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'abc',
        state: ExecutionState.TERMINATED
      })
    ]);

    expect(actions).toEqual([]);
  });

  describe('Task', () => {
    let taskState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_ABSTRACT_TASK, (_, definitions) => {
        taskState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state TERMINATED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...taskState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Task_13g2n7k',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'Task_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.TERMINATED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'Task_13g2n7k',
          state: ExecutionState.TERMINATED,
          tokenHandling: {
            endToken: TokenState.TERMINATED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('ScriptTask', () => {
    let scriptState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_SCRIPT_TASK, (_, definitions) => {
        scriptState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state TERMINATED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...scriptState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'ScriptTask_13g2n7k',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'ScriptTask_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.TERMINATED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'ScriptTask_13g2n7k',
          state: ExecutionState.TERMINATED,
          tokenHandling: {
            endToken: TokenState.TERMINATED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('UserTask', () => {
    let userState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_USER_TASK, (_, definitions) => {
        userState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state TERMINATED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...userState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'UserTask_13g2n7k',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'UserTask_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.TERMINATED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'UserTask_13g2n7k',
          state: ExecutionState.TERMINATED,
          tokenHandling: {
            endToken: TokenState.TERMINATED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('SubProcess', () => {
    let subprocessState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_SUBPROCESS, (_, definitions) => {
        subprocessState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state TERMINATED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...subprocessState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Subprocess_1jk1d2r',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'Subprocess_1jk1d2r',
            tokenId: 'abc',
            state: ExecutionState.TERMINATED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'Subprocess_1jk1d2r',
          state: ExecutionState.TERMINATED,
          tokenHandling: {
            endToken: TokenState.TERMINATED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('CallActivity', () => {
    let callActivityState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_CALL_ACTIVITY, (_, definitions) => {
        callActivityState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state TERMINATED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...callActivityState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'CallActivity_1mcfm7t',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'CallActivity_1mcfm7t',
            tokenId: 'abc',
            state: ExecutionState.TERMINATED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'CallActivity_1mcfm7t',
          state: ExecutionState.TERMINATED,
          tokenHandling: {
            endToken: TokenState.TERMINATED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });
});

describe('FAILED', () => {
  it('should not respond to other bpmn element type', async () => {
    const epic = endActivityEpic();
    const actions = await testEpic(epic, baseState, [
      endActivityAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'abc',
        state: ExecutionState.FAILED
      })
    ]);

    expect(actions).toEqual([]);
  });

  describe('Task', () => {
    let taskState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_ABSTRACT_TASK, (_, definitions) => {
        taskState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state FAILED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...taskState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Task_13g2n7k',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'Task_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.FAILED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'Task_13g2n7k',
          state: ExecutionState.FAILED,
          tokenHandling: {
            endToken: TokenState.FAILED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('ScriptTask', () => {
    let scriptState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_SCRIPT_TASK, (_, definitions) => {
        scriptState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state FAILED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...scriptState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'ScriptTask_13g2n7k',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'ScriptTask_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.FAILED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'ScriptTask_13g2n7k',
          state: ExecutionState.FAILED,
          tokenHandling: {
            endToken: TokenState.FAILED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('UserTask', () => {
    let userState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_USER_TASK, (_, definitions) => {
        userState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state FAILED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...userState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'UserTask_13g2n7k',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'UserTask_13g2n7k',
            tokenId: 'abc',
            state: ExecutionState.FAILED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'UserTask_13g2n7k',
          state: ExecutionState.FAILED,
          tokenHandling: {
            endToken: TokenState.FAILED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('SubProcess', () => {
    let subprocessState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_SUBPROCESS, (_, definitions) => {
        subprocessState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state FAILED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...subprocessState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Subprocess_1jk1d2r',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'Subprocess_1jk1d2r',
            tokenId: 'abc',
            state: ExecutionState.FAILED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'Subprocess_1jk1d2r',
          state: ExecutionState.FAILED,
          tokenHandling: {
            endToken: TokenState.FAILED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });

  describe('CallActivity', () => {
    let callActivityState: ProcessState;
    beforeAll(done => {
      moddle.fromXML(BPMN_XML_CALL_ACTIVITY, (_, definitions) => {
        callActivityState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
        done();
      });
    });

    it('should end token with state FAILED', async () => {
      const epic = endActivityEpic();
      const actions = await testEpic(
        epic,
        {
          ...callActivityState,
          tokens: [
            {
              tokenId: 'abc',
              state: TokenState.FAILED,
              localStartTime: 100,
              currentFlowElement: {
                id: 'CallActivity_1mcfm7t',
                startTime: 100
              },
              localExecutionTime: 100
            }
          ]
        },
        [
          endActivityAction({
            flowNodeId: 'CallActivity_1mcfm7t',
            tokenId: 'abc',
            state: ExecutionState.FAILED
          })
        ]
      );

      expect(actions).toEqual([
        endFlowNodeAction({
          tokenId: 'abc',
          flowNodeId: 'CallActivity_1mcfm7t',
          state: ExecutionState.FAILED,
          tokenHandling: {
            endToken: TokenState.FAILED
          },
          endTime: expect.any(Number)
        })
      ]);
    });
  });
});
