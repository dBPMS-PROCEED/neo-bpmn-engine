import { endTokenAction, endFlowNodeAction, endActivityAction } from '../../../../../src/state/actions';
import embeddedSubprocessEndFlowNodeEpic from '../../../../../src/state/epics/activity/subprocess/embeddedSubprocessEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../../src/state/reducers';
import BpmnModdle from '../../../../../src/util/BpmnModdle';
import { testEpic } from '../../../../helpers/epicTester';
import { createMockLogger } from '../../../../helpers/mockLogger';
import { TokenState } from '../../../../../src/state/reducers/tokens';
import { ExecutionState } from '../../../../../src/state/reducers/log';
import { registerCustomTokenEndStates } from '../../../../../src/Configuration';

const BPMN_XML_SUBPROCESS = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_0i6szkk" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0kk744j" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_00oflkn</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_00oflkn" sourceRef="StartEvent_1" targetRef="Subprocess_1jk1d2r" />
    <bpmn:subProcess id="Subprocess_1jk1d2r">
      <bpmn:incoming>Flow_00oflkn</bpmn:incoming>
      <bpmn:outgoing>Flow_1gj7cwl</bpmn:outgoing>
      <bpmn:startEvent id="StartEvent_1n4nikm">
        <bpmn:outgoing>Flow_0q2atb3</bpmn:outgoing>
      </bpmn:startEvent>
      <bpmn:task id="Task_1bq3ax5">
        <bpmn:incoming>Flow_0q2atb3</bpmn:incoming>
        <bpmn:outgoing>Flow_05idzmy</bpmn:outgoing>
      </bpmn:task>
      <bpmn:sequenceFlow id="Flow_0q2atb3" sourceRef="StartEvent_1n4nikm" targetRef="Task_1bq3ax5" />
      <bpmn:endEvent id="EndEvent_1sh2sct">
        <bpmn:incoming>Flow_05idzmy</bpmn:incoming>
      </bpmn:endEvent>
      <bpmn:sequenceFlow id="Flow_05idzmy" sourceRef="Task_1bq3ax5" targetRef="EndEvent_1sh2sct" />
    </bpmn:subProcess>
    <bpmn:endEvent id="EndEvent_15nr0en">
      <bpmn:incoming>Flow_1gj7cwl</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1gj7cwl" sourceRef="Subprocess_1jk1d2r" targetRef="EndEvent_15nr0en" />
  </bpmn:process>
</bpmn:definitions>`;

const mockLogger = createMockLogger();

beforeEach(() => {
  mockLogger.reset();
});

let baseState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_SUBPROCESS, (_, definitions) => {
    baseState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('ABORTED', () => {
  it('should not respond to other bpmn element type', async () => {
    const epic = embeddedSubprocessEndFlowNodeEpic(mockLogger);
    const actions = await testEpic(epic, baseState, [
      endActivityAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'abc',
        state: ExecutionState.ABORTED
      })
    ]);

    expect(actions).toEqual([]);
  });

  it('should log execution as aborted, abort tokens inside and emit pass token event', async () => {
    const epic = embeddedSubprocessEndFlowNodeEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Subprocess_1jk1d2r',
              startTime: 100
            }
          },
          {
            tokenId: 'abc#123',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Task_02z411f',
              startTime: 100
            }
          }
        ]
      },
      [
        endFlowNodeAction({
          flowNodeId: 'Subprocess_1jk1d2r',
          tokenId: 'abc',
          state: ExecutionState.ABORTED
        })
      ]
    );

    expect(actions).toEqual([
      endTokenAction({
        tokenId: 'abc#123',
        state: TokenState.ABORTED,
        endTime: expect.any(Number)
      })
    ]);
  });
});

describe('Token on subprocess ended', () => {
  it('should not respond to other bpmn element type', async () => {
    const epic = embeddedSubprocessEndFlowNodeEpic(mockLogger);
    const actions = await testEpic(epic, baseState, [
      endTokenAction({
        tokenId: 'abc',
        state: TokenState.TERMINATED,
        endTime: 100
      })
    ]);

    expect(actions).toEqual([]);
  });

  it('should close elements and set token state to TERMINATED for every token inside subprocess', async () => {
    registerCustomTokenEndStates('CUSTOM-ENDED');

    const epic = embeddedSubprocessEndFlowNodeEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.TERMINATED,
            currentFlowElement: {
              id: 'Subprocess_1jk1d2r', // subprocess
              startTime: 100
            }
          },
          {
            tokenId: 'xyz#123',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'Task_1bq3ax5', // task inside subprocess
              startTime: 100
            }
          },
          {
            tokenId: 'xyz#456',
            state: TokenState.ENDED,
            currentFlowElement: {
              id: 'Task_2', // task inside subprocess
              startTime: 100
            }
          },
          {
            tokenId: 'xyz#456',
            state: 'CUSTOM-ENDED', // This custom end state should be handled like any default end state
            currentFlowElement: {
              id: 'Task_3', // task inside subprocess
              startTime: 100
            }
          }
        ]
      },
      [
        endFlowNodeAction({
          tokenId: 'xyz',
          flowNodeId: 'Subprocess_1jk1d2r',
          state: ExecutionState.TERMINATED,
          endTime: 100
        })
      ]
    );

    expect(mockLogger.debug).toHaveBeenCalledWith({
      message: `Terminating SubProcess Subprocess_1jk1d2r with all containing elements`,
      params: { processInstanceId: 'p1' }
    });

    expect(actions).toEqual([
      endTokenAction({
        tokenId: 'xyz#123',
        state: TokenState.TERMINATED,
        endTime: expect.any(Number)
      })
    ]);
  });
});
