import { activateFlowNodeAction, endActivityAction } from '../../../../../src/state/actions';
import abstractTaskActivateEpic from '../../../../../src/state/epics/activity/tasks/abstractTaskEpic';
import { DEFAULT_PROCESS_STATE } from '../../../../../src/state/reducers';
import { ExecutionState } from '../../../../../src/state/reducers/log';
import BpmnModdle from '../../../../../src/util/BpmnModdle';
import { testEpic } from '../../../../helpers/epicTester';

const BPMN_XML_ABSTRACT_TASK = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1by602o" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0u68iyx" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0ogkvto</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_13g2n7k">
      <bpmn:incoming>Flow_0ogkvto</bpmn:incoming>
      <bpmn:outgoing>Flow_17bkkgc</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="Flow_0ogkvto" sourceRef="StartEvent_1" targetRef="Task_13g2n7k" />
    <bpmn:endEvent id="EndEvent_1txooty">
      <bpmn:incoming>Flow_17bkkgc</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_17bkkgc" sourceRef="Task_13g2n7k" targetRef="EndEvent_1txooty" />
  </bpmn:process>
</bpmn:definitions>`;

const state = DEFAULT_PROCESS_STATE;

state.tokens = [
  {
    tokenId: 'abc',
    state: 'READY',
    localStartTime: 0,
    localExecutionTime: 100,
    currentFlowElement: {
      id: 'Task_13g2n7k'
    },
    intermediateVariablesState: {}
  },
  {
    tokenId: 'def',
    state: 'READY',
    localStartTime: 0,
    localExecutionTime: 100,
    currentFlowElement: {
      id: 'Flow_0ogkvto'
    },
    intermediateVariablesState: null
  }
];

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_ABSTRACT_TASK, (_, definitions) => {
    state.moddleDefinitions = definitions;
    done();
  });
});

describe('abstractTaskActivatedEpic', () => {
  it('should not respond to other bpmn element types', async () => {
    const epic = abstractTaskActivateEpic();
    const actions = await testEpic(epic, state, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'abc'
      })
    ]);

    expect(actions).toEqual([]);
  });

  it('should not respond to non existing token', async () => {
    const epic = abstractTaskActivateEpic();
    const actions = await testEpic(epic, state, [
      activateFlowNodeAction({
        flowNodeId: 'Task_13g2n7k',
        tokenId: 'xyz'
      })
    ]);

    expect(actions).toEqual([]);
  });

  it('should not respond to token on other different element', async () => {
    const epic = abstractTaskActivateEpic();
    const actions = await testEpic(epic, state, [
      activateFlowNodeAction({
        flowNodeId: 'Task_13g2n7k',
        tokenId: 'def'
      })
    ]);
  });

  it('should complete activity', async () => {
    const epic = abstractTaskActivateEpic();
    const actions = await testEpic(epic, state, [
      activateFlowNodeAction({
        flowNodeId: 'Task_13g2n7k',
        tokenId: 'abc'
      })
    ]);

    expect(actions).toEqual([
      endActivityAction({
        flowNodeId: 'Task_13g2n7k',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED
      })
    ]);
  });
});
