import { getService } from '../../../../../src/CustomService';
import { ScriptExecutionError, BpmnEscalation, BpmnError } from '../../../../../src/errors';
import {
  DEFAULT_SCRIPT_EXECUTOR,
  provideScriptExecutor,
  ScriptExecutor
} from '../../../../../src/ScriptExecutionEnvironment';
import {
  activateFlowNodeAction,
  setVariablesAction,
  propagateEscalationAction,
  propagateErrorAction,
  endTokenAction,
  respondThrowingEventAction,
  ProcessAction,
  taskThrowErrorAction,
  taskThrowEscalationAction,
  interruptAction,
  endActivityAction
} from '../../../../../src/state/actions';
import {
  scriptTaskActivateEpic,
  scriptTaskThrowErrorEpic,
  scriptTaskThrowEscalationEpic
} from '../../../../../src/state/epics/activity/tasks/scriptTaskEpic';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../../../src/state/reducers';
import BpmnModdle from '../../../../../src/util/BpmnModdle';
import { testEpic } from '../../../../helpers/epicTester';
import { createMockLogger } from '../../../../helpers/mockLogger';
import { TokenState } from '../../../../../src/state/reducers/tokens';
import { ActionsObservable } from 'redux-observable';
import { merge, of } from 'rxjs';
import { delay } from 'rxjs/internal/operators/delay';
import { ExecutionState } from '../../../../../src/state/reducers/log';

const mockLogger = createMockLogger();
const mockScriptExecutor: ScriptExecutor = {
  execute: jest.fn(),
  stop: jest.fn()
};
jest.mock('../../../../../src/CustomService', () => {
  const getService = jest.fn().mockReturnValue({});
  return {
    getService
  };
});

beforeAll(() => {
  provideScriptExecutor(mockScriptExecutor);
});

beforeEach(() => {
  mockLogger.reset();
  (mockScriptExecutor.execute as jest.Mock).mockReset();
  (mockScriptExecutor.stop as jest.Mock).mockReset();
});

afterAll(() => {
  provideScriptExecutor(DEFAULT_SCRIPT_EXECUTOR);
});

const BPMN_XML_SCRIPT_TASK = `
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1by602o" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="4.3.0">
  <bpmn:process id="Process_0u68iyx" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_0ogkvto</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_0ogkvto" sourceRef="StartEvent_1" targetRef="ScriptTask_13g2n7k" />
    <bpmn:endEvent id="EndEvent_1txooty">
      <bpmn:incoming>Flow_17bkkgc</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_17bkkgc" sourceRef="ScriptTask_13g2n7k" targetRef="EndEvent_1txooty" />
    <bpmn:scriptTask id="ScriptTask_13g2n7k">
      <bpmn:incoming>Flow_0ogkvto</bpmn:incoming>
      <bpmn:outgoing>Flow_17bkkgc</bpmn:outgoing>
      <bpmn:script>
      log.info('Hello World');
      </bpmn:script>
    </bpmn:scriptTask>
  </bpmn:process>
</bpmn:definitions>`;

let baseState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_SCRIPT_TASK, (_, definitions) => {
    baseState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('Activate (script execution)', () => {
  it('should not respond to other bpmn element types', async () => {
    const epic = scriptTaskActivateEpic(mockLogger);
    const actions = await testEpic(epic, baseState, [
      activateFlowNodeAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'abc'
      })
    ]);

    expect(actions).toEqual([]);
    expect(mockScriptExecutor.execute).not.toHaveBeenCalled();
  });

  it('should call the script executor with given script and dependencies and complete activity', async () => {
    (mockScriptExecutor.execute as jest.Mock).mockReturnValue(Promise.resolve());
    const epic = scriptTaskActivateEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        })
      ]
    );

    const SCRIPT = `log.info('Hello World');`.trim();
    expect(mockScriptExecutor.execute).toHaveBeenCalledWith(
      'myProcess',
      'p1',
      'abc',
      SCRIPT,
      expect.objectContaining({
        log: expect.any(Object),
        console: expect.any(Object),
        variable: expect.any(Object),
        getService,
        BpmnEscalation,
        BpmnError
      })
    );
    expect(actions).toEqual([
      endActivityAction({ flowNodeId: 'ScriptTask_13g2n7k', tokenId: 'abc', state: ExecutionState.COMPLETED })
    ]);
  });

  it('should not set variables if none are returned', async () => {
    (mockScriptExecutor.execute as jest.Mock).mockReturnValue(Promise.resolve()); // nothing returned from executed script
    const epic = scriptTaskActivateEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        })
      ]
    );
    expect(mockScriptExecutor.execute).toHaveBeenCalledWith(
      'myProcess',
      'p1',
      'abc',
      expect.stringMatching(''),
      expect.objectContaining({
        log: expect.anything(),
        console: expect.any(Object),
        variable: expect.anything(),
        getService,
        BpmnEscalation,
        BpmnError
      })
    );

    expect(actions).toEqual([
      endActivityAction({ flowNodeId: 'ScriptTask_13g2n7k', tokenId: 'abc', state: ExecutionState.COMPLETED })
    ]);
  });

  it('should set variables if returned', async () => {
    // variables to be set returned from executed script
    (mockScriptExecutor.execute as jest.Mock).mockReturnValue(Promise.resolve({ x: 100, y: 1000 }));
    const epic = scriptTaskActivateEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        })
      ]
    );

    expect(mockScriptExecutor.execute).toHaveBeenCalledWith(
      'myProcess',
      'p1',
      'abc',
      expect.stringMatching(''),
      expect.objectContaining({
        log: expect.anything(),
        console: expect.any(Object),
        variable: expect.anything(),
        getService,
        BpmnEscalation,
        BpmnError
      })
    );

    expect(actions).toEqual([
      setVariablesAction({ changedBy: 'ScriptTask_13g2n7k', variables: { x: 100, y: 1000 } }),
      endActivityAction({ flowNodeId: 'ScriptTask_13g2n7k', tokenId: 'abc', state: ExecutionState.COMPLETED })
    ]);
  });

  it('should emit taskthrowEscalation-action if BpmnEscalation was thrown in script', async () => {
    // thrown BpmnEscalation from executed script
    (mockScriptExecutor.execute as jest.Mock).mockReturnValue(
      Promise.reject(new BpmnEscalation('Bpmn-Escalation', 'Throw an Escalation'))
    );
    const epic = scriptTaskActivateEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        })
      ]
    );

    expect(mockScriptExecutor.execute).toHaveBeenCalledWith(
      'myProcess',
      'p1',
      'abc',
      expect.stringMatching(''),
      expect.objectContaining({
        log: expect.anything(),
        console: expect.any(Object),
        variable: expect.anything(),
        getService,
        BpmnEscalation,
        BpmnError
      })
    );

    expect(actions).toEqual([
      taskThrowEscalationAction({ flowNodeId: 'ScriptTask_13g2n7k', tokenId: 'abc', refId: 'Bpmn-Escalation' })
    ]);

    expect(mockLogger.debug).toHaveBeenCalledWith({
      message: 'Throw an Escalation',
      params: { processInstanceId: 'p1' }
    });
  });

  it('should emit taskthrowerror-action if BpmnError was thrown in script', async () => {
    // thrown BpmnError from executed script
    (mockScriptExecutor.execute as jest.Mock).mockReturnValue(
      Promise.reject(new BpmnError('Bpmn-Error', 'Throw an Error'))
    );
    const epic = scriptTaskActivateEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        })
      ]
    );

    expect(mockScriptExecutor.execute).toHaveBeenCalledWith(
      'myProcess',
      'p1',
      'abc',
      expect.stringMatching(''),
      expect.objectContaining({
        log: expect.anything(),
        console: expect.any(Object),
        variable: expect.anything(),
        getService,
        BpmnEscalation,
        BpmnError
      })
    );

    expect(actions).toEqual([
      taskThrowErrorAction({ flowNodeId: 'ScriptTask_13g2n7k', tokenId: 'abc', refId: 'Bpmn-Error' })
    ]);

    expect(mockLogger.debug).toHaveBeenCalledWith({
      message: 'Throw an Error',
      params: { processInstanceId: 'p1' }
    });
  });

  it('should end token with state ERROR-TECHNICAL if technical error occured in script', async () => {
    // thrown error from executed script
    (mockScriptExecutor.execute as jest.Mock).mockReturnValue(
      Promise.reject(new ScriptExecutionError('Some technical Error Occurred'))
    );
    const epic = scriptTaskActivateEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        })
      ]
    );

    expect(mockScriptExecutor.execute).toHaveBeenCalledWith(
      'myProcess',
      'p1',
      'abc',
      expect.stringMatching(''),
      expect.objectContaining({
        log: expect.anything(),
        console: expect.any(Object),
        variable: expect.anything(),
        getService,
        BpmnEscalation,
        BpmnError
      })
    );

    expect(actions).toEqual([
      endTokenAction({
        tokenId: 'abc',
        state: TokenState.ERROR_TECHNICAL,
        errorMessage: 'Technical Error occured: Error | Error executing script: Some technical Error Occurred',
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should stop execution if flowNode was interrupted', async () => {
    (mockScriptExecutor.execute as jest.Mock).mockReturnValue(Promise.resolve());
    const epic = scriptTaskActivateEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        }),
        interruptAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        })
      ]
    );

    expect(mockScriptExecutor.execute).toHaveBeenCalledWith(
      'myProcess',
      'p1',
      'abc',
      expect.stringMatching(''),
      expect.objectContaining({
        log: expect.anything(),
        console: expect.any(Object),
        variable: expect.anything(),
        getService,
        BpmnEscalation,
        BpmnError
      })
    );

    expect(actions).toEqual([]);
  });

  it('should stop execution if token is ended', async () => {
    (mockScriptExecutor.execute as jest.Mock).mockReturnValue(Promise.resolve());
    const epic = scriptTaskActivateEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      [
        activateFlowNodeAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc'
        }),
        endTokenAction({
          tokenId: 'abc',
          state: 'ABORTED'
        })
      ]
    );

    expect(mockScriptExecutor.execute).toHaveBeenCalledWith(
      'myProcess',
      'p1',
      'abc',
      expect.stringMatching(''),
      expect.objectContaining({
        log: expect.anything(),
        console: expect.any(Object),
        variable: expect.anything(),
        getService,
        BpmnEscalation,
        BpmnError
      })
    );

    expect(actions).toEqual([]);
  });
});

describe('throwEscalation', () => {
  it('should not respond to other bpmn element type', async () => {
    const epic = scriptTaskThrowEscalationEpic();
    const actions = await testEpic(epic, baseState, [
      endActivityAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED
      })
    ]);

    expect(actions).toEqual([]);
  });

  it('should propagate escalation and end token with state ERROR-SEMANTIC if escalation was not caught', async () => {
    const epic = scriptTaskThrowEscalationEpic();

    const action$ = merge(
      of(
        taskThrowEscalationAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          refId: 'ref-123'
        })
      ),
      of(
        respondThrowingEventAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          caught: false,
          continueExecution: false
        })
      ).pipe(delay(1))
    ) as ActionsObservable<ProcessAction>;

    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      action$
    );

    expect(actions).toEqual([
      propagateEscalationAction({
        flowNodeId: 'ScriptTask_13g2n7k',
        tokenId: 'abc',
        refId: 'ref-123'
      }),
      endTokenAction({
        tokenId: 'abc',
        state: TokenState.ERROR_SEMANTIC,
        errorMessage: 'Thrown Escalation ref-123 was not caught. Execution of Token is ended.',
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should propagate escalation and complete activity if escalation was caught and is non-interrupting', async () => {
    const epic = scriptTaskThrowEscalationEpic();

    const action$ = merge(
      of(
        taskThrowEscalationAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          refId: 'ref-123'
        })
      ),
      of(
        respondThrowingEventAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          caught: true,
          continueExecution: true
        })
      ).pipe(delay(1))
    ) as ActionsObservable<ProcessAction>;

    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      action$
    );

    expect(actions).toEqual([
      propagateEscalationAction({
        flowNodeId: 'ScriptTask_13g2n7k',
        tokenId: 'abc',
        refId: 'ref-123'
      }),
      endActivityAction({
        flowNodeId: 'ScriptTask_13g2n7k',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED
      })
    ]);
  });
});

describe('throwError', () => {
  it('should not respond to other bpmn element type', async () => {
    const epic = scriptTaskThrowErrorEpic();
    const actions = await testEpic(epic, baseState, [
      endActivityAction({
        flowNodeId: 'StartEvent_1',
        tokenId: 'abc',
        state: ExecutionState.COMPLETED
      })
    ]);

    expect(actions).toEqual([]);
  });

  it('should propagate error and end token with state ERROR-SEMANTIC if error was not caught', async () => {
    const epic = scriptTaskThrowErrorEpic();

    const action$ = merge(
      of(
        taskThrowErrorAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          refId: 'ref-123'
        })
      ),
      of(
        respondThrowingEventAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          caught: false,
          continueExecution: false
        })
      ).pipe(delay(1))
    ) as ActionsObservable<ProcessAction>;

    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      action$
    );

    expect(actions).toEqual([
      propagateErrorAction({
        flowNodeId: 'ScriptTask_13g2n7k',
        tokenId: 'abc',
        refId: 'ref-123'
      }),
      endTokenAction({
        tokenId: 'abc',
        state: TokenState.ERROR_SEMANTIC,
        errorMessage: 'Thrown Error ref-123 was not caught. Execution of Token is ended.',
        endTime: expect.any(Number)
      })
    ]);
  });

  it('should propagate error', async () => {
    const epic = scriptTaskThrowErrorEpic();

    const action$ = merge(
      of(
        taskThrowErrorAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          refId: 'ref-123'
        })
      ),
      of(
        respondThrowingEventAction({
          flowNodeId: 'ScriptTask_13g2n7k',
          tokenId: 'abc',
          caught: true,
          continueExecution: false
        })
      ).pipe(delay(1))
    ) as ActionsObservable<ProcessAction>;

    const actions = await testEpic(
      epic,
      {
        ...baseState,
        tokens: [
          {
            tokenId: 'abc',
            state: TokenState.RUNNING,
            currentFlowElement: {
              id: 'ScriptTask_13g2n7k',
              startTime: 100
            }
          }
        ],
        processId: 'myProcess',
        processInstanceId: 'p1'
      },
      action$
    );

    expect(actions).toEqual([
      propagateErrorAction({
        flowNodeId: 'ScriptTask_13g2n7k',
        tokenId: 'abc',
        refId: 'ref-123'
      })
    ]);
  });
});
