import {
  initFlowNodeAction,
  willPassTokenAction,
  updateTokenAction,
  didPassTokenAction,
  endProcessAction,
  createTokenAction,
  endTokenAction,
  logExecutionAction,
  endFlowNodeAction,
  interruptAction,
  pauseTokenAction,
  willActivateFlowNodeAction,
  activateFlowNodeAction,
  endActivityAction,
  removeTokenAction,
  stopProcessAction,
  reactivateProcessAction,
  setVariablesAction,
  moveTokenAction
} from '../../../src/state/actions';
import {
  willPassTokenEpic,
  didPassTokenEpic,
  endTokenEpic,
  endFlowNodeEpic,
  updateTokenEpic,
  removeTokenEpic,
  willActivateFlowNodeEpic,
  flowNodeInitEpic,
  createTokenEpic,
  moveTokenEpic
} from '../../../src/state/epics/dispatcher';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../src/state/reducers';
import BpmnModdle from '../../../src/util/BpmnModdle';
import {
  BPMN_XML_EXCLUSIVE_XOR_GATEWAY,
  BPMN_XML_INCLUSIVE_GATEWAY,
  BPMN_XML_INTERMEDIATE_CATCH,
  BPMN_XML_PARALLEL_GATEWAY,
  BPMN_XML_SCRIPT_TASK,
  BPMN_XML_SINGLE_NONE_START_EVENT,
  BPMN_XML_EMBEDDED_SUBPROCESS,
  BPMN_XML_USER_TASK,
  BPMN_XML_CALL_ACTIVITY
} from '../../fixtures/bpmnXml';
import { testEpic } from '../../helpers/epicTester';
import { createMockLogger } from '../../helpers/mockLogger';
import { TokenState, FlowNodeState } from '../../../src/state/reducers/tokens';
import { ExecutionState } from '../../../src/state/reducers/log';
import { ProcessStatus } from '../../../src/state/reducers/status';
import { registerCustomTokenEndStates } from '../../../src/Configuration';

const mockLogger = createMockLogger();

beforeEach(() => {
  mockLogger.reset();
});

beforeAll(() => {
  const mockDate = new Date(1000000000000);
  jest.spyOn(global, 'Date').mockImplementation(() => (mockDate as unknown) as string); // mock date to keep tests consistent all the time
});

let simpleState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_SINGLE_NONE_START_EVENT, (_, definitions) => {
    simpleState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

let parallelState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_PARALLEL_GATEWAY, (_, definitions) => {
    parallelState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

let inclusiveState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_INCLUSIVE_GATEWAY, (_, definitions) => {
    inclusiveState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

let exclusiveState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_EXCLUSIVE_XOR_GATEWAY, (_, definitions) => {
    exclusiveState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

let intermediateState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_INTERMEDIATE_CATCH, (_, definitions) => {
    intermediateState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

let scriptTaskState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_SCRIPT_TASK, (_, definitions) => {
    scriptTaskState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

let subprocessState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_EMBEDDED_SUBPROCESS, (_, definitions) => {
    subprocessState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

let userTaskState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_USER_TASK, (_, definitions) => {
    userTaskState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

let callActivityState: ProcessState;

beforeAll(done => {
  new BpmnModdle().fromXML(BPMN_XML_CALL_ACTIVITY, (_, definitions) => {
    callActivityState = { ...DEFAULT_PROCESS_STATE, moddleDefinitions: definitions };
    done();
  });
});

describe('willPassToken', () => {
  it('should ignore if sequenceFlow id does not exist in context', async () => {
    const epic = willPassTokenEpic();
    const actions = await testEpic(epic, simpleState, [
      willPassTokenAction({
        sequenceId: 'some-SequenceFlowId',
        tokenId: 'xyz'
      })
    ]);
    expect(actions).toEqual([]);
  });

  it('should not pass token if shouldPassTokenHook is interrupted by PAUSE-action', async () => {
    const epic = willPassTokenEpic(() => Promise.resolve(true));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.RUNNING,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        willPassTokenAction({
          sequenceId: 'SequenceFlow_07so23a',
          tokenId: 'xyz'
        }),
        pauseTokenAction({
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not pass token if shouldPassTokenHook is interrupted by INTERRUPT-action', async () => {
    const epic = willPassTokenEpic(() => Promise.resolve(true));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.RUNNING,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        willPassTokenAction({
          sequenceId: 'SequenceFlow_07so23a',
          tokenId: 'xyz'
        }),
        interruptAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not pass token if shouldPassTokenHook is token is ended', async () => {
    const epic = willPassTokenEpic(() => Promise.resolve(true));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.RUNNING,
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        willPassTokenAction({
          sequenceId: 'SequenceFlow_07so23a',
          tokenId: 'xyz'
        }),
        endTokenAction({
          tokenId: 'xyz',
          state: 'ABORTED'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not pass token to next element if shouldPassTokenHook returns with false', async () => {
    const epic = willPassTokenEpic(() => Promise.resolve(false));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        willPassTokenAction({
          sequenceId: 'SequenceFlow_07so23a',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  describe('sample bpmn:Task', () => {
    it('should generate didPassToken action to pass token and updateToken action to update previousFlowElement info', async () => {
      const epic = willPassTokenEpic();
      const actions = await testEpic(
        epic,
        {
          ...simpleState,
          processId: 'myProcess',
          processInstanceId: 'p1',
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.READY,
              localStartTime: 100,
              currentFlowElement: {
                id: 'StartEvent_1',
                startTime: 100
              }
            }
          ]
        },
        [
          willPassTokenAction({
            sequenceId: 'SequenceFlow_07so23a',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        didPassTokenAction({
          tokenId: 'xyz',
          to: 'Task_175vb4p'
        })
      ]);
    });
  });

  describe('bpmn:ParallelGateway split side - single incoming', () => {
    it('should generate didPassToken action to move token and updateToken action to update previousFlowElement info', async () => {
      const epic = willPassTokenEpic();
      const actions = await testEpic(
        epic,
        {
          ...parallelState,
          processId: 'myProcess',
          processInstanceId: 'p1',
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.READY,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Task_0rav7hn',
                startTime: 100
              }
            }
          ]
        },
        [
          willPassTokenAction({
            sequenceId: 'SequenceFlow_01das5j',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        didPassTokenAction({
          to: 'ParallelGateway_1gb46mc',
          tokenId: 'xyz'
        })
      ]);
    });

    describe('parallel split', () => {
      it('should map each willPassToken result to didPassTokenAction to split token', async () => {
        const epic = willPassTokenEpic();
        const actions = await testEpic(
          epic,
          {
            ...parallelState,
            processId: 'myProcess',
            processInstanceId: 'p1',
            tokens: [
              {
                tokenId: 'abc|123',
                state: TokenState.READY,
                localStartTime: 100,
                currentFlowElement: {
                  id: 'Task_1p4tmqj'
                }
              },
              {
                tokenId: 'abc|456',
                state: TokenState.READY,
                localStartTime: 100,
                currentFlowElement: {
                  id: 'Task_0j6jido'
                }
              },
              {
                tokenId: 'abc|789',
                state: TokenState.READY,
                localStartTime: 100,
                currentFlowElement: {
                  id: 'Task_0l2ayv1'
                }
              }
            ]
          },
          [
            willPassTokenAction({
              sequenceId: 'SequenceFlow_0zcellh',
              tokenId: 'abc|123'
            }),
            willPassTokenAction({
              sequenceId: 'SequenceFlow_0glegwn',
              tokenId: 'abc|456'
            }),
            willPassTokenAction({
              sequenceId: 'SequenceFlow_07jqmiu',
              tokenId: 'abc|789'
            })
          ]
        );

        expect(actions).toEqual([
          didPassTokenAction({ to: 'Task_1p4tmqj', tokenId: 'abc|123' }),
          didPassTokenAction({ to: 'Task_0j6jido', tokenId: 'abc|456' }),
          didPassTokenAction({ to: 'Task_0l2ayv1', tokenId: 'abc|789' })
        ]);
      });
    });
  });

  describe('bpmn:InclusiveGateway split side - single incoming', () => {
    it('should generate didPassToken action to move token', async () => {
      const epic = willPassTokenEpic();
      const actions = await testEpic(
        epic,
        {
          ...inclusiveState,
          processId: 'myProcess',
          processInstanceId: 'p1',
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.READY,
              localStartTime: 100,
              currentFlowElement: {
                id: 'StartEvent_1',
                startTime: 100
              }
            }
          ]
        },
        [
          willPassTokenAction({
            sequenceId: 'SequenceFlow_0wterhl',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        didPassTokenAction({
          to: 'InclusiveGateway_1s9snq4',
          tokenId: 'xyz'
        })
      ]);
    });
  });

  describe('bpmn:IntermediateCatchEvent', () => {
    it('should generate didPassToken action', async () => {
      const epic = willPassTokenEpic();
      const actions = await testEpic(
        epic,
        {
          ...intermediateState,
          processId: 'myProcess',
          processInstanceId: 'p1',
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.READY,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Task_0twpunq',
                startTime: 100
              }
            }
          ]
        },
        [
          willPassTokenAction({
            sequenceId: 'SequenceFlow_14aau7v',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        didPassTokenAction({
          to: 'IntermediateCatchEvent_0o02gmp',
          tokenId: 'xyz'
        })
      ]);
    });
  });

  describe('bpmn:ScriptTask', () => {
    it('should generate didPassToken action', async () => {
      const epic = willPassTokenEpic();
      const actions = await testEpic(
        epic,
        {
          ...scriptTaskState,
          processId: 'myProcess',
          processInstanceId: 'p1',
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.READY,
              localStartTime: 100,
              currentFlowElement: {
                id: 'StartEvent_1',
                startTime: 100
              }
            }
          ]
        },
        [
          willPassTokenAction({
            sequenceId: 'SequenceFlow_17hik6f',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        didPassTokenAction({
          to: 'Task_0rozrh3',
          tokenId: 'xyz'
        })
      ]);
    });
  });
});

describe('didPassToken', () => {
  it('should ignore if token does not exist', async () => {
    const epic = didPassTokenEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: []
      },
      [
        didPassTokenAction({
          to: 'Task_175vb4p',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should set token state to RUNNING and store sequence flow id in previousFlowElementId and init element which token lies on', async () => {
    const epic = didPassTokenEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'SequenceFlow_07so23a',
              startTime: 100
            }
          }
        ]
      },
      [
        didPassTokenAction({
          to: 'Task_175vb4p',
          tokenId: 'xyz'
        })
      ]
    );

    expect(mockLogger.debug).toHaveBeenCalledWith({
      message: `Token xyz gets passed to element Task_175vb4p on this machine`,
      params: { processInstanceId: 'p1' }
    });

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.RUNNING,
        currentFlowElement: { id: 'Task_175vb4p' },
        previousFlowElementId: 'SequenceFlow_07so23a'
      }),
      initFlowNodeAction({
        tokenId: 'xyz',
        flowNodeId: 'Task_175vb4p'
      })
    ]);
  });

  describe('bpmn:SubProcess', () => {
    describe('Starting a Subprocess from beginning', () => {
      it('should init subprocess and create token for its startEvent', async () => {
        const epic = didPassTokenEpic(mockLogger);
        const actions = await testEpic(
          epic,
          {
            ...subprocessState,
            processId: 'myProcess',
            processInstanceId: 'p1',
            tokens: [
              {
                tokenId: 'xyz',
                state: TokenState.READY,
                localStartTime: 100,
                currentFlowElement: {
                  id: 'Flow_13xi3sc',
                  startTime: 100
                },
                localExecutionTime: 100
              }
            ]
          },
          [
            didPassTokenAction({
              to: 'Subprocess_1hfk5eg',
              tokenId: 'xyz'
            })
          ]
        );

        expect(actions).toEqual([
          updateTokenAction({
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            currentFlowElement: { id: 'Subprocess_1hfk5eg' },
            previousFlowElementId: 'Flow_13xi3sc'
          }),
          initFlowNodeAction({
            flowNodeId: 'Subprocess_1hfk5eg',
            tokenId: 'xyz'
          }),
          createTokenAction({
            tokenId: expect.stringMatching(/^xyz\#.+/),
            state: TokenState.READY,
            currentFlowElement: { id: 'StartEvent_1hsc96f' },
            localExecutionTime: 100,
            localStartTime: 100
          }),
          initFlowNodeAction({
            flowNodeId: 'StartEvent_1hsc96f',
            tokenId: expect.stringMatching(/^xyz\#.+/)
          })
        ]);
      });
    });

    describe('Running element inside of a subprocess', () => {
      it('should init element inside of subprocess and create token for subprocess itself', async () => {
        const epic = didPassTokenEpic(mockLogger);
        const actions = await testEpic(
          epic,
          {
            ...subprocessState,
            processId: 'myProcess',
            processInstanceId: 'p1',
            tokens: [
              {
                tokenId: 'xyz#123',
                state: TokenState.READY,
                localStartTime: 100,
                currentFlowElement: {
                  id: 'Task_02z411f',
                  startTime: 100
                },
                localExecutionTime: 100
              }
            ]
          },
          [
            didPassTokenAction({
              to: 'Task_02z411f',
              tokenId: 'xyz#123'
            })
          ]
        );

        expect(actions).toEqual([
          updateTokenAction({
            tokenId: 'xyz#123',
            state: TokenState.RUNNING,
            currentFlowElement: { id: 'Task_02z411f', startTime: 100 }
          }),
          initFlowNodeAction({
            flowNodeId: 'Task_02z411f',
            tokenId: 'xyz#123'
          }),
          createTokenAction({
            tokenId: 'xyz',
            state: TokenState.READY,
            currentFlowElement: { id: 'Subprocess_1hfk5eg' },
            localExecutionTime: 100,
            localStartTime: 100
          }),
          didPassTokenAction({
            to: 'Subprocess_1hfk5eg',
            tokenId: 'xyz'
          })
        ]);
      });
    });
  });
});

describe('initFlowNodeEpic', () => {
  it('should request activation of the flowNode', async () => {
    const epic = flowNodeInitEpic();
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        initFlowNodeAction({
          flowNodeId: 'StartEvent_1',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([willActivateFlowNodeAction({ flowNodeId: 'StartEvent_1', tokenId: 'xyz' })]);
  });
});

describe('willActivateFlowNodeEpic', () => {
  it('should ignore if token is not currently on this flowNode', async () => {
    const epic = willActivateFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        willActivateFlowNodeAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not activate the task if shouldActivateFlowNodeHook is interrupted by PAUSE-action', async () => {
    const epic = willActivateFlowNodeEpic(() => Promise.resolve(true));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_175vb4p',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        willActivateFlowNodeAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz'
        }),
        pauseTokenAction({
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not activate the task if shouldActivateFlowNodeHook is interrupted by INTERRUPT-action', async () => {
    const epic = willActivateFlowNodeEpic(() => Promise.resolve(true));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_175vb4p',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        willActivateFlowNodeAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz'
        }),
        interruptAction({
          tokenId: 'xyz',
          flowNodeId: 'Task_175vb4p'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not activate the task if shouldActivateFlowNodeHook is interrupted by END-FLOWNODE-action', async () => {
    const epic = willActivateFlowNodeEpic(() => Promise.resolve(true));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_175vb4p',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        willActivateFlowNodeAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz'
        }),
        endFlowNodeAction({
          tokenId: 'xyz',
          flowNodeId: 'Task_175vb4p',
          state: FlowNodeState.TERMINATED
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not activate the task if shouldActivateFlowNodeHook is interrupted by the token being ended', async () => {
    const epic = willActivateFlowNodeEpic(() => Promise.resolve(true));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_175vb4p',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        willActivateFlowNodeAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz'
        }),
        endTokenAction({
          tokenId: 'xyz',
          state: FlowNodeState.TERMINATED
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not activate a user task if no shouldActivateFlowNodeHook is given', async () => {
    const epic = willActivateFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...callActivityState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'CallActivity_1mcfm7t',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        willActivateFlowNodeAction({
          flowNodeId: 'CallActivity_1mcfm7t',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not activate a call activity if no shouldActivateFlowNodeHook is given', async () => {
    const epic = willActivateFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...userTaskState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'UserTask_1lmr4l7',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        willActivateFlowNodeAction({
          flowNodeId: 'UserTask_1lmr4l7',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not activate the task if shouldActivateFlowNodeHook returns false', async () => {
    const epic = willActivateFlowNodeEpic(() => Promise.resolve(false));
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.READY,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_175vb4p',
              startTime: 100,
              state: FlowNodeState.READY
            }
          }
        ]
      },
      [
        willActivateFlowNodeAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  describe('sample bpmn:Task', () => {
    it('should generate activateFlowNode action to start execution of the flowNode', async () => {
      const epic = willActivateFlowNodeEpic();
      const actions = await testEpic(
        epic,
        {
          ...simpleState,
          processId: 'myProcess',
          processInstanceId: 'p1',
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.READY,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Task_175vb4p',
                startTime: 100,
                state: FlowNodeState.READY
              }
            }
          ]
        },
        [
          willActivateFlowNodeAction({
            flowNodeId: 'Task_175vb4p',
            tokenId: 'xyz'
          })
        ]
      );

      expect(actions).toEqual([
        activateFlowNodeAction({
          tokenId: 'xyz',
          flowNodeId: 'Task_175vb4p'
        })
      ]);
    });
  });
});

describe('endFlowNodeEpic', () => {
  it('should emit logExecution-action for the flowNode with given attributes', async () => {
    const epic = endFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            },
            intermediateVariablesState: {}
          }
        ]
      },
      [
        endFlowNodeAction({
          flowNodeId: 'StartEvent_1',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      logExecutionAction({
        flowElementId: 'StartEvent_1',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        startTime: 100,
        endTime: 200
      })
    ]);
  });
  it('should end the token if the endToken attribute is set', async () => {
    const epic = endFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        endFlowNodeAction({
          flowNodeId: 'StartEvent_1',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            endToken: TokenState.TERMINATED
          },
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      logExecutionAction({
        flowElementId: 'StartEvent_1',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        startTime: 100,
        endTime: 200
      }),
      endTokenAction({
        tokenId: 'xyz',
        state: TokenState.TERMINATED,
        endTime: 200
      })
    ]);
  });
  it('should remove token if the removeToken attribute is set', async () => {
    const epic = endFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        endFlowNodeAction({
          flowNodeId: 'StartEvent_1',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            removeToken: true
          },
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      logExecutionAction({
        flowElementId: 'StartEvent_1',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        startTime: 100,
        endTime: 200
      }),
      removeTokenAction({
        tokenId: 'xyz'
      })
    ]);
  });

  it('should pass token if the passToken attribute is set', async () => {
    const epic = endFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'StartEvent_1',
              startTime: 100
            }
          }
        ]
      },
      [
        endFlowNodeAction({
          flowNodeId: 'StartEvent_1',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          tokenHandling: {
            passTokenTo: 'SequenceFlow_123'
          },
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      logExecutionAction({
        flowElementId: 'StartEvent_1',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        startTime: 100,
        endTime: 200
      }),
      willPassTokenAction({
        tokenId: 'xyz',
        sequenceId: 'SequenceFlow_123'
      })
    ]);
  });

  it('should commit any variable changes made during execution to the global variable state', async () => {
    const epic = endFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_175vb4p',
              startTime: 100
            },
            intermediateVariablesState: { a: 'test' }
          }
        ]
      },
      [
        endFlowNodeAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      setVariablesAction({ changedBy: 'Task_175vb4p', variables: { a: 'test' } }),
      logExecutionAction({
        flowElementId: 'Task_175vb4p',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        startTime: 100,
        endTime: 200
      })
    ]);
  });
  it('should NOT commit any variable changes made during execution to the global variable state if the flow node was not succesfully completed', async () => {
    const epic = endFlowNodeEpic();
    const actions = await testEpic(
      epic,
      {
        ...simpleState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_175vb4p',
              startTime: 100
            },
            intermediateVariablesState: { a: 'test' }
          }
        ]
      },
      [
        endFlowNodeAction({
          flowNodeId: 'Task_175vb4p',
          tokenId: 'xyz',
          state: ExecutionState.FAILED,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      logExecutionAction({
        flowElementId: 'Task_175vb4p',
        tokenId: 'xyz',
        executionState: ExecutionState.FAILED,
        startTime: 100,
        endTime: 200
      })
    ]);
  });
});

describe('endTokenEpic', () => {
  it('should not emit any action if token is ended already', async () => {
    const epic = endTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|123',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido',
              startTime: 100
            }
          }
        ]
      },
      [
        endTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.ENDED,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not emit any action if token is ended already with a custom end state', async () => {
    registerCustomTokenEndStates('CUSTOM-ENDED');

    const epic = endTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|123',
            state: 'CUSTOM-ENDED',
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido',
              startTime: 100
            }
          }
        ]
      },
      [
        endTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.ENDED,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should emit endFlowNode-action and update token accordingly', async () => {
    const epic = endTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido',
              startTime: 100
            }
          }
        ]
      },
      [
        endTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.ENDED,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      endFlowNodeAction({
        flowNodeId: 'Task_0j6jido',
        tokenId: 'xyz|123',
        state: ExecutionState.COMPLETED,
        endTime: 200
      }),
      updateTokenAction({
        tokenId: 'xyz|123',
        state: 'ENDED',
        intermediateVariablesState: null
      })
    ]);
  });
  it('should not return endFlowNodeAction for already ended flowNodes', async () => {
    const epic = endTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        log: [
          {
            tokenId: 'xyz|123',
            flowElementId: 'Task_0j6jido',
            ExecutionState: ExecutionState.TERMINATED,
            startTime: 100,
            endTime: expect.any(Number)
          }
        ],
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido',
              state: FlowNodeState.TERMINATED,
              startTime: 100
            }
          }
        ]
      },
      [
        endTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.TERMINATED,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz|123',
        state: TokenState.TERMINATED,
        intermediateVariablesState: null
      })
    ]);
  });

  it('should remove token after ending it if removeAfterEnding is set', async () => {
    const epic = endTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        log: [
          {
            tokenId: 'xyz|123',
            flowElementId: 'Task_0j6jido',
            ExecutionState: ExecutionState.TERMINATED,
            startTime: 100,
            endTime: expect.any(Number)
          }
        ],
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido',
              state: FlowNodeState.TERMINATED,
              startTime: 100
            }
          }
        ]
      },
      [
        endTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.TERMINATED,
          endTime: 200,
          removeAfterEnding: true
        }),
        logExecutionAction({
          tokenId: 'xyz|123',
          flowElementId: 'Task_0j6jido',
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([
      updateTokenAction({
        tokenId: 'xyz|123',
        state: TokenState.TERMINATED,
        intermediateVariablesState: null
      }),
      removeTokenAction({ tokenId: 'xyz|123' })
    ]);
  });
});

describe('updateTokenEpic', () => {
  it('should not end process when not all token in an end state', async () => {
    const epic = updateTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|123',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido',
              startTime: 100
            }
          }
        ]
      },
      [
        updateTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.ENDED,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should end process when all tokens in an end state', async () => {
    registerCustomTokenEndStates('CUSTOM-ENDED');

    const epic = updateTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido'
            }
          },
          {
            tokenId: 'xyz|789',
            state: 'CUSTOM-ENDED',
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_3'
            }
          },
          {
            tokenId: 'xyz|123',
            state: TokenState.ERROR_TECHNICAL,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        updateTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.ERROR_TECHNICAL,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([endProcessAction({ tokenId: 'xyz|123' })]);
  });

  it('should not reactivate the process when a changed token is still in an ended state', async () => {
    const epic = updateTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.ENDED,
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido'
            }
          },
          {
            tokenId: 'xyz|123',
            state: TokenState.ERROR_TECHNICAL,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            },
            additionalInfo: 'test'
          }
        ]
      },
      [
        updateTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.ERROR_TECHNICAL,
          endTime: 200,
          additionalInfo: 'test'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should not reactivate the process when a changed token is still in a custom end state', async () => {
    registerCustomTokenEndStates('CUSTOM-ENDED');

    const epic = updateTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.ENDED,
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido'
            }
          },
          {
            tokenId: 'xyz|123',
            state: 'CUSTOM-ENDED',
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            },
            additionalInfo: 'test'
          }
        ]
      },
      [
        updateTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.ERROR_TECHNICAL,
          endTime: 200,
          additionalInfo: 'test'
        })
      ]
    );

    expect(actions).toEqual([]);
  });

  it('should reactivate the process when a token is changed back into a running state', async () => {
    const epic = updateTokenEpic();
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.ENDED,
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_0j6jido'
            }
          },
          {
            tokenId: 'xyz|123',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        updateTokenAction({
          tokenId: 'xyz|123',
          state: TokenState.RUNNING,
          endTime: 200
        })
      ]
    );

    expect(actions).toEqual([reactivateProcessAction()]);
  });

  describe('Subprocess', () => {
    it('should complete subprocess when every token inside is ended', async () => {
      registerCustomTokenEndStates('CUSTOM-ENDED');

      const epic = updateTokenEpic();
      const actions = await testEpic(
        epic,
        {
          ...subprocessState,
          processId: 'myProcess',
          processInstanceId: 'p1',
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Subprocess_1hfk5eg',
                startTime: 100
              }
            },
            {
              tokenId: 'xyz#123|abc',
              state: TokenState.ENDED,
              localStartTime: 100,
              currentFlowElement: {
                id: 'EndEvent_1k3mqep',
                startTime: 100
              }
            },
            {
              tokenId: 'xyz#456|abc',
              state: 'CUSTOM-ENDED',
              localStartTime: 100,
              currentFlowElement: {
                id: 'EndEvent_2',
                startTime: 100
              }
            }
          ]
        },
        [
          updateTokenAction({
            tokenId: 'xyz#123|abc',
            state: TokenState.ENDED,
            endTime: 200
          })
        ]
      );

      expect(actions).toEqual([
        endActivityAction({
          flowNodeId: 'Subprocess_1hfk5eg',
          tokenId: 'xyz',
          state: ExecutionState.COMPLETED
        })
      ]);
    });

    it('should not complete subprocess when not every token inside is ended', async () => {
      const epic = updateTokenEpic();
      const actions = await testEpic(
        epic,
        {
          ...subprocessState,
          processId: 'myProcess',
          processInstanceId: 'p1',
          tokens: [
            {
              tokenId: 'xyz',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Subprocess_1hfk5eg',
                startTime: 100
              }
            },
            {
              tokenId: 'xyz#123|abc',
              state: TokenState.ENDED,
              localStartTime: 100,
              currentFlowElement: {
                id: 'EndEvent_1k3mqep',
                startTime: 100
              }
            },
            {
              tokenId: 'xyz#123|def',
              state: TokenState.RUNNING,
              localStartTime: 100,
              currentFlowElement: {
                id: 'Task_09z86me',
                startTime: 100
              }
            }
          ]
        },
        [
          updateTokenAction({
            tokenId: 'xyz#123|abc',
            state: TokenState.ENDED,
            endTime: 200
          })
        ]
      );
      expect(actions).toEqual([]);
    });
  });
});

describe('createTokenEpic', () => {
  it('should do nothing if the instance is still running', async () => {
    const epic = createTokenEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.RUNNING,
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        createTokenAction({
          tokenId: 'test123',
          state: TokenState.RUNNING,
          localStartTime: 0,
          localExecutionTime: 0,
          currentFlowElement: { id: 'Flow_123' }
        })
      ]
    );

    expect(mockLogger.info).not.toHaveBeenCalled();
    expect(actions).toEqual([]);
  });

  it('should do nothing if the instance is ended but the token is added in an ended state', async () => {
    const epic = createTokenEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        status: ProcessStatus.ENDED,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        createTokenAction({
          tokenId: 'test123',
          state: TokenState.ENDED,
          localStartTime: 0,
          localExecutionTime: 0,
          currentFlowElement: { id: 'Flow_123' }
        })
      ]
    );

    expect(mockLogger.info).not.toHaveBeenCalled();
    expect(actions).toEqual([]);
  });

  it('should do nothing if the instance is ended but the token is added in a custom ended state', async () => {
    registerCustomTokenEndStates('CUSTOM-ENDED');

    const epic = createTokenEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        status: ProcessStatus.ENDED,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        createTokenAction({
          tokenId: 'test123',
          state: 'CUSTOM-ENDED',
          localStartTime: 0,
          localExecutionTime: 0,
          currentFlowElement: { id: 'Flow_123' }
        })
      ]
    );

    expect(mockLogger.info).not.toHaveBeenCalled();
    expect(actions).toEqual([]);
  });

  it('should reactivate an ended instance if a running token is added', async () => {
    const epic = createTokenEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        status: ProcessStatus.ENDED,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.ENDED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          },
          // since the reducer of redux will not run during this test we have to add the token already
          {
            tokenId: 'test123',
            state: TokenState.RUNNING,
            localStartTime: 0,
            currentFlowElement: {
              id: 'Flow_123'
            }
          }
        ]
      },
      [
        createTokenAction({
          tokenId: 'test123',
          state: TokenState.RUNNING,
          localStartTime: 0,
          localExecutionTime: 0,
          currentFlowElement: { id: 'Flow_123' }
        })
      ]
    );

    expect(mockLogger.info).toHaveBeenCalled();
    expect(actions).toEqual([reactivateProcessAction()]);
  });
});

describe('moveTokenEpic', () => {
  it('should do nothing if token does not exist', async () => {
    const epic = moveTokenEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.RUNNING,
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        moveTokenAction({
          token: {
            tokenId: 'test123'
          },
          elementId: 'Task_0l2ayv1'
        })
      ]
    );

    expect(mockLogger.info).not.toHaveBeenCalled();
    expect(actions).toEqual([]);
  });

  it('should do nothing if new FlowElement does not exist', async () => {
    const epic = moveTokenEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.RUNNING,
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        moveTokenAction({
          token: {
            tokenId: 'xyz|456'
          },
          elementId: 'non-existing'
        })
      ]
    );

    expect(mockLogger.info).not.toHaveBeenCalled();
    expect(actions).toEqual([]);
  });

  it('should trigger willPassToken Action if new element is a sequenceFlow', async () => {
    const dateInMs = +new Date();

    const epic = moveTokenEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.RUNNING,
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        moveTokenAction({
          token: {
            tokenId: 'xyz|456'
          },
          elementId: 'SequenceFlow_10n4zv7'
        }),
        logExecutionAction({
          tokenId: 'xyz|456',
          flowElementId: 'Task_1p4tmqj',
          endTime: dateInMs
        })
      ]
    );

    expect(mockLogger.info).toHaveBeenCalled();
    expect(actions).toEqual([
      endFlowNodeAction({
        tokenId: 'xyz|456',
        flowNodeId: 'Task_1p4tmqj',
        state: 'SKIPPED',
        endTime: expect.any(Number)
      }),
      updateTokenAction({
        tokenId: 'xyz|456',
        currentFlowElement: { id: 'SequenceFlow_10n4zv7' },
        previousFlowElementId: 'Task_1p4tmqj',
        state: 'READY'
      }),
      willPassTokenAction({ tokenId: 'xyz|456', sequenceId: 'SequenceFlow_10n4zv7' })
    ]);
  });

  it('should trigger didPassToken Action if new element is a flowNode', async () => {
    const dateInMs = +new Date();

    const epic = moveTokenEpic(mockLogger);

    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        status: ProcessStatus.RUNNING,
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        moveTokenAction({
          token: {
            tokenId: 'xyz|456'
          },
          elementId: 'EndEvent_02qfml5'
        }),
        logExecutionAction({
          tokenId: 'xyz|456',
          flowElementId: 'Task_1p4tmqj',
          endTime: dateInMs
        })
      ]
    );

    expect(mockLogger.info).toHaveBeenCalled();
    expect(actions).toEqual([
      endFlowNodeAction({
        tokenId: 'xyz|456',
        flowNodeId: 'Task_1p4tmqj',
        state: 'SKIPPED',
        endTime: expect.any(Number)
      }),
      updateTokenAction({
        tokenId: 'xyz|456',
        currentFlowElement: { id: 'EndEvent_02qfml5' },
        previousFlowElementId: 'Task_1p4tmqj',
        state: 'READY'
      }),
      didPassTokenAction({ tokenId: 'xyz|456', to: 'EndEvent_02qfml5' })
    ]);
  });
});

describe('removeTokenEpic', () => {
  it('should not end the instance when there are tokens left', async () => {
    const epic = removeTokenEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.RUNNING,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          }
        ]
      },
      [
        removeTokenAction({
          tokenId: 'xyz|123'
        })
      ]
    );

    expect(mockLogger.info).not.toHaveBeenCalled();
    expect(actions).toEqual([]);
  });

  it('should stop the instance when there are no tokens left', async () => {
    const epic = removeTokenEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: []
      },
      [
        removeTokenAction({
          tokenId: 'abc'
        })
      ]
    );

    expect(mockLogger.info).toHaveBeenCalled();
    expect(actions).toEqual([stopProcessAction()]);
  });

  it('should stop the instance when there are no running tokens left', async () => {
    registerCustomTokenEndStates('CUSTOM-ENDED');

    const epic = removeTokenEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: [
          {
            tokenId: 'xyz|456',
            state: TokenState.FAILED,
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_1p4tmqj',
              startTime: 100
            }
          },
          {
            tokenId: 'xyz|789',
            state: 'CUSTOM-ENDED',
            localStartTime: 100,
            currentFlowElement: {
              id: 'Task_2',
              startTime: 100
            }
          }
        ]
      },
      [
        removeTokenAction({
          tokenId: 'xyz|123'
        })
      ]
    );

    expect(mockLogger.info).toHaveBeenCalled();
    expect(actions).toEqual([stopProcessAction()]);
  });

  it('should not stop the instance when it already ended', async () => {
    const epic = removeTokenEpic(mockLogger);
    const actions = await testEpic(
      epic,
      {
        ...parallelState,
        status: ProcessStatus.ENDED,
        processId: 'myProcess',
        processInstanceId: 'p1',
        tokens: []
      },
      [
        removeTokenAction({
          tokenId: 'abc'
        })
      ]
    );

    expect(mockLogger.info).not.toHaveBeenCalled();
    expect(actions).toEqual([]);
  });
});
