import { FlowNode } from 'bpmn-moddle';
import { filter, map, switchMap, take } from 'rxjs/operators';
import BpmnProcess from '../../src/BpmnProcess';
import BpmnProcessInstance from '../../src/BpmnProcessInstance';
import { InvalidTokenPlacementError } from '../../src/errors';
import { AdaptationType } from '../../src/state/reducers/adaptationLog';
import { ExecutionState } from '../../src/state/reducers/log';
import { FlowNodeState, TokenState } from '../../src/state/reducers/tokens';
import {
  BPMN_XML_INTERMEDIATE_CATCH,
  BPMN_XML_PARALLEL_GATEWAY_MESSAGE_CATCH,
  BPMN_XML_SINGLE_NONE_START_EVENT,
  BPMN_XML_EMBEDDED_SUBPROCESS_DOUBLE_SPLIT
} from '../fixtures/bpmnXml';

let simpleProcess: BpmnProcess;
let mergeProcess: BpmnProcess;

let mockShouldPassHook = jest.fn();
let mockShouldActivateFlowNodeHook = jest.fn();

beforeEach(async () => {
  simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_INTERMEDIATE_CATCH, {
    shouldPassTokenHook: mockShouldPassHook,
    shouldActivateFlowNodeHook: mockShouldActivateFlowNodeHook
  });

  mockShouldPassHook.mockReset();
  mockShouldActivateFlowNodeHook.mockReset();
  mockShouldPassHook.mockImplementation(async () => true);
  mockShouldActivateFlowNodeHook.mockImplementation(async () => true);

  mergeProcess = await BpmnProcess.fromXml('mergeProcess', BPMN_XML_PARALLEL_GATEWAY_MESSAGE_CATCH, {
    shouldActivateFlowNodeHook: mockShouldActivateFlowNodeHook
  });
});

afterEach(() => {
  simpleProcess.undeploy();
  mergeProcess.undeploy();
});

describe('placeTokenAt()', () => {
  describe('non-existant element', () => {
    it('should throw error', done => {
      mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to) => {
        if (to === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);
          expect(() => instance.placeTokenAt('does not exist', { tokenId: 'newToken123' })).toThrowError(
            InvalidTokenPlacementError
          );
          done();
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });
  });

  describe('illegal placement', () => {
    it('should throw when token is placed on parallel gateway', done => {
      mockShouldActivateFlowNodeHook.mockImplementation(async (processId, instanceId, tokenId, flowNode) => {
        if (flowNode.id === 'IntermediateCatchEvent_1ws4k2m') {
          const instance = mergeProcess.getInstanceById(instanceId);
          expect(() => instance.placeTokenAt('ParallelGateway_1ax8dnq')).toThrowError(InvalidTokenPlacementError);
          done();
        }

        return true;
      });

      mergeProcess.deployAndStart();
    });
  });

  describe('placing a new token', () => {
    it('should place new token with given id and continue execution', done => {
      mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to, tokenId) => {
        if (to === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const sequenceFlow = instance.getSequenceFlowId('Task_141i1em', 'EndEvent_03gcun2');
          expect(sequenceFlow).toEqual('SequenceFlow_08u409x');
          expect(() => instance.placeTokenAt(sequenceFlow, { tokenId: 'newToken123' })).not.toThrow();

          setTimeout(() => {
            const endState = instance.getState();

            expect(endState.tokens).toStrictEqual([
              {
                tokenId,
                state: TokenState.READY,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'IntermediateCatchEvent_0wpudxa',
                currentFlowNodeState: FlowNodeState.ACTIVE,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_06rz3px',
                intermediateVariablesState: {}
              },
              {
                tokenId: 'newToken123',
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_03gcun2',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_08u409x',
                intermediateVariablesState: null
              }
            ]);

            expect(endState.adaptationLog).toEqual([
              {
                type: AdaptationType.TOKEN_ADD,
                time: expect.any(Number),
                tokenId: 'newToken123',
                targetFlowElementId: sequenceFlow
              }
            ]);

            done();
          }, 50);
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });
    it('should implicitly generate a new token and continue execution', done => {
      mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to, tokenId) => {
        if (to === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const sequenceFlow = instance.getSequenceFlowId('Task_141i1em', 'EndEvent_03gcun2');
          expect(sequenceFlow).toEqual('SequenceFlow_08u409x');
          let newTokenId: string;
          expect(() => {
            newTokenId = instance.placeTokenAt(sequenceFlow);
          }).not.toThrow();

          setTimeout(() => {
            const endState = instance.getState();

            expect(endState.tokens).toStrictEqual([
              {
                tokenId,
                state: TokenState.READY,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'IntermediateCatchEvent_0wpudxa',
                currentFlowNodeState: FlowNodeState.ACTIVE,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_06rz3px',
                intermediateVariablesState: {}
              },
              {
                tokenId: newTokenId,
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_03gcun2',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_08u409x',
                intermediateVariablesState: null
              }
            ]);

            expect(endState.adaptationLog).toEqual([
              {
                type: AdaptationType.TOKEN_ADD,
                time: expect.any(Number),
                tokenId: newTokenId,
                targetFlowElementId: sequenceFlow
              }
            ]);

            done();
          }, 50);
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });
    it('should use the additional data when generating a new token and continue execution', done => {
      mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to, tokenId) => {
        if (to === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const sequenceFlow = instance.getSequenceFlowId('Task_141i1em', 'EndEvent_03gcun2');
          expect(sequenceFlow).toEqual('SequenceFlow_08u409x');
          let newTokenId: string;
          expect(() => {
            newTokenId = instance.placeTokenAt(sequenceFlow, { additionalInfo: 'info' });
          }).not.toThrow();

          setTimeout(() => {
            const endState = instance.getState();

            expect(endState.tokens).toStrictEqual([
              {
                tokenId,
                state: TokenState.READY,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'IntermediateCatchEvent_0wpudxa',
                currentFlowNodeState: FlowNodeState.ACTIVE,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_06rz3px',
                intermediateVariablesState: {}
              },
              {
                tokenId: newTokenId,
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_03gcun2',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_08u409x',
                intermediateVariablesState: null,
                additionalInfo: 'info'
              }
            ]);

            expect(endState.adaptationLog).toEqual([
              {
                type: AdaptationType.TOKEN_ADD,
                time: expect.any(Number),
                tokenId: newTokenId,
                targetFlowElementId: sequenceFlow
              }
            ]);

            done();
          }, 50);
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });
    it('should put the instance back into a running state when a token is added', async done => {
      let hasEndedOnce = false;
      let firstTokenId: string;
      let instance: BpmnProcessInstance;
      const process = await BpmnProcess.fromXml('testProcess', BPMN_XML_SINGLE_NONE_START_EVENT, {
        shouldActivateFlowNodeHook: async (processId, instanceId, tokenId, flowNode, state) => {
          if (!hasEndedOnce && flowNode.id === 'EndEvent_1umdfcw') {
            firstTokenId = tokenId;
            setTimeout(() => {
              instance = process.getInstanceById(instanceId);
              expect(instance.isEnded()).toBeTruthy();
              expect(instance.getState().tokens).toEqual([
                {
                  tokenId,
                  state: TokenState.ENDED,
                  currentFlowElementId: 'EndEvent_1umdfcw',
                  currentFlowNodeState: FlowNodeState.COMPLETED,
                  currentFlowElementStartTime: expect.any(Number),
                  previousFlowElementId: 'SequenceFlow_12bv6av',
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  intermediateVariablesState: null
                }
              ]);
              expect(instance.getState().instanceState).toEqual(['ENDED']);
              hasEndedOnce = true;
              instance.placeTokenAt('SequenceFlow_07so23a');
            }, 10);
          }

          if (hasEndedOnce && flowNode.id === 'Task_175vb4p') {
            expect(instance.isEnded()).toBeFalsy();
            expect(instance.getState().tokens).toEqual([
              {
                tokenId: firstTokenId,
                state: TokenState.ENDED,
                currentFlowElementId: 'EndEvent_1umdfcw',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_12bv6av',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId,
                state: TokenState.RUNNING,
                currentFlowElementId: 'Task_175vb4p',
                currentFlowNodeState: FlowNodeState.READY,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_07so23a',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              }
            ]);
            expect(instance.getState().instanceState).toEqual(['ENDED', 'RUNNING']);

            expect(instance.getState().adaptationLog).toEqual([
              {
                type: AdaptationType.TOKEN_ADD,
                time: expect.any(Number),
                tokenId,
                targetFlowElementId: 'SequenceFlow_07so23a'
              }
            ]);

            done();
          }

          return true;
        }
      });

      process.deployAndStart();
    });
    it('should not log the token adaptation if function is called with silent flag', async done => {
      mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to, tokenId) => {
        if (to === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const sequenceFlow = instance.getSequenceFlowId('Task_141i1em', 'EndEvent_03gcun2');
          expect(sequenceFlow).toEqual('SequenceFlow_08u409x');
          expect(() => instance.placeTokenAt(sequenceFlow, { tokenId: 'newToken123' }, true)).not.toThrow();

          setTimeout(() => {
            const endState = instance.getState();

            expect(endState.tokens).toStrictEqual([
              {
                tokenId,
                state: TokenState.READY,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'IntermediateCatchEvent_0wpudxa',
                currentFlowNodeState: FlowNodeState.ACTIVE,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_06rz3px',
                intermediateVariablesState: {}
              },
              {
                tokenId: 'newToken123',
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_03gcun2',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_08u409x',
                intermediateVariablesState: null
              }
            ]);

            expect(endState.adaptationLog).toEqual([]);

            done();
          }, 50);
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });
  });

  describe('parallel gateway merge', () => {
    it('should be able to merge with the parent token', done => {
      let parentTokenId = '';
      mergeProcess
        .getInstance$()
        .pipe(take(1))
        .subscribe(pi => {
          // Wait for token to reach the merge parallel gateway and message catch
          pi.getState$()
            .pipe(
              map(state => state.tokens),
              filter(
                tokens =>
                  tokens.filter(t => t.currentFlowElement.id === 'IntermediateCatchEvent_1ws4k2m').length > 0 &&
                  tokens.filter(t => t.currentFlowElement.id === 'ParallelGateway_1ax8dnq').length > 0
              ),
              take(1),
              switchMap(tokens => {
                const tokenAtMessageEvent = tokens.find(
                  t => t.currentFlowElement.id === 'IntermediateCatchEvent_1ws4k2m'
                )!;
                parentTokenId = tokenAtMessageEvent.tokenId
                  .split('|')
                  .slice(0, -1)
                  .join('|');
                const newTokenId = parentTokenId + '|newId';
                const sequenceFlow = pi.getSequenceFlowId('Task_00y2su1', 'ParallelGateway_1ax8dnq');
                expect(() => pi.placeTokenAt(sequenceFlow, { tokenId: newTokenId })).not.toThrow();
                return pi.getState$();
              }),
              map(state => state.tokens),
              filter(tokens => tokens.filter(t => t.currentFlowElement.id.startsWith('EndEvent')).length > 0),
              take(1)
            )
            .subscribe({
              next: tokens => {
                const endActivity = tokens.find(t => t.currentFlowElement.id.startsWith('EndEvent'))!
                  .currentFlowElement;
                expect(endActivity).toEqual({
                  id: expect.stringMatching(/^EndEvent.*/)
                });
              },
              complete: done
            });
        });

      mergeProcess.deployAndStart();
    });
  });

  describe('move existing token', () => {
    describe('illegal placement', () => {
      it('should throw when token is moved directly to parallel gateway', done => {
        mockShouldActivateFlowNodeHook.mockImplementation(async (processId, instanceId, tokenId, flowNode) => {
          if (flowNode.id === 'IntermediateCatchEvent_1ws4k2m') {
            const instance = mergeProcess.getInstanceById(instanceId);
            expect(() => instance.placeTokenAt('ParallelGateway_1ax8dnq', { tokenId })).toThrowError(
              InvalidTokenPlacementError
            );
            done();
          }

          return true;
        });

        mergeProcess.deployAndStart();
      });
    });

    it('should move token from one sequence flow to another', done => {
      mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to, tokenId) => {
        if (to === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const sequenceFlow = instance.getSequenceFlowId('Task_141i1em', 'EndEvent_03gcun2');
          expect(sequenceFlow).toEqual('SequenceFlow_08u409x');
          expect(() => instance.placeTokenAt(sequenceFlow, { tokenId })).not.toThrow();

          setTimeout(() => {
            const endState = instance.getState();

            expect(endState.tokens).toStrictEqual([
              {
                tokenId,
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_03gcun2',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_08u409x',
                intermediateVariablesState: null
              }
            ]);

            expect(endState.log).toStrictEqual([
              {
                tokenId,
                flowElementId: 'StartEvent_1',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                tokenId,
                flowElementId: 'EndEvent_03gcun2',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(instance.getState().adaptationLog).toEqual([
              {
                type: AdaptationType.TOKEN_MOVE,
                time: expect.any(Number),
                tokenId,
                currentFlowElementId: instance.getSequenceFlowId(from, to),
                targetFlowElementId: sequenceFlow
              }
            ]);

            done();
          }, 50);

          return false;
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });

    it('should move token from a flowNode to a sequence flow and abort the flowNode', done => {
      mockShouldActivateFlowNodeHook.mockImplementation(async (processId, instanceId, tokenId, flowNode) => {
        if (flowNode.id === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const onFlowNodeExecuted = jest.fn();
          instance.onFlowNodeExecuted(onFlowNodeExecuted);

          const sequenceFlow = instance.getSequenceFlowId('Task_141i1em', 'EndEvent_03gcun2');
          expect(sequenceFlow).toEqual('SequenceFlow_08u409x');
          expect(() => instance.placeTokenAt(sequenceFlow, { tokenId })).not.toThrow();

          setTimeout(() => {
            const endState = instance.getState();

            expect(endState.tokens).toStrictEqual([
              {
                tokenId,
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_03gcun2',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_08u409x',
                intermediateVariablesState: null
              }
            ]);

            expect(endState.log).toStrictEqual([
              {
                tokenId,
                flowElementId: 'StartEvent_1',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                tokenId,
                flowElementId: 'IntermediateCatchEvent_0wpudxa',
                executionState: ExecutionState.SKIPPED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                tokenId,
                flowElementId: 'EndEvent_03gcun2',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(onFlowNodeExecuted).toHaveBeenCalledWith(
              {
                tokenId,
                flowElementId: 'IntermediateCatchEvent_0wpudxa',
                executionState: ExecutionState.SKIPPED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                tokenId,
                currentFlowElement: {
                  id: 'IntermediateCatchEvent_0wpudxa',
                  startTime: expect.any(Number),
                  state: FlowNodeState.TERMINATED // Skipping leads to state TERMINATED for current flow Node
                },
                intermediateVariablesState: {},
                localExecutionTime: expect.any(Number),
                localStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_06rz3px',
                state: 'RUNNING'
              }
            );

            expect(instance.getState().adaptationLog).toEqual([
              {
                type: AdaptationType.TOKEN_MOVE,
                time: expect.any(Number),
                tokenId,
                currentFlowElementId: 'IntermediateCatchEvent_0wpudxa',
                targetFlowElementId: sequenceFlow
              }
            ]);

            done();
          }, 50);
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });

    it('should move token from one flowNode to another and abort the initial flowNode', done => {
      mockShouldActivateFlowNodeHook.mockImplementation(async (processId, instanceId, tokenId, flowNode) => {
        if (flowNode.id === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);

          const onFlowNodeExecuted = jest.fn();
          instance.onFlowNodeExecuted(onFlowNodeExecuted);

          expect(() => instance.placeTokenAt('EndEvent_03gcun2', { tokenId })).not.toThrow();

          setTimeout(() => {
            const endState = instance.getState();

            expect(endState.tokens).toStrictEqual([
              {
                tokenId,
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_03gcun2',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'IntermediateCatchEvent_0wpudxa',
                intermediateVariablesState: null
              }
            ]);

            expect(endState.log).toStrictEqual([
              {
                tokenId,
                flowElementId: 'StartEvent_1',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                tokenId,
                flowElementId: 'IntermediateCatchEvent_0wpudxa',
                executionState: ExecutionState.SKIPPED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                tokenId,
                flowElementId: 'EndEvent_03gcun2',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(onFlowNodeExecuted).toHaveBeenCalledWith(
              {
                tokenId,
                flowElementId: 'IntermediateCatchEvent_0wpudxa',
                executionState: ExecutionState.SKIPPED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                tokenId,
                currentFlowElement: {
                  id: 'IntermediateCatchEvent_0wpudxa',
                  startTime: expect.any(Number),
                  state: FlowNodeState.TERMINATED
                },
                intermediateVariablesState: {},
                localExecutionTime: expect.any(Number),
                localStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_06rz3px',
                state: 'RUNNING'
              }
            );

            expect(instance.getState().adaptationLog).toEqual([
              {
                type: AdaptationType.TOKEN_MOVE,
                time: expect.any(Number),
                tokenId,
                currentFlowElementId: 'IntermediateCatchEvent_0wpudxa',
                targetFlowElementId: 'EndEvent_03gcun2'
              }
            ]);

            done();
          }, 50);
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });

    it('should trigger onFlowNodeExecuted again when it is executed by the same token again', async done => {
      const finishedFlowNodes: string[] = [];
      let process: BpmnProcess;

      async function shouldActivate(processId: string, instanceId: string, tokenId: string, flowNode: FlowNode) {
        if (flowNode.$type === 'bpmn:StartEvent') {
          process
            .getInstanceById(instanceId)
            .onFlowNodeExecuted((execution: { flowElementId: string }) =>
              finishedFlowNodes.push(execution.flowElementId)
            );
        }

        return true;
      }

      let wasMoved = false;

      async function shouldPass(processId: string, instanceId: string, from: string, to: string, tokenId: string) {
        if (to === 'EndEvent_1umdfcw') {
          const instance = process.getInstanceById(instanceId);

          if (!wasMoved) {
            instance.placeTokenAt('SequenceFlow_07so23a', { tokenId });
          }

          wasMoved = true;

          setTimeout(() => {
            expect(finishedFlowNodes).toStrictEqual(['StartEvent_1', 'Task_175vb4p', 'Task_175vb4p']);
            done();
          }, 50);

          return false;
        }

        return true;
      }

      process = await BpmnProcess.fromXml('testProcess', BPMN_XML_SINGLE_NONE_START_EVENT, {
        shouldActivateFlowNodeHook: shouldActivate,
        shouldPassTokenHook: shouldPass
      });

      process.deployAndStart();
    });

    it('should abort all tokens inside a subprocess when the token is moved away', async done => {
      const process = await BpmnProcess.fromXml('p1', BPMN_XML_EMBEDDED_SUBPROCESS_DOUBLE_SPLIT);

      process.deploy();

      process.getInstance$().subscribe(instance => {
        let tokenId: string;
        setTimeout(() => {
          // wait for tasks in subprocess to become active and then move token on subprocess to end of process
          const token = instance.getState().tokens.find(t => !t.tokenId.includes('#'))!;
          tokenId = token.tokenId;
          instance.placeTokenAt('SequenceFlow_0k92bg2', { tokenId: tokenId });
        }, 50);

        instance.onEnded(() => {
          const state = instance.getState();

          expect(state.tokens).toStrictEqual([
            {
              tokenId: expect.any(String),
              localStartTime: expect.any(Number),
              state: 'ENDED',
              currentFlowElementId: 'EndEvent_0wsgk5o',
              currentFlowNodeState: 'COMPLETED',
              currentFlowElementStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              previousFlowElementId: 'SequenceFlow_0k92bg2',
              intermediateVariablesState: null
            },
            {
              tokenId: expect.any(String),
              localStartTime: expect.any(Number),
              state: 'SKIPPED',
              currentFlowElementId: 'Task_0c7ed5k',
              currentFlowNodeState: 'TERMINATED',
              currentFlowElementStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              previousFlowElementId: 'SequenceFlow_1fiv3is',
              intermediateVariablesState: null
            },
            {
              tokenId: expect.any(String),
              localStartTime: expect.any(Number),
              state: 'SKIPPED',
              currentFlowElementId: 'Task_0if8lmi',
              currentFlowNodeState: 'TERMINATED',
              currentFlowElementStartTime: expect.any(Number),
              localExecutionTime: expect.any(Number),
              previousFlowElementId: 'SequenceFlow_0l7asdj',
              intermediateVariablesState: null
            }
          ]);

          expect(state.log.length).toBe(7);
          expect(state.log).toStrictEqual([
            {
              flowElementId: 'StartEvent_1', // startevent of process
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'StartEvent_1og4rvt', // startevent of subprocess
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'ExclusiveGateway_1i43e7i', // parallel gateway of subprocess
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'SubProcess_0phiv1k', // subprocess
              tokenId: expect.any(String),
              executionState: ExecutionState.SKIPPED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'Task_0c7ed5k', // first user task in subprocess
              tokenId: expect.any(String),
              executionState: ExecutionState.SKIPPED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'Task_0if8lmi', // second user task in subprocess
              tokenId: expect.any(String),
              executionState: ExecutionState.SKIPPED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'EndEvent_0wsgk5o', // endevent after subprocess
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            }
          ]);

          expect(instance.getState().adaptationLog).toEqual([
            {
              type: AdaptationType.TOKEN_MOVE,
              time: expect.any(Number),
              tokenId,
              currentFlowElementId: 'SubProcess_0phiv1k',
              targetFlowElementId: 'SequenceFlow_0k92bg2'
            }
          ]);

          done();
        });
      });

      process.start();
    });

    it('should put the instance back into a running state when a token is moved', async done => {
      let hasEndedOnce = false;
      let firstTokenId: string;
      let instance: BpmnProcessInstance;
      const process = await BpmnProcess.fromXml('testProcess', BPMN_XML_SINGLE_NONE_START_EVENT, {
        shouldActivateFlowNodeHook: async (processId, instanceId, tokenId, flowNode, state) => {
          if (!hasEndedOnce && flowNode.id === 'EndEvent_1umdfcw') {
            firstTokenId = tokenId;
            setTimeout(() => {
              instance = process.getInstanceById(instanceId);
              expect(instance.isEnded()).toBeTruthy();
              expect(instance.getState().tokens).toEqual([
                {
                  tokenId,
                  state: TokenState.ENDED,
                  currentFlowElementId: 'EndEvent_1umdfcw',
                  currentFlowNodeState: FlowNodeState.COMPLETED,
                  currentFlowElementStartTime: expect.any(Number),
                  localStartTime: expect.any(Number),
                  localExecutionTime: expect.any(Number),
                  previousFlowElementId: 'SequenceFlow_12bv6av',
                  intermediateVariablesState: null
                }
              ]);
              expect(instance.getState().instanceState).toEqual(['ENDED']);
              hasEndedOnce = true;
              instance.placeTokenAt('SequenceFlow_07so23a', { tokenId });
            }, 10);
          }

          if (hasEndedOnce && flowNode.id === 'Task_175vb4p') {
            expect(instance.isEnded()).toBeFalsy();
            expect(instance.getState().tokens).toEqual([
              {
                tokenId,
                state: TokenState.RUNNING,
                currentFlowElementId: 'Task_175vb4p',
                currentFlowNodeState: FlowNodeState.READY,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_07so23a',
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              }
            ]);
            expect(instance.getState().instanceState).toEqual(['RUNNING']);

            expect(instance.getState().adaptationLog).toEqual([
              {
                type: AdaptationType.TOKEN_MOVE,
                time: expect.any(Number),
                tokenId,
                currentFlowElementId: 'EndEvent_1umdfcw',
                targetFlowElementId: 'SequenceFlow_07so23a'
              }
            ]);

            done();
          }

          return true;
        }
      });

      process.deployAndStart();
    });

    it('should not log the token adaptation if function is called with silent flag', async done => {
      mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to, tokenId) => {
        if (to === 'IntermediateCatchEvent_0wpudxa') {
          const instance = simpleProcess.getInstanceById(instanceId);
          const sequenceFlow = instance.getSequenceFlowId('Task_141i1em', 'EndEvent_03gcun2');
          expect(sequenceFlow).toEqual('SequenceFlow_08u409x');
          expect(() => instance.placeTokenAt(sequenceFlow, { tokenId }, true)).not.toThrow();

          setTimeout(() => {
            const endState = instance.getState();

            expect(endState.tokens).toStrictEqual([
              {
                tokenId,
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                localExecutionTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_03gcun2',
                currentFlowNodeState: FlowNodeState.COMPLETED,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_08u409x',
                intermediateVariablesState: null
              }
            ]);

            expect(endState.log).toStrictEqual([
              {
                tokenId,
                flowElementId: 'StartEvent_1',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                tokenId,
                flowElementId: 'EndEvent_03gcun2',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(instance.getState().adaptationLog).toEqual([]);

            done();
          }, 50);

          return false;
        }
        return true;
      });

      simpleProcess.deployAndStart();
    });
  });
});
