import BpmnProcess from '../../src/BpmnProcess';
import { AdaptationType } from '../../src/state/reducers/adaptationLog';
import { BPMN_XML_INTERMEDIATE_CATCH } from '../fixtures/bpmnXml';

let simpleProcess: BpmnProcess;

let mockShouldPassHook = jest.fn();
let mockShouldActivateFlowNodeHook = jest.fn();

beforeEach(async () => {
  simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_INTERMEDIATE_CATCH, {
    shouldPassTokenHook: mockShouldPassHook,
    shouldActivateFlowNodeHook: mockShouldActivateFlowNodeHook
  });

  mockShouldPassHook.mockReset();
  mockShouldActivateFlowNodeHook.mockReset();
  mockShouldPassHook.mockImplementation(async () => true);
  mockShouldActivateFlowNodeHook.mockImplementation(async () => true);
});

afterEach(() => {
  simpleProcess.undeploy();
});

describe('updateVariables()', () => {
  it('should change the variable and log the change as an adaptation', done => {
    mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to) => {
      if (to === 'IntermediateCatchEvent_0wpudxa') {
        const instance = simpleProcess.getInstanceById(instanceId);
        expect(() => instance.updateVariables({ test: 123 })).not.toThrowError();

        setTimeout(() => {
          expect(instance.getState().variables).toEqual({
            test: {
              value: 123,
              log: [
                {
                  changedBy: 'manual',
                  changedTime: expect.any(Number)
                }
              ]
            }
          });

          expect(instance.getState().adaptationLog).toEqual([
            {
              type: AdaptationType.VARIABLE_ADAPTATION,
              time: expect.any(Number),
              changes: [
                {
                  variableName: 'test',
                  oldValue: undefined,
                  newValue: 123
                }
              ]
            }
          ]);

          done();
        }, 10);
      }
      return true;
    });

    simpleProcess.deployAndStart();
  });

  it('should not log the change as an adaptation if the silent flag is used', done => {
    mockShouldPassHook.mockImplementation(async (processId, instanceId, from, to) => {
      if (to === 'IntermediateCatchEvent_0wpudxa') {
        const instance = simpleProcess.getInstanceById(instanceId);
        expect(() => instance.updateVariables({ test: 123 }, true)).not.toThrowError();

        setTimeout(() => {
          expect(instance.getState().variables).toEqual({
            test: {
              value: 123,
              log: [
                {
                  changedBy: 'manual',
                  changedTime: expect.any(Number)
                }
              ]
            }
          });

          expect(instance.getState().adaptationLog).toEqual([]);

          done();
        }, 10);
      }
      return true;
    });

    simpleProcess.deployAndStart();
  });
});
