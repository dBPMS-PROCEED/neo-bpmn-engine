import { setVariablesAction } from '../../../src/state/actions';
import { DEFAULT_VARIABLES_STATE, variablesReducer } from '../../../src/state/reducers/variables';

it('should set variables when none exist', () => {
  const newVariables = variablesReducer(
    DEFAULT_VARIABLES_STATE,
    setVariablesAction({
      changedBy: 'flowNode-123',
      variables: {
        a: 5,
        b: 10
      }
    })
  );
  expect(newVariables).toEqual({
    a: { value: 5, log: [{ changedTime: expect.any(Number), changedBy: 'flowNode-123' }] },
    b: { value: 10, log: [{ changedTime: expect.any(Number), changedBy: 'flowNode-123' }] }
  });
});

it('should merge with the existing variables', () => {
  const newVariables = variablesReducer(
    { ...DEFAULT_VARIABLES_STATE, a: { value: 0, log: [{ changedTime: 100 }] } },
    setVariablesAction({
      changedBy: 'flowNode-123',
      variables: {
        b: 10
      }
    })
  );
  expect(newVariables).toEqual({
    a: { value: 0, log: [{ changedTime: 100 }] },
    b: { value: 10, log: [{ changedTime: expect.any(Number), changedBy: 'flowNode-123' }] }
  });
});

it('should update the last write on write of new value', () => {
  const newVariables = variablesReducer(
    { ...DEFAULT_VARIABLES_STATE, a: { value: 0, log: [{ changedTime: 100 }] } },
    setVariablesAction({
      changedBy: 'flowNode-123',
      variables: {
        a: 5
      }
    })
  );
  expect(newVariables).toEqual({
    a: {
      value: 5,
      log: [{ changedTime: 100 }, { changedTime: expect.any(Number), changedBy: 'flowNode-123', oldValue: 0 }]
    }
  });
  expect((newVariables.a as any).log[1].changedTime).toBeGreaterThan(100);
});
