import {
  pauseProcessAction,
  resumeProcessAction,
  endProcessAction,
  updateProcessStatusAction
} from '../../../src/state/actions';
import { DEFAULT_STATUS_STATE, statusReducer, ProcessStatus } from '../../../src/state/reducers/status';

describe('pause', () => {
  it('should set to pause when not paused', () => {
    const newStatus = statusReducer(DEFAULT_STATUS_STATE, pauseProcessAction());
    expect(newStatus).toEqual(ProcessStatus.PAUSED);
  });

  it('should set to pause when already paused', () => {
    const newStatus = statusReducer(ProcessStatus.PAUSED, pauseProcessAction());
    expect(newStatus).toEqual(ProcessStatus.PAUSED);
  });

  it('should not set paused when already ended', () => {
    const newStatus = statusReducer(ProcessStatus.ENDED, pauseProcessAction());
    expect(newStatus).toEqual(ProcessStatus.ENDED);
  });
});

describe('resume', () => {
  it('should set to not paused when already not paused', () => {
    const newStatus = statusReducer(DEFAULT_STATUS_STATE, resumeProcessAction());
    expect(newStatus).toEqual(ProcessStatus.RUNNING);
  });

  it('should set to not paused when paused', () => {
    const newStatus = statusReducer(ProcessStatus.PAUSED, resumeProcessAction());
    expect(newStatus).toEqual(ProcessStatus.RUNNING);
  });
});

describe('end', () => {
  it('should set to ended', () => {
    const newStatus = statusReducer(DEFAULT_STATUS_STATE, endProcessAction({ tokenId: 'some-element' }));
    expect(newStatus).toEqual(ProcessStatus.ENDED);
  });

  it('should have no effect on ended process', () => {
    const newStatus = statusReducer(ProcessStatus.ENDED, endProcessAction({ tokenId: 'some-element' }));
    expect(newStatus).toEqual(ProcessStatus.ENDED);
  });
});

describe('custom', () => {
  it('should set to custom status', () => {
    const newStatus = statusReducer(DEFAULT_STATUS_STATE, updateProcessStatusAction({ status: 'custom-state' }));
    expect(newStatus).toEqual('custom-state');
  });
});
