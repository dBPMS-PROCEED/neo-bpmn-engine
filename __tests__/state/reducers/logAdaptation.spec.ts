import { logAdaptationAction, updateAdaptationLogAction } from '../../../src/state/actions';
import { adaptationLogReducer, AdaptationType, MigrationEntry } from '../../../src/state/reducers/adaptationLog';

describe('log adaptation', () => {
  it('should log execution with given state', () => {
    const newEntry: MigrationEntry = {
      type: AdaptationType.MIGRATION,
      time: 123,
      from: 'origin',
      to: 'target'
    };
    const log = adaptationLogReducer([], logAdaptationAction(newEntry));

    expect(log).toEqual([newEntry]);
  });

  it('should allow an existing log entry to be edited', () => {
    const existingEntry = {
      type: AdaptationType.MIGRATION,
      time: 123,
      from: 'origin',
      to: 'target'
    };

    const log = adaptationLogReducer(
      [existingEntry],
      updateAdaptationLogAction({
        time: 123,
        type: AdaptationType.MIGRATION,
        additionalInformation: 'test'
      })
    );

    expect(log).toEqual([
      {
        ...existingEntry,
        additionalInformation: 'test'
      }
    ]);
  });
});
