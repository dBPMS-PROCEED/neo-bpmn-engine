import { activateFlowNodeAction } from '../../../src/state/actions';
import { FlowNodeState, tokenReducer, TokenState } from '../../../src/state/reducers/tokens';

describe('set a flowNode to active', () => {
  it('should ignore if token does not exist', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'abc',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_456',
            startTime: 100,
            state: FlowNodeState.READY
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      activateFlowNodeAction({
        flowNodeId: 'Task_123',
        tokenId: 'xyz'
      })
    );
    expect(tokens).toEqual([
      {
        tokenId: 'abc',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_456',
          startTime: 100,
          state: FlowNodeState.READY
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: {}
      }
    ]);
  });

  it('will set flow node status to active', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_123',
            startTime: 100,
            state: FlowNodeState.READY
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      activateFlowNodeAction({
        flowNodeId: 'Task_123',
        tokenId: 'xyz'
      })
    );
    expect(tokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_123',
          startTime: 100,
          state: FlowNodeState.ACTIVE
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: {}
      }
    ]);
  });
});
