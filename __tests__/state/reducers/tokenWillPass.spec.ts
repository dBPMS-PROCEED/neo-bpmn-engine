import { willPassTokenAction } from '../../../src/state/actions';
import { tokenReducer, DEFAULT_TOKENS_STATE, TokenState } from '../../../src/state/reducers/tokens';

describe('pass token', () => {
  it('ignore if token does not exist', () => {
    const newTokens = tokenReducer(
      DEFAULT_TOKENS_STATE,
      willPassTokenAction({
        sequenceId: 'flow-123',
        tokenId: 'xyz|123'
      })
    );

    expect(newTokens).toEqual([]);
  });

  it('should ignore if token is not READY or RUNNING', () => {
    const newTokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.ENDED,
          localStartTime: 100,
          currentFlowElement: {
            id: 'Task_123'
          },
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      willPassTokenAction({
        tokenId: 'xyz',
        sequenceId: 'flow-123'
      })
    );
    expect(newTokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.ENDED,
        localStartTime: 100,
        currentFlowElement: {
          id: 'Task_123'
        },
        localExecutionTime: 0,
        intermediateVariablesState: {}
      }
    ]);
  });

  it('should change state of token to ready and update some token attributes to represent the token being passed to a sequence flow', () => {
    const newTokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.RUNNING,
          localStartTime: 100,
          currentFlowElement: {
            id: 'Task_123'
          },
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      willPassTokenAction({
        tokenId: 'xyz',
        sequenceId: 'flow-123'
      })
    );
    expect(newTokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.READY,
        localStartTime: 100,
        currentFlowElement: {
          id: 'flow-123',
          startTime: expect.any(Number)
        },
        previousFlowElementId: 'Task_123',
        localExecutionTime: 0,
        intermediateVariablesState: null
      }
    ]);
  });
});
