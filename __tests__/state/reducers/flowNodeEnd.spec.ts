import { endFlowNodeAction } from '../../../src/state/actions';
import { ExecutionState } from '../../../src/state/reducers/log';
import { FlowNodeState, tokenReducer, TokenState } from '../../../src/state/reducers/tokens';

describe('end a flowNode instance', () => {
  it('should ignore if token does not exist', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'abc',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_456',
            startTime: 100
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      endFlowNodeAction({
        flowNodeId: 'Task_123',
        tokenId: 'xyz',
        state: ExecutionState.COMPLETED,
        endTime: 200
      })
    );
    expect(tokens).toEqual([
      {
        tokenId: 'abc',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_456',
          startTime: 100
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: {}
      }
    ]);
  });

  it('should close flowNode and increase execution time if state is COMPLETED', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_123',
            startTime: 100
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      endFlowNodeAction({
        flowNodeId: 'Task_123',
        tokenId: 'xyz',
        state: ExecutionState.COMPLETED,
        endTime: 200
      })
    );
    expect(tokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_123',
          startTime: 100,
          state: FlowNodeState.COMPLETED
        },
        localStartTime: 100,
        localExecutionTime: 100,
        intermediateVariablesState: {}
      }
    ]);
  });

  it('should close flowNode if state is not COMPLETED', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_123',
            startTime: 100
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      endFlowNodeAction({
        flowNodeId: 'Task_123',
        tokenId: 'xyz',
        state: ExecutionState.ABORTED,
        endTime: 200
      })
    );
    expect(tokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_123',
          startTime: 100,
          state: FlowNodeState.TERMINATED
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: {}
      }
    ]);
  });
});
