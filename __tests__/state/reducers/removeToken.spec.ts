import { removeTokenAction } from '../../../src/state/actions';
import { tokenReducer, TokenState } from '../../../src/state/reducers/tokens';

describe('remove token', () => {
  it('should remove token of the given id', () => {
    const newTokens = tokenReducer(
      [
        {
          tokenId: 'xyz|123',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_123'
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: {}
        },
        {
          tokenId: 'xyz|456',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_456'
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: {}
        },
        {
          tokenId: 'xyz|789',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_789'
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      removeTokenAction({
        tokenId: 'xyz|123'
      })
    );

    expect(newTokens).toEqual([
      {
        tokenId: 'xyz|456',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_456'
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: {}
      },
      {
        tokenId: 'xyz|789',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_789'
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: {}
      }
    ]);
  });
});
