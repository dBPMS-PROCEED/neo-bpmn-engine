import { updateVariablesAction } from '../../../src/state/actions';
import { DEFAULT_VARIABLES_STATE, variablesReducer } from '../../../src/state/reducers/variables';

it('should update variables when none exist', () => {
  const newVariables = variablesReducer(
    DEFAULT_VARIABLES_STATE,
    updateVariablesAction({
      variables: {
        a: {
          value: 5,
          log: [{ changedTime: 100, changedBy: 'flowNode-123', oldValue: 2 }]
        }
      }
    })
  );
  expect(newVariables).toEqual({
    a: { value: 5, log: [{ changedTime: 100, changedBy: 'flowNode-123', oldValue: 2 }] }
  });
});

it('should update value if updated value is more recent and add log entry to existing variable', () => {
  const newVariables = variablesReducer(
    {
      ...DEFAULT_VARIABLES_STATE,
      a: { value: 2, log: [{ changedTime: 100, changedBy: 'flowNode-123', oldValue: 0 }] }
    },
    updateVariablesAction({
      variables: {
        a: {
          value: 5,
          log: [{ changedTime: 200, changedBy: 'flowNode-456', oldValue: 2 }]
        }
      }
    })
  );
  expect(newVariables).toEqual({
    a: {
      value: 5,
      log: [
        { changedTime: 100, changedBy: 'flowNode-123', oldValue: 0 },
        { changedTime: 200, changedBy: 'flowNode-456', oldValue: 2 }
      ]
    }
  });
});

it('should keep value if old value is more recent and add log entry to existing variable', () => {
  const newVariables = variablesReducer(
    {
      ...DEFAULT_VARIABLES_STATE,
      a: { value: 2, log: [{ changedTime: 100, changedBy: 'flowNode-123', oldValue: 0 }] }
    },
    updateVariablesAction({
      variables: {
        a: {
          value: 5,
          log: [{ changedTime: 50, changedBy: 'flowNode-456', oldValue: 2 }]
        }
      }
    })
  );
  expect(newVariables).toEqual({
    a: {
      value: 2,
      log: [
        { changedTime: 100, changedBy: 'flowNode-123', oldValue: 0 },
        { changedTime: 50, changedBy: 'flowNode-456', oldValue: 2 }
      ]
    }
  });
});

it('should ignore duplicate log entry for existing variable', () => {
  const newVariables = variablesReducer(
    {
      ...DEFAULT_VARIABLES_STATE,
      a: { value: 2, log: [{ changedTime: 100, changedBy: 'flowNode-123', oldValue: 0 }] }
    },
    updateVariablesAction({
      variables: {
        a: {
          value: 2,
          log: [{ changedTime: 100, changedBy: 'flowNode-123', oldValue: 0 }]
        }
      }
    })
  );
  expect(newVariables).toEqual({
    a: { value: 2, log: [{ changedTime: 100, changedBy: 'flowNode-123', oldValue: 0 }] }
  });
});
