import { logExecutionAction } from '../../../src/state/actions';
import { logReducer, ExecutionState } from '../../../src/state/reducers/log';

describe('log execution', () => {
  it('should log execution with given state', () => {
    const log = logReducer(
      [],
      logExecutionAction({
        flowElementId: 'Task_123',
        tokenId: 'xyz',
        executionState: ExecutionState.ERROR_SEMANTIC
      })
    );

    expect(log).toEqual([
      {
        flowElementId: 'Task_123',
        tokenId: 'xyz',
        executionState: ExecutionState.ERROR_SEMANTIC
      }
    ]);
  });

  it('should log execution with given state and time', () => {
    const log = logReducer(
      [],
      logExecutionAction({
        flowElementId: 'Task_123',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        startTime: 100,
        endTime: 200
      })
    );

    expect(log).toEqual([
      {
        flowElementId: 'Task_123',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        startTime: 100,
        endTime: 200
      }
    ]);
  });

  it('should log execution with given state and optional attributes', () => {
    const log = logReducer(
      [],
      logExecutionAction({
        flowElementId: 'Task_123',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        abc: 123,
        pqr: 456
      })
    );

    expect(log).toEqual([
      {
        flowElementId: 'Task_123',
        tokenId: 'xyz',
        executionState: ExecutionState.COMPLETED,
        abc: 123,
        pqr: 456
      }
    ]);
  });
});
