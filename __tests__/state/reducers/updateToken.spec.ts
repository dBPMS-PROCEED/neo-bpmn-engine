import { updateTokenAction } from '../../../src/state/actions';
import { tokenReducer, DEFAULT_TOKENS_STATE, TokenState } from '../../../src/state/reducers/tokens';

describe('set to RUNNING OR READY', () => {
  it('should ignore if token does not exist', () => {
    const newTokens = tokenReducer(
      DEFAULT_TOKENS_STATE,
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.RUNNING
      })
    );

    expect(newTokens).toEqual([]);
  });

  it('should set state of token to given state', () => {
    const newTokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.RUNNING,
          localStartTime: 100,
          currentFlowElement: {
            id: 'Task_123'
          },
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY
      })
    );

    expect(newTokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.READY,
        localStartTime: 100,
        currentFlowElement: {
          id: 'Task_123'
        },
        localExecutionTime: 0,
        intermediateVariablesState: {}
      }
    ]);
  });

  it('should set state of token to given state and add optional attributes', () => {
    const newTokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.RUNNING,
          localStartTime: 100,
          currentFlowElement: {
            id: 'Task_123'
          },
          localExecutionTime: 0,
          intermediateVariablesState: {}
        }
      ],
      updateTokenAction({
        tokenId: 'xyz',
        state: TokenState.READY,
        abc: 123,
        pqr: 456
      })
    );

    expect(newTokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.READY,
        localStartTime: 100,
        currentFlowElement: {
          id: 'Task_123'
        },
        intermediateVariablesState: {},
        localExecutionTime: 0,
        abc: 123,
        pqr: 456
      }
    ]);
  });
});
