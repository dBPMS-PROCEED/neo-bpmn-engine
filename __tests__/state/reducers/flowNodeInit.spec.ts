import { initFlowNodeAction } from '../../../src/state/actions';
import { FlowNodeState, tokenReducer, TokenState } from '../../../src/state/reducers/tokens';

describe('init a flowNode instance', () => {
  it('should ignore if token does not exist', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'abc',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_456'
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: null
        }
      ],
      initFlowNodeAction({
        flowNodeId: 'Task_123',
        tokenId: 'xyz'
      })
    );
    expect(tokens).toEqual([
      {
        tokenId: 'abc',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_456'
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: null
      }
    ]);
  });

  it('should set startTime and READY State for the flownode and initialize the intermediate variables state of the token', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'Task_123'
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: null
        }
      ],
      initFlowNodeAction({
        flowNodeId: 'Task_123',
        tokenId: 'xyz'
      })
    );

    expect(tokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'Task_123',
          startTime: expect.any(Number),
          state: FlowNodeState.READY
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: {}
      }
    ]);
  });
});
