import { initBoundaryEventAction } from '../../../src/state/actions';
import { FlowNodeState, tokenReducer, TokenState } from '../../../src/state/reducers/tokens';

describe('creating an boundary event instance', () => {
  it('should set startTime and ready state to the boundary event', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'xyz',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'BoundaryEvent_123'
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: null
        }
      ],
      initBoundaryEventAction({
        flowNodeId: 'BoundaryEvent_123',
        tokenId: 'xyz'
      })
    );

    expect(tokens).toEqual([
      {
        tokenId: 'xyz',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'BoundaryEvent_123',
          startTime: expect.any(Number),
          state: FlowNodeState.READY
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: null
      }
    ]);
  });

  it('should ignore if token does not exist', () => {
    const tokens = tokenReducer(
      [
        {
          tokenId: 'abc',
          state: TokenState.RUNNING,
          currentFlowElement: {
            id: 'BoundaryEvent_456'
          },
          localStartTime: 100,
          localExecutionTime: 0,
          intermediateVariablesState: null
        }
      ],
      initBoundaryEventAction({
        flowNodeId: 'BoundaryEvent_123',
        tokenId: 'xyz'
      })
    );
    expect(tokens).toEqual([
      {
        tokenId: 'abc',
        state: TokenState.RUNNING,
        currentFlowElement: {
          id: 'BoundaryEvent_456'
        },
        localStartTime: 100,
        localExecutionTime: 0,
        intermediateVariablesState: null
      }
    ]);
  });
});
