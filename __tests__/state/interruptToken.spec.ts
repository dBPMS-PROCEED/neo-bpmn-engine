import { take } from 'rxjs/operators';
import BpmnProcess from '../../src/BpmnProcess';
import { BPMN_XML_INTERMEDIATE_CATCH } from '../fixtures/bpmnXml';
import { FlowNode } from 'bpmn-moddle';
import { createMockLogger } from '../helpers/mockLogger';
import { ProcessState } from '../../src/state/reducers';
import BpmnProcessInstance from '../../src/BpmnProcessInstance';

let simpleProcess: BpmnProcess;
const mockLogger = createMockLogger();

let mockOnActivation: Function;
let mockOnTokenPass: Function;

beforeEach(() => {
  mockLogger.reset();
});

beforeEach(async () => {
  simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_INTERMEDIATE_CATCH, {
    async shouldPassTokenHook(processId, processInsanceId, from, to, tokenId, state) {
      mockOnTokenPass(tokenId, state);
      return true;
    },
    async shouldActivateFlowNodeHook(processId, processInstanceId, tokenId, flowNode, state) {
      mockOnActivation(tokenId, flowNode);
      return true;
    }
  });
});

afterEach(() => {
  simpleProcess.undeploy();
});

describe('interruptToken()', () => {
  it('should not pass token to the next sequenceFlow when a message is received after flowNode was interrupted', done => {
    let instance: BpmnProcessInstance;
    const subscribeStub = jest.fn();

    mockOnTokenPass = (tokenId: string, state: ProcessState) => {
      const token = state.tokens.find(token => token.tokenId === tokenId);
      if (token?.currentFlowElement.id === 'SequenceFlow_0aj6p9w') {
        subscribeStub();
      }
    };

    mockOnActivation = (tokenId: string, flowNode: FlowNode) => {
      if (flowNode.id === 'IntermediateCatchEvent_0wpudxa') {
        setTimeout(() => {
          instance.interruptToken(tokenId);
          instance.receiveMessage('Message_1m7hrlz');

          setTimeout(() => {
            expect(subscribeStub).not.toHaveBeenCalled();
            done();
          }, 50);
        }, 0);
      }
    };

    simpleProcess
      .getInstance$()
      .pipe(take(1))
      .subscribe(pi => (instance = pi));

    simpleProcess.deployAndStart();
  });

  it('should pass token to the next sequenceFlow when a message is received before process is interrupted', done => {
    let instance: BpmnProcessInstance;
    const subscribeStub = jest.fn();

    mockOnTokenPass = (tokenId: string, state: ProcessState) => {
      const token = state.tokens.find(token => token.tokenId === tokenId);
      if (token?.currentFlowElement.id === 'SequenceFlow_0aj6p9w') {
        subscribeStub();
      }
    };

    mockOnActivation = (tokenId: string, flowNode: FlowNode) => {
      if (flowNode.id === 'IntermediateCatchEvent_0wpudxa') {
        setTimeout(() => {
          instance.receiveMessage('Message_1m7hrlz');
          instance.interruptToken(tokenId);

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalled();
            done();
          }, 50);
        }, 0);
      }
    };

    simpleProcess
      .getInstance$()
      .pipe(take(1))
      .subscribe(pi => (instance = pi));

    simpleProcess.deployAndStart();
  });
});
