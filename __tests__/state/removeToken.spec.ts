import BpmnProcess from '../../src/BpmnProcess';
import BpmnProcessInstance from '../../src/BpmnProcessInstance';
import { AdaptationType } from '../../src/state/reducers/adaptationLog';
import { ExecutionState } from '../../src/state/reducers/log';

import { BPMN_XML_USER_TASK, BPMN_XML_PARALLEL_USER_TASKS, BPMN_XML_SCRIPT_TASK_SET_VAR } from '../fixtures/bpmnXml';

describe('removeToken()', () => {
  it('does not throw on non-existant token and will not change the instance', async done => {
    // create the process to run the test on
    const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_USER_TASK, {
      // create the logic that will activate the user task inside the process and then try to remove a token
      shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
        if (flowNode.id === 'UserTask_1lmr4l7') {
          const instance = simpleProcess.getInstanceById(instanceId);

          setTimeout(() => {
            // the removeToken function should ignore non-existant tokens
            expect(() => instance.removeToken('non-existant token')).not.toThrow();
            setTimeout(() => {
              // the instance state should not have changed
              let newInstanceState = simpleProcess.getInstanceById(instanceId);
              expect(newInstanceState).toStrictEqual(instance);

              expect(instance.getState().adaptationLog).toEqual([]);

              done();
            }, 10);
          }, 10);
        }

        return true;
      }
    });
    // start an instance
    simpleProcess.deployAndStart();
  });

  describe('one existing token', () => {
    let instance: BpmnProcessInstance;
    let tokenId: string;
    let mockInterruptFunction = jest.fn();

    beforeAll(async done => {
      // create the process to run the test on
      const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_USER_TASK, {
        // create the logic that will activate the user task inside the process and then remove the token
        shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
          if (!tokenId) {
            tokenId = tId;
          }
          if (!instance) {
            instance = simpleProcess.getInstanceById(instanceId);
            instance.onActivityInterrupted('bpmn:UserTask', mockInterruptFunction);
          }

          if (flowNode.id === 'UserTask_1lmr4l7') {
            setTimeout(() => {
              instance.removeToken(instance.getState().tokens[0].tokenId);
              setTimeout(done, 10);
            }, 10);
          }

          return true;
        }
      });
      // start an instance
      simpleProcess.deployAndStart();
    });

    it('should have removed the token from the instance', () => {
      expect(instance.getState().tokens.length).toBe(0);
    });

    it('should have terminated the instance since there is no token left', () => {
      expect(instance.getState().instanceState).toStrictEqual(['STOPPED']);
      expect(instance.isEnded()).toBeTruthy();
    });

    it('should have ended the user task before removing the token', () => {
      expect(mockInterruptFunction).toHaveBeenCalledWith(
        {
          flowElementId: 'UserTask_1lmr4l7',
          tokenId,
          executionState: ExecutionState.SKIPPED,
          startTime: expect.any(Number),
          endTime: expect.any(Number)
        },
        {
          currentFlowElement: {
            id: 'UserTask_1lmr4l7',
            startTime: expect.any(Number),
            state: 'TERMINATED'
          },
          intermediateVariablesState: {},
          localExecutionTime: expect.any(Number),
          localStartTime: expect.any(Number),
          previousFlowElementId: 'SequenceFlow_0gm2ucm',
          state: 'RUNNING',
          tokenId: expect.any(String)
        }
      );
    });

    it('should have logged the manual token removal in the adaptation log', () => {
      expect(instance.getState().adaptationLog).toEqual([
        {
          type: AdaptationType.TOKEN_REMOVE,
          time: expect.any(Number),
          tokenId,
          currentFlowElementId: 'UserTask_1lmr4l7'
        }
      ]);
    });
  });

  describe('multiple existing tokens', () => {
    let instance: BpmnProcessInstance;
    let tokenId: string;
    let mockInterruptFunction = jest.fn();

    beforeAll(async done => {
      // create the process to run the test on
      const parallelProcess = await BpmnProcess.fromXml('parallelProcess', BPMN_XML_PARALLEL_USER_TASKS, {
        // create the logic that will activate the user task inside the process and then remove the token
        shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
          if (flowNode.id === 'Task_05qgw49') {
            if (!tokenId) {
              tokenId = tId;
            }
            if (!instance) {
              instance = parallelProcess.getInstanceById(instanceId);
              instance.onActivityInterrupted('bpmn:UserTask', mockInterruptFunction);
            }

            setTimeout(() => {
              instance.removeToken(tokenId);
              setTimeout(done, 100);
            }, 10);
          }

          return true;
        }
      });
      // start an instance
      parallelProcess.deployAndStart();
    });

    it('should have removed the token from the instance', () => {
      expect(instance.getState().tokens.length).toBe(1);
      expect(instance.getState().tokens).not.toContainEqual({
        tokenId
      });
    });

    it('should not have terminated the instance since there is a token left that is still running', () => {
      expect(instance.getState().instanceState).not.toStrictEqual(['STOPPED']);
      expect(instance.isEnded()).toBeFalsy();
    });

    it('should have ended the user task before removing the token', () => {
      expect(mockInterruptFunction).toHaveBeenCalledWith(
        {
          flowElementId: 'Task_05qgw49',
          tokenId,
          executionState: ExecutionState.SKIPPED,
          startTime: expect.any(Number),
          endTime: expect.any(Number)
        },
        {
          currentFlowElement: { id: 'Task_05qgw49', startTime: expect.any(Number), state: 'TERMINATED' },
          intermediateVariablesState: {},
          localExecutionTime: expect.any(Number),
          localStartTime: expect.any(Number),
          previousFlowElementId: 'Flow_1cwddpk',
          state: 'RUNNING',
          tokenId: expect.any(String)
        }
      );
    });

    it('should have logged the manual token removal in the adaptation log', () => {
      expect(instance.getState().adaptationLog).toEqual([
        {
          type: AdaptationType.TOKEN_REMOVE,
          time: expect.any(Number),
          tokenId,
          currentFlowElementId: 'Task_05qgw49'
        }
      ]);
    });
  });

  describe('interruption of running flow nodes', () => {
    it('interrupts a script task preventing it from changing the instance variables', async done => {
      const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_SCRIPT_TASK_SET_VAR, {
        // create the logic that will activate the user task inside the process and then remove the token
        shouldActivateFlowNodeHook: async (_pId, instanceId, tokenId, flowNode, _eState) => {
          if (flowNode.id === 'Task_1u9njea') {
            setTimeout(() => {
              const instance = simpleProcess.getInstanceById(instanceId);

              expect(instance.getState().tokens[0].intermediateVariablesState).toStrictEqual({
                a: 15
              });

              instance.removeToken(tokenId);

              setTimeout(() => {
                expect(instance.getState().tokens).toEqual([]);
                expect(instance.getVariables()).toEqual({ a: 5, b: 10 });
                done();
              }, 2600);
            }, 10);
          }

          return true;
        }
      });
      // start an instance
      simpleProcess.deployAndStart({ variables: { a: { value: 5 }, b: { value: 10 } } });
    });
  });

  describe('silent change', () => {
    it('should not log the change as an adaptation if done with a silent flag', async done => {
      // create the process to run the test on
      const simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_USER_TASK, {
        // create the logic that will activate the user task inside the process and then remove the token
        shouldActivateFlowNodeHook: async (_pId, instanceId, tId, flowNode, _eState) => {
          const instance = simpleProcess.getInstanceById(instanceId);

          if (flowNode.id === 'UserTask_1lmr4l7') {
            setTimeout(() => {
              instance.removeToken(instance.getState().tokens[0].tokenId, true);

              setTimeout(() => {
                expect(instance.getState().adaptationLog).toEqual([]);
                done();
              }, 10);
            }, 10);
          }

          return true;
        }
      });
      // start an instance
      simpleProcess.deployAndStart();
    });
  });
});
