import { filter, map, switchMap, take } from 'rxjs/operators';
import BpmnProcess from '../../src/BpmnProcess';
import { BPMN_XML_INTERMEDIATE_CATCH } from '../fixtures/bpmnXml';

let simpleProcess: BpmnProcess;

beforeEach(async () => {
  simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_INTERMEDIATE_CATCH);
});

afterEach(() => {
  simpleProcess.undeploy();
});

describe('receiveSignal()', () => {
  it('should continue with the execution when a signal is received', done => {
    simpleProcess
      .getInstance$()
      .pipe(take(1))
      .subscribe(pi => {
        pi.getState$()
          .pipe(
            map(state => state.tokens),
            map(tokens => tokens.filter(t => t.currentFlowElement.id === 'IntermediateCatchEvent_0o02gmp')),
            filter(tokens => tokens.length > 0),
            take(1)
          )
          .pipe(
            switchMap(() => {
              setTimeout(() => pi.receiveSignal('Signal_1gcpp7v'), 0);
              return pi.getState$();
            }),
            map(state => state.tokens),
            filter(tokens => tokens.filter(t => t.currentFlowElement.id.startsWith('Task_017n45t')).length > 0),
            take(1)
          )
          .subscribe({
            next: tokens => {
              const nextTask = tokens.find(t => t.currentFlowElement.id.startsWith('Task_017n45t'))!.currentFlowElement;
              expect(nextTask).toBeDefined();
            },
            complete: done
          });
      });

    simpleProcess.deployAndStartAt({ tokens: [{ currentFlowElementId: 'Task_0twpunq' }] });
  });

  it('should not continue if a different signal is received', done => {
    const subscribeStub = jest.fn();
    simpleProcess
      .getInstance$()
      .pipe(take(1))
      .subscribe(async pi => {
        pi.getState$()
          .pipe(
            map(state => state.tokens),
            map(tokens => tokens.filter(t => t.currentFlowElement.id === 'IntermediateCatchEvent_0o02gmp')),
            filter(tokens => tokens.length > 0),
            take(1),
            switchMap(() => {
              setTimeout(() => pi.receiveSignal('Signal_xyz'), 0);
              return pi.getState$();
            }),
            map(state => state.tokens),
            filter(tokens => tokens.filter(t => t.currentFlowElement.id.startsWith('Task_017n45t')).length > 0),
            take(1)
          )
          .subscribe(() => subscribeStub());

        setTimeout(() => {
          expect(subscribeStub).not.toHaveBeenCalled();
          done();
        }, 50);
      });

    simpleProcess.deployAndStartAt({ tokens: [{ currentFlowElementId: 'Task_0twpunq' }] });
  });
});
