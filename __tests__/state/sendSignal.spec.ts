import BpmnProcess from '../../src/BpmnProcess';
import { BPMN_XML_INTERMEDIATE_THROW_SIGNAL } from '../fixtures/bpmnXml';

describe('sendSignal()', () => {
  it('should invoke the given hook when a signal throw is encountered', done => {
    const sendSignalHook = jest.fn();
    BpmnProcess.fromXml('p1', BPMN_XML_INTERMEDIATE_THROW_SIGNAL, { sendSignalHook }).then(process => {
      process.deployAndStartAt({ tokens: [{ currentFlowElementId: 'ThrowEvent_05wouxt' }] });

      setTimeout(() => {
        expect(sendSignalHook).toHaveBeenCalledWith('p1', expect.anything(), 'Signal_1qpz0p4');
        done();
      }, 50);
    });
  });
});
