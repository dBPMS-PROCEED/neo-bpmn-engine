import BpmnProcess from '../../src/BpmnProcess';
import { BPMN_XML_INTERMEDIATE_THROW_MESSAGE } from '../fixtures/bpmnXml';

describe('sendMessage()', () => {
  it('should invoke the given hook when a message throw is encountered', done => {
    const sendMessageHook = jest.fn();
    BpmnProcess.fromXml('p1', BPMN_XML_INTERMEDIATE_THROW_MESSAGE, { sendMessageHook }).then(process => {
      process.deployAndStartAt({ tokens: [{ currentFlowElementId: 'ThrowEvent_05wouxt' }] });

      setTimeout(() => {
        expect(sendMessageHook).toHaveBeenCalledWith('p1', expect.anything(), 'Message_09n54fw');
        done();
      }, 50);
    });
  });
});
