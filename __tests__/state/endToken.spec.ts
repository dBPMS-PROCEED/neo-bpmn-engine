import { FlowNode } from 'bpmn-moddle';
import BpmnProcess from '../../src/BpmnProcess';
import BpmnProcessInstance from '../../src/BpmnProcessInstance';
import { TokenState } from '../../src/state/reducers/tokens';
import { BPMN_XML_INTERMEDIATE_CATCH } from '../fixtures/bpmnXml';
import { createMockLogger } from '../helpers/mockLogger';

let simpleProcess: BpmnProcess;
const mockLogger = createMockLogger();

beforeEach(() => {
  mockLogger.reset();
});

const shouldPassTokenStub = jest.fn();
const shouldActivateStub = jest.fn();

beforeEach(async () => {
  jest.resetAllMocks();
  shouldPassTokenStub.mockResolvedValue(true);
  shouldActivateStub.mockResolvedValue(true);
  simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_INTERMEDIATE_CATCH, {
    shouldPassTokenHook: shouldPassTokenStub,
    shouldActivateFlowNodeHook: shouldActivateStub
  });
});

afterEach(() => {
  simpleProcess.undeploy();
});

describe('endToken()', () => {
  it('should not pass token to the next sequenceFlow when a message is received after token is updated with state ABORTED', done => {
    shouldActivateStub.mockImplementation(async (_1, instanceId, tokenId, flowNode: FlowNode) => {
      // let the instance receive a message after it was aborted and check that the message had no effect
      if (flowNode.id === 'IntermediateCatchEvent_0wpudxa') {
        const instance = simpleProcess.getInstanceById(instanceId);
        setTimeout(() => {
          instance.endToken(tokenId, { state: TokenState.ABORTED });
          instance.receiveMessage('Message_1m7hrlz');

          setTimeout(() => {
            const state = instance.getState();
            expect(state.instanceState).toEqual(['ABORTED']);
            expect(instance.getState().tokens).toEqual([
              {
                tokenId,
                currentFlowElementId: 'IntermediateCatchEvent_0wpudxa',
                currentFlowNodeState: 'TERMINATED',
                state: 'ABORTED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_06rz3px',
                localExecutionTime: expect.any(Number),
                localStartTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);
            done();
          });
        }, 10);
      }

      return true;
    });

    simpleProcess.deployAndStart();
  });

  it('should pass token to the next sequenceFlow when a message is received before token is updated with state ABORTED', done => {
    let instance: BpmnProcessInstance;
    shouldPassTokenStub.mockImplementation(async (_1, instanceId, from, to) => {
      if (!instance) {
        instance = simpleProcess.getInstanceById(instanceId);
      }
      // check that the instance is aborted after the token is passed to the sequence flow following the catching event
      if (instance.getSequenceFlowId(from, to) === 'SequenceFlow_0aj6p9w') {
        setTimeout(() => {
          const state = instance.getState();
          expect(state.instanceState).toEqual(['ABORTED']);
          expect(state.tokens[0].state).toBe('ABORTED');
          done();
        }, 50);
      }

      return true;
    });
    shouldActivateStub.mockImplementation(async (_1, _2, tokenId, flowNode: FlowNode) => {
      // let the instance receive a message and abort the instance afterwards
      if (flowNode.id === 'IntermediateCatchEvent_0wpudxa') {
        setTimeout(() => {
          instance.receiveMessage('Message_1m7hrlz');
          instance.endToken(tokenId, { state: TokenState.ABORTED });
        }, 10);
      }

      return true;
    });

    simpleProcess.deployAndStart();
  });
});
