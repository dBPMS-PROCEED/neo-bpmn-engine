import { filter, map, switchMap, take } from 'rxjs/operators';
import BpmnProcess from '../../src/BpmnProcess';
import { BPMN_XML_INTERMEDIATE_CATCH } from '../fixtures/bpmnXml';

let simpleProcess: BpmnProcess;

beforeEach(async () => {
  simpleProcess = await BpmnProcess.fromXml('simpleProcess', BPMN_XML_INTERMEDIATE_CATCH);
});

afterEach(() => {
  simpleProcess.undeploy();
});

describe('receiveMessage()', () => {
  it('should continue with the execution when a message is received', done => {
    simpleProcess
      .getInstance$()
      .pipe(take(1))
      .subscribe(pi => {
        pi.getState$()
          .pipe(
            map(state => state.tokens),
            map(tokens => tokens.filter(t => t.currentFlowElement.id === 'IntermediateCatchEvent_0wpudxa')),
            filter(tokens => tokens.length > 0),
            take(1)
          )
          .pipe(
            switchMap(() => {
              setTimeout(() => pi.receiveMessage('Message_1m7hrlz'), 0);
              return pi.getState$();
            }),
            map(state => state.tokens),
            filter(tokens => tokens.filter(t => t.currentFlowElement.id.startsWith('Task_0twpunq')).length > 0),
            take(1)
          )
          .subscribe({
            next: tokens => {
              const nextTask = tokens.find(t => t.currentFlowElement.id.startsWith('Task_0twpunq'))!.currentFlowElement;
              expect(nextTask).toBeDefined();
            },
            complete: done
          });
      });

    simpleProcess.deployAndStart();
  });

  it('should not continue if a different message is received', done => {
    const subscribeStub = jest.fn();
    simpleProcess
      .getInstance$()
      .pipe(take(1))
      .subscribe(async pi => {
        pi.getState$()
          .pipe(
            map(state => state.tokens),
            map(tokens => tokens.filter(t => t.currentFlowElement.id === 'IntermediateCatchEvent_0wpudxa')),
            filter(tokens => tokens.length > 0),
            take(1),
            switchMap(() => {
              setTimeout(() => pi.receiveMessage('Message_xyz'), 0);
              return pi.getState$();
            }),
            map(state => state.tokens),
            filter(tokens => tokens.filter(t => t.currentFlowElement.id.startsWith('Task_0twpunq')).length > 0),
            take(1)
          )
          .subscribe(() => subscribeStub());

        setTimeout(() => {
          expect(subscribeStub).not.toHaveBeenCalled();
          done();
        }, 50);
      });

    simpleProcess.deployAndStart();
  });
});
