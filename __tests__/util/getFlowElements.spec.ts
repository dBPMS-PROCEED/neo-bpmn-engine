import { Definitions, Process, SubProcess } from 'bpmn-moddle';
import BpmnModdle from '../../src/util/BpmnModdle';
import { getAllNestedFlowElements } from '../../src/util/getFlowElements';
import {
  BPMN_XML_EMBEDDED_SUBPROCESS,
  BPMN_XML_SINGLE_NONE_START_EVENT,
  BPMN_XML_TERMINATED_SUBPROCESS
} from '../fixtures/bpmnXml';

const moddle = new BpmnModdle();

async function getDefinitions(xml: string): Promise<Definitions> {
  return new Promise((resolve, reject) => {
    moddle.fromXML(xml, (err, definitions) => {
      if (err) {
        reject(err);
      } else {
        resolve(definitions);
      }
    });
  });
}

describe('getAllNestedFlowElements', () => {
  it('returns all flow elements inside a given process element', async () => {
    const definitions = await getDefinitions(BPMN_XML_SINGLE_NONE_START_EVENT);

    const processElement = definitions.rootElements[0] as Process;

    expect(getAllNestedFlowElements(processElement)).toStrictEqual(processElement.flowElements);
  });
  it('returns all flow elements inside a process element inside the given definitions', async () => {
    const definitions = await getDefinitions(BPMN_XML_SINGLE_NONE_START_EVENT);

    const processElement = definitions.rootElements[0] as Process;

    expect(getAllNestedFlowElements(definitions)).toStrictEqual(processElement.flowElements);
  });
  it('returns all flow elements inside a given subprocess element', async () => {
    const definitions = await getDefinitions(BPMN_XML_EMBEDDED_SUBPROCESS);

    const subprocessElement = (definitions.rootElements[0] as Process).flowElements.find(
      el => el.$type === 'bpmn:SubProcess'
    )! as SubProcess;

    expect(getAllNestedFlowElements(subprocessElement)).toStrictEqual(subprocessElement.flowElements);
  });
  it('returns a flattened list of all elements contained in a process and the elements inside a subprocess contained in the process when given a definitions element', async () => {
    const definitions = await getDefinitions(BPMN_XML_EMBEDDED_SUBPROCESS);

    const processElement = definitions.rootElements[0] as Process;
    const subprocessElement = processElement.flowElements.find(el => el.$type === 'bpmn:SubProcess')! as SubProcess;

    expect(getAllNestedFlowElements(definitions)).toStrictEqual([
      ...processElement.flowElements,
      ...subprocessElement.flowElements
    ]);
  });
  it('returns a flattened list of all elements contained in a process and of elements inside subprocesses with differing degrees of nesting when given a definitions element', async () => {
    const definitions = await getDefinitions(BPMN_XML_TERMINATED_SUBPROCESS);

    const processElement = definitions.rootElements[0] as Process;
    const outerSubprocessElement = processElement.flowElements.find(
      el => el.$type === 'bpmn:SubProcess'
    )! as SubProcess;
    const innerSubprocessElement = outerSubprocessElement.flowElements.find(
      el => el.$type === 'bpmn:SubProcess'
    )! as SubProcess;

    expect(getAllNestedFlowElements(definitions)).toStrictEqual([
      ...processElement.flowElements,
      ...outerSubprocessElement.flowElements,
      ...innerSubprocessElement.flowElements
    ]);
  });
});
