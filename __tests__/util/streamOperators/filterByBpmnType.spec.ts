import { from, Observable } from 'rxjs';
import { initFlowNodeAction, ProcessAction, willPassTokenAction } from '../../../src/state/actions';
import { filterByBpmnType } from '../../../src/util/streamOperators/filterByBpmnType';
import { StateObservable } from 'redux-observable';
import { Subject } from 'rxjs';
import { DEFAULT_PROCESS_STATE, ProcessState } from '../../../src/state/reducers';

const mockModdleDefinitions: any = {
  $type: 'bpmn:Definitions',
  rootElements: [
    {
      $type: 'bpmn:Process',
      flowElements: [
        {
          id: 'Task_a',
          $type: 'bpmn:Task'
        },
        {
          id: 'Task_b',
          $type: 'bpmn:UserTask'
        },
        {
          id: 'Task_c',
          $type: 'bpmn:ScriptTask'
        },
        {
          id: 'Activity_a',
          $type: 'bpmn:CallActivity'
        },
        {
          id: 'Subprocess_a',
          $type: 'bpmn:SubProcess',
          flowElements: [
            {
              id: 'Task_d',
              $type: 'bpmn:Task'
            }
          ]
        },
        {
          $type: 'bpmn:ExclusiveGateway',
          id: 'ExclusiveGateway_a'
        }
      ]
    }
  ]
};

let state$ = new StateObservable<ProcessState>(new Subject(), {
  ...DEFAULT_PROCESS_STATE,
  moddleDefinitions: mockModdleDefinitions
});

describe('filterByBpmnType', () => {
  describe('process action does not have id', () => {
    it('should filter out the action', done => {
      const action$ = from([willPassTokenAction({ sequenceId: 'flow-456', tokenId: 'xyz' })]) as Observable<
        ProcessAction
      >;
      const returnedActions = [] as ProcessAction[];

      action$.pipe(filterByBpmnType('bpmn:SequenceFlow', state$)).subscribe(a => returnedActions.push(a));

      setTimeout(() => {
        expect(returnedActions).toEqual([]);
        done();
      }, 50);
    });
  });

  describe('non-existent element', () => {
    it('should filter out the action', done => {
      const action$ = from([initFlowNodeAction({ flowNodeId: 'Task_x', tokenId: 'xyz' })]) as Observable<ProcessAction>;
      const returnedActions = [] as ProcessAction[];

      action$.pipe(filterByBpmnType('bpmn:Task', state$)).subscribe(a => returnedActions.push(a));

      setTimeout(() => {
        expect(returnedActions).toEqual([]);
        done();
      }, 50);
    });
  });

  describe('existent-element, incorrect type', () => {
    it('should filter out the action', done => {
      const action$ = from([initFlowNodeAction({ flowNodeId: 'Task_b', tokenId: 'xyz' })]) as Observable<ProcessAction>;
      const returnedActions = [] as ProcessAction[];

      action$.pipe(filterByBpmnType('bpmn:Task', state$)).subscribe(a => returnedActions.push(a));

      setTimeout(() => {
        expect(returnedActions).toEqual([]);
        done();
      }, 50);
    });
  });

  describe('element exists and type matches', () => {
    it('should allow the action', done => {
      const action$ = from([initFlowNodeAction({ flowNodeId: 'Task_a', tokenId: 'xyz' })]) as Observable<ProcessAction>;
      const returnedActions = [] as ProcessAction[];

      action$.pipe(filterByBpmnType('bpmn:Task', state$)).subscribe(a => returnedActions.push(a));

      setTimeout(() => {
        expect(returnedActions).toEqual([initFlowNodeAction({ flowNodeId: 'Task_a', tokenId: 'xyz' })]);
        done();
      }, 50);
    });
  });

  describe('subprocess', () => {
    it('should allow the action for subprocess', done => {
      const action$ = from([initFlowNodeAction({ flowNodeId: 'Subprocess_a', tokenId: 'xyz' })]) as Observable<
        ProcessAction
      >;
      const returnedActions = [] as ProcessAction[];

      action$.pipe(filterByBpmnType('bpmn:SubProcess', state$)).subscribe(a => returnedActions.push(a));

      setTimeout(() => {
        expect(returnedActions).toEqual([initFlowNodeAction({ flowNodeId: 'Subprocess_a', tokenId: 'xyz' })]);
        done();
      }, 50);
    });

    it('should allow the action for element inside subprocess', done => {
      const action$ = from([initFlowNodeAction({ flowNodeId: 'Task_d', tokenId: 'xyz|abc' })]) as Observable<
        ProcessAction
      >;
      const returnedActions = [] as ProcessAction[];

      action$.pipe(filterByBpmnType('bpmn:Task', state$)).subscribe(a => returnedActions.push(a));

      setTimeout(() => {
        expect(returnedActions).toEqual([initFlowNodeAction({ flowNodeId: 'Task_d', tokenId: 'xyz|abc' })]);
        done();
      }, 50);
    });
  });
});
