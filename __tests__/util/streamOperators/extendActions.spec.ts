import { from, Observable, Subject } from 'rxjs';
import { StateObservable } from 'redux-observable';

import { updateTokenAction, initFlowNodeAction, FlowNodeAction, TokenAction } from '../../../src/state/actions';
import { ProcessState } from '../../../src/state/reducers';
import { TokenState } from '../../../src/state/reducers/tokens';

import {
  extendTokenAction,
  filterNonExistantToken,
  extendFlowNodeAction,
  filterNonExistantTokenOrFlowNode,
  filterTokenFlowNodeMismatch,
  filterFlowNodeTypeMismatch,
  filterNonActivityType
} from '../../../src/util/streamOperators/extendActions';

import { findElementById } from '../../../src/util/getFlowElements';

const targetTask = {
  id: 'Task_a',
  $type: 'bpmn:Task'
};

const mockModdleDefinitions: any = {
  $type: 'bpmn:Definitions',
  rootElements: [
    {
      $type: 'bpmn:Process',
      flowElements: [
        targetTask,
        {
          id: 'Task_b',
          $type: 'bpmn:UserTask'
        },
        {
          id: 'Task_c',
          $type: 'bpmn:ScriptTask'
        },
        {
          id: 'Activity_a',
          $type: 'bpmn:CallActivity'
        },
        {
          id: 'Subprocess_a',
          $type: 'bpmn:SubProcess',
          flowElements: [
            {
              id: 'Task_d',
              $type: 'bpmn:Task'
            }
          ]
        },
        {
          $type: 'bpmn:ExclusiveGateway',
          id: 'ExclusiveGateway_a'
        }
      ]
    }
  ]
};

let state$: StateObservable<ProcessState>;
let existingToken = {
  tokenId: 'abc',
  state: TokenState.RUNNING,
  localStartTime: 100,
  currentFlowElement: {
    id: 'Task_a',
    startTime: 100
  },
  localExecutionTime: 100,
  intermediateVariablesState: {}
};
beforeAll(() => {
  state$ = new StateObservable(new Subject(), {
    processId: 'Process',
    processInstanceId: 'Instance',
    creationTime: 0,
    status: 'RUNNING',
    tokens: [existingToken],
    log: [],
    variables: {},
    moddleDefinitions: mockModdleDefinitions,
    adaptationLog: []
  });
});

describe('extendTokenAction', () => {
  it("will set token to undefined if it doesn't exist", done => {
    const invalidAction = updateTokenAction({ tokenId: 'xyz' });
    const action$ = from([invalidAction]) as Observable<TokenAction>;

    action$.pipe(extendTokenAction(state$)).subscribe(result => {
      expect(result).toStrictEqual({
        ...invalidAction,
        token: undefined
      });
      done();
    });
  });

  it('returns an extended action with the token if it exists', done => {
    const invalidAction = updateTokenAction({ tokenId: 'abc' });
    const action$ = from([invalidAction]) as Observable<TokenAction>;

    action$.pipe(extendTokenAction(state$)).subscribe(result => {
      expect(result).toStrictEqual({
        ...invalidAction,
        token: existingToken
      });
      done();
    });
  });
});

describe('filterNonExistantToken', () => {
  it('does not forward action if token does not exist', done => {
    const invalidAction = updateTokenAction({ tokenId: 'xyz' });
    const action$ = from([invalidAction]) as Observable<TokenAction>;

    let result: any;

    action$.pipe(extendTokenAction(state$), filterNonExistantToken()).subscribe(res => (result = res));

    setTimeout(() => {
      expect(result).toBe(undefined);
      done();
    }, 50);
  });

  it('forwards action if the token exists', done => {
    const invalidAction = updateTokenAction({ tokenId: 'abc' });
    const action$ = from([invalidAction]) as Observable<TokenAction>;

    action$.pipe(extendTokenAction(state$), filterNonExistantToken()).subscribe(result => {
      expect(result).toStrictEqual({
        ...invalidAction,
        token: existingToken
      });
      done();
    });
  });
});

describe('extendFlowNodeAction', () => {
  it("will set flowNode and token to undefined if they don't exist", done => {
    const invalidAction = initFlowNodeAction({ flowNodeId: 'Task_Z', tokenId: 'xyz' });
    const action$ = from([invalidAction]) as Observable<FlowNodeAction>;

    action$.pipe(extendFlowNodeAction(state$)).subscribe(result => {
      expect(result).toStrictEqual({
        ...invalidAction,
        token: undefined,
        flowNode: undefined
      });
      done();
    });
  });

  it('returns an extended action with the token and flowNode if they exist', done => {
    const action = initFlowNodeAction({ flowNodeId: 'Task_a', tokenId: 'abc' });
    const action$ = from([action]) as Observable<FlowNodeAction>;

    action$.pipe(extendFlowNodeAction(state$)).subscribe(result => {
      expect(result).toStrictEqual({
        ...action,
        token: existingToken,
        flowNode: targetTask
      });
      done();
    });
  });
});

describe('filterNonExistantTokenOrFlowNode', () => {
  it('does not forward action if token does not exist', done => {
    const invalidAction = initFlowNodeAction({ tokenId: 'xyz', flowNodeId: 'Task_a' });
    const action$ = from([invalidAction]) as Observable<FlowNodeAction>;

    let result: any;

    action$.pipe(extendFlowNodeAction(state$), filterNonExistantTokenOrFlowNode()).subscribe(res => (result = res));

    setTimeout(() => {
      expect(result).toBe(undefined);
      done();
    }, 50);
  });

  it('does not forward action if flowNode does not exist', done => {
    const invalidAction = initFlowNodeAction({ tokenId: 'abc', flowNodeId: 'Task_z' });
    const action$ = from([invalidAction]) as Observable<FlowNodeAction>;

    let result: any;

    action$.pipe(extendFlowNodeAction(state$), filterNonExistantTokenOrFlowNode()).subscribe(res => (result = res));

    setTimeout(() => {
      expect(result).toBe(undefined);
      done();
    }, 50);
  });

  it('forwards action if token and flowNode exist', done => {
    const action = initFlowNodeAction({ tokenId: 'abc', flowNodeId: 'Task_a' });
    const action$ = from([action]) as Observable<FlowNodeAction>;

    action$.pipe(extendFlowNodeAction(state$), filterNonExistantTokenOrFlowNode()).subscribe(result => {
      expect(result).toStrictEqual({
        ...action,
        token: existingToken,
        flowNode: targetTask
      });
      done();
    });
  });
});

describe('filterTokenFlowNodeMismatch', () => {
  it('does not forward action if token is on another flowNode', done => {
    const invalidAction = initFlowNodeAction({ tokenId: 'abc', flowNodeId: 'Task_b' });
    const action$ = from([invalidAction]) as Observable<FlowNodeAction>;

    let result: any;

    action$.pipe(extendFlowNodeAction(state$), filterTokenFlowNodeMismatch()).subscribe(res => (result = res));

    setTimeout(() => {
      expect(result).toBe(undefined);
      done();
    }, 50);
  });

  it('forwards action if token and flowNode exist and token is on flowNode', done => {
    const action = initFlowNodeAction({ tokenId: 'abc', flowNodeId: 'Task_a' });
    const action$ = from([action]) as Observable<FlowNodeAction>;

    action$.pipe(extendFlowNodeAction(state$), filterTokenFlowNodeMismatch()).subscribe(result => {
      expect(result).toStrictEqual({
        ...action,
        token: existingToken,
        flowNode: targetTask
      });
      done();
    });
  });
});

describe('filterFlowNodeTypeMismatch', () => {
  it("does not forward action if flowNode doesn't have the expected type", done => {
    const invalidAction = initFlowNodeAction({ tokenId: 'abc', flowNodeId: 'Task_a' });
    const action$ = from([invalidAction]) as Observable<FlowNodeAction>;

    let result: any;

    action$
      .pipe(extendFlowNodeAction(state$), filterFlowNodeTypeMismatch('wrong:type'))
      .subscribe(res => (result = res));

    setTimeout(() => {
      expect(result).toBe(undefined);
      done();
    }, 50);
  });

  it('forwards action if flowNode has the expected type', done => {
    const action = initFlowNodeAction({ tokenId: 'abc', flowNodeId: 'Task_a' });
    const action$ = from([action]) as Observable<FlowNodeAction>;

    action$.pipe(extendFlowNodeAction(state$), filterFlowNodeTypeMismatch('bpmn:Task')).subscribe(result => {
      expect(result).toStrictEqual({
        ...action,
        token: existingToken,
        flowNode: targetTask
      });
      done();
    });
  });
});

describe('filterNonActivityType', () => {
  describe('non-existent element', () => {
    it('should filter out the action', done => {
      const invalidAction = initFlowNodeAction({ flowNodeId: 'Task_x', tokenId: 'xyz' });
      const action$ = from([invalidAction]) as Observable<FlowNodeAction>;

      let result: any;

      action$.pipe(extendFlowNodeAction(state$), filterNonActivityType()).subscribe(res => (result = res));

      setTimeout(() => {
        expect(result).toEqual(undefined);
        done();
      }, 50);
    });
  });

  describe('existent non-activity element', () => {
    it('should filter out the action', done => {
      const invalidAction = initFlowNodeAction({ flowNodeId: 'ExclusiveGateway_a', tokenId: 'xyz' });
      const action$ = from([invalidAction]);

      let result: any;

      action$.pipe(extendFlowNodeAction(state$), filterNonActivityType()).subscribe(res => (result = res));

      setTimeout(() => {
        expect(result).toEqual(undefined);
        done();
      }, 50);
    });
  });

  describe('existent activity element', () => {
    function testForType(flowNodeId: string, done: any) {
      const action = initFlowNodeAction({ flowNodeId, tokenId: 'xyz' });
      const action$ = from([action]);

      action$.pipe(extendFlowNodeAction(state$), filterNonActivityType()).subscribe(result => {
        expect(result).toStrictEqual({
          ...action,
          token: undefined,
          flowNode: findElementById(mockModdleDefinitions, flowNodeId)
        });
        done();
      });
    }

    it('should allow the action for a task', done => {
      testForType('Task_a', done);
    });
    it('should allow the action for a user task', done => {
      testForType('Task_b', done);
    });
    it('should allow the action for a script task', done => {
      testForType('Task_c', done);
    });
    it('should allow the action for a call-activity', done => {
      testForType('Activity_a', done);
    });
    it('should allow the action for a sub-process', done => {
      testForType('Subprocess_a', done);
    });
    it('should allow the action for a task in a subprocess', done => {
      testForType('Task_d', done);
    });
  });
});
