/**
 * @jest-environment jsdom
 */
import { parseIsoDuration } from '../../src/util/durationParser';
import { MalformedDurationError } from '../../src/errors';

describe('iso duration', () => {
  describe('repeat count', () => {
    it('should throw error if repeat count is not parseable', () => {
      expect(() => parseIsoDuration('RAx/PT2S')).toThrowError(MalformedDurationError);
    });

    it('should return repeat count to positive infinity if just R', () => {
      const [repeatCount] = parseIsoDuration('R/PT2S');
      expect(repeatCount).toEqual(Number.POSITIVE_INFINITY);
    });

    it('should return repeat count to positive infinity if not specified', () => {
      const [repeatCount] = parseIsoDuration('PT2S');
      expect(repeatCount).toEqual(Number.POSITIVE_INFINITY);
    });

    it('should return repeat count when positive', () => {
      const [repeatCount] = parseIsoDuration('R10/PT2S');
      expect(repeatCount).toEqual(10);
    });
  });

  describe('duration', () => {
    it('should throw error if duration is not parseable', () => {
      expect(() => parseIsoDuration('R3/xyz')).toThrowError(MalformedDurationError);
    });

    it('should throw error if duration parses to 0', () => {
      expect(() => parseIsoDuration('R3/PT0S')).toThrowError(MalformedDurationError);
    });

    it('should parse iso 8601 duration', () => {
      const [, duration] = parseIsoDuration('R/PT2S');
      expect(duration).toEqual(2000);
    });
  });
});
