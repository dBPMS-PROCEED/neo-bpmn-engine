import { evaluateExpression } from '../../src/util/expressionEvaluator';
import { ExpressionEvalError } from '../../src/errors';

it('should evaluate expression without errors to true', async () => {
  const result = evaluateExpression('a < b', { a: 4, b: 5 });
  expect(result).toEqual(true);
});

it('should evaluate expression without errors to false', async () => {
  const result = evaluateExpression('a > b', { a: 4, b: 5 });
  expect(result).toEqual(false);
});

it('should convert evaluate expression truthy result to boolean true', async () => {
  const result = evaluateExpression('a + b', { a: 4, b: 5 });
  expect(result).toEqual(true);
});

it('should convert evaluate expression falsy result to boolean false', async () => {
  const result = evaluateExpression('b - a - 1', { a: 4, b: 5 });
  expect(result).toEqual(false);
});

it('should throw error for undefined variable', async () => {
  expect(() => {
    evaluateExpression('a < c', { a: 4, b: 5 });
  }).toThrowError(ExpressionEvalError);
});
