import { ScriptExecutionError, BpmnEscalation, BpmnError } from '../src/errors';
import { DEFAULT_SCRIPT_EXECUTOR } from '../src/ScriptExecutionEnvironment';
import { createMockLogger } from './helpers/mockLogger';

const scriptExecutor = DEFAULT_SCRIPT_EXECUTOR;

describe('default script execution', () => {
  it('script execution - should call the log functions', async () => {
    const log = {
      info: jest.fn()
    };
    const initialVariables: any = { x: 10, y: 5 };

    const variable = {
      set: jest.fn().mockImplementation((name: string, value: any) => {
        initialVariables[name] = value;
      }),
      get: jest.fn().mockImplementation((name: string) => initialVariables[name]),
      getAll: jest.fn().mockImplementation(() => initialVariables)
    };
    const getService = jest.fn();

    await scriptExecutor.execute(
      'myProcess',
      'p1',
      'token-123',
      `
      log.info('Hello');
      log.info('Variables: ' + variable.get('x')  + ' ' + variable.get('y'));
      variable.set('z', 20);
      const allVariables = variable.getAll();
      `,
      { log, getService, variable }
    );

    expect(variable.get).toHaveBeenCalledWith('x');
    expect(variable.get).toHaveBeenCalledWith('y');
    expect(variable.set).toHaveBeenCalledWith('z', 20);
    expect(variable.getAll).toHaveBeenCalledTimes(1);
    expect(variable.getAll).toHaveReturnedWith({ x: 10, y: 5, z: 20 });
    expect(log.info).toHaveBeenCalledWith('Hello');
    expect(log.info).toHaveBeenCalledWith('Variables: 10 5');
  });

  it('should use internal logging system when calling console', async () => {
    const log = createMockLogger();

    const console = {
      trace: jest.fn(),
      debug: jest.fn(),
      info: jest.fn(),
      warn: jest.fn(),
      error: jest.fn()
    };

    await scriptExecutor.execute(
      'myProcess',
      'p1',
      'token-123',
      ` 
        console.trace('Trace');
        console.debug('Debug');
        console.info('Info');
        console.warn('Warning')
        console.error('Error');
        `,
      { log, console }
    );

    expect(console.trace).toHaveBeenCalledWith('Trace');
    expect(console.debug).toHaveBeenCalledWith('Debug');
    expect(console.info).toHaveBeenCalledWith('Info');
    expect(console.warn).toHaveBeenCalledWith('Warning');
    expect(console.error).toHaveBeenCalledWith('Error');
  });

  it('should throw if script execution errors out', async () => {
    const log = {
      info: jest.fn()
    };

    try {
      await scriptExecutor.execute(
        'myProcess',
        'p1',
        'token-123',
        `
        // this is an error
        someError
        log.info('not an error');
        `,
        { log }
      );
    } catch (e) {
      expect(e).toBeInstanceOf(ScriptExecutionError);
    } finally {
      expect(log.info).not.toHaveBeenCalled();
    }
  });

  it('should throw BpmnEscalation if script says so', async () => {
    const log = createMockLogger();

    try {
      await scriptExecutor.execute(
        'myProcess',
        'p1',
        'token-123',
        `
        // this is an error
        throw new BpmnEscalation('Error');
        log.info('not an error');
        `,
        { log, BpmnEscalation }
      );
    } catch (e) {
      expect(e).toBeInstanceOf(BpmnEscalation);
    } finally {
      expect(log.info).not.toHaveBeenCalled();
    }
  });

  it('should throw BpmnError if script says so', async () => {
    const log = createMockLogger();

    try {
      await scriptExecutor.execute(
        'myProcess',
        'p1',
        'token-123',
        `
        // this is an error
        throw new BpmnError('Error');
        log.info('not an error');
        `,
        { log, BpmnError }
      );
    } catch (e) {
      expect(e).toBeInstanceOf(BpmnError);
    } finally {
      expect(log.info).not.toHaveBeenCalled();
    }
  });

  it('should be able to call a provided service', async () => {
    const log = createMockLogger();
    const emailService = {
      send: jest.fn()
    };
    const getService = jest.fn().mockImplementation(name => {
      if (name === 'com.proceed.email') return emailService;
    });

    await scriptExecutor.execute(
      'myProcess',
      'p1',
      'token-123',
      `
      const emailService = getService('com.proceed.email');
      emailService.send('hello', 'world');
      `,
      { log, getService }
    );

    expect(emailService.send).toHaveBeenCalledWith('myProcess', 'p1', 'token-123', 'hello', 'world');
  });

  it('should get back returned variables', async () => {
    const log = createMockLogger();
    const variable = {
      set: jest.fn(),
      get: jest.fn().mockReturnValue(10)
    };
    const getService = jest.fn();

    const returnedValue = await scriptExecutor.execute(
      'myProcess',
      'p1',
      'token-123',
      `
      return { x: variable.get('x') + 10 };
      `,
      { log, getService, variable }
    );

    expect(variable.get).toHaveBeenCalledWith('x');
    expect(returnedValue).toEqual({ x: 20 });
  });

  it('should throw error when trying to set variables with data type not supported by JSON ', async () => {
    const log = createMockLogger();
    const variable = {
      set: jest.fn().mockImplementation((variable, value) => {
        if (typeof value === 'function') throw new TypeError('datatype not supported by JSON');
      }),
      get: jest.fn().mockReturnValue(10)
    };
    const getService = jest.fn();

    try {
      await scriptExecutor.execute(
        'myProcess',
        'p1',
        'token-123',
        `
        // this is an error
        variable.set('a', () => {});
        log.info('after error')
        `,
        { log, variable, getService }
      );
    } catch (e) {
      expect(e).toBeInstanceOf(ScriptExecutionError);
    } finally {
      expect(log.info).not.toHaveBeenCalled();
    }

    expect(variable.set).toHaveBeenCalledWith('a', expect.any(Function));
  });

  it('should not throw error when using try-catch block inside of script', async () => {
    const log = {
      error: jest.fn()
    };
    const variable = {
      set: jest.fn().mockImplementation((processId, processInstanceId, elementId, variable, value) => {
        throw TypeError('datatype not supported by JSON');
      }),
      get: jest.fn().mockReturnValue(10)
    };
    const getService = jest.fn();

    await scriptExecutor.execute(
      'myProcess',
      'p1',
      'token-123',
      `
      try {
        // this is an error
        variable.set('a', () => {});
      } catch(e) {
        log.error('Caught error');
      }

      `,
      { log, getService, variable }
    );

    expect(variable.set).toHaveBeenCalledWith('a', expect.any(Function));
    expect(variable.set).toHaveBeenCalledTimes(1);
    expect(variable.set).toThrow();
    expect(log.error).toHaveBeenCalledWith('Caught error');
  });

  it('should be able to access processId and processInstanceId from script', async () => {
    const log = {
      info: jest.fn()
    };

    await scriptExecutor.execute(
      'myProcess',
      'p1',
      'token-123',
      `
        log.info(processId);
        log.info(processInstanceId);
        `,
      { log }
    );

    expect(log.info).toHaveBeenCalledWith('myProcess');
    expect(log.info).toHaveBeenCalledWith('p1');
  });
});
