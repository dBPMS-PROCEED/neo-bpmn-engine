import fs from 'fs';
import path from 'path';
import { interval, VirtualAction, VirtualTimeScheduler } from 'rxjs';
import { take } from 'rxjs/operators';
import BpmnModdle from '../src/util/BpmnModdle';
import { findElementById } from '../src/util/getFlowElements';
import BpmnProcess from '../src/BpmnProcess';
import { LogItem } from '../src/LoggerFactory';
import {
  BpmnElementNotFoundError,
  BpmnProcessNotDeployedError,
  InvalidBpmnXmlError,
  MessageRefNotFoundError,
  NoStartEventSpecifiedError,
  SignalRefNotFoundError
} from '../src/errors';
import {
  BPMN_XML_MULTIPLE_NONE_START_EVENT,
  BPMN_XML_SINGLE_DURATION_TIMER_START_EVENT,
  BPMN_XML_SINGLE_MESSAGE_START_EVENT,
  BPMN_XML_SINGLE_NONE_START_EVENT,
  BPMN_XML_SINGLE_SINGLE_START_EVENT,
  BPMN_XML_USER_TASK,
  BPMN_XML_WITHOUT_START_EVENT,
  BPMN_XML_PARALLEL_GATEWAY,
  BPMN_XML_MULTIPLE_END_EVENTS,
  BPMN_XML_SCRIPT_TASK_SET_VAR,
  BPMN_XML_CALL_ACTIVITY,
  BPMN_XML_EMBEDDED_SUBPROCESS,
  BPMN_XML_TERMINATED_SUBPROCESS,
  BPMN_XML_SCRIPT_TASK_SEMANTIC_ERROR,
  BPMN_XML_SCRIPT_TASK_TECHNICAL_ERROR,
  BPMN_XML_TERMINATE_END_EVENT,
  BPMN_XML_SUBPROCESS_INTERRUPTING_ESCALATION_BOUNDARY_EVENT,
  BPMN_XML_EXCLUSIVE_XOR_GATEWAY,
  BPMN_XML_EXCLUSIVE_XOR_GATEWAY_SIMPLE_LOOP,
  BPMN_XML_INCLUSIVE_GATEWAY,
  BPMN_XML_EXCLUSIVE_XOR_GATEWAY_MULTIPLE_INOUT,
  BPMN_XML_PARALLEL_GATEWAY_MULTIPLE_INOUT,
  BPMN_XML_SCRIPT_TASK_INTERRUPTING_TIMER_BOUNDARY_EVENT,
  BPMN_XML_SUBPROCESS_NON_INTERRUPTING_TIMER_BOUNDARY_EVENT,
  BPMN_XML_SUBPROCESS_INTERRUPTING_TIMER_BOUNDARY_EVENT,
  BPMN_XML_SUBPROCESS_ERROR_BOUNDARY_EVENT,
  BPMN_XML_SCRIPT_TASK_ERROR_BOUNDARY_EVENT,
  BPMN_XML_SCRIPT_TASK_INTERRUPTING_ESCALATION_BOUNDARY_EVENT,
  BPMN_XML_PARALLEL_USER_TASKS,
  BPMN_XML_SCRIPT_TASK_NON_INTERRUPTING_TIMER_BOUNDARY_EVENT,
  BPMN_XML_TERMINATED_SUBPROCESS_TRIPLE_SPLIT,
  BPMN_XML_SCRIPT_TASK_SEMANTIC_ERROR_GLOBAL_VARIABLE,
  BPMN_XML_SCRIPT_TASK_ACCESS_GLOBAL_VARIABLE,
  BPMN_XML_SCRIPT_TASK_ACCESS_ALL_GLOBAL_VARIABLES,
  BPMN_XML_SUBPROCESS_UNTRIGGERED_INTERRUPTING_TIMER_BOUNDARY_EVENT,
  BPMN_XML_INTERMEDIATE_CATCH,
  BPMN_XML_EVENT_BASED_GATEWAY
} from './fixtures/bpmnXml';
import BpmnProcessInstance from '../src/BpmnProcessInstance';
import { FlowNodeState, TokenState } from '../src/state/reducers/tokens';
import { ExecutionState } from '../src/state/reducers/log';
import { ExportedProcessState } from '../src/state/reducers';

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

describe('process creation', () => {
  it('should be able to create a process with valid bpmn xml', async () => {
    let p = null;
    const errorStub = jest.fn();
    try {
      p = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_NONE_START_EVENT);
    } catch (e) {
      errorStub(e);
    }

    expect(p).toBeDefined();
    expect(errorStub).not.toHaveBeenCalled();
  });

  it('should be able to return the id', async () => {
    const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT);
    expect(p.getId()).toEqual('p1');
  });

  it('should throw error for invalid xml', async () => {
    let p = null;
    try {
      p = await BpmnProcess.fromXml('myProcess', '');
    } catch (e) {
      expect(e).toBeInstanceOf(InvalidBpmnXmlError);
    }

    expect(p).toBeNull();
  });
});

describe('deployment', () => {
  it('should default to undeployed state', async () => {
    const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_NONE_START_EVENT);
    expect(p.isDeployed()).toBeFalsy();
  });

  it('should alter deploy state', async () => {
    const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_NONE_START_EVENT);
    p.deploy();
    expect(p.isDeployed()).toBeTruthy();
  });

  it('should undeploy', async () => {
    const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_NONE_START_EVENT);
    p.deploy();
    p.undeploy();
    expect(p.isDeployed()).toBeFalsy();
  });
});

describe('start process instance', () => {
  it('should throw error when in undeployed state', async () => {
    const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_NONE_START_EVENT);
    expect(() => p.start()).toThrowError(BpmnProcessNotDeployedError);
  });

  it('should start with no start event', async () => {
    const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_WITHOUT_START_EVENT);
    p.deploy();
    expect(() => p.start()).not.toThrow();
  });

  it('should inform all subscribers on the instance stream about new instances being started', async () => {
    const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_NONE_START_EVENT);
    p.deploy();

    const subscriber = jest.fn();
    p.getInstance$().subscribe(subscriber);

    p.start();

    await sleep(100);

    expect(subscriber).toHaveBeenCalledTimes(1);
  });

  it('should return the created instance', async () => {
    const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_NONE_START_EVENT);
    p.deploy();

    const instance = p.start();

    expect(p.getAllInstances()[0]).toBe(instance);
  });

  it('should pass the instance to the instance stream once it has been created', async done => {
    const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_NONE_START_EVENT);
    p.deploy();

    p.getInstance$().subscribe(newInstance => {
      expect(typeof p.getInstanceById(newInstance.getState().processInstanceId) === 'object').toBeTruthy();
      done();
    });

    p.start();
  });

  describe('none start', () => {
    it('should start with multiple start events', async () => {
      const p = await BpmnProcess.fromXml('myProcess', BPMN_XML_MULTIPLE_NONE_START_EVENT);
      p.deploy();
      expect(() => p.start()).not.toThrow();
    });

    it('should start with default none start event', async () => {
      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT);
      p.deploy();
      expect(() => p.start()).not.toThrow();
    });

    it('should notify the instance$ on creation of process', done => {
      const subscribeStub = jest.fn();
      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          subscribeStub(instance);
        });

        p.start();
        expect(p.getAllInstances()).toHaveLength(1); // new instance in store

        setTimeout(() => {
          expect(subscribeStub).toHaveBeenCalledTimes(1);
          done();
        }, 150);
      });
    });

    it('should remove instance from store if deleted', done => {
      const subscribeStub = jest.fn();
      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          subscribeStub(instance);
        });

        p.start();
        const allInstances = p.getAllInstances();
        expect(allInstances).toHaveLength(1); // new instance in store

        const instanceIdToDelete = allInstances[0].getId();
        p.deleteInstanceById(instanceIdToDelete);
        expect(p.getInstanceById(instanceIdToDelete)).toBeUndefined(); // instance was deleted
        expect(p.getAllInstances()).toHaveLength(0); // no instance remaining
        done();
      });
    });

    it('should include process id, process instance id and creation time in the state', done => {
      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.getInstance$().subscribe(instance => {
          instance
            .getState$()
            .pipe(take(1))
            .subscribe(state => {
              expect(state.processId).toEqual('p1');
              expect(state.processInstanceId).toBeTruthy();
              expect(+new Date() - state.creationTime).toBeLessThanOrEqual(250);
              done();
            });
        });

        p.deployAndStart();
      });
    });

    it('should add additional information to flownode-log', done => {
      const subscribeStub = jest.fn();
      let onEnded = jest.fn();
      let thisInstance: BpmnProcessInstance;
      let onFlowNodeExecuted = jest.fn();

      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          subscribeStub(instance);
          onFlowNodeExecuted.mockImplementation(e => {
            instance.updateLog(e.flowElementId, e.tokenId, { machine: 'machine-1' });
          });
          instance.onEnded(onEnded);
          instance.onFlowNodeExecuted(onFlowNodeExecuted);
          thisInstance = instance;
        });

        p.start();

        setTimeout(() => {
          expect(subscribeStub).toHaveBeenCalledTimes(1);
          expect(onEnded).toHaveBeenCalledTimes(1);
          expect(onFlowNodeExecuted).toHaveBeenCalledTimes(3);

          const state = thisInstance.getState();
          expect(state.log).toEqual([
            {
              flowElementId: 'StartEvent_1',
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: 'machine-1'
            },
            {
              flowElementId: 'Task_175vb4p',
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: 'machine-1'
            },
            {
              flowElementId: 'EndEvent_1umdfcw',
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number),
              machine: 'machine-1'
            }
          ]);

          expect(state.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'ENDED',
              localStartTime: expect.any(Number),
              currentFlowElementId: 'EndEvent_1umdfcw',
              currentFlowNodeState: 'COMPLETED',
              currentFlowElementStartTime: expect.any(Number),
              previousFlowElementId: 'SequenceFlow_12bv6av',
              localExecutionTime: expect.any(Number),
              intermediateVariablesState: null
            }
          ]);
          expect(state.instanceState).toEqual(['ENDED']);
          done();
        }, 150);
      });
    });

    it('should update process status with custom value', done => {
      let thisInstance: BpmnProcessInstance;

      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          thisInstance = instance;
        });

        p.start();

        setTimeout(() => {
          const state = thisInstance.getState();

          expect(state.instanceState).toEqual(['ENDED']);
          setTimeout(() => {
            thisInstance.updateProcessStatus('custom-status');

            const state = thisInstance.getState();

            expect(state.instanceState).toEqual(['custom-status']);
          });
          done();
        }, 150);
      });
    });

    it('should call given callbacks if state changes', done => {
      const subscribeStub = jest.fn();
      let onEnded = jest.fn();
      let onTokenEnded = jest.fn();
      let onInstanceStateChange = jest.fn();
      let onFlowNodeExecuted = jest.fn();

      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          subscribeStub(instance);
          instance.onEnded(onEnded);
          instance.onTokenEnded(onTokenEnded);
          instance.onInstanceStateChange(onInstanceStateChange);
          instance.onFlowNodeExecuted(onFlowNodeExecuted);
        });

        p.start();

        setTimeout(() => {
          expect(subscribeStub).toHaveBeenCalledTimes(1);
          expect(onEnded).toHaveBeenCalledTimes(1);
          expect(onTokenEnded).toHaveBeenCalledTimes(1);
          expect(onFlowNodeExecuted).toHaveBeenCalledTimes(3);
          // RUNNING -> READY -> RUNNING -> READY -> RUNNING -> ENDED
          expect(onInstanceStateChange).toHaveBeenCalledTimes(6);
          expect(onInstanceStateChange).toHaveBeenCalledWith(['RUNNING']);
          expect(onInstanceStateChange).toHaveBeenCalledWith(['READY']);
          expect(onInstanceStateChange).toHaveBeenCalledWith(['ENDED']);
          done();
        }, 150);
      });
    });

    it('should call onTokenEnded again if a token was put back into an executing state and ended again', done => {
      let onTokenEnded = jest.fn();

      let endedBefore = false;

      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          instance.onEnded(() => {
            if (!endedBefore) {
              endedBefore = true;
              instance.placeTokenAt('SequenceFlow_12bv6av', { tokenId: instance.getState().tokens[0].tokenId });
            }
          });
          instance.onTokenEnded(onTokenEnded);
        });

        p.start();

        setTimeout(() => {
          expect(onTokenEnded).toHaveBeenCalledTimes(2);
          done();
        }, 150);
      });
    });

    it('should be able to start processInstance with given parameters', done => {
      const subscribeStub = jest.fn();
      let onEnded = jest.fn();
      let thisInstance: BpmnProcessInstance;

      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          thisInstance = instance;
          subscribeStub(instance);
          instance.onEnded(onEnded);
        });

        p.start({
          token: {
            abc: 123,
            def: 456
          },
          variables: { a: { value: 5 } }
        });

        setTimeout(() => {
          expect(subscribeStub).toHaveBeenCalledTimes(1);
          expect(onEnded).toHaveBeenCalledTimes(1);

          const state = thisInstance.getState();
          expect(state.variables).toEqual({
            a: { value: 5, log: [{ changedTime: expect.any(Number) }] }
          }); // included variables given at start
          expect(state.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: 'ENDED',
              localStartTime: expect.any(Number),
              currentFlowElementId: 'EndEvent_1umdfcw',
              currentFlowNodeState: 'COMPLETED',
              currentFlowElementStartTime: expect.any(Number),
              previousFlowElementId: 'SequenceFlow_12bv6av',
              localExecutionTime: expect.any(Number),
              intermediateVariablesState: null,
              abc: 123,
              def: 456
            }
          ]);

          expect(state.log).toEqual([
            // included execution given at start
            {
              flowElementId: 'StartEvent_1',
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'Task_175vb4p',
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'EndEvent_1umdfcw',
              tokenId: expect.any(String),
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            }
          ]);
          expect(state.instanceState).toEqual(['ENDED']);
          done();
        }, 150);
      });
    });

    it('should be able to start processInstance at certain element with given parameters', done => {
      const subscribeStub = jest.fn().mockImplementation(instance => instance.getId());
      let onEnded = jest.fn();
      let thisInstance: BpmnProcessInstance;

      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          thisInstance = instance;
          subscribeStub(instance);
          instance.onEnded(onEnded);
        });

        p.startAt({
          instanceId: 'p1-123',
          globalStartTime: 100,
          tokens: [
            {
              currentFlowElementId: 'Task_175vb4p',
              tokenId: 'xyz',
              abc: 123,
              def: 456,
              intermediateVariablesState: { b: 123 }
            }
          ],
          variables: { a: { value: 5, log: [{ changedTime: 100 }] } },
          log: [
            {
              flowElementId: 'StartEvent_1',
              tokenId: 'xyz',
              executionState: ExecutionState.COMPLETED,
              startTime: 100,
              endTime: 200,
              abc: 65
            }
          ]
        });

        setTimeout(() => {
          expect(subscribeStub).toHaveBeenCalledTimes(1);
          expect(subscribeStub).toHaveReturnedWith('p1-123');
          expect(onEnded).toHaveBeenCalledTimes(1);
          const state = thisInstance.getState();
          expect(state.processInstanceId).toEqual('p1-123');
          expect(state.globalStartTime).toEqual(100);
          expect(state.variables).toEqual({
            a: { value: 5, log: [{ changedTime: 100 }] },
            b: { value: 123, log: [{ changedTime: expect.any(Number), changedBy: 'Task_175vb4p' }] }
          }); // included variables given at start
          expect(state.tokens).toEqual([
            {
              tokenId: 'xyz',
              state: 'ENDED',
              localStartTime: expect.any(Number),
              currentFlowElementId: 'EndEvent_1umdfcw',
              currentFlowNodeState: 'COMPLETED',
              currentFlowElementStartTime: expect.any(Number),
              previousFlowElementId: 'SequenceFlow_12bv6av',
              localExecutionTime: expect.any(Number),
              intermediateVariablesState: null,
              abc: 123,
              def: 456
            }
          ]);

          expect(state.log).toEqual([
            // included execution given at start
            {
              flowElementId: 'StartEvent_1',
              tokenId: 'xyz',
              executionState: ExecutionState.COMPLETED,
              startTime: 100,
              endTime: 200,
              abc: 65
            },
            {
              flowElementId: 'Task_175vb4p',
              tokenId: 'xyz',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'EndEvent_1umdfcw',
              tokenId: 'xyz',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            }
          ]);
          expect(state.instanceState).toEqual(['ENDED']);
          done();
        }, 150);
      });
    });

    it('should log when starting a processInstance', done => {
      let instanceId: String;
      const subscribeStub = jest.fn();
      let onEnded = jest.fn();
      const logs: LogItem[] = [];

      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(instance => {
          instance.getLog$().subscribe(logItem => {
            logs.push(logItem);
          });
          instanceId = instance.getId();
          subscribeStub(instance);
          instance.onEnded(onEnded);
        });

        p.start();

        setTimeout(() => {
          expect(subscribeStub).toHaveBeenCalledTimes(1);
          expect(onEnded).toHaveBeenCalledTimes(1);

          expect(logs).toContainEqual(
            expect.objectContaining({
              message: expect.stringMatching(`processInstance ${instanceId} started.`)
            })
          );

          done();
        }, 150);
      });
    });

    describe('PARALLEL GATEWAY', () => {
      it('should wait at parallel gateway merge for all required incoming tokens', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_PARALLEL_GATEWAY).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            instance.onEnded(onEnded);
            thisInstance = instance;
          });

          p.startAt({ tokens: [{ tokenId: 'abc|123', currentFlowElementId: 'ParallelGateway_1u9g4km' }] });
          // start processinstance after parallel split -> only 1 token of required 3 will be at gateway merge

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(onEnded).toHaveBeenCalledTimes(0);
            const state = thisInstance.getState();

            // token at gateway
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                state: TokenState.READY,
                localStartTime: expect.any(Number),
                currentFlowElementId: 'ParallelGateway_1u9g4km', // merging gateway
                currentFlowElementStartTime: expect.any(Number),
                currentFlowNodeState: 'ACTIVE',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              }
            ]);

            // no done execution yet
            expect(state.log).toEqual([]);

            // processinstance does not end, still waiting for tokens at gateway merge
            expect(state.instanceState).toEqual(['READY']);
            done();
          }, 150);
        });
      });

      it('should place token and merge new flowNodeLog with existing', async done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        const p = await BpmnProcess.fromXml('p1', BPMN_XML_PARALLEL_GATEWAY);

        p.deploy();
        p.getInstance$().subscribe(instance => {
          subscribeStub(instance);
          instance.onEnded(onEnded);
          thisInstance = instance;
        });

        // start at task after parallel gateway, add flowNode-log
        p.startAt({
          tokens: [{ tokenId: 'abc|123', currentFlowElementId: 'Task_1p4tmqj' }],
          log: [
            {
              flowElementId: 'StartEvent_1',
              tokenId: 'abc',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'ParallelGateway_1gb46mc',
              tokenId: 'abc',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            }
          ]
        });

        setTimeout(() => {
          expect(subscribeStub).toHaveBeenCalledTimes(1);
          expect(onEnded).toHaveBeenCalledTimes(0);
          const state = thisInstance.getState();

          // token at gateway
          expect(state.tokens).toEqual([
            {
              tokenId: expect.any(String),
              state: TokenState.READY,
              localStartTime: expect.any(Number),
              currentFlowElementId: 'ParallelGateway_1u9g4km', // merging gateway
              currentFlowElementStartTime: expect.any(Number),
              currentFlowNodeState: 'ACTIVE',
              previousFlowElementId: 'SequenceFlow_0fsi00m',
              intermediateVariablesState: {},
              localExecutionTime: expect.any(Number)
            }
          ]);

          // initial flowNode-logs and executed task
          expect(state.log).toEqual([
            {
              flowElementId: 'StartEvent_1',
              tokenId: 'abc',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'ParallelGateway_1gb46mc',
              tokenId: 'abc',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'Task_1p4tmqj',
              tokenId: 'abc|123',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            }
          ]);

          // processinstance does not end, still waiting for tokens at gateway merge
          expect(state.instanceState).toEqual(['READY']);
          // place token to enable merging at parallel gateway
          const newExecutionLog = [
            {
              flowElementId: 'StartEvent_1',
              tokenId: 'abc',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'ParallelGateway_1gb46mc',
              tokenId: 'abc',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'Task_0j6jido',
              tokenId: 'abc|456',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            },
            {
              flowElementId: 'Task_0l2ayv1',
              tokenId: 'abc|789',
              executionState: ExecutionState.COMPLETED,
              startTime: expect.any(Number),
              endTime: expect.any(Number)
            }
          ];
          // merge new executionlog with existing
          thisInstance.mergeFlowNodeLog(newExecutionLog);

          // place missing tokens at gateway to continue execution
          thisInstance.placeTokenAt('SequenceFlow_0u19mqy', { tokenId: 'abc|456' });

          setTimeout(() => {
            thisInstance.placeTokenAt('SequenceFlow_10n4zv7', { tokenId: 'abc|789' });
          }, 0);

          setTimeout(() => {
            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: 'abc',
                state: TokenState.ENDED,
                localStartTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_02qfml5',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_0ssjqj4',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            // merged log
            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1',
                tokenId: 'abc',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ParallelGateway_1gb46mc',
                tokenId: 'abc',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1p4tmqj',
                tokenId: 'abc|123',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0j6jido',
                tokenId: 'abc|456',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0l2ayv1',
                tokenId: 'abc|789',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ParallelGateway_1u9g4km',
                tokenId: 'abc|789',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_02qfml5',
                tokenId: 'abc',
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);
            done();
          }, 150);
        }, 150);
      });

      it('should call onended when using parallel split and merge', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let onTokenEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;
        let shouldPassHook = jest.fn().mockImplementation(async () => true);

        BpmnProcess.fromXml('p1', BPMN_XML_PARALLEL_GATEWAY, { shouldPassTokenHook: shouldPassHook }).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onTokenEnded(onTokenEnded);
            instance.onEnded(onEnded);
            thisInstance = instance;
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(onTokenEnded).toHaveBeenCalledTimes(1);
            expect(shouldPassHook).toHaveBeenCalledTimes(9);
            const tokens = thisInstance.getState().tokens;
            expect(tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_02qfml5',
                currentFlowElementStartTime: expect.any(Number),
                currentFlowNodeState: 'COMPLETED',
                previousFlowElementId: 'SequenceFlow_0ssjqj4',
                state: TokenState.ENDED,
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            const instanceState = thisInstance.getState().instanceState;
            expect(instanceState).toEqual(['ENDED']);
            expect(onEnded).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should use certain paths when using parallel gateway with merge/join behaviour', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_PARALLEL_GATEWAY_MULTIPLE_INOUT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0epyvhv',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1il6zgb',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0xxe63e',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0z0w917',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ParallelGateway_171ay3q',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0wvzeht',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0enpu7i',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_05j6ajf',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ParallelGateway_1fjs5fh',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0f27ao8',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0co3kqh',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0epyvhv',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0xxe63e',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED']);

            done();
          }, 150);
        });
      });
    });

    describe('SCRIPT TASK', () => {
      it('should end with unhandled semantic error in script task', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_SEMANTIC_ERROR).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onEnded(onEnded);
            thisInstance = instance;
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              // token at script task
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                currentFlowElementId: 'Task_1egm6i7',
                currentFlowNodeState: 'FAILED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1wj8jmp',
                state: TokenState.ERROR_SEMANTIC,
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);
            // the interrupted script task should not have changed the instance variable state
            expect(state.variables).toStrictEqual({});
            expect(state.instanceState).toEqual(['ERROR-SEMANTIC']);
            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1egm6i7',
                tokenId: expect.any(String),
                executionState: ExecutionState.ERROR_SEMANTIC,
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                errorMessage: expect.any(String)
              }
            ]);
            expect(onEnded).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should have written to the global variable state of the instance if the setGlobal function was used', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_SEMANTIC_ERROR_GLOBAL_VARIABLE).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onEnded(onEnded);
            thisInstance = instance;
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            const state = thisInstance.getState();
            // the interrupted script task should have changed the instance variable state when the script task has called the global variant of the set function
            expect(state.variables).toStrictEqual({
              a: { value: 10, log: [{ changedBy: 'Task_1egm6i7', changedTime: expect.any(Number) }] }
            });
            expect(onEnded).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should get different values on variable.get and variable.getGlobal after using variable.set', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_ACCESS_GLOBAL_VARIABLE).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onEnded(onEnded);
            thisInstance = instance;
          });

          p.start({ variables: { a: { value: 20, log: [{ changedBy: 'init', changedTime: 0 }] } } });

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            const state = thisInstance.getState();
            expect(state.variables).toStrictEqual({
              a: {
                value: 10,
                log: [
                  {
                    changedBy: 'init',
                    changedTime: 0
                  },
                  {
                    changedBy: 'Task_1egm6i7',
                    changedTime: expect.any(Number),
                    oldValue: 20
                  }
                ]
              },
              b: {
                value: 30,
                log: [
                  {
                    changedBy: 'Task_1egm6i7',
                    changedTime: expect.any(Number)
                  }
                ]
              }
            });
            expect(onEnded).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should get different values on variable.getAll and variable.getGlobalAll after using variable.set', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_ACCESS_ALL_GLOBAL_VARIABLES).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onEnded(onEnded);
            thisInstance = instance;
          });

          p.start({
            variables: {
              a: { value: 20, log: [{ changedBy: 'init', changedTime: 0 }] },
              b: { value: 15, log: [{ changedBy: 'init', changedTime: 0 }] }
            }
          });

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            const state = thisInstance.getState();
            expect(state.variables).toStrictEqual({
              a: {
                value: 1,
                log: [
                  { changedBy: 'init', changedTime: 0 },
                  { changedBy: 'Task_1egm6i7', changedTime: expect.any(Number), oldValue: 20 }
                ]
              },
              b: {
                value: 1,
                log: [
                  { changedBy: 'init', changedTime: 0 },
                  { changedBy: 'Task_1egm6i7', changedTime: expect.any(Number), oldValue: 15 }
                ]
              },
              sum: { value: 2, log: [{ changedBy: 'Task_1egm6i7', changedTime: expect.any(Number) }] },
              globalSum: { value: 35, log: [{ changedBy: 'Task_1egm6i7', changedTime: expect.any(Number) }] }
            });
            expect(onEnded).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should execute error boundary event for semantic error in script task', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_ERROR_BOUNDARY_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onEnded(onEnded);
            thisInstance = instance;
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_01w8uee',
                currentFlowElementStartTime: expect.any(Number),
                currentFlowNodeState: 'COMPLETED',
                previousFlowElementId: 'Flow_1j8azpy',
                state: TokenState.ENDED,
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);
            // the interrupted script task should not have changed the instance variable state
            expect(state.variables).toStrictEqual({});
            expect(state.instanceState).toEqual(['ENDED']);
            expect(onEnded).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should execute interrupting escalation boundary event for semantic escalation in script task', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_INTERRUPTING_ESCALATION_BOUNDARY_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onEnded(onEnded);
            thisInstance = instance;
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_01w8uee',
                currentFlowElementStartTime: expect.any(Number),
                currentFlowNodeState: 'COMPLETED',
                previousFlowElementId: 'Flow_1j8azpy',
                state: TokenState.ENDED,
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);
            // the interrupted script task should not have changed the instance variable state
            expect(state.variables).toStrictEqual({});
            expect(state.instanceState).toEqual(['ENDED']);
            expect(onEnded).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should end with technical error in script task', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;
        let onScriptTaskError = jest.fn();

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_TECHNICAL_ERROR).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onEnded(onEnded);
            instance.onScriptTaskError(onScriptTaskError);
            thisInstance = instance;
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            const tokens = thisInstance.getState().tokens;
            expect(tokens).toEqual([
              // token at script task
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                currentFlowElementId: 'Task_1egm6i7',
                currentFlowElementStartTime: expect.any(Number),
                currentFlowNodeState: 'FAILED',
                previousFlowElementId: 'Flow_1wj8jmp',
                state: TokenState.ERROR_TECHNICAL,
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);
            const state = thisInstance.getState();
            // the interrupted script task should not have changed the instance variable state
            expect(state.variables).toStrictEqual({});
            expect(state.instanceState).toEqual(['ERROR-TECHNICAL']);
            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1egm6i7',
                tokenId: expect.any(String),
                executionState: ExecutionState.ERROR_TECHNICAL,
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                errorMessage: expect.any(String)
              }
            ]);
            expect(onEnded).toHaveBeenCalledTimes(1);
            expect(onScriptTaskError).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });
    });

    describe('INCLUSIVE GATEWAY', () => {
      it('should use certain paths when using inclusive gateway', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;
        let shouldPassHook = jest.fn().mockImplementation(async () => true);

        BpmnProcess.fromXml('p1', BPMN_XML_INCLUSIVE_GATEWAY, { shouldPassTokenHook: shouldPassHook }).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start({
            variables: {
              var1: {
                value: true
              },
              var2: {
                value: false
              },
              var3: {
                value: true
              },
              var4: {
                value: false
              }
            }
          });

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);
            expect(shouldPassHook).toHaveBeenCalledTimes(8);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_07rysvv',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_0swggtx',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'InclusiveGateway_1s9snq4', // split or-gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1i7zw55', // task when var1 is true
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0tndtci', // task when var3 is true
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0tzw41g', // following task when var1 is true
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0vldujn', // following task when var3 is true
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'InclusiveGateway_0qdieps', // join or-gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_07rysvv', // endevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED']);
            done();
          }, 150);
        });
      });
    });

    describe('EXCLUSIVE GATEWAY', () => {
      it('should use certain path when using exclusive gateway', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;
        let shouldPassHook = jest.fn().mockImplementation(async () => true);

        BpmnProcess.fromXml('p1', BPMN_XML_EXCLUSIVE_XOR_GATEWAY, { shouldPassTokenHook: shouldPassHook }).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start({
            variables: {
              hasGermanPassport: {
                value: true
              },
              age: {
                value: 17
              }
            }
          });

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);
            expect(shouldPassHook).toHaveBeenCalledTimes(6);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_142vabd',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_0hkmgev',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_0tmdz2f', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ExclusiveGateway_1577cb8', // split xor-gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1mlhorc', // task when hasGermanPassport is true
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ExclusiveGateway_0103oxb', // join xor-gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1uro9hz', // task when age < 18
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ExclusiveGateway_0mtspc3', // join gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_142vabd', // endevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED']);

            done();
          }, 150);
        });
      });

      it('should use certain paths when using exclusive gateway with merge/join behaviour', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_EXCLUSIVE_XOR_GATEWAY_MULTIPLE_INOUT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start({
            variables: {
              hasGermanPassport: {
                value: true
              },
              age: {
                value: 20
              }
            }
          });

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_1ve3kxo',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_08nke87',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ExclusiveGateway_1hdcxk3', // split xor-gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0tb6lfh', // task when age >= 18 && age <67
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ExclusiveGateway_068uetn', // join xor-gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1wr870c', // task when hasGermanPassport
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_1ve3kxo', // end event
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED']);

            done();
          }, 150);
        });
      });

      it('allows execution of simple loops', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;
        let shouldPassHook = jest.fn().mockImplementation(async () => true);

        BpmnProcess.fromXml('p1', BPMN_XML_EXCLUSIVE_XOR_GATEWAY_SIMPLE_LOOP, {
          shouldPassTokenHook: shouldPassHook
        }).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start({
            variables: {
              invocations: {
                value: 0
              }
            }
          });

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);
            expect(shouldPassHook).toHaveBeenCalledTimes(7);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'Event_1nb6v76',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_19qa7a8',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_0l3khfx', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Gateway_1ydo9ch', // split xor-gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_1l4zfsg', // task when hasGermanPassport is true
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Gateway_04kczqf', // join xor-gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Gateway_1ydo9ch', // task when age < 18
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Activity_1l4zfsg', // join gateway
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Gateway_04kczqf', // endevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Event_1nb6v76', // endevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED']);

            done();
          }, 150);
        });
      });
    });

    describe('SUBPROCESS', () => {
      it('should execute alternative execution flow when error boundary event at subprocess gets triggered', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SUBPROCESS_ERROR_BOUNDARY_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'FAILED',
                currentFlowElementId: 'EndEvent_0a1l09r',
                currentFlowNodeState: 'FAILED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0qsihfu',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0nvh0en',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_13z0f3n',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'StartEvent_007g88m', // startevent of subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_10tcyso', // task inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'BoundaryEvent_024dao6', // boundaryevent on subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Subprocess_18kg880', // subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.FAILED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0a1l09r', // endevent inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.FAILED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0344sje', // task after boundaryevent
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0nvh0en', // endevent after boundaryevent
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['FAILED', 'ENDED']);
            done();
          }, 2500);
        });
      });

      it('should execute alternative execution flow when non-interrupting timer boundary event at subprocess gets triggered', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SUBPROCESS_NON_INTERRUPTING_TIMER_BOUNDARY_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0zoa1gk',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0lcnd5t',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0zhdpvu',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0edtmf1',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0gwzjkj',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1hcaka0',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'StartEvent_0s64s70', // startevent inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'BoundaryEvent_1fxt72p', // boundaryevent at subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0ke18vq', // task after boundary event
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0gwzjkj', // end event after boundary event
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_09azc5q', // task inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0zhdpvu', // endevent inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Subprocess_1mvfe17', // subprocess itself
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0zoa1gk', // endevent after subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED']);
            done();
          }, 2500);
        });
      });

      it('should execute alternative execution flow when interrupting timer boundary event at subprocess gets triggered', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SUBPROCESS_INTERRUPTING_TIMER_BOUNDARY_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'TERMINATED',
                currentFlowElementId: 'Task_09azc5q',
                currentFlowNodeState: 'TERMINATED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0od8cnw',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0gwzjkj',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1hcaka0',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'StartEvent_0s64s70', // startevent inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'BoundaryEvent_1fxt72p', // boundaryevent at subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Subprocess_1mvfe17', // subprocess itself
                tokenId: expect.any(String),
                executionState: ExecutionState.TERMINATED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_09azc5q', // task inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.TERMINATED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0ke18vq', // task after boundary event
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0gwzjkj', // end event after boundary event
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['TERMINATED', 'ENDED']);
            done();
          }, 2500);
        });
      });

      it('should execute alternative execution flow when interrupting escalation boundary event at subprocess gets triggered', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SUBPROCESS_INTERRUPTING_ESCALATION_BOUNDARY_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'TERMINATED',
                currentFlowElementId: 'ThrowingEvent_1o2e395',
                currentFlowNodeState: 'TERMINATED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0g46voo',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0ynbzkf',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0fyplyh',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'StartEvent_190w8v7', // startevent inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0gnmest', // task inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'BoundaryEvent_095s9sb', // boundary event on subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Subprocess_1in1j2j', // aborted subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.TERMINATED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ThrowingEvent_1o2e395', // throwing escalation inside subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.TERMINATED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1k7sd5q', // task after boundary event
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0ynbzkf', // end event after boundary event
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['TERMINATED', 'ENDED']);
            done();
          }, 150);
        });
      });

      it('should call onTokenEnded every time token reaches endEvent when using subprocess', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let onTokenEnded = jest.fn();

        BpmnProcess.fromXml('p1', BPMN_XML_EMBEDDED_SUBPROCESS).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            instance.onEnded(onEnded);
            instance.onTokenEnded(onTokenEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(onTokenEnded).toHaveBeenCalledTimes(2);
            expect(onEnded).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should abort every running token of subprocess if terminate end event entered', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_TERMINATED_SUBPROCESS).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);

            const state = thisInstance.getState();
            expect(state.tokens).toStrictEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0j72evn',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_14ai8no',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ABORTED',
                currentFlowElementId: 'Subprocess_04qvd0i',
                currentFlowNodeState: 'TERMINATED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_012yafq',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_01ss3hf',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_19vq393',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            expect(state.log).toStrictEqual([
              {
                flowElementId: 'StartEvent_1', // startevent of process
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'StartEvent_141t3ys', // startevent of subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ParallelGateway_10ecdd3', // parallel gateway of subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_1hf4n8k', // task of subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_01ss3hf', // terminate-endevent of subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Subprocess_0fg8x7f', // subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.ABORTED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Subprocess_04qvd0i', // inner subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.ABORTED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0gw0mdy', // task after subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0j72evn', // endevent after subprocess
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED', 'ABORTED']);

            done();
          }, 150);
        });
      });

      it('should not complete the subprocess again while aborting the other tokens inside it', async done => {
        const process = await BpmnProcess.fromXml('p1', BPMN_XML_TERMINATED_SUBPROCESS_TRIPLE_SPLIT, {
          shouldActivateFlowNodeHook: async (processId, instanceId, tokenId, flowNode) => {
            if (flowNode.id === 'EndEvent_1npa2ar') {
              // make sure to wait for the other tasks to be reached
              await new Promise(resolve =>
                setTimeout(() => {
                  resolve(true);
                }, 50)
              );

              setTimeout(() => {
                const instance = process.getInstanceById(instanceId);
                const state = instance.getState();

                expect(state.tokens).toStrictEqual([
                  {
                    tokenId: expect.any(String),
                    localStartTime: expect.any(Number),
                    state: 'ENDED',
                    currentFlowElementId: 'EndEvent_0wsgk5o',
                    currentFlowNodeState: 'COMPLETED',
                    currentFlowElementStartTime: expect.any(Number),
                    previousFlowElementId: 'SequenceFlow_0k92bg2',
                    localExecutionTime: expect.any(Number),
                    intermediateVariablesState: null
                  },
                  {
                    tokenId: expect.any(String),
                    localStartTime: expect.any(Number),
                    state: 'ABORTED',
                    currentFlowElementId: 'Task_0c7ed5k',
                    currentFlowNodeState: 'TERMINATED',
                    currentFlowElementStartTime: expect.any(Number),
                    previousFlowElementId: 'SequenceFlow_1fiv3is',
                    localExecutionTime: expect.any(Number),
                    intermediateVariablesState: null
                  },
                  {
                    tokenId: expect.any(String),
                    localStartTime: expect.any(Number),
                    state: 'ABORTED',
                    currentFlowElementId: 'Task_0if8lmi',
                    currentFlowNodeState: 'TERMINATED',
                    currentFlowElementStartTime: expect.any(Number),
                    previousFlowElementId: 'SequenceFlow_0l7asdj',
                    localExecutionTime: expect.any(Number),
                    intermediateVariablesState: null
                  },
                  {
                    tokenId: expect.any(String),
                    localStartTime: expect.any(Number),
                    state: 'ENDED',
                    currentFlowElementId: 'EndEvent_1npa2ar',
                    currentFlowNodeState: 'COMPLETED',
                    currentFlowElementStartTime: expect.any(Number),
                    previousFlowElementId: 'SequenceFlow_1k0vqr2',
                    localExecutionTime: expect.any(Number),
                    intermediateVariablesState: null
                  }
                ]);

                expect(state.log.length).toBe(8);
                expect(state.log).toStrictEqual([
                  {
                    flowElementId: 'StartEvent_1', // startevent of process
                    tokenId: expect.any(String),
                    executionState: ExecutionState.COMPLETED,
                    startTime: expect.any(Number),
                    endTime: expect.any(Number)
                  },
                  {
                    flowElementId: 'StartEvent_1og4rvt', // startevent of subprocess
                    tokenId: expect.any(String),
                    executionState: ExecutionState.COMPLETED,
                    startTime: expect.any(Number),
                    endTime: expect.any(Number)
                  },
                  {
                    flowElementId: 'ExclusiveGateway_1i43e7i', // parallel gateway of subprocess
                    tokenId: expect.any(String),
                    executionState: ExecutionState.COMPLETED,
                    startTime: expect.any(Number),
                    endTime: expect.any(Number)
                  },
                  {
                    flowElementId: 'EndEvent_1npa2ar', // terminate-endevent of subprocess
                    tokenId: expect.any(String),
                    executionState: ExecutionState.COMPLETED,
                    startTime: expect.any(Number),
                    endTime: expect.any(Number)
                  },
                  {
                    flowElementId: 'SubProcess_0phiv1k', // subprocess
                    tokenId: expect.any(String),
                    executionState: ExecutionState.ABORTED,
                    startTime: expect.any(Number),
                    endTime: expect.any(Number)
                  },
                  {
                    flowElementId: 'Task_0c7ed5k', // first user task in subprocess
                    tokenId: expect.any(String),
                    executionState: ExecutionState.ABORTED,
                    startTime: expect.any(Number),
                    endTime: expect.any(Number)
                  },
                  {
                    flowElementId: 'Task_0if8lmi', // second user task in subprocess
                    tokenId: expect.any(String),
                    executionState: ExecutionState.ABORTED,
                    startTime: expect.any(Number),
                    endTime: expect.any(Number)
                  },
                  {
                    flowElementId: 'EndEvent_0wsgk5o', // endevent after subprocess
                    tokenId: expect.any(String),
                    executionState: ExecutionState.COMPLETED,
                    startTime: expect.any(Number),
                    endTime: expect.any(Number)
                  }
                ]);

                done();
              }, 50);
            }

            return true;
          }
        });

        process.deployAndStart();
      });
    });

    describe('BOUNDARY EVENT', () => {
      it('should execute alternative execution flow when interrupting timer boundary event gets triggered', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let onTokenEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_INTERRUPTING_TIMER_BOUNDARY_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
            instance.onTokenEnded(onTokenEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);
            expect(onTokenEnded).toHaveBeenCalledTimes(2);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0umagrf',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0t5xhnp',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            // variables changed by the interrupted script task should not have been written to the instance
            expect(state.variables).toStrictEqual({
              a: { value: 10, log: [{ changedTime: expect.any(Number), changedBy: 'Task_1romxxp' }] }
            });

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'Task_1romxxp',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'BoundaryEvent_14r9ipi',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'Task_1wg4yqd',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.TERMINATED
              },
              {
                flowElementId: 'Task_1fgmhtf',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'EndEvent_0umagrf',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED']);
            done();
          }, 2500);
        });
      });

      it('should execute alternative execution flow when non-interrupting timer boundary event gets triggered', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let onTokenEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_NON_INTERRUPTING_TIMER_BOUNDARY_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            thisInstance = instance;
            instance.onEnded(onEnded);
            instance.onTokenEnded(onTokenEnded);
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(p.getAllInstances()).toHaveLength(1);
            expect(onEnded).toHaveBeenCalledTimes(1);
            expect(onTokenEnded).toHaveBeenCalledTimes(2);

            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0e6skps',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_1kml794',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0umagrf',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'Flow_0t5xhnp',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);

            // since the script task was not interrupted its variable changes should be written to the instance
            expect(state.variables).toStrictEqual({
              a: { value: 10, log: [{ changedTime: expect.any(Number), changedBy: 'Task_1romxxp' }] },
              b: { value: 300, log: [{ changedTime: expect.any(Number), changedBy: 'Task_1wg4yqd' }] }
            });

            expect(state.log).toEqual([
              {
                flowElementId: 'StartEvent_1',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'Task_1romxxp',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'BoundaryEvent_14r9ipi',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'Task_1fgmhtf',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'EndEvent_0umagrf',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'Task_1wg4yqd',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'Task_03dall0',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              },
              {
                flowElementId: 'EndEvent_0e6skps',
                tokenId: expect.any(String),
                startTime: expect.any(Number),
                endTime: expect.any(Number),
                executionState: ExecutionState.COMPLETED
              }
            ]);

            expect(state.instanceState).toEqual(['ENDED']);
            done();
          }, 2500);
        });
      });
    });

    describe('TERMINATE END EVENT', () => {
      it('should end process when a token reaches terminate end event', done => {
        const subscribeStub = jest.fn();
        let onAborted = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_TERMINATE_END_EVENT).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub();
            instance.onAborted(onAborted);
            thisInstance = instance;
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            const tokens = thisInstance.getState().tokens;
            expect(tokens).toEqual([
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                currentFlowElementId: 'EndEvent_0x3jocc',
                currentFlowElementStartTime: expect.any(Number),
                currentFlowNodeState: 'COMPLETED',
                previousFlowElementId: 'Flow_04yeb4k',
                state: TokenState.ENDED,
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                currentFlowElementId: 'IntermediateCatchEvent_103sfzn',
                currentFlowElementStartTime: expect.any(Number),
                currentFlowNodeState: 'TERMINATED',
                previousFlowElementId: 'Flow_0k1si90',
                state: TokenState.ABORTED,
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);
            const log = thisInstance.getState().log;
            expect(log).toEqual([
              {
                flowElementId: 'StartEvent_1',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'ParallelGateway_198xm9v',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'Task_0a5ear3',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'EndEvent_0x3jocc',
                tokenId: expect.any(String),
                executionState: ExecutionState.COMPLETED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              },
              {
                flowElementId: 'IntermediateCatchEvent_103sfzn',
                tokenId: expect.any(String),
                executionState: ExecutionState.ABORTED,
                startTime: expect.any(Number),
                endTime: expect.any(Number)
              }
            ]);
            const instanceState = thisInstance.getState().instanceState;
            expect(instanceState).toEqual(['ENDED', 'ABORTED']);
            expect(onAborted).toHaveBeenCalledTimes(1);
            done();
          }, 150);
        });
      });

      it('should call onTokenEnded every time token reaches endEvent', done => {
        const subscribeStub = jest.fn();
        let onEnded = jest.fn();
        let onTokenEnded = jest.fn();
        let thisInstance: BpmnProcessInstance;

        BpmnProcess.fromXml('p1', BPMN_XML_MULTIPLE_END_EVENTS).then(p => {
          p.deploy();
          p.getInstance$().subscribe(instance => {
            subscribeStub(instance);
            instance.onEnded(onEnded);
            instance.onTokenEnded(onTokenEnded);
            thisInstance = instance;
          });

          p.start();

          setTimeout(() => {
            expect(subscribeStub).toHaveBeenCalledTimes(1);
            expect(onTokenEnded).toHaveBeenCalledTimes(2);
            expect(onEnded).toHaveBeenCalledTimes(1);
            const state = thisInstance.getState();
            expect(state.tokens).toEqual([
              // two end events -> token in each end event
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_0a4ljay',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_0elmfim',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              },
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'ENDED',
                currentFlowElementId: 'EndEvent_018jvql',
                currentFlowNodeState: 'COMPLETED',
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_0p6dlnq',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: null
              }
            ]);
            expect(state.instanceState).toEqual(['ENDED']);
            done();
          }, 150);
        });
      });
    });
  });

  describe('message start', () => {
    it('should throw for undefined messageRef', async () => {
      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_MESSAGE_START_EVENT);
      p.deploy();
      expect(() => p.start()).toThrowError(NoStartEventSpecifiedError);
    });

    it('should throw for non-existent messageRef', done => {
      const onInstanceCreation = jest.fn();
      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_MESSAGE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(() => onInstanceCreation());
        expect(() => p.start({ messageRef: 'noMessage' })).toThrowError(MessageRefNotFoundError);
        setTimeout(() => {
          expect(onInstanceCreation).not.toHaveBeenCalled();
          done();
        }, 25);
      });
    });

    it('should start process for existent messageRef', done => {
      const onInstanceCreation = jest.fn();
      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_MESSAGE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(() => onInstanceCreation());
        expect(() => p.start({ messageRef: 'startMessage1' })).not.toThrow();
        setTimeout(() => {
          expect(onInstanceCreation).toHaveBeenCalledTimes(1);
          done();
        }, 25);
      });
    });
  });

  describe('signal start', () => {
    it('should throw for undefined signalRef', async () => {
      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_SINGLE_START_EVENT);
      p.deploy();
      expect(() => p.start()).toThrowError(NoStartEventSpecifiedError);
    });

    it('should throw for non-existent signalRef', done => {
      const onInstanceCreation = jest.fn();
      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_SINGLE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(() => onInstanceCreation());
        expect(() => p.start({ signalRef: 'noSignal' })).toThrowError(SignalRefNotFoundError);
        setTimeout(() => {
          expect(onInstanceCreation).not.toHaveBeenCalled();
          done();
        }, 25);
      });
    });

    it('should start process for existent signalRef', done => {
      const onInstanceCreation = jest.fn();
      BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_SINGLE_START_EVENT).then(p => {
        p.deploy();
        p.getInstance$().subscribe(() => onInstanceCreation());
        expect(() => p.start({ signalRef: 'startSignal1' })).not.toThrow();
        setTimeout(() => {
          expect(onInstanceCreation).toHaveBeenCalledTimes(1);
          done();
        }, 25);
      });
    });
  });

  describe('duration timer start', () => {
    it('should auto-start instances on deploy', async () => {
      const instanceStub = jest.fn();
      const scheduler = new VirtualTimeScheduler(VirtualAction, 10000);

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_DURATION_TIMER_START_EVENT);

      p.__set_deploy_scheduler(scheduler);

      p.getInstance$().subscribe(() => {
        instanceStub();
      });

      p.deploy();
      scheduler.flush();
      expect(instanceStub).toHaveBeenCalledTimes(3);
      p.__restore_deploy_scheduler();
    });

    it('calling undeploy should prevent any scheduled deploys', async () => {
      const instanceStub = jest.fn();
      const scheduler = new VirtualTimeScheduler(VirtualAction, 10000);

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_DURATION_TIMER_START_EVENT);

      p.__set_deploy_scheduler(scheduler);

      p.getInstance$().subscribe(() => {
        instanceStub();
      });

      p.deploy();

      // Undeploy at 5 second mark
      interval(5000, scheduler).subscribe(() => {
        p.undeploy();
      });

      scheduler.flush();
      expect(instanceStub).toHaveBeenCalledTimes(2);

      p.__restore_deploy_scheduler();
    });
  });

  describe('start at an arbitrary element', () => {
    it('should throw for invalid id', async () => {
      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT);
      p.deploy();
      expect(() => p.startAt({ tokens: [{ currentFlowElementId: 'xyz' }] })).toThrowError(BpmnElementNotFoundError);
    });

    it('should not throw if an existent flowNode id is supplied', async () => {
      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT);
      p.deploy();
      expect(() => p.startAt({ tokens: [{ currentFlowElementId: 'Task_175vb4p' }] })).not.toThrow();
    });

    it('should return the started instance', async () => {
      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT);
      p.deploy();
      const instance = p.startAt({ tokens: [{ currentFlowElementId: 'Task_175vb4p' }] });
      expect(p.getAllInstances()[0]).toBe(instance);
    });

    it('should not throw if an existent sequenceFlow id is supplied', async () => {
      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT);
      p.deploy();
      expect(() => p.startAt({ tokens: [{ currentFlowElementId: 'SequenceFlow_07so23a' }] })).not.toThrow();
    });
  });

  describe('start at a partially executed state', () => {
    it('allows an instance that was partially executed to be continued', async done => {
      let taskActivations = 0;

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT, {
        shouldActivateFlowNodeHook: async (pId, pIId, tId, flowNode, _) => {
          if (flowNode.id === 'Task_175vb4p' && taskActivations === 0) {
            const interruptionState = p.getInstanceById(pIId).getState();

            p.deleteInstanceById(pIId);
            expect(p.getAllInstances().length).toBe(0);

            expect(() =>
              p.startAt({
                // To retrigger the execution logic of an element we have to put the token back into the READY state
                tokens: interruptionState.tokens.map(t => ({ ...t, state: TokenState.READY })),
                variables: interruptionState.variables,
                instanceId: interruptionState.processInstanceId,
                globalStartTime: interruptionState.globalStartTime,
                log: interruptionState.log
              })
            ).not.toThrow();

            // check that the instance was successfully added back
            expect(p.getAllInstances().length).toBe(1);
            expect(p.getInstanceById(pIId).getState()).toEqual({
              ...interruptionState,
              instanceState: ['READY'],
              tokens: [
                {
                  ...interruptionState.tokens[0],
                  state: 'READY'
                }
              ]
            });

            setTimeout(() => {
              // check that the instance was able to finish correctly
              expect(p.getAllInstances().length).toBe(1);

              const [instance] = p.getAllInstances();
              const finalState = instance.getState();

              expect(finalState).toStrictEqual({
                ...interruptionState,
                instanceState: ['ENDED'],
                tokens: [
                  {
                    ...interruptionState.tokens[0],
                    state: 'ENDED',
                    previousFlowElementId: 'SequenceFlow_12bv6av',
                    currentFlowElementId: 'EndEvent_1umdfcw',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'COMPLETED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  }
                ],
                log: [
                  ...interruptionState.log,
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Task_175vb4p',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EndEvent_1umdfcw',
                    tokenId: tId
                  }
                ]
              });
              done();
            }, 100);

            ++taskActivations;
            return false;
          }

          return true;
        }
      });
      p.deploy();

      p.start();
    });

    it('allows an instance with a subprocess that was partially executed to be continued; timer boundary wont interrupt if the subprocess finishes in time', async done => {
      let taskActivations = 0;

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SUBPROCESS_UNTRIGGERED_INTERRUPTING_TIMER_BOUNDARY_EVENT, {
        shouldActivateFlowNodeHook: async (pId, pIId, tId, flowNode, _) => {
          if (flowNode.id === 'Task_09azc5q' && taskActivations === 0) {
            const interruptionState = p.getInstanceById(pIId).getState();

            p.deleteInstanceById(pIId);
            expect(p.getAllInstances().length).toBe(0);

            expect(() =>
              p.startAt({
                tokens: interruptionState.tokens.map(t => {
                  if (t.currentFlowElementId !== 'Subprocess_1mvfe17') {
                    // To retrigger the execution logic of an element we have to put the token back into the READY state
                    // We DONT want to retrigger the subprocess since continuing the contained tokens should be enough to continue its execution
                    return { ...t, state: 'READY' };
                  }

                  return t;
                }),
                variables: interruptionState.variables,
                instanceId: interruptionState.processInstanceId,
                globalStartTime: interruptionState.globalStartTime,
                log: interruptionState.log
              })
            ).not.toThrow();

            setTimeout(() => {
              expect(p.getAllInstances().length).toBe(1);

              const [instance] = p.getAllInstances();
              const finalState = instance.getState();

              expect(finalState).toStrictEqual({
                ...interruptionState,
                instanceState: ['ENDED'],
                tokens: [
                  {
                    ...interruptionState.tokens[0],
                    state: 'ENDED',
                    previousFlowElementId: 'Flow_0lcnd5t',
                    currentFlowElementId: 'EndEvent_0zoa1gk',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'COMPLETED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  },
                  {
                    ...interruptionState.tokens[1],
                    state: 'ENDED',
                    previousFlowElementId: 'Flow_0edtmf1',
                    currentFlowElementId: 'EndEvent_0zhdpvu',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'COMPLETED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  }
                ],
                log: [
                  ...interruptionState.log,
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Task_09azc5q',
                    tokenId: interruptionState.tokens[1].tokenId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EndEvent_0zhdpvu',
                    tokenId: interruptionState.tokens[1].tokenId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Subprocess_1mvfe17',
                    tokenId: interruptionState.tokens[0].tokenId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EndEvent_0zoa1gk',
                    tokenId: interruptionState.tokens[0].tokenId
                  }
                ]
              });
              done();
            }, 1000);

            ++taskActivations;
            return false;
          }

          return true;
        }
      });
      p.deploy();

      p.start();
    });

    it('allows an instance with a subprocess that was partially executed to be continued; timer boundary will interrupt if the execution of the subprocess including the time the instance was not running exceeds its timer', async done => {
      let taskActivations = 0;

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SUBPROCESS_UNTRIGGERED_INTERRUPTING_TIMER_BOUNDARY_EVENT, {
        shouldActivateFlowNodeHook: async (pId, pIId, tId, flowNode, _) => {
          if (flowNode.id === 'Task_09azc5q' && taskActivations === 0) {
            const interruptionState = p.getInstanceById(pIId).getState();

            p.deleteInstanceById(pIId);
            expect(p.getAllInstances().length).toBe(0);

            await new Promise(resolve => setTimeout(resolve, 600));

            expect(() =>
              p.startAt({
                tokens: interruptionState.tokens.map(t => {
                  if (t.currentFlowElementId !== 'Subprocess_1mvfe17') {
                    // To retrigger the execution logic of an element we have to put the token back into the READY state
                    // We DONT want to retrigger the subprocess since continuing the contained tokens should be enough to continue its execution
                    return { ...t, state: 'READY' };
                  }

                  return t;
                }),
                variables: interruptionState.variables,
                instanceId: interruptionState.processInstanceId,
                globalStartTime: interruptionState.globalStartTime,
                log: interruptionState.log
              })
            ).not.toThrow();

            setTimeout(() => {
              expect(p.getAllInstances().length).toBe(1);

              const [instance] = p.getAllInstances();
              const finalState = instance.getState();

              expect(finalState).toStrictEqual({
                ...interruptionState,
                instanceState: ['TERMINATED', 'ENDED'],
                tokens: [
                  {
                    ...interruptionState.tokens[1],
                    state: 'TERMINATED',
                    previousFlowElementId: 'Flow_0od8cnw',
                    currentFlowElementId: 'Task_09azc5q',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'TERMINATED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  },
                  {
                    ...interruptionState.tokens[1],
                    tokenId: expect.any(String),
                    state: 'ENDED',
                    previousFlowElementId: 'Flow_1hcaka0',
                    currentFlowElementId: 'EndEvent_0gwzjkj',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'COMPLETED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  }
                ],
                log: [
                  ...interruptionState.log,
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'BoundaryEvent_1fxt72p',
                    tokenId: expect.any(String)
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'TERMINATED',
                    flowElementId: 'Subprocess_1mvfe17',
                    tokenId: interruptionState.tokens[0].tokenId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'TERMINATED',
                    flowElementId: 'Task_09azc5q',
                    tokenId: interruptionState.tokens[1].tokenId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Task_0ke18vq',
                    tokenId: expect.any(String)
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EndEvent_0gwzjkj',
                    tokenId: expect.any(String)
                  }
                ]
              });
              done();
            }, 1000);

            ++taskActivations;
            return false;
          }

          return true;
        }
      });
      p.deploy();

      p.start();
    });

    it('allows an instance with a subprocess that was partially executed to be continued; timer boundary will interrupt if the time in which the instance was not running exceeds its timer', async done => {
      let taskActivations = 0;

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_SUBPROCESS_UNTRIGGERED_INTERRUPTING_TIMER_BOUNDARY_EVENT, {
        shouldActivateFlowNodeHook: async (pId, pIId, tId, flowNode, _) => {
          if (flowNode.id === 'Task_09azc5q' && taskActivations === 0) {
            const interruptionState = p.getInstanceById(pIId).getState();

            p.deleteInstanceById(pIId);
            expect(p.getAllInstances().length).toBe(0);

            await sleep(1100);

            expect(() =>
              p.startAt({
                tokens: interruptionState.tokens.map(t => {
                  if (t.currentFlowElementId !== 'Subprocess_1mvfe17') {
                    // To retrigger the execution logic of an element we have to put the token back into the READY state
                    // We DONT want to retrigger the subprocess since continuing the contained tokens should be enough to continue its execution
                    return { ...t, state: 'READY' };
                  }

                  return t;
                }),
                variables: interruptionState.variables,
                instanceId: interruptionState.processInstanceId,
                globalStartTime: interruptionState.globalStartTime,
                log: interruptionState.log
              })
            ).not.toThrow();

            setTimeout(() => {
              expect(p.getAllInstances().length).toBe(1);

              const [instance] = p.getAllInstances();
              const finalState = instance.getState();

              expect(finalState).toStrictEqual({
                ...interruptionState,
                instanceState: ['TERMINATED', 'ENDED'],
                tokens: [
                  {
                    ...interruptionState.tokens[1],
                    state: 'TERMINATED',
                    previousFlowElementId: 'Flow_0od8cnw',
                    currentFlowElementId: 'Task_09azc5q',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'TERMINATED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  },
                  {
                    ...interruptionState.tokens[1],
                    tokenId: expect.any(String),
                    state: 'ENDED',
                    previousFlowElementId: 'Flow_1hcaka0',
                    currentFlowElementId: 'EndEvent_0gwzjkj',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'COMPLETED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  }
                ],
                log: [
                  ...interruptionState.log,
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'BoundaryEvent_1fxt72p',
                    tokenId: expect.any(String)
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'TERMINATED',
                    flowElementId: 'Subprocess_1mvfe17',
                    tokenId: interruptionState.tokens[0].tokenId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'TERMINATED',
                    flowElementId: 'Task_09azc5q',
                    tokenId: interruptionState.tokens[1].tokenId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Task_0ke18vq',
                    tokenId: expect.any(String)
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EndEvent_0gwzjkj',
                    tokenId: expect.any(String)
                  }
                ]
              });
              done();
            }, 1000);

            ++taskActivations;
            return false;
          }

          return true;
        }
      });
      p.deploy();

      p.start();
    });

    it('allows intermediate events to trigger after a restart', async done => {
      let activations: { [id: string]: number } = {
        IntermediateCatchEvent_0wpudxa: 0,
        IntermediateCatchEvent_0o02gmp: 0,
        IntermediateCatchEvent_09pxge5: 0
      };

      let interruptionState: ExportedProcessState;

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_INTERMEDIATE_CATCH, {
        shouldActivateFlowNodeHook: async (pId, pIId, tId, flowNode, _) => {
          if (activations[flowNode.id] === 0) {
            expect(p.getAllInstances().length).toBe(1);
            const instance = p.getInstanceById(pIId);
            const currentState = instance.getState();

            if (flowNode.id === 'IntermediateCatchEvent_0wpudxa') {
              setTimeout(() => {
                p.getInstanceById(pIId).receiveMessage('Message_1m7hrlz');
              }, 10);
            }

            if (flowNode.id === 'IntermediateCatchEvent_0o02gmp') {
              expect(currentState).toStrictEqual({
                ...interruptionState,
                instanceState: ['RUNNING'],
                tokens: [
                  {
                    ...interruptionState.tokens[0],
                    state: 'RUNNING',
                    previousFlowElementId: 'SequenceFlow_14aau7v',
                    currentFlowElementId: 'IntermediateCatchEvent_0o02gmp',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'READY',
                    intermediateVariablesState: {},
                    localExecutionTime: expect.any(Number)
                  }
                ],
                log: [
                  ...interruptionState.log,
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'IntermediateCatchEvent_0wpudxa',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Task_0twpunq',
                    tokenId: tId
                  }
                ]
              });

              setTimeout(() => {
                p.getInstanceById(pIId).receiveSignal('Signal_1gcpp7v');
              }, 10);
            }

            if (flowNode.id === 'IntermediateCatchEvent_09pxge5') {
              expect(currentState).toStrictEqual({
                ...interruptionState,
                instanceState: ['RUNNING'],
                tokens: [
                  {
                    ...interruptionState.tokens[0],
                    state: 'RUNNING',
                    previousFlowElementId: 'SequenceFlow_0b8wb69',
                    currentFlowElementId: 'IntermediateCatchEvent_09pxge5',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'READY',
                    intermediateVariablesState: {},
                    localExecutionTime: expect.any(Number)
                  }
                ],
                log: [
                  ...interruptionState.log,
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'IntermediateCatchEvent_0o02gmp',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Task_017n45t',
                    tokenId: tId
                  }
                ]
              });

              setTimeout(() => {
                const instance = p.getInstanceById(pIId);
                const currentState = instance.getState();

                expect(currentState).toStrictEqual({
                  ...interruptionState,
                  instanceState: ['ENDED'],
                  tokens: [
                    {
                      ...interruptionState.tokens[0],
                      state: 'ENDED',
                      previousFlowElementId: 'SequenceFlow_08u409x',
                      currentFlowElementId: 'EndEvent_03gcun2',
                      currentFlowElementStartTime: expect.any(Number),
                      currentFlowNodeState: 'COMPLETED',
                      intermediateVariablesState: null,
                      localExecutionTime: expect.any(Number)
                    }
                  ],
                  log: [
                    ...interruptionState.log,
                    {
                      startTime: expect.any(Number),
                      endTime: expect.any(Number),
                      executionState: 'COMPLETED',
                      flowElementId: 'IntermediateCatchEvent_09pxge5',
                      tokenId: tId
                    },
                    {
                      startTime: expect.any(Number),
                      endTime: expect.any(Number),
                      executionState: 'COMPLETED',
                      flowElementId: 'Task_141i1em',
                      tokenId: tId
                    },
                    {
                      startTime: expect.any(Number),
                      endTime: expect.any(Number),
                      executionState: 'COMPLETED',
                      flowElementId: 'EndEvent_03gcun2',
                      tokenId: tId
                    }
                  ]
                });

                // check that the event did not wait longer than 2 seconds even with the instance being stopped intermediatly
                const timerEventLog = currentState.log.find(
                  entry => entry.flowElementId === 'IntermediateCatchEvent_09pxge5'
                );
                // we can't use exact equality since the logic that runs between the timer ending and the log being written with the endTime may take a few milliseconds
                expect((timerEventLog?.endTime! - timerEventLog?.startTime!) / 1000).toBeCloseTo(2, 1);

                done();
              }, 2100);
            }

            interruptionState = currentState;

            p.deleteInstanceById(pIId);
            expect(p.getAllInstances().length).toBe(0);

            // wait some time before continuing the instance when at the timer event
            if (flowNode.id === 'IntermediateCatchEvent_09pxge5') {
              await sleep(1500);
            }

            expect(() =>
              p.startAt({
                // To retrigger the execution logic of an element we have to put the token back into the READY state
                tokens: currentState.tokens.map(t => ({ ...t, state: TokenState.READY })),
                variables: currentState.variables,
                instanceId: currentState.processInstanceId,
                globalStartTime: currentState.globalStartTime,
                log: currentState.log
              })
            ).not.toThrow();

            ++activations[flowNode.id];
            return false;
          }

          return true;
        }
      });
      p.deploy();

      p.start();
    });

    it('allows event gateways to resume after the instance was imported; if the instance is resumed before timers have run out a message event path can be chosen', async done => {
      let taskActivations = 0;

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_EVENT_BASED_GATEWAY, {
        shouldActivateFlowNodeHook: async (pId, pIId, tId, flowNode, _) => {
          if (flowNode.id === 'EventBasedGateway_1b2kp9o' && taskActivations === 0) {
            const interruptionState = p.getInstanceById(pIId).getState();

            p.deleteInstanceById(pIId);
            expect(p.getAllInstances().length).toBe(0);

            expect(() =>
              p.startAt({
                // To retrigger the execution logic of an element we have to put the token back into the READY state
                tokens: interruptionState.tokens.map(t => ({ ...t, state: TokenState.READY })),
                variables: interruptionState.variables,
                instanceId: interruptionState.processInstanceId,
                globalStartTime: interruptionState.globalStartTime,
                log: interruptionState.log
              })
            ).not.toThrow();

            setTimeout(() => {
              p.getInstanceById(pIId).receiveMessage('Message_0io414n');
            });

            setTimeout(() => {
              expect(p.getAllInstances().length).toBe(1);

              const [instance] = p.getAllInstances();
              const finalState = instance.getState();

              expect(finalState).toStrictEqual({
                ...interruptionState,
                instanceState: ['ENDED'],
                tokens: [
                  {
                    ...interruptionState.tokens[0],
                    state: 'ENDED',
                    previousFlowElementId: 'SequenceFlow_1361hlc',
                    currentFlowElementId: 'EndEvent_1ssivqu',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'COMPLETED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  }
                ],
                log: [
                  ...interruptionState.log,
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EventBasedGateway_1b2kp9o',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Task_022jsr6',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'ExclusiveGateway_01v8v7y',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EndEvent_1ssivqu',
                    tokenId: tId
                  }
                ]
              });
              done();
            }, 100);

            ++taskActivations;
            return false;
          }

          return true;
        }
      });
      p.deploy();

      p.start();
    });

    it('allows event gateways to resume after the instance was imported; if the instance is resumed after timers have run out the timer path with the smallest timeout will be chosen', async done => {
      let taskActivations = 0;

      const p = await BpmnProcess.fromXml('p1', BPMN_XML_EVENT_BASED_GATEWAY, {
        shouldActivateFlowNodeHook: async (pId, pIId, tId, flowNode, _) => {
          if (flowNode.id === 'EventBasedGateway_1b2kp9o' && taskActivations === 0) {
            const interruptionState = p.getInstanceById(pIId).getState();

            p.deleteInstanceById(pIId);
            expect(p.getAllInstances().length).toBe(0);

            await sleep(3000);

            expect(() =>
              p.startAt({
                // To retrigger the execution logic of an element we have to put the token back into the READY state
                tokens: interruptionState.tokens.map(t => ({ ...t, state: TokenState.READY })),
                variables: interruptionState.variables,
                instanceId: interruptionState.processInstanceId,
                globalStartTime: interruptionState.globalStartTime,
                log: interruptionState.log
              })
            ).not.toThrow();

            setTimeout(() => {
              p.getInstanceById(pIId).receiveMessage('Message_0io414n');
            }, 90);

            // TODO: make sure that this also works correctly when there are two timer events with different timings (the one with the smaller duration should always be chosen)

            setTimeout(() => {
              expect(p.getAllInstances().length).toBe(1);

              const [instance] = p.getAllInstances();
              const finalState = instance.getState();

              expect(finalState).toStrictEqual({
                ...interruptionState,
                instanceState: ['ENDED'],
                tokens: [
                  {
                    ...interruptionState.tokens[0],
                    state: 'ENDED',
                    previousFlowElementId: 'SequenceFlow_1361hlc',
                    currentFlowElementId: 'EndEvent_1ssivqu',
                    currentFlowElementStartTime: expect.any(Number),
                    currentFlowNodeState: 'COMPLETED',
                    intermediateVariablesState: null,
                    localExecutionTime: expect.any(Number)
                  }
                ],
                log: [
                  ...interruptionState.log,
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EventBasedGateway_1b2kp9o',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'Task_1on8mml',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'ExclusiveGateway_01v8v7y',
                    tokenId: tId
                  },
                  {
                    startTime: expect.any(Number),
                    endTime: expect.any(Number),
                    executionState: 'COMPLETED',
                    flowElementId: 'EndEvent_1ssivqu',
                    tokenId: tId
                  }
                ]
              });
              done();
            }, 100);

            ++taskActivations;
            return false;
          }

          return true;
        }
      });
      p.deploy();

      p.start();
    });
  });
});

describe('handle variables', () => {
  it('should throw when variables data type is not json-valid', async () => {
    const p = await BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT);
    p.getInstance$().subscribe(instance => {
      expect(() => instance.setVariable('', 'a', () => {})).toThrow();
    });
    p.deployAndStart({ variables: { a: { value: 5 }, b: { value: 10 } } });
  });

  it('should return the correct variables', done => {
    BpmnProcess.fromXml('p1', BPMN_XML_SINGLE_NONE_START_EVENT).then(p => {
      p.getInstance$().subscribe(instance => {
        expect(instance.getVariables()).toEqual({ a: 5, b: 10 });
        done();
      });
      p.deployAndStart({ variables: { a: { value: 5 }, b: { value: 10 } } });
    });
  });

  it('should update variables with script', done => {
    BpmnProcess.fromXml('p1', BPMN_XML_SCRIPT_TASK_SET_VAR).then(p => {
      p.getInstance$().subscribe(instance => {
        setTimeout(() => {
          expect(instance.getVariables()).toEqual({ a: 15, b: 10 });
          done();
        }, 2600);
      });

      p.deployAndStart({ variables: { a: { value: 5 }, b: { value: 10 } } });
    });
  });
});

describe('import and run a process exported from PROCEED management', () => {
  it('should create the process', async () => {
    let p = null;
    const errorStub = jest.fn();
    try {
      p = await BpmnProcess.fromXml(
        'myProcess',
        fs.readFileSync(path.resolve(__dirname, 'fixtures', 'management-export.bpmn')).toString('utf-8')
      );
    } catch (e) {
      errorStub(e);
    }

    expect(p).toBeDefined();
    expect(errorStub).not.toHaveBeenCalled();
  });

  it('should deploy and start the process', done => {
    BpmnProcess.fromXml(
      'myProcess',
      fs.readFileSync(path.resolve(__dirname, 'fixtures', 'management-export.bpmn')).toString('utf-8')
    )
      .then(process => {
        process.getInstance$().subscribe(instance => {
          expect(instance.getId()).toBeDefined();
          done();
        });

        process.deployAndStart();
      })
      .catch(e => done(e));
  });
});

describe('handle user tasks', () => {
  describe('user task activation', () => {
    it('should notify via hook', async done => {
      const process = await BpmnProcess.fromXml('process1', BPMN_XML_USER_TASK, {
        async shouldActivateFlowNodeHook(processId, instanceId, tokenId, task, state) {
          if (task.$type === 'bpmn:UserTask') {
            expect(processId).toBe('process1');
            expect(instanceId).toMatch(/^process1/);

            const token = state.tokens.find(t => t.tokenId === tokenId);
            expect(token?.currentFlowElement?.id === 'UserTask_1lmr4l7');
            expect(token?.currentFlowElement?.state === FlowNodeState.READY);

            new BpmnModdle().fromXML(BPMN_XML_USER_TASK, (_, definitions) => {
              const el = findElementById(definitions, 'UserTask_1lmr4l7');
              expect(task).toStrictEqual(el);
              done();
            });
          }
          return true;
        }
      });

      process.deployAndStart();
    });

    it('should set user task to active if hook returns true', async done => {
      const process = await BpmnProcess.fromXml('process1', BPMN_XML_USER_TASK, {
        async shouldActivateFlowNodeHook(processId, instanceId, tokenId, task, state) {
          if (task.$type === 'bpmn:UserTask') {
            setTimeout(() => {
              const token = state.tokens.find(t => t.tokenId === tokenId);
              expect(token?.currentFlowElement?.id === 'UserTask_1lmr4l7');
              expect(token?.currentFlowElement?.state === FlowNodeState.ACTIVE);
              done();
            }, 10);
          }
          return true;
        }
      });

      process.deployAndStart();
    });

    it('should list the user tasks currently active', done => {
      BpmnProcess.fromXml('process1', BPMN_XML_USER_TASK).then(process => {
        process.getInstance$().subscribe(processInstance => {
          setTimeout(() => {
            new BpmnModdle().fromXML(BPMN_XML_USER_TASK, (_, definitions) => {
              const el = findElementById(definitions, 'UserTask_1lmr4l7');
              expect(processInstance.getUserTasks()).toEqual([
                {
                  tokenId: expect.any(String),
                  element: el
                }
              ]);
              done();
            });
          }, 250);
        });

        process.deployAndStart();
      });
    });
  });

  describe('user task completion', () => {
    it('should not do anything if token was not found', async done => {
      const process = await BpmnProcess.fromXml('process1', BPMN_XML_USER_TASK, {
        async shouldActivateFlowNodeHook(processId, instanceId, tokenId, task, state) {
          if (task.$type === 'bpmn:UserTask') {
            const instance = process.getInstanceById(instanceId);

            expect(instance.getUserTasks().length).toBe(1);

            const exportState = instance.getState();

            const expectedTokenState = [
              // token at user task
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'RUNNING',
                currentFlowElementId: 'UserTask_1lmr4l7',
                currentFlowNodeState: FlowNodeState.READY,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_0gm2ucm',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              }
            ];

            expect(exportState.tokens).toEqual(expectedTokenState);

            expect(exportState.instanceState).toEqual(['RUNNING']);

            instance.completeActivity(task.id, tokenId + 'xyz');

            setTimeout(() => {
              expect(instance.getUserTasks().length).toBe(1);
              expect(exportState.tokens).toEqual(expectedTokenState);
              done();
            }, 10);
          }

          return true;
        }
      });

      process.deployAndStart();
    });

    it('should complete the user task that was activated', async done => {
      const process = await BpmnProcess.fromXml('process1', BPMN_XML_USER_TASK, {
        async shouldActivateFlowNodeHook(processId, instanceId, tokenId, task, state) {
          if (task.$type === 'bpmn:UserTask') {
            const instance = process.getInstanceById(instanceId);

            expect(instance.getUserTasks().length).toBe(1);

            const exportState = instance.getState();

            const expectedTokenState = [
              // token at user task
              {
                tokenId: expect.any(String),
                localStartTime: expect.any(Number),
                state: 'RUNNING',
                currentFlowElementId: 'UserTask_1lmr4l7',
                currentFlowNodeState: FlowNodeState.READY,
                currentFlowElementStartTime: expect.any(Number),
                previousFlowElementId: 'SequenceFlow_0gm2ucm',
                localExecutionTime: expect.any(Number),
                intermediateVariablesState: {}
              }
            ];

            expect(exportState.tokens).toEqual(expectedTokenState);

            expect(exportState.instanceState).toEqual(['RUNNING']);

            instance.completeActivity(task.id, tokenId);

            setTimeout(() => {
              expect(instance.getUserTasks()).toEqual([]);
              expect(instance.getVariables()).toEqual({});
              expect(instance.getVariablesWithLogs()).toEqual({});

              const endState = instance.getState();
              expect(endState.tokens).toEqual([
                // token at end event
                {
                  tokenId: expect.any(String),
                  localStartTime: expect.any(Number),
                  state: 'ENDED',
                  currentFlowElementId: 'EndEvent_0bzp8ym',
                  currentFlowNodeState: FlowNodeState.COMPLETED,
                  currentFlowElementStartTime: expect.any(Number),
                  previousFlowElementId: 'SequenceFlow_0vq5x2o',
                  localExecutionTime: expect.any(Number),
                  intermediateVariablesState: null
                }
              ]);
              expect(endState.instanceState).toEqual(['ENDED']);
              done();
            }, 10);
          }

          return true;
        }
      });

      process.deployAndStart();
    });

    it('should call shouldActivateFlowNodeHook every time new user task is reached', async done => {
      const activatedTasks: any = [];
      const userTaskActivated = jest.fn().mockImplementation(userTask => activatedTasks.push(userTask));
      let processInstance: any;

      const process = await BpmnProcess.fromXml('process1', BPMN_XML_PARALLEL_USER_TASKS, {
        async shouldActivateFlowNodeHook(processId, instanceId, tokenId, task, state) {
          if (task.$type === 'bpmn:UserTask') {
            processInstance = process.getInstanceById(instanceId);
            userTaskActivated(task);
          }

          return true;
        }
      });

      process.deployAndStart();

      setTimeout(() => {
        const userTasks = processInstance.getUserTasks();
        // first two parallel usertasks active
        expect(userTasks.length).toEqual(2);

        const state = processInstance.getState();
        expect(userTaskActivated).toHaveBeenCalledTimes(2);
        expect(state.instanceState).toEqual(['RUNNING']);

        // mark one of parallel user tasks as completed so usertask after can be activated
        processInstance.completeActivity(userTasks[0].element.id, userTasks[0].tokenId);

        setTimeout(() => {
          const userTasks = processInstance.getUserTasks();
          expect(userTasks.length).toEqual(2);

          const state = processInstance.getState();
          expect(userTaskActivated).toHaveBeenCalledTimes(3);
          expect(state.instanceState).toEqual(['RUNNING']);

          // mark next user task as completed which ends this parallel token and only the other parallel user task stays active
          processInstance.completeActivity(userTasks[0].element.id, userTasks[0].tokenId);

          setTimeout(() => {
            const userTasks = processInstance.getUserTasks();
            expect(userTasks.length).toEqual(1);

            const state = processInstance.getState();
            expect(userTaskActivated).toHaveBeenCalledTimes(3);
            expect(state.instanceState).toEqual(['ENDED', 'RUNNING']);

            // mark user task as completed so usertask after can be activated
            processInstance.completeActivity(userTasks[0].element.id, userTasks[0].tokenId);

            setTimeout(() => {
              const userTasks = processInstance.getUserTasks();
              expect(userTasks.length).toEqual(1);

              const state = processInstance.getState();
              expect(userTaskActivated).toHaveBeenCalledTimes(4);
              expect(state.instanceState).toEqual(['ENDED', 'RUNNING']);

              // mark last user task as completed so this token ends
              processInstance.completeActivity(userTasks[0].element.id, userTasks[0].tokenId);
              setTimeout(() => {
                const userTasks = processInstance.getUserTasks();
                expect(userTasks.length).toEqual(0);

                const state = processInstance.getState();
                expect(userTaskActivated).toHaveBeenCalledTimes(4);
                expect(state.instanceState).toEqual(['ENDED']);
                done();
              }, 150);
            }, 150);
          }, 150);
        }, 150);
      }, 150);
    });

    it('should complete the user task that was activated and update variables', done => {
      BpmnProcess.fromXml('process1', BPMN_XML_USER_TASK).then(process => {
        process.getInstance$().subscribe(processInstance => {
          setTimeout(() => {
            const userTasks = processInstance.getUserTasks();
            expect(userTasks.length).toEqual(1);
            processInstance.completeActivity(userTasks[0].element.id, userTasks[0].tokenId, { pens: 5, papers: 25 });

            setTimeout(() => {
              expect(processInstance.getUserTasks()).toEqual([]);
              expect(processInstance.getVariables()).toEqual({ pens: 5, papers: 25 });
              expect(processInstance.getVariablesWithLogs()).toEqual({
                pens: {
                  value: 5,
                  log: [
                    { changedTime: expect.any(Number) },
                    { changedTime: expect.any(Number), changedBy: userTasks[0].element.id, oldValue: 1 }
                  ]
                },
                papers: {
                  value: 25,
                  log: [
                    { changedTime: expect.any(Number) },
                    { changedTime: expect.any(Number), changedBy: userTasks[0].element.id, oldValue: 10 }
                  ]
                }
              });
              done();
            }, 150);
          }, 150);
        });

        process.deployAndStart({ variables: { pens: { value: 1 }, papers: { value: 10 } } });
      });
    });
  });
});

describe('handle call activities', () => {
  describe('call activity activation', () => {
    it('should notify via shouldActivateFlowNodeHook', async done => {
      const process = await BpmnProcess.fromXml('process1', BPMN_XML_CALL_ACTIVITY, {
        async shouldActivateFlowNodeHook(processId, instanceId, tokenId, activity, state) {
          if (activity.$type === 'bpmn:CallActivity') {
            expect(processId).toBe('process1');
            expect(instanceId).toMatch(/^process1/);
            expect(activity).toHaveProperty('calledElement');

            const token = state.tokens.find(t => t.tokenId === tokenId);
            expect(token?.currentFlowElement?.id === 'CallActivity_1mcfm7t');
            expect(token?.currentFlowElement?.state === FlowNodeState.READY);

            new BpmnModdle().fromXML(BPMN_XML_CALL_ACTIVITY, (_, definitions) => {
              const el = findElementById(definitions, 'CallActivity_1mcfm7t');
              expect(activity).toStrictEqual(el);
              done();
            });
          }
          return true;
        }
      });

      process.deployAndStart();
    });

    it('should list the call activities currently active', done => {
      BpmnProcess.fromXml('process1', BPMN_XML_CALL_ACTIVITY).then(process => {
        process.getInstance$().subscribe(processInstance => {
          new BpmnModdle().fromXML(BPMN_XML_CALL_ACTIVITY, (_, definitions) => {
            const el = findElementById(definitions, 'CallActivity_1mcfm7t');
            expect(processInstance.getCallActivities()).toEqual([
              {
                tokenId: expect.any(String),
                element: el
              }
            ]);
            done();
          });
        });

        process.deployAndStart();
      });
    });
  });

  describe('call activity completion', () => {
    it('should not do anything if token was not found', done => {
      BpmnProcess.fromXml('process1', BPMN_XML_CALL_ACTIVITY).then(process => {
        process.getInstance$().subscribe(processInstance => {
          setTimeout(() => {
            const callActivites = processInstance.getCallActivities();
            expect(callActivites.length).toEqual(1);
            processInstance.completeActivity(callActivites[0].element.id, callActivites[0].tokenId + 'xyz');

            setTimeout(() => {
              expect(processInstance.getCallActivities()).toEqual(callActivites);
              done();
            }, 150);
          }, 150);
        });

        process.deployAndStart();
      });
    });

    it('should complete the call activity that was activated', async done => {
      const process = await BpmnProcess.fromXml('process1', BPMN_XML_CALL_ACTIVITY);

      process.getInstance$().subscribe(processInstance => {
        setTimeout(() => {
          const callActivites = processInstance.getCallActivities();
          expect(callActivites.length).toEqual(1);
          processInstance.completeActivity(callActivites[0].element.id, callActivites[0].tokenId);

          setTimeout(() => {
            expect(processInstance.getCallActivities()).toEqual([]);
            expect(processInstance.getVariables()).toEqual({});
            expect(processInstance.getVariablesWithLogs()).toEqual({});
            done();
          }, 150);
        }, 150);
      });

      process.deployAndStart();
    });

    it('should complete the call activity that was activated and update variables', done => {
      BpmnProcess.fromXml('process1', BPMN_XML_CALL_ACTIVITY).then(process => {
        process.getInstance$().subscribe(processInstance => {
          setTimeout(() => {
            const callActivites = processInstance.getCallActivities();
            expect(callActivites.length).toEqual(1);
            processInstance.completeActivity(callActivites[0].element.id, callActivites[0].tokenId, {
              pens: 5,
              papers: 25
            });

            setTimeout(() => {
              expect(processInstance.getCallActivities()).toEqual([]);
              expect(processInstance.getVariables()).toEqual({ pens: 5, papers: 25 });

              expect(processInstance.getVariablesWithLogs()).toEqual({
                pens: {
                  value: 5,
                  log: [
                    { changedTime: expect.any(Number) },
                    { changedTime: expect.any(Number), changedBy: callActivites[0].element.id, oldValue: 1 }
                  ]
                },
                papers: {
                  value: 25,
                  log: [
                    { changedTime: expect.any(Number) },
                    { changedTime: expect.any(Number), changedBy: callActivites[0].element.id, oldValue: 10 }
                  ]
                }
              });
              done();
            }, 150);
          }, 150);
        });

        process.deployAndStart({ variables: { pens: { value: 1 }, papers: { value: 10 } } });
      });
    });
  });
});

describe('shouldActivateFlowNodeHook', () => {
  let mockProcess: BpmnProcess;
  let mockHook = jest.fn();
  beforeEach(async () => {
    mockProcess = await BpmnProcess.fromXml('mockProcess', BPMN_XML_SINGLE_NONE_START_EVENT, {
      shouldActivateFlowNodeHook: mockHook
    });
    mockHook.mockReset();
    mockProcess.deploy();
  });

  it('will get called for every flowNode in the process', done => {
    mockHook.mockImplementation(async () => true);

    mockProcess.getInstance$().subscribe(instance => {
      instance.onEnded(() => {
        const [token] = instance.getState().tokens;
        expect(mockHook).toHaveBeenCalledTimes(3);
        // called for start event
        expect(mockHook).toHaveBeenCalledWith(
          'mockProcess',
          instance.getId(),
          token.tokenId,
          expect.objectContaining({
            id: 'StartEvent_1',
            $type: 'bpmn:StartEvent'
          }),
          expect.objectContaining({
            processId: 'mockProcess',
            processInstanceId: instance.getId(),
            globalStartTime: expect.any(Number),
            instanceState: ['RUNNING'],
            tokens: [
              expect.objectContaining({
                state: 'RUNNING',
                currentFlowElementId: 'StartEvent_1',
                currentFlowNodeState: 'READY'
              })
            ]
          })
        );
        // called for task
        expect(mockHook).toHaveBeenCalledWith(
          'mockProcess',
          instance.getId(),
          token.tokenId,
          expect.objectContaining({
            id: 'Task_175vb4p',
            $type: 'bpmn:Task'
          }),
          expect.objectContaining({
            processId: 'mockProcess',
            processInstanceId: instance.getId(),
            globalStartTime: expect.any(Number),
            instanceState: ['RUNNING'],
            tokens: [
              expect.objectContaining({
                state: 'RUNNING',
                currentFlowElementId: 'Task_175vb4p',
                currentFlowNodeState: 'READY'
              })
            ]
          })
        );
        // called for end event
        expect(mockHook).toHaveBeenCalledWith(
          'mockProcess',
          instance.getId(),
          token.tokenId,
          expect.objectContaining({
            id: 'EndEvent_1umdfcw',
            $type: 'bpmn:EndEvent'
          }),
          expect.objectContaining({
            processId: 'mockProcess',
            processInstanceId: instance.getId(),
            globalStartTime: expect.any(Number),
            instanceState: ['RUNNING'],
            tokens: [
              expect.objectContaining({
                state: 'RUNNING',
                currentFlowElementId: 'EndEvent_1umdfcw',
                currentFlowNodeState: 'READY'
              })
            ]
          })
        );
        done();
      });
    });

    mockProcess.start();
  });

  it('will prevent flowNode from being executed if it returns false', done => {
    mockHook.mockImplementation(async (processId, instanceId, tokenId, flowNode, state) => {
      if (flowNode.id === 'Task_175vb4p') {
        setTimeout(() => {
          const { tokens } = mockProcess.getInstanceById(instanceId).getState();

          expect(tokens[0]).toEqual({
            tokenId: expect.any(String),
            state: 'RUNNING',
            localStartTime: expect.any(Number),
            localExecutionTime: expect.any(Number),
            currentFlowElementId: 'Task_175vb4p',
            currentFlowNodeState: 'READY',
            currentFlowElementStartTime: expect.any(Number),
            previousFlowElementId: 'SequenceFlow_07so23a',
            intermediateVariablesState: {}
          });

          done();
        }, 10);

        return false;
      }

      return true;
    });

    mockProcess.start();
  });

  it('will only allow a flowNode to be activated after the hook returned true', done => {
    mockHook.mockImplementation(async (processId, instanceId, tokenId, flowNode, { tokens }) => {
      if (flowNode.id === 'Task_175vb4p') {
        expect(tokens[0]).toEqual({
          tokenId: expect.any(String),
          state: 'RUNNING',
          localStartTime: expect.any(Number),
          localExecutionTime: expect.any(Number),
          currentFlowElementId: 'Task_175vb4p',
          currentFlowNodeState: 'READY',
          currentFlowElementStartTime: expect.any(Number),
          previousFlowElementId: 'SequenceFlow_07so23a',
          intermediateVariablesState: {}
        });

        setTimeout(() => {
          const endState = mockProcess.getInstanceById(instanceId).getState();

          expect(endState.instanceState).toEqual(['ENDED']);

          done();
        }, 10);
      }

      return true;
    });

    mockProcess.start();
  });
});
