import LoggerFactory, { getlog$, LogLevel, LogItem, setLogLevel } from '../src/LoggerFactory';

it('should log all the levels', done => {
  const log = LoggerFactory.getLogger('myname');
  const recordedLogs: Partial<LogItem>[] = [];

  getlog$().subscribe(logItem => {
    delete logItem['timestamp'];
    recordedLogs.push(logItem);
  });

  log.trace({ message: 'text trace', params: { elementId: '123' } });
  log.debug({ message: 'text debug', params: { elementId: '456' } });
  log.info({ message: 'text info', params: { elementId: '789' } });
  log.warn({ message: 'text warn', params: { elementId: 'abc' } });
  log.error({ message: 'text error', params: { elementId: 'pqr' } });

  setTimeout(() => {
    expect(recordedLogs).toHaveLength(5);
    expect(recordedLogs).toEqual([
      {
        level: LogLevel.TRACE,
        name: 'myname',
        message: 'text trace',
        params: { elementId: '123' }
      },
      {
        level: LogLevel.DEBUG,
        name: 'myname',
        message: 'text debug',
        params: { elementId: '456' }
      },
      {
        level: LogLevel.INFO,
        name: 'myname',
        message: 'text info',
        params: { elementId: '789' }
      },
      {
        level: LogLevel.WARN,
        name: 'myname',
        message: 'text warn',
        params: { elementId: 'abc' }
      },
      {
        level: LogLevel.ERROR,
        name: 'myname',
        message: 'text error',
        params: { elementId: 'pqr' }
      }
    ]);
    done();
  }, 150);
});

describe('set global log level', () => {
  beforeEach(() => {
    setLogLevel(LogLevel.TRACE);
  });

  it('should log only level above or equal to error', done => {
    const log = LoggerFactory.getLogger('myname');
    const recordedLogs: Partial<LogItem>[] = [];

    getlog$().subscribe(logItem => {
      delete logItem['timestamp'];
      recordedLogs.push(logItem);
    });

    setLogLevel(LogLevel.ERROR);

    log.trace({ message: 'text trace', params: { elementId: '123' } });
    log.debug({ message: 'text debug', params: { elementId: '456' } });
    log.info({ message: 'text info', params: { elementId: '789' } });
    log.warn({ message: 'text warn', params: { elementId: 'abc' } });
    log.error({ message: 'text error', params: { elementId: 'pqr' } });

    setTimeout(() => {
      expect(recordedLogs).toHaveLength(1);
      expect(recordedLogs).toEqual([
        {
          level: LogLevel.ERROR,
          name: 'myname',
          message: 'text error',
          params: { elementId: 'pqr' }
        }
      ]);
      done();
    }, 150);
  });

  it('should log only level above or equal to info', done => {
    const log = LoggerFactory.getLogger('myname');
    const recordedLogs: Partial<LogItem>[] = [];

    getlog$().subscribe(logItem => {
      delete logItem['timestamp'];
      recordedLogs.push(logItem);
    });

    setLogLevel(LogLevel.INFO);

    log.trace({ message: 'text trace', params: { elementId: '123' } });
    log.debug({ message: 'text debug', params: { elementId: '456' } });
    log.info({ message: 'text info', params: { elementId: '789' } });
    log.warn({ message: 'text warn', params: { elementId: 'abc' } });
    log.error({ message: 'text error', params: { elementId: 'pqr' } });

    setTimeout(() => {
      expect(recordedLogs).toHaveLength(3);
      expect(recordedLogs).toEqual([
        {
          level: LogLevel.INFO,
          name: 'myname',
          message: 'text info',
          params: { elementId: '789' }
        },
        {
          level: LogLevel.WARN,
          name: 'myname',
          message: 'text warn',
          params: { elementId: 'abc' }
        },
        {
          level: LogLevel.ERROR,
          name: 'myname',
          message: 'text error',
          params: { elementId: 'pqr' }
        }
      ]);
      done();
    }, 150);
  });
});

describe('local log level', () => {
  beforeEach(() => {
    setLogLevel(LogLevel.TRACE);
  });
  it('should log only level above or equal to error for one logger', done => {
    const log = LoggerFactory.getLogger('myname');
    const otherLog = LoggerFactory.getLogger('othername');
    const recordedLogs: Partial<LogItem>[] = [];

    log.setLevel(LogLevel.ERROR);

    getlog$().subscribe(logItem => {
      delete logItem['timestamp'];
      recordedLogs.push(logItem);
    });

    otherLog.trace({ message: 'text trace', params: { elementId: '123' } });
    log.trace({ message: 'text trace', params: { elementId: '123' } });
    log.debug({ message: 'text debug', params: { elementId: '456' } });
    log.info({ message: 'text info', params: { elementId: '789' } });
    log.warn({ message: 'text warn', params: { elementId: 'abc' } });
    log.error({ message: 'text error', params: { elementId: 'pqr' } });

    setTimeout(() => {
      expect(recordedLogs).toHaveLength(2);
      expect(recordedLogs).toEqual([
        {
          level: LogLevel.TRACE,
          name: 'othername',
          message: 'text trace',
          params: { elementId: '123' }
        },
        {
          level: LogLevel.ERROR,
          name: 'myname',
          message: 'text error',
          params: { elementId: 'pqr' }
        }
      ]);
      done();
    }, 150);
  });

  it('should log only level above or equal to info for one logger', done => {
    const log = LoggerFactory.getLogger('myname');
    const otherLog = LoggerFactory.getLogger('othername');
    const recordedLogs: Partial<LogItem>[] = [];

    log.setLevel(LogLevel.INFO);

    getlog$().subscribe(logItem => {
      delete logItem['timestamp'];
      recordedLogs.push(logItem);
    });

    otherLog.trace({ message: 'text trace', params: { elementId: '123' } });
    log.trace({ message: 'text trace', params: { elementId: '123' } });
    log.debug({ message: 'text debug', params: { elementId: '456' } });
    log.info({ message: 'text info', params: { elementId: '789' } });
    log.warn({ message: 'text warn', params: { elementId: 'abc' } });
    log.error({ message: 'text error', params: { elementId: 'pqr' } });

    setTimeout(() => {
      expect(recordedLogs).toHaveLength(4);
      expect(recordedLogs).toEqual([
        {
          level: LogLevel.TRACE,
          name: 'othername',
          message: 'text trace',
          params: { elementId: '123' }
        },
        {
          level: LogLevel.INFO,
          name: 'myname',
          message: 'text info',
          params: { elementId: '789' }
        },
        {
          level: LogLevel.WARN,
          name: 'myname',
          message: 'text warn',
          params: { elementId: 'abc' }
        },
        {
          level: LogLevel.ERROR,
          name: 'myname',
          message: 'text error',
          params: { elementId: 'pqr' }
        }
      ]);
      done();
    }, 150);
  });
});
