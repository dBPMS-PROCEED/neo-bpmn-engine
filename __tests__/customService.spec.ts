import { getService, provideService } from '../src/CustomService';

describe('access a service before providing', () => {
  test('should return undefined', () => {
    expect(getService('com.myEmail.send')).toBeUndefined();
  });
});

describe('provide a service and access it', () => {
  test('should be have been set and called', () => {
    const myEmailService = {
      send: jest.fn()
    };

    provideService('com.proceed.email', myEmailService);
    const service = getService('com.proceed.email') as any;
    service.send('hello', 'world');

    expect(myEmailService.send).toHaveBeenCalledWith('hello', 'world');
  });
});
