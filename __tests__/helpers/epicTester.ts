import { AnyAction } from 'redux';
import { ActionsObservable, Epic, StateObservable } from 'redux-observable';
import { from, Subject } from 'rxjs';

export async function testEpic(
  epic: Epic,
  initState: unknown,
  initActionsOr$: AnyAction[] | ActionsObservable<AnyAction>,
  collectActionsForMillis = 50,
  state$?: StateObservable<any>
) {
  return new Promise<AnyAction[]>((resolve, _reject) => {
    const action$ = new Subject<AnyAction>();
    const _state$ = new Subject<any>();
    const receivedActions: AnyAction[] = [];

    const actionRet$ = epic(new ActionsObservable(action$), new StateObservable(_state$, initState), undefined);

    actionRet$.subscribe(a => {
      // pipe back events
      action$.next(a);

      // actions to return
      receivedActions.push(a);
    });

    from(initActionsOr$).subscribe(action$);
    if (state$) {
      state$.subscribe(_state$);
    }

    setTimeout(() => {
      resolve(receivedActions);
    }, collectActionsForMillis);
    // TODO: Investigate how to use custom scheduler
    // - {SB}: Use global dependency injection?
  });
}
