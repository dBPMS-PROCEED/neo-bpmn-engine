import { Logger } from '../../src/LoggerFactory';

interface MockedLogger extends Logger {
  reset: () => void;
}

export function createMockLogger(): MockedLogger {
  const setLevel = jest.fn();
  const trace = jest.fn();
  const debug = jest.fn();
  const info = jest.fn();
  const warn = jest.fn();
  const error = jest.fn();

  return {
    setLevel,
    trace,
    debug,
    info,
    warn,
    error,
    reset: () => {
      setLevel.mockReset();
      trace.mockReset();
      debug.mockReset();
      info.mockReset();
      warn.mockReset();
      error.mockReset();
    }
  };
}
