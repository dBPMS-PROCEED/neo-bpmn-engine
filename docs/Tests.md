# Tests

NEO-BPMN-ENGINE is written in Typescript using several libraries such as Redux, Redux-observable and RXJS to implement transition and state changes of a BPMN process. A detailed description of what this means, can be found in the [THEORY part](./THEORY.md).

In short, the engine consists of an object storing the state-information of a BPMN Processinstance and so called `reducers` take dispatched actions to update the state object. `Epics` are used to listen for certain actions (after possibly passing a reducer) and dispatch new actions.

There are test files for every reducer and for every Epic implemented in the engine which can be found under the directory [`__tests__/state/`](../__tests__/state/).
For Epic test-files the corresponding epic takes the moddleDefinitions-element (the running BPMN process as a moddleElement created by BPMNmoddle library), the current state and a dispatched action to be handled by this epic and it is tested if the epic returns the suitable action(s) as a response.

While these tests only test isolated components of the engine, there can be also found more [comprehensive tests](../__tests__BpmnProcess.spec.ts) in which the engine runs an instance of a BPMN process and after that the state of this processinstance can be tested: e.g. the flowNode-log, every token, the instanceState. See [State Description](./API#state-desciption) for more.
