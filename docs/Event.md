# Events

An Event is something that “happens” during the course of a Process. These Events affect the flow of the Process
and usually have a cause or an impact. The term “event” is general enough to cover many things in a Process. The start
of an Activity, the end of an Activity, the change of state of a document, a Message that arrives, etc., all could be
considered Events. However, BPMN has restricted the use of Events to include only those types of Events that will
affect the sequence or timing of Activities of a Process. [[OMG, BPMN Specification Version 2.0]](https://www.omg.org/spec/BPMN/2.0/About-BPMN/)

## None Start Event

The start event is the beginning of the process. When starting a process, the start event is the default element for placing a token to start execution.

## None End Event

A token reaching the none end event will change its state to `ENDED`, thus stopping its execution.

## Terminate End Event

A token reaching the terminate end event will change its state to `ENDED`, thus stopping its execution. Also, every other running token in the process will change its state to `ABORTED` and stoppings its execution.

## Intermediate Event

An intermediate event can be either throwing, meaning a certain event will be triggered or catching, meaning it will be waited until a certain event is caught before the execution can be continued.

## Boundary Event

Boundary Events are certain events, attached to activity or subprocess. If the defined event is caught, a token will be placed at the boundary event, starting an alternative execution flow. Boundary events can be interrupting, meaning the activity or the subprocess the boundary event is attached is interrupted and its token removed; or non-interrupting, meaning the original token at the activity or the subprocess will continue its execution in parallel to the execution starting at the boundary event.

## Messages/Signals

Messages and signals need to be received and sent by the process during execution, for starting the process or during the end of the process. The receiving part can be identified by looking at the activites where the intermediate catch activity pauses in READY phase and the `receiveMessage/receiveSignal` method has to be invoked on the processInstance with the message/signal id. When a process wants to send a signal/message, it will invoke the `sendSignalHook/sendMessageHook` attached to the process with processId, processInstanceId and the signal/message id.
