# Tasks

A Task is an atomic Activity within a Process flow. A Task is used when the work in the Process cannot be broken
down to a finer level of detail. Generally, an end-user and/or applications are used to perform the Task when it is
executed. [[OMG, BPMN Specification Version 2.0]](https://www.omg.org/spec/BPMN/2.0/About-BPMN/)

## Script Task

The script task is designed to execute the provided script in a standard script task BPMN element. The execution of javascript depends on the script execution environment (DOM or NodeJS) provided. The engine already provides a default script execution environment, details in later part of this section. Auxillary to the script task, the internal variables service is available via the `getService()` call.

### Scripting Environment and Execution semantics

The scripting environment is provided by an object called `ScriptExecutor` and can be set in the engine before starting any process via the `Engine.provideScriptExecutor(executor)` method,

```js
import * as Engine from 'engine';

Engine.provideScriptExecutor(myExecutor);
```

The executor is expected to contain two methods `execute()` and `stop()`. The `execute` method is should be async and wrapped in a promise. The execute method will receive the following params in order,

- _processId_: The xml id of the process.
- _processInstanceId_: The id of the current process instance that wants to execute the script.
- _scriptString_: The script, provided as is from the BPMN XML.
- _dependencies_: Any dependencies, that are expected by the script in a key-value pair. Currently, the engine passes `log` and `getService` as dependencies, more can be added by the executor.

Once the executor returns the promise, the script task is marked as COMPLETED and the token is further passed based on BPMN semantics. If the engine rejects `execute()` promise due to failure, it must throw `ScriptExecutionError`, the script task is them marked as FAILED.

The return value of the script can be an object, serialized in JSON, which is considered as the updated variable values by the engine. The variables are merged with the existing variable values, before the BPMN Script task reached COMPLETED state. Note that any variables that were set using the variables service cannot be reverted, this is the advantage of returning the variables on completion of the script.

The executor must also provide a `stop()` method, which is invoked if the script is going to be interrupted at any point. The stop method must accept `processId` and `processInstanceId` as the params. The engine may pass `*` as a possible value, indicating that it applies to all processes and instances respectively.

### Default script executor

The engine already provides an executor for Javascript, this is not recommended though due to security reasons, but should be ideal for testing and quick prototyping. The executor wraps each script in a promise and runs it as an async function, therefore `await` calls can be made in the script to external services. A sample script is provided below,

```js
// Assuming that the email service was provided earlier,
const email = getService('com.proceed.email');

if (!variable.get('notified')) {
  const user = variable.get('user');
  await email.send(user.email, 'notification', 'your order is being processed');

  // Returned variables object are treated as updates
  return { notified: true }; // either resolve this way or use variable.set('notified', true);
}
```

_Note: if `variable.get()` is called immediately after `variable.set()`, the value is not guaranteed to be immediately set, because the call to setVar is asynchronous. If the consistency is really necessary, it can be achieved by scheduling the access in the next event loop with setTimeout(..callback.., 0);_

_Note: The script environment is not leveraged for evaluating sequence expressions. It still must use Javascript expressions._

## User Task

The user tasks are handled by creating a special list and a stream in the process instance. To complete the user task that is currently active, again a method is provided in the process instance. The engine does not handle form rendering or user authentication/authorization, the engine has to be decorated based on specific requirements. On completion of a user task, the process instance can be requested to set variables to a new state. If an error occures in the user task, it can be aborted. An example is given below,

```js
import * as Engine from 'neo-bpmn-engine';

const myProcess = Engine.BpmnProcess.fromXml('process-id', '....');

myProcess.getInstance$().subscribe(pi => {
  // subscribe to user tasks stream, will invoke when a user task becomes active
  pi.getUserTask$().subscribe(userTask => {
    //... at a later point

    // complete a user task with task id and token id with optional variables
    pi.completeActivity(userTask.id, userTask.tokenId, { pens: 5 });

    // OR: pi.abortUserTask(userTask.id, userTask.tokenId);
  });

  // Additionally, user tasks for the process instance can be obtained via,
  pi.getUserTasks(); // returns array of all the user task objects with id and tokenId.
});

myProcess.deployAndStart();
```
