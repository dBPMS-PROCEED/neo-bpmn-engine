# Logging

All components in the engine emit logs via a common stream. The fields of the log items can be adapted to your needs, every log items is of the following format,

```js
{
  timestamp: '<ISO-8601>',
  name: '<logger-name>',
  level: 0, // 0-4 for TRACE, DEBUG, INFO, WARN and ERROR
  message: '<message-string>',
  params: { } // additional fields
}
```

The logstream level can be set via the function `setLogLevel`. This pattern can be extended to `pino` and other popular loggers.

```js
import * as Engine from 'neo-bpmn-engine';

Engine.setLogLevel(2); // set level to info
```
