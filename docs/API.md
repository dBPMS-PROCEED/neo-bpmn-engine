# API

This BPMN-Engine offers several interfaces, e.g. for state requesting and manipulating and controling the execution of an processinstance.

## State Description

The state of a Neo-BPMN-Engine process instance consists of the following attributes. Custom attributes, e.g. for tokens can be inserted via the provided interfaces.

| Attribute         | Description                                                                                              |
| ----------------- | -------------------------------------------------------------------------------------------------------- |
| ProcessID         | ID of the BPMN process                                                                                   |
| ProcessInstanceID | ID of the running instance of the process                                                                |
| GlobalStartTime   | Time when instance was started                                                                           |
| InstanceState     | Current state of the instance, composed by current states of running tokens                              |
| Tokens            | [See Token Description](#token-description)                                                              |
| Variables         | All variables of the processinstance. Every variable consists of a value and a log with previous changes |
| Log               | [See Log Description](#log-description)                                                                  |
| AdaptationLog     | [See Adaptation Log Description](#adaptation-log-description)                                            |

<br>

## Token Description

| Attribute            | Description                                                 |
| -------------------- | ----------------------------------------------------------- |
| ID                   | Unique ID of the token                                      |
| State                | Current State of the token, [see Token State](#token-state) |
| LocalStartTime       | Starttime of token                                          |
| LocalExecutionTime   | Total time for execution of flow nodes for this token       |
| CurrentFlowElementId | Flow node the token is currently executing                  |
| CurrentFlowNodeState | State of the execution of the current flow node             |

<br>

### Token State

|     | State           | Description                                                                                  | End State?         |
| --- | --------------- | -------------------------------------------------------------------------------------------- | ------------------ |
|     | READY           | Currently no execution, e.g. waiting for Event (Intermediate Catch) or tokens (join gateway) |                    |
|     | RUNNING         | Execution of flow Node                                                                       |                    |
|     | ENDED           | Token ended properly                                                                         | :white_check_mark: |
|     | TERMINATED      | Token terminated, e.g. because of interrupting boundary event                                | :white_check_mark: |
|     | FAILED          | Token failed, e.g. because of error boundary event                                           | :white_check_mark: |
|     | ABORTED         | Token aborted because of terminate end event                                                 | :white_check_mark: |
|     | ERROR-SEMANTIC  | An Event was thrown, but no handling happened                                                | :white_check_mark: |
|     | ERROR-TECHNICAL | Technical Error inside of a flow Node, e.g. SyntaxError in Script Task                       | :white_check_mark: |
|     | ERROR-UNKNOWN   | An unknown error was thrown during the execution of the process                              | :white_check_mark: |

<br>

### Flow Node State

|     | State      | Description                                                       | End State?         |
| --- | ---------- | ----------------------------------------------------------------- | ------------------ |
|     | READY      | Currently no execution, e.g. waiting for flow node to be executed |                    |
|     | ACTIVE     | Execution of flow Node                                            |                    |
|     | COMPLETED  | Flow node ended properly                                          | :white_check_mark: |
|     | FAILED     | Execution failed, e.g. because of error boundary event            | :white_check_mark: |
|     | TERMINATED | Execution interrupted, e.g. because of escalation boundary event  | :white_check_mark: |

## Log Description

| Attibute       | Description                                                                        |
| -------------- | ---------------------------------------------------------------------------------- |
| ID             | ID of the executed flow Node                                                       |
| TokenID        | ID of the token which executed the flow Node                                       |
| ExecutionState | End state of the execution                                                         |
| StartTime      | Time when execution of flow Node started                                           |
| EndTime        | Time when execution of flow Node ended                                             |
| ErrorMessage   | Optional attribute for executions with state `ERROR-SEMANTIC` or `ERROR-TECHNICAL` |

<br>

## Adaptation Log Description

| Attibute             | Description                                                                          |
| -------------------- | ------------------------------------------------------------------------------------ |
| Type                 | The kind of change that was made (Migration, Token Add/Remove/Move, Variable Change) |
| Time                 | The time at which the change was made                                                |
| From                 | The version that was used before a migration took place (only in case of migration)  |
| To                   | The version that is used after a migration took place (only in case of migration)    |
| CurrentFlowElementId | The flow element a token was removed from (only in case of token move/remove)        |
| TargetFlowElementId  | The flow element a token was added/moved to (only in case of token add/move)         |

<br>

## State Requesting and Manipulating

Every processinstance holds information about its current state, the tokens inside the instance and more. The current state can be requested and also manipulated.

### getState()

Request a copy of the current state in the form of:

```
{
    processId,
    processInstanceId,
    globalStartTime,
    instanceState,
    tokens,
    variables,
    log: {
        flowNodeExecutions
    }
    adaptationLog: {
        adaptations
    }
}
```

### updateProcessStatus(status: String)

Sets the state of the instance to a custom value.

### isEnded()

Check if the processinstance has already ended (-> all tokens are in an ended state).

### onEnded(callback: Function)

Commit a callback to be called when the processinstance ended.

### isPaused()

Check if the processinstance is paused.

### onPaused(callback: Function)

Commit a callback to be called when the processinstance paused.

### onAborted(callback: Function)

Commit a callback to be called when the processinstance is aborted (-> token reached terminating End Event).

### onTokenEnded(callback: Function)

Commit a callback to be called when a token of the processinstance is in an ended state.

### onFlowNodeExecuted(callback: Function)

Commit a callback to be called when a log entry for an executed flow Node was created.

### onInstanceStateChange(callback: Function)

Commit a callback to be called when the instance state changes.

### onScriptTaskError(callback: Function)

Commit a callback to be called when it occurs an error inside of a Script Task.

### onActivityInterrupted(activityType: String, callback: Function)

Commit a callback to be called when an activity (e.g. `bpmn:UserTask`, `bpmn:CallActivity`) was interrupted

### onUserTaskInterrupted(callback: Function)

Commit a callback to be called when an User Task was interrupted

### onCallActivityInterrupted(callback: Function)

Commit a callback to be called when a Call Activity was interrupted

### updateVariables(variables: { [variableName: String]: any }, silent: Boolean)

Update the values of variables in the instance. New variables might be added. If silent flag is falsy the changes will be added to the adaptation Log

### mergeVariableChanges(variables: VariablesState)

Update the variables of the processinstance. New variables containing of a value and a log will be added and for an existing variable the value will be updated and log entries merged with existing.

### updateLog(flowElementId: string, tokenId: string, attributes: {})

Existing log entry will be updated with given attributes.

### logExecution(flowElementId: string, tokenId: string, attributes: {})

Add a log entry for an executed Flow Node.

### mergeFlowNodeLog(log: LogState)

Update the flowNode log of the processinstance. New log entries will be added and existing will be overritten.

### logAdaptation(type: string, time: number, attributes = {})

Add an entry to the adaptation log. Optional time attribute is the time stamp to be used for the entry.

### updateAdaptationLog(type: string, time: number, attributes: object)

Can be used to update an existing adaptation log entry that was added at a specific time with new attributes.

### mergeAdaptationLog(log: AdaptationLogState)

Update the adaptation log of the process instance. New log entries will be added. Already existing ones are left as they are.

### receiveMessage(messageId: String)

Allows the instance to receive messages, e.g. for Message Events.

### receiveSignal(signalId: String)

Allows the instance to receive a signal, e.g. for Signal Events.

<br>

## Activities

Activities might be executed outside of this BPMN-Engine and others like User Tasks and Call Activities have to be executed outside.
This engine offers interfaces to inform about activities becoming active and to complete activities from outside.

### getOpenElements(bpmnType: String)

Returns a list of all currently active activities of a type.

### getUserTasks()

Returns a list of all currently active User Tasks.

### getCallActivities()

Returns a list of all currently active Call Activities.

### completeActivity(activityId: String, tokenId: String, variables: { [key: string]: unknown } = {})

Mark an active Activity as completed. Parameters are the id of the Activity and the tokenId of the token executing the Activity. Optional variables can be given as a parameter to update all of the variables changed in the Activity.

### terminateActivity(activityId: String, tokenId: String)

Mark an Activity as terminated. Parameters are the id of the Activity and the tokenId of the token executing the Activity. The token that activated the Activity will not be passed to the next flow element.

### failActivity(activityId: String, tokenId: String)

Mark an Activity a failed. Parameters are the id of the Activity and the tokenId of the token executing the Activity. The token that activated the Activity will not be passed to the next flow element.

### setFlowNodeState(tokenId: String, state: String)

Sets the state of an Activity currently being executed by a token to a given value.

<br>

## Logstream

At several occasions, the BPMN Engine emits log messages resulting in a stream of log messages.

### getLog\$()

Subscribe to a stream of logging messages. Subscription will be triggered every time a log message is emited.

<br>

## Execution Log Stream

Every time a flow node is finished its execution is logged resulting in a stream of execution log entries.

### getExecutionLog\$()

Subscribe to a stream of execution log entries. Subscription will be triggered every time a new entry is added to the log.

### getExecutionLog()

Get a copy of the current state of the execution log.

<br>

## Adaptation Log Stream

Every time a user requests a manual adaptation of the instance execution an adaptation log entry is created resulting in a stream of adaptation log entries.

### getAdaptationLog\$()

Subscribe to a stream of adaptation log entries. Subscription will be triggered every time a new entry is added to the log.

### getAdaptationLog()

Get a copy of the current state of the adaptation log.

<br>

## Control Execution

### pause()

Processinstance will change its state to `PAUSED` and every running token will pause after finishing its current execution.

### resume()

Processinstance is resumed after being paused. Every active token will resume its execution.

### stop()

Processinstance will change its state to `STOPPED` and every running token will stop its current execution immediately. After being stopped the processinstance can not be restarted, but the last state can be requested.

### removeToken(tokenId: string, silent?: boolean)

Removes a token from the processInstance. Optional silent flag might be used to prevent the removal from being logged in the Adaptation Log.

### addToken(elementId: String, token: { [key: String]: any } = {}, silent?: boolean)

Adds a new token to the processInstance. Optional attributes for the token can be provided. Optional silent flag might be used to prevent the addition of the token from being logged in the Adaptation Log.

### moveToken(elementId: string, token: { [key: String]: any }, silent?: boolean)

Moves an existing token from one flow element to another. The token attribute should contain the id of the token and might contain additional attributes to be added to the token on move. Optional silent flag might be used to prevent the move from being logged in the Adaptation Log.

### placeTokenAt(sequenceId: string, token: { key: string]: any } = {}, silent?: boolean)

This facility aims to address the use-case where custom control-flow over BPMN execution is required. You can place a given token on any flow element in the BPMN graph. It will either use the moveToken function if the token attribute contains an id of an existing token or otherwise the addToken function. The optional silent flag is passed to the other functions.

```js
myProcessInstance.placeTokenAt('sequenceFlow_dfc015', { id: 'abc|1-3-xyz' });
// throws InvalidTokenPlacementError if flow element with the given id does not exist
```

Once the token is placed, it initializes the the target element. Any condition expression on sequence flows is not evaluated.

_Note: tokenIds should follow the scheme described in [Gateway - Token ID](./Gateway.md#token-id). In general, use with caution when dealing with merge flows._

### updateToken(tokenId: string, attributes: { [key: string]: any })

Updates attributes of a token

### endToken(tokenId: string, attributes: { state: string; [key: string]: any })

Ends Token with given state and optional other attributes

### interruptToken(tokenId: String)

Stops token execution at current position but does not change the token state

### pauseToken(tokenId: String)

Pause the execution of the flow node currently activated by the token.

## Variables

Execution of activities can update the variables that are stored in the instance state.

### getVariables()

Provides the values for all variables in the instance.

### getVariablesWithLogs()

Provides additional information about the changes that were made to the variables during the execution of the instance.

### setVariable(tokenId: string, name: string, value: any)

Allows the change to variables in relation to a token.

### updateVariables(variables: { [key: string]: any }, silent?: boolean)

Allows variables to be changed outside the normal execution flow. The optional silent flag might be used to prevent the change from being logged to the adaptation log.
