# Theory

## Motivation

A _business process_ consists of a set of activities that are performed in coordination in an organizational and technical environment, to realize a business goal [Mathias, 2012]. Existence of such a process is non-dependent on IT systems. However, _business process management systems_ exist to enact the defined business processes [Mathias, 2012]. A simple purchase action on an e-commerce website could trigger a set specific process, that needs to be managed and coordinated, which, if sufficient scope of repitition exists, can be automated by an IT system. The coordination is done by engine as there can be several paths of execution, human intervention, delegation to custom actions and communication to external tasks.

Business Process Model and Notation (BPMN) is an XML-based notation language for encoding business process models. The engine is responsible for parsing the given XML and enact the given workflow. At the time of writing, several popular enterprise and open-source solutions exist for executing BPMN in the Java virtual machine (JVM) ecosystem, with very few targetting the Javascript ecosystem. Javascript's reach provides ample opportunity to widen the scope of BPMN-based programming models and also target low-powered devices, which is considered a challenge with JVM [Klues, 2012]. In additon, javascript's execution model relies on a single-threaded event loop and the programming model is event-based with callback for asynchronous event handling, which has its own pros and cons, the pros of which are explored in this project.

## Rationale

The engine is partly an academic exploration on building a customizable BPMN execution engine, but also built with a concerete set of goals and objectives. The research questions that the engine aims to answer are as follows:

- Is Javascript's event-loop viable for multi-process BPMN execution?
- Is application of state-machine and reactive programming styles suitable for implementing general workflow patterns?
- Does the state-machine architecture provide a means of ensuring reliability with snapshot?
- Can BPMN-based programming be utilized for frontend (browser) programming abstractions?

In addition to answering the above major research questions, the project would have the following primary objectives:

- Execution conformance as best as possible, specified by BPMN v2.0.2.
- Provide interface for extensions and requirements, specified by PROCEED.
- Maintain browser compatibility and minimize engine code bundle, suitable for browser execution.

The engine is written in Typescript, compiled to Javascript (ES5) and some key technologies are:

- Redux: for maintaining state and implementing state-machine transitions.
- Redux-obsevable: for handling asynchronous events and observing the event flow.
- RxJS: stream handling abstraction and convenience operators.
- Bpmn-moddle: BPMN XML parser.
- Jest: test runner and assert library.

## Core Execution Semantics

Each `BpmnProcessInstance` consists of a simple redux state machine, where state transitions are triggered by special `actions` as illustrated below.

<p align="center" style="text-align: center">
  <img alt="Synchronous State Machine" src="./images/sync-state-machine.png" width="450" />
</p>

In the figure, each state can be thought of as a snapshot of the entire context of the process instance at a point in time, which can be serialized, exported and later imported to create a new instance of the same process. For each subsequent action, the new state is fed back in as the next state and the synchronous handling continues. Roughly, what goes into a state is illustrated in the block below.

```js
{
  tokens: [{
    id: "Task_12c8ab",
    instanceId: "01c8xn",
    phase: "inactive",
    ...
  }],
  variables: {
    pens: 5,
    username: "sbdfsdf",
    /* ... key-value pairs */
  }
}
```

`tokens` is an array of the current 'tokens' in the execution, the concept of virtual tokens is described in execution semantics of BPMN specification [OMG, 2014]. `variables` is a key-value pair of the current variable values in the execution.

A generic action object consumed by the state machine reducer contains a `type` field and the action-specific `payload`. For example, an action to pass token from one element to the other may look like,

```js
{g
  type: "PASS_TOKEN",
  payload: {
    from: "Task_acd8fa",
    to: "Task_dffc8x",
    /* ..additional params.. */
  }
}
```

The above action will be consumed by the reducer and a new state object is returned. By now, it should be apparent that the reducer is a pure function which consumes state and action returns a new state, `(state, action) -> state`. If this is the case, then how are asynchronous events handled? This leads to the requirement of "waiting" for some event/condition to occur before a corresponding action is generated. Also, entering such a "waiting" state may require us to observe for some previous action. A concrete example follows, but first the state-machine has to be adapted for the upcoming scenario,

<p align="center" style="text-align: center">
  <img alt="Asynchronous State Machine" src="./images/async-state-machine.png" width="500" />
</p>

In contrast to the syncrhonous state machine, the actions component is now a stream of actions and we have an additional component that consumes the stream of actions and the state, coming out of the reducer (for illustration only, in the actual code, these components are decoupled and interact via actions only). The async-exec in turn waits/executes async actions and puts back in synchronous actions back into the pipe for synchronous handling. Note that the actions reach the async-exec only after it is passed through the synchronous reducer.

A concrete example would be a `PASS_TOKEN` action where a token reaches an `IntermediateMessageCatchEvent`. The reducer handles the action by setting the state appropritely to indicate the token's arrival. The async-exec is also listening to the `PASS_TOKEN` action and it decides that the token has reached a message catch event and that it has to "wait" for a specific message, therefore it waits for some `RECV_MESSAGE` action with a matching `messageRef`.

After say 20 seconds, the `RECV_MESSAGE` action arrives. The reducer sees the action but also realizes that it doesn't need to handle anything, the new state is therefore unchanged from previous. The async-exec now receives the same `RECV_MESSAGE` action and it ends its wait, while also generating a new action `PASS_TOKEN` (for illustration) for outgoing sequence flows, which instructs the reducer to pass token to the next element. The simple async handler looks like,

```js
function catchingMessageEvent(action$, state$) {
  return action$.ofType('PASS_TOKEN').pipe(
    filter(a => getBpmnType(a.payload.to) === 'bpmn:IntermediateCatchEvent'),
    switchMap(a =>
      $action.ofType('RECV_MESSAGE').pipe(
        filter(ma => ma.payload.messageRef === getMessageRef(a.payload)),
        mapTo('PASS_TOKEN')
      )
    )
  );
}
```

The above piece of code may be overwhelming at first, but actually follows the concrete use-case that was previously illustrated. Firstly, the function `catchingMessageEvent` consumes two streams, `action$`, `state$` and returns the same `action$` (pipe back actions) with a different set of actions. The high level description of the above function body is, we tap into the `action$` with `ofType` operator, that looks for `PASS_TOKEN` actions in the stream, further, it looks for `bpmn:IntermediateCatchEvent` type by filtering actions that have the specific type, then `switchMap` switches the stream to listen for actions of type `RECV_MESSAGE`. This is the waiting part, no downstream actions are executed until we have the `RECV_MESSAGE` actions. When the action indeed comes through, we check if the `messageRef` matches and generate a new action `PASS_TOKEN` to pass the token to the next bpmn element (handled by the reducer).

The streams are just RxJS streams and various other operators can be included into the flow to get the desired outcome. For e.g, lets say the action was received from an event gateway and that a signal was parallely received which means the `RECV_MESSAGE` should no longer be considered. It is easy to implement this with `pipe(takeUntil(action$.ofType('WITHDRAW_TOKEN')))`. More information on basics of RxJS operators can be found at [Learn RxJs](http://learnrxjs.io). For the async-exec bits, the engine uses the redux-observable project, which provides "epics", which complement the synchronous reducer as mentioned previously.

### State transitions for Abstract Activity

TODO

## References

[Klues, 2012] Ko, J., Klues, K., Richter, C., Hofer, W., Kusy, B., Bruenig, M., ... & Terzis, A. (2012, February). Low power or high performance? a tradeoff whose time has come (and nearly gone). In European Conference on Wireless Sensor Networks (pp. 98-114). Springer, Berlin, Heidelberg.

[Mathias, 2012] Mathias, W. (2012). Business Process Management: Concepts, Languages, Architectures 2nd ed., XV, 403 p. 300 illus. Hardcover ISBN 978-3-642-28615-5McLeod, Raymond and Schell, George P. 2008 Management Information Systems, Upper Saddle River New Jersey 07458.

[OMG, 2014] Object Management Group (2014). Business Process Model and Notation Specification. https://www.omg.org/spec/BPMN

[Russell, 2006] Russell, N., Ter Hofstede, A. H., Van Der Aalst, W. M., & Mulyar, N. (2006). Workflow control-flow patterns: A revised view. BPM Center Report BPM-06-22, BPMcenter. org, 06-22.
