# Gateways

Gateways are used to control how the Process flows (how Tokens flow) through Sequence Flows as they converge
and diverge within a Process. If the flow does not need to be controlled, then a Gateway is not needed. The term
“gateway” implies that there is a gating mechanism that either allows or disallows passage through the Gateway--that is
as tokens arrive at a Gateway, they can be merged together on input and/or split apart on output as the Gateway
mechanisms are invoked. [[OMG, BPMN Specification Version 2.0]](https://www.omg.org/spec/BPMN/2.0/About-BPMN/)

When a token arrives at a gateway, it must be checked if the required number of incoming tokens for this gateway is reached; if so further handling can be done: Depending on the number of incoming and outgoing sequence flows, either splitting/joining actions or both are done.

This BPMN-Engine does it as follows: All of the incoming tokens present at the gateway are deleted and for every allowed outgoing sequence flow there will be created a new token containing all of the attributes (expect for token-ID) held by the origin token(s).

## [Token ID](#token-id)

With every token running in the process having an unique ID, the ID of new tokens generated due to splitting (so called "subtokens") of an original token (so called "parentToken") follows a specific scheme containg information about the amount of tokens generated at the gateway:

`<parentToken-id>|<token-no.>-<totally-generated-tokens-at-this-GW>-<random-id>`

The id of an token is processed when entering a gateway with merging behaviour. The number of totally generated tokens indicates the amount of tokens which have to be present at the gateway for further processing. When merging multiple tokens into a single one, the ID of the original token ("parentToken-id") is regenerated.

<p align="center" style="text-align: center">
  <img alt="Token IDs" src="./images/token_id_explanation.png" />
</p>

## Parallel Gateway

A Parallel gateway is used to synchronize parallel flows and to create parallel flows. For every incoming sequence flow of a parallel gateway, there has to be a token present at the gateway to continue execution. For every outgoing sequence flow, there will be generated a new subtoken.

## Exclusive Gateway

An exclusive gateway is used to create alternative paths within a process flow. To continue execution when reaching an exclusive gateway, there has to be exactly one token present at the gateway, independent of the amount of incoming sequence flows at the execlusive gateway. Analogous, exactly one token will leave the exclusive gateway to continue execution.
The decision on which of the outgoing sequence flows the token will follow is based on so called `ConditionExpressions`.

## Inclusive Gateway

An inclusive gateway is used to create alternative but also parallel paths within a process flow. To continue execution when reaching an inclusive gateway, the amount of tokens to be present at the gateway depends on the amount of tokens generated earlier on at the corresponding splitting gateway. The ID of the tokens provide information for that.
For splitting a token into subtoken(s), the decision on which of the outgoing sequence flow(s) will be followed and therefore the amount of generated tokens is based on `ConditionExpressions`.
