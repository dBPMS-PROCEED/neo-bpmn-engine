import { BpmnProcess } from '../src';
import { BPMN_XML_SINGLE_DURATION_TIMER_START_EVENT } from '../__tests__/fixtures/bpmnXml';

async function run() {
  const bpmnProcess = await BpmnProcess.fromXml('myProcess', BPMN_XML_SINGLE_DURATION_TIMER_START_EVENT);

  bpmnProcess.getInstance$().subscribe(instance => {
    console.log(' Got a new instance: ', instance.getId());
  });

  bpmnProcess.getInstance$().subscribe(instance => {});

  bpmnProcess.deploy();
}

run().then();
